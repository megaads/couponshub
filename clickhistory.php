<?php include_once 'clickhistory_field.php';?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('My Earning');?> - <?php echo SITE_NAME;?></title>
                               
<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/main.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
         

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
  <script type="text/javascript">
    $(document).ready(function(){
        $(".ui-tabs").tabs();
    });
</script>
</head>
<body>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="myaccount">
     <div class="container">
      <?php include_once 'breadcrumb.php';?>
     <div class="ui-tabs">
          <div class="col-md-3"><?php include 'custmenu.php';?></div>
        <div class="col-md-9 white-body">
    <div id="tabs-1" class="ui-tabs-panel">
        <h4><?php echo('Click History');?></h4>
           

       <div class="table-responsive">
                  <table class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th><?php echo('Last Visit');?></th>
                        <th><?php echo('Merchant');?></th>                                           
                        <th><?php echo('Go to Store');?></th>                                          
                      </tr>
                    </thead>
                   <tbody>
                   <?php foreach($clickhistory as $click):?>
                   <tr>
                   <td><?php echo date('M d,Y',strtotime($click['created_date']))?></td>
                   <td><a href="<?php echo WEB_ROOT?>store/<?php echo $click['identifier']; ?>"><?php echo $click['retailer_name']; ?></a></td>                                                   
                    <td><a href="<?php echo WEB_ROOT ?>doRedirect.php?sid=<?php echo $click['retailer_id']; ?>" class="clickbtn"><?php echo('Go to Store');?></a> </td>      
                       
                   </tr>
                   
                   <?php endforeach;?>
                   
                   </tbody>       
                  </table>
                    <ul class="pagination">
            <?php echo $paging;?>
             
        </ul>
                </div>
    </div>
    </div>
        
</div>
     
     
     </div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
     <?php include 'js.php';?>      
</body>
</html>