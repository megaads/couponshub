<?php include_once 'category_field.php';?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php echo $catName;?> coupons & offers for <?php echo date("M Y");?><?php echo $store['name'];?> upto 80%off cashback coupons | <?php echo SITE_NAME;?></title>   
<meta name="description" content="Get <?php echo $catName;?> discount and deal coupons from top stores <?php echo $store['name'];?> for <?php echo date("M Y");?> ">
<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" /> 
<!--MAIN STYLE-->
   <?php include 'script.php';?>
   <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/font.css" />
    <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/font-awesome.css" />
    <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/normalize.css" />
    <!--css plugin-->
    <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/flexslider.css" />
    <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/jquery.nouislider.css"/>
    <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/jquery.popupcommon.css" />

    <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/style.css" type="text/css"/>
     <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/res-menu.css" />
    <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/responsive.css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style>
.top-w-deal li {
    width: 30%;
    float: left;
    padding: 0 15px;
    margin-bottom: 30px;
}
.coupons-code-item .center-img a img {
    max-height: 80%;
    max-width :60%
}
.menu > li > a {
    height: 2.75em;         /* 36/13 � 2.75*/
    line-height: 2.75em;    /* 36/13 � 2.75*/
    text-indent: 2.75em;    /* 36/13 � 2.75*/
}
.menu ul li a:active{
	background:yellow;
}
.menu ul li a {
    background: #fff;
    border-bottom: 1px solid #efeff0;
    width: 100%;
    height: 2.75em;
    line-height: 2.75em;
    text-indent: 2.75em;
    display: block;
    position: relative;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 0.923em;
    font-weight: 400;
    color: #878d95;
}
.menu ul li:last-child a {
    border-bottom: 1px solid #33373d;
}
.menu > li > a:hover, 
.menu > li > a.active {
    background-color: #FF5733;
    background-image: -webkit-gradient(linear, left top, left bottom, from(rgb(69, 199, 235)),to(rgb(38, 152, 219)));
    background-image: -webkit-linear-gradient(top, rgb(69, 199, 235), rgb(38, 152, 219));
    background-image: -moz-linear-gradient(top, rgb(69, 199, 235), rgb(38, 152, 219));
    background-image: -o-linear-gradient(top, rgb(69, 199, 235), rgb(38, 152, 219));
    background-image: -ms-linear-gradient(top, rgb(69, 199, 235), rgb(38, 152, 219));
    background-image: linear-gradient(top, rgb(69, 199, 235), rgb(38, 152, 219));
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,StartColorStr='#45c7eb', EndColorStr='#2698db');
    border-bottom: 1px solid #103c56;
    -webkit-box-shadow: inset 0px 1px 0px 0px #6ad2ef;
    -moz-box-shadow: inset 0px 1px 0px 0px #6ad2ef;
    box-shadow: inset 0px 1px 0px 0px #6ad2ef;
}
.menu > li > a.active {
    border-bottom: 1px solid #1a638f;
	background:yellow;
}
.menu > li > a:before {
    content: '';
    background-image: url(../images/sprite.png);
    background-repeat: no-repeat;
    font-size: 36px;
    height: 1em;
    width: 1em;
    position: absolute;
    left: 0;
    top: 50%;
    margin: -.5em 0 0 0;
}
menu {
    width: auto;
    height: auto;
    -webkit-box-shadow: 0px 1px 3px 0px rgba(0,0,0,.73), 0px 0px 18px 0px rgba(0,0,0,.13);
    -moz-box-shadow: 0px 1px 3px 0px rgba(0,0,0,.73), 0px 0px 18px 0px rgba(0,0,0,.13);
    box-shadow: 0px 1px 3px 0px rgba(0,0,0,.73), 0px 0px 18px 0px rgba(0,0,0,.13);
}
.menu > li > a {
    background-color: #FF5733;
    background-image: -webkit-gradient(linear, left top, left bottom, from(rgb(114, 122, 134)),to(rgb(80, 88, 100)));
    background-image: -webkit-linear-gradient(top, rgb(114, 122, 134), rgb(80, 88, 100));
    background-image: -moz-linear-gradient(top, rgb(114, 122, 134), rgb(80, 88, 100));
    background-image: -o-linear-gradient(top, rgb(114, 122, 134), rgb(80, 88, 100));
    background-image: -ms-linear-gradient(top, rgb(114, 122, 134), rgb(80, 88, 100));
    background-image: linear-gradient(top, rgb(114, 122, 134), rgb(80, 88, 100));
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,StartColorStr='#727a86', EndColorStr='#505864');
    border-bottom: 1px solid #33373d;
    -webkit-box-shadow: inset 0px 1px 0px 0px #878e98;
    -moz-box-shadow: inset 0px 1px 0px 0px #878e98;
    box-shadow: inset 0px 1px 0px 0px #878e98;
    width: 100%;
    height: 2.75em;
    line-height: 2.75em;
    text-indent: 2.75em;
    display: block;
    position: relative;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: 600;
    color: #fff;
    text-shadow: 0px 1px 0px rgba(0,0,0,.5);
}
.menu > li > a:hover span, .menu > li a.active span {
    background: #2173a1;
}
.menu ul li a:hover {
    background-color: yellowgreen;
}
.tittle h3 {
    text-align: -webkit-center;
    color: #ffffff;
    font-size: xx-large;
    text-shadow: 2px 2px 4px #f3f3f3;
}
.tittle p{
	color: #ffffff;
}
.lbl-work {
    position: relative;
    display: inline-block;
   
}

.lbl-work .tooltiptext {
    visibility: hidden;
    width: 200px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;

    /* Position the tooltip */
    position: absolute;
    z-index: 100;
}

.lbl-work:hover .tooltiptext {
    visibility: visible;
}
.lbl-work .tooltiptext {
    top: -5px;
    right: 105%; 
}
.sci {
   
    margin-left: 79%;
}
i.fa.fa-scissors {
    z-index: 999;
    position: absolute;
    -webkit-transform: rotate(-75deg);
    -moz-transform: rotate(-75deg);
    -o-transform: rotate(-75deg);
    -ms-transform: rotate(-75deg);
    transform: rotate(-75deg);
}
@media only screen and (max-width: 500px) {
	.category-store .left-bar{
		display:none;
	}
}

</style>
</head>
<body>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
     <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      <input type="hidden" name="pagenumber" id="pagenumber" value="2"  />
  <input type="hidden" name="total_page" id="total_page" value="<?php echo $totalPage;?>"  />   
   
       
  <!--======= SIGN UP =========-->
  <section class="stores category-store">
  <div class="sp-header">
 <div class="img-head">
 <?php include_once 'breadcrumb.php';?>
<div class="tittle">
        <h3><?php echo $catName;?> Coupons , Deals & Offers</h3>
		<p>Get latest <?php echo $catName;?> coupons & deals from India's top Online shopping stores </p>
        
        <!--======= FILTERS LETTERS =========-->
                </div> 
 </div>
 </div>
    <div class="container"> 
      <!--======= TITTLE =========-->
      
      
    
	<div class="grid_frame page-content">
	<div class="left-bar">
            <div class="left-menu-store">
			<div>
<script>
  (function() {
    var cx = '012980148060334113433:lgp9zjifmy8';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
</div>
              <div class="left-title">Shop By Category</div>
        
     <?php 
     
                                   echo getCategoriesDropdown($catId);
									 
                                  // echo display_menu();    
                                     // echo funcCombo($catlist,"parent_id","parent_id","parent_id",$rec['make'],$Extra);
                                   ?> 
      </div>
	  
	  <div data-WRID="WRID-146521500164064543" data-widgetType="searchWidget" data-class="affiliateAdsByFlipkart" height="250" width="300" ></div><script async src="//affiliate.flipkart.com/affiliate/widgets/FKAffiliateWidgets.js"></script>
	  <script charset="utf-8" type="text/javascript">
amzn_assoc_ad_type = "responsive_search_widget";
amzn_assoc_tracking_id = "coupo00e-21";
amzn_assoc_marketplace = "amazon";
amzn_assoc_region = "IN";
amzn_assoc_placement = "";
amzn_assoc_search_type = "search_widget";
amzn_assoc_width = "auto";
amzn_assoc_height = "auto";
amzn_assoc_default_search_category = "";
amzn_assoc_default_search_key = "";
amzn_assoc_theme = "light";
amzn_assoc_bg_color = "FFFFFF";
</script>
<script src="//z-in.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&Operation=GetScript&ID=OneJS&WS=1&MarketPlace=IN"></script>
<iframe width='300' height='250' frameborder='0' scrolling='no' src='https://www.cuelinks.com/widgets/11572?pub_id=12461CL11305'></iframe>
<div data-WRID="WRID-147529734468183576" data-widgetType="Push Content"  data-class="affiliateAdsByFlipkart" height="250" width="300"></div><script async src="//affiliate.flipkart.com/affiliate/widgets/FKAffiliateWidgets.js"></script>
<div data-SDID="1558367504"  data-identifier="SnapdealAffiliateAds" data-height="300" data-width="250" ></div><script async id="snap_zxcvbnhg" src="https://affiliate-ads.snapdeal.com/affiliate/js/snapdealAffiliate.js"></script>
      </div>
	 

<div class="right-bar">
    
    
            <div class="container_grid" > 
    
        <ul class="row storepage" id="couponslist">
      <div class="coupons-code-item right-action flex">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-f3+i+3a-ol+y9"
     data-ad-client="ca-pub-4120507368931039"
     data-ad-slot="1912954302"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
          
          <!--======= COUPEN DEALS =========-->
           <?php foreach($coupons as $rec): 
 $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
            if($rec['coupon_imageurl']!="")
            {
                $rec['coupon_image']=$rec['coupon_imageurl'];
            }
           else
            {
                 $rec['coupon_image']=COUPON_IMAGE_PATH_SHOW.(($rec['coupon_image']!="")?$rec['coupon_image']:'no-image.jpg');
                   $rec['coupon_image']= WEB_ROOT."SampleImage.php?src=".$rec['coupon_image']."&h=135&q=40&zc=0";           
            }
                
               $rec['retailer_logo']=RETAILER_IMAGE_PATH_SHOW.(($rec['retailer_logo']!="")?$rec['retailer_logo']:'no-image.jpg');                                                                                                                     
                       
            
           
            
          ?>
          <li>
		  <div class="coupons-code-item right-action flex">
		  
                           <div class="brand-logo thumb-left">
                                        <div class="wrap-logo">
                                            <div class="center-img">
                                                <span class="ver_hold"></span>
                <a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>" class="ver_container">  <img class="img-responsive" src="<?php echo $rec['retailer_logo']?>"" alt="<?php echo $rec['identifier']; ?>" height="135px" width="143px" ></a>
       </div>
                                        </div>
										 <div class="wrap-logo">
                                            <div class="center-img">
                                              <?php if ($rec['price']!=NULL) : ?>
                                                <span class="ver_hold"><?php echo $rec['price'] ?></span>
											<?php else: ?>
												<span class="ver_hold"><?php echo 'Best Deal' ?></span>
												<?php endif; ?>
       </div>
                                        </div>
                                    </div>
									 
                                       
                                   
              
             <div class="right-content flex-body">
			 <p class="rs save-price"><a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>"><?php echo substr($rec['name'],0,58)?></a></p>
               
               
<p class="rs coupon-desc"><?php echo substr($rec['description'],0,100)?><a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>"> Read MORE</p>
<p><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp Expiry Date : <?php echo date("M d, Y",strtotime($rec['end_date']))?> &nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-check-square-o" aria-hidden="true"></i> Verified</p>
 <div class="bottom-action">
                                           <div class="left-vote">
											<?php if (str_replace($good,$bad,$rec['cashback'])==0):?>
											 <span class="lbl-work">Cashback Not Available </span>
											 <?php else: ?>
                                                <span class="lbl-work">+upto <?php echo str_replace($good,$bad,$rec['cashback'])?> Extra Cashback&nbsp <i class="fa fa-info-circle" aria-hidden="true"></i>
												<span class="tooltiptext"><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbspLogin or Signup to earn rewards (real money!) into your CouponsHub account when you make a transaction </span>
																						</span>
                                             <?php endif; ?>
                                            </div>
                <?php if($rec['coupon_code']==NULL): ?>   
                <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn btn-blue btn-take-coupon showcoupon" ><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp<?php echo('Get Deal');?></a>
                <?php else: ?>
                <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn btn-green btn-take-coupon showcoupon" ><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp<?php echo('Get Code');?></a>
                <?php endif; ?>
				</div>
              </div>
              <div class="sci">
				<i class="fa fa-scissors fa-2x" aria-hidden="true"></i>
				</div>    
            </div>
            
          </li>
         <?php endforeach;?> 
              <div class="coupons-code-item right-action flex">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-f3+i+3a-ol+y9"
     data-ad-client="ca-pub-4120507368931039"
     data-ad-slot="1912954302"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
                    </ul>
                     
         <!--======= PAGINATION =========-->
              <button class="loadMore btn btn-blue btn-take-coupon" style="width:100%;font-size:20px;padding:10px 10px" >&nbsp&nbsp&nbspLoad More&nbsp&nbsp&nbsp<i class="fa fa-angle-double-down "></i></button>
         <div id="pagination" style="text-align: center;display: none;">
             <!----  <img src="<?php echo WEB_ROOT?>images/loading.gif">  --->
   <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
<span class="sr-only">Loading...</span>
          </div>
      </div>
      
            </div>
  </section> 
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
             <?php include 'js.php';?>  
                              
  <script type="text/javascript">
var url="<?php echo WEB_ROOT."getcategorycoupons.php"?>";  
var docroot="<?php echo DOC_ROOT."images/"?>";
var cat="<?php echo $catId;?>"; 
   function lastmsg_funtion(pagenumber){
    $('#pagination').show();                                              
    $.post(url, {catid:cat,PageNum: pagenumber}, function(result){
    $('#pagination').hide();                                              
      $('#couponslist').append(result);
      $(".btncashback").leanModal({top : 100, overlay : 0.6, closeButton: ".modal_close" });  
       $(".showcoupon").leanModal({top :100 , overlay : 0.6, closeButton: ".modal_close" }); 
    });
} 

$(window).scroll(function(){

if ($(window).scrollTop() == $(document).height() - $(window).height()){
  
   $('.loadMore').fadeIn(200);
  
   
}

});

$('.loadMore').click(function () {
    
   var pagenumber=eval($('#pagenumber').val());
    
   $('#pagenumber').val(pagenumber+1); 
   var total_page=$('#total_page').val();   
   
   if(pagenumber<=total_page) 
{
lastmsg_funtion(pagenumber);

}
 else
jQuery('button.loadMore').attr('disabled', 'disabled').text('No More Coupons ');
  

}); 

$(window).scroll(function(){

if ($(window).scrollTop()  == $(document).height() - $(window).height()){
  
  var pagenumber=eval($('#pagenumber').val());
    
   $('#pagenumber').val(pagenumber+1); 
   var total_page=$('#total_page').val();   
   
  if(pagenumber<=total_page) 
  lastmsg_funtion(pagenumber);
  
   
}
});       

</script>                                          
</body>
</html>