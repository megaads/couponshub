
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Pocketmall.in : Cashback,Coupons,Deals</title>
<meta name="keywords" content="Pocket Mall,cashback,earn cashback" >
<meta name="description" content="The future of online shopping">
<link rel="icon" href="http://pocketmall.in/images/favicon.ico" type="image/x-icon" />
<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
<link href="http://pocketmall.in/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="http://pocketmall.in/css/main.css" rel="stylesheet" type="text/css">
<link href="http://pocketmall.in/css/style.css" rel="stylesheet" type="text/css">
<link href="http://pocketmall.in/css/animate.css" rel="stylesheet" type="text/css">
<link href="http://pocketmall.in/css/responsive.css" rel="stylesheet" type="text/css">
<link href="http://pocketmall.in/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<script>
  var gacode="UA-77094022-1" ;
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', gacode, 'auto');
  ga('send', 'pageview');

</script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
    <link rel="icon" href="http://pocketmall.in/images/favicon.ico" type="image/x-icon" />
<!--======= HEADER =========-->
<header>
  <div class="container"> 
    
    <!--======= LOGO =========-->
    <div class="logo"> <a href="http://pocketmall.in/"><img src="http://pocketmall.in/images/logo-new.png" alt="" ></a> </div>
    
    <!--======= SEARCH =========-->
     <div class="top-bar">
      <ul class="left-bar-side">
                                                                                         
        <!-- <li> <a href="http://pocketmall.in/myaccount.php"><i class="fa fa-user"></i>My Account</a> </li>
        <li> <a href="http://pocketmall.in/logout.php"><i class="fa fa-lock"></i>Log Out</a> </li>-->

                <li class="user-info dropdown"> <a href="javascript:void(0)" class="register_form dropbtn" onclick="myFunction()"><i class="user-icon"></i><span><!--Welcome-->Srinadh Sri<br />
      <strong>My Balance:  Rs.</strong></span></a>
      <div class="dropdown-content" id="myDropdown"> 
      <a href="http://pocketmall.in/myaccount.php">Dashboard</a> 
      <a href="http://pocketmall.in/myearning.php">My Earnings</a> 
      <a href="http://pocketmall.in/payment.php">Payment</a> 
      <a href="http://pocketmall.in/tickets.php">Missing Cashback</a> 
      <a href="http://pocketmall.in/refernearn.php">Refer & Earn</a> 
      <a href="http://pocketmall.in/myprofile.php">My Profile</a> 
      <a href="http://pocketmall.in/changepassword.php">Change Password</a> 
      <a href="http://pocketmall.in/logout.php">Log out</a> 
     </div>
    </li>
          
            
      </ul>
                  
                          
    </div>
<div id="modal" class="popupContainer" style="display:none;">
        <header class="popupHeader">
            <span class="header_title">Login</span>
            <span class="modal_close"><i class="fa fa-times"></i></span>
        </header>
        
        <section class="popupBody">                                   
            <!-- Social Login -->
                      
            <div class="social_login">
                <div class="">
                                      <a href="http://pocketmall.in/login.php?provider=Facebook" class="social_box fb">
                        <span class="icon"><i class="fa fa-facebook"></i></span>
                        <span class="icon_title">Login with Facebook</span>
                        
                    </a>
                                                              <a href="" class="social_box google">
                        <span class="icon"><i class="fa fa-google-plus"></i></span>
                        <span class="icon_title">Login with Google</span>
                    </a>
                      
                </div>

                <div class="centeredText">
                    <span>OR</span>
                </div>

                <div class="action_btns">
                    <div class="one_half"><a href="#" id="login_form" class="btn1"><i class="fa fa-lock"></i>Login to Your Account</a></div>
                    <div class="one_half last"><a href="#" id="register_form" class="btn1"><i class="fa fa-user"></i>Create Your Account</a></div>
                </div>
            </div>
             
            <!-- Username & Password Login form -->
                     
            <div class="user_login"  style="">
            <div class="errormsglogin"></div>
                <form name="frm" id="frmlogin">
                    <label>Email / Username</label>
                    <input type="text" name="username" value="" id="username" />
                    <br />

                    <label>Password</label>
                    <input type="password" name="password" id="password" value="" />
                    <br />

                    <div class="checkbox">
                        <input id="rememberme" type="checkbox"  value="1" style="margin-left: 0px;margin-right:0px; position:relative;" />
                        <label for="rememberme">Remember me on this computer</label>
                    </div>

                    <div class="action_btns">
                               
                        <div class="one_half"><a href="#" class="btn1 back_btn"><i class="fa fa-angle-double-left"></i> Back</a></div>
                                                                                                                        
                        <div class="one_half last"><a href="javascript:login()" class="btn1 btn_red">Login</a></div>
                    </div>
                </form>

                <a href="http://pocketmall.in/forgotpassword.php" class="forgot_password">Forgot password?</a>
            </div>

            <!-- Register Form -->
            <div class="user_register">
            <div class="errormsgreg"></div>
                <div class="register-left-form"><form id="frmreg">
                    <label>Full Name</label>
                    <input type="text" name="name"  id="name"/>
                    <br />

                    <label>Email Address</label>
                    <input type="text" name="email" id="email" />
                    <br />

                    <label>Password</label>
                    <input type="password" name="password" id="password" />
                    <br />

                    <div class="checkbox">
                        <input id="send_updates" type="checkbox" name="newsletter" value="1" style="margin-left: 0px;margin-right:0px; margin-top:2px; position:relative;" />
                        <label for="send_updates">Send me occasional email updates</label>
                    </div>

                    <div class="action_btns">
                                
                        <div class="one_half"><a href="#" class="btn1 back_btn"><i class="fa fa-angle-double-left"></i>Back </a></div>
                       
                        <div class="one_half last"><a href="javascript:register()" class="btn1 btn_red">Register</a></div>
                    </div>
                </form></div>
                
                <div class="register-right-form">
                	<p>PocketMall.in Introduces Cashback<br />
                    <span>Save more when you shop at your favourite stores<br />and earn Cashback over and above discounts.</span></p>
                    <div class="referal-code">
                    	<h1>Don't have a referral code?<br /><span>Use our joining bonus.</span></h1>
                        <strong>PMBONUS100</strong>
                    </div>
                    <div class="joining-offer"><span>Get Rs. 100 as a joining bonus</span><br />with your first confirmed cashback.</div>
                </div>
                
            </div>
            <div class="cashback-link">Or <a id="cashback-link" target="_blank" href="#">Continue Without Cashback</a></div>
        </section>
    </div> 
    
<script type="text/javascript">
var base_url = "http://pocketmall.in/";

   function login(){
              
        username=$('#frmlogin > #username').val();
        password=$('#frmlogin > #password').val(); 
        
        if($('#rememberme').is(":checked"))
        {
            remember=1;
        }
        else
            remember=0;
        if(!isEmail(username))
        {
            $('#frmlogin > #username').val('');
            $('#frmlogin > #username').attr("placeholder","Please enter valid email address");
             $('#frmlogin > #username').addClass('errorbox')                                                     
             return;
        }
        else
        {
           $('#frmlogin > #username').attr("placeholder","");
           $('#frmlogin > #username').removeClass('errorbox')                                                      
        }
        
        if(password=="")
        {
           $('#frmlogin > #password').val('');
           $('#frmlogin > #password').attr("placeholder","Please enter password");
           $('#frmlogin > #password').addClass('errorbox');
            return;          
        }
        else
        {
           $('#frmlogin > #password').attr("placeholder","");
           $('#frmlogin > #password').removeClass('errorbox')                                                      
        }
                                        
                                                     
       $.post(base_url+'login.php', {username:username,password: password,remember:remember,txtMode:'login','ajax':1}, function(result){
       if(result=="Login Success")  
       {
         if(($('.cashback-link').is(":visible"))) {
             url= $('#cashback-link').attr('href');   
            
             location.reload(); 
 window.open(url);  
         }
         else
         {
             window.location=base_url;      
         }
         
          
       }    
       else
          $('.errormsglogin').html(result);
                                                
         
    });      
} 

 function register(){
        name=$('#frmreg > #name').val();
         email=$('#frmreg > #email').val();
        password=$('#frmreg > #password').val(); 
        
        if($('#send_updates').is(":checked"))
        {
            newsletter=1;
        }
        else
            newsletter=0;
            
            
        if($.trim(name)=="")
        {
           $('#frmreg > #name').val('');
           $('#frmreg > #name').attr("placeholder","Please enter name");
           $('#frmreg > #name').addClass('errorbox');
            return;          
        }
        else
        {
           $('#frmreg > #name').attr("placeholder","");
           $('#frmreg > #name').removeClass('errorbox')                                                      
        }      
            
            
        if(!isEmail(email))
        {
            $('#frmreg > #email').val('');
            $('#frmreg > #email').attr("placeholder","Please enter valid email address");
             $('#frmreg > #email').addClass('errorbox')                                                     
             return;
        }
        else
        {
           $('#frmreg > #email').attr("placeholder","");
           $('#frmreg > #email').removeClass('errorbox')                                                      
        }
        
         if($.trim(password)=="")
        {
           $('#frmreg > #password').val('');
           $('#frmreg > #password').attr("placeholder","Please enter password");
           $('#frmreg > #password').addClass('errorbox');
            return;          
        }
        else
        {
           $('#frmreg > #password').attr("placeholder","");
           $('#frmreg > #password').removeClass('errorbox')                                                      
        }                
        
        if(password.length<6)
        {
           $('#frmreg > #password').val('');
           $('#frmreg > #password').attr("placeholder","Password must be atleast 6 characters");
           $('#frmreg > #password').addClass('errorbox');
            return;          
        }
        else
        {
           $('#frmreg > #password').attr("placeholder","");
           $('#frmreg > #password').removeClass('errorbox')                                                      
        }                
        
        
                                        
                                                     
       $.post(base_url+'signup.php', {name:name,email:email,password:password,newsletter:newsletter,txtMode:'add','ajax':1}, function(result){
       if(result=="Registration Success")   
           window.location=base_url+'thanks.php';
       else
       {
           if(result=="duperr")
           var msg="Email already Exist";
          $('.errormsgreg').html(msg); 
       }
          
                                                
         
    });
 }

</script> 
<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(e) {
  if (!e.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    for (var d = 0; d < dropdowns.length; d++) {
      var openDropdown = dropdowns[d];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>    <div class="search">
      <form name="frmsearch" id="frmsearch" action="http://pocketmall.in/search.php" >
        <input class="form-control" id="search" value="" name="q" placeholder="Enter your keyword...">
        <button type="submit">SEARCH</button>
      </form>
    </div>
  </div>
  
  <!--======= NAV =========-->
  <nav>
    <div class="container"> 
      
      <!--======= MENU START =========-->
      <ul class="ownmenu">
        <li style="display:none;"><a href="http://pocketmall.in/">Home</a> </li>
        <!-- <li><a href="http://pocketmall.in/page/aboutus">About Us</a></li> -->
        <li style="display:none;"><a href="javascript:;">Shop by Category</a> <ul class="dropdown"><li>
	<a href="http://pocketmall.in/category/electronics">Electronics</a></li>
<li>
	<a href="http://pocketmall.in/category/fashion">Fashion</a></li>
<li>
	<a href="http://pocketmall.in/category/mobiles">Mobiles</a></li>
<li>
	<a href="http://pocketmall.in/category/travel">Travel</a></li>
<li>
	<a href="http://pocketmall.in/category/books">Books</a></li>
<li>
	<a href="http://pocketmall.in/category/baby-kids">Baby & Kids</a></li>
<li>
	<a href="http://pocketmall.in/category/home-kitchen">Home & Kitchen</a></li>
<li>
	<a href="http://pocketmall.in/category/health-beauty">Health & Beauty</a></li>
<li>
	<a href="http://pocketmall.in/category/gifts">Gifts</a></li>
<li>
	<a href="http://pocketmall.in/category/mobile-recharge">Mobile Recharge</a></li>
<li>
	<a href="http://pocketmall.in/category/furniture">Furniture</a></li>
<li>
	<a href="http://pocketmall.in/category/grocery">Grocery</a></li>
<li>
	<a href="http://pocketmall.in/category/games">Games</a></li>
<li>
	<a href="http://pocketmall.in/category/recharge-bill-payment">Recharge/Bill Payment</a></li>
<li>
	<a href="http://pocketmall.in/category/personalized-stuff">Personalized Stuff</a></li>
</ul> </li>
        <li style="display:none;"><a href="http://pocketmall.in/stores">Shop by Stores</a> 
          <!--======= DROP DOWN =========-->
          <ul class="dropdown">
                        <li><a href="http://pocketmall.in/store/1mg">1mg</a></li>
                        <li><a href="http://pocketmall.in/store/abof">abof</a></li>
                        <li><a href="http://pocketmall.in/store/amazon">Amazon</a></li>
                        <li><a href="http://pocketmall.in/store/americanswan">AmericanSwan</a></li>
                        <li><a href="http://pocketmall.in/store/a4u">Art4u</a></li>
                        <li><a href="http://pocketmall.in/store/coolwinks">CoolWinks</a></li>
                        <li><a href="http://pocketmall.in/stores">View All</a></li>
          </ul>
        </li>
        <li style="display:none;"><a href="http://pocketmall.in/coupons">Coupons</a> </li>
        <li style="display:none;"><a href="http://pocketmall.in/page/how-it-works">How it works</a> </li>
        <li style="display:none;"><a href="http://pocketmall.in/testimonial">Testimonials</a> </li>
        <li style="display:none;"><a href="http://pocketmall.in/contact">Contact</a> </li>
      </ul>
    </div>
  </nav>
</header>
  
  <!--======= BANNER =========--> 
  
  <!--======= BANNER =========-->
  <section class="great-deals homepage">
    <div class="flexslider">
      <ul class="slides">
                
        <li>
                    <a target="_blank" href="http://dl.flipkart.com/dl/home-entertainment/televisions/pr?p%5B%5D=facets.brand%255B%255D%3DVu&affid=erjaldipg&filterNone=true&sid=ckf%2Cczl&affExtParam1={USERID}">
                    
        <img src="http://pocketmall.in/images/banners/2156flipkart vu.jpg" alt="" >
        </a>        
        </li>
                 
        <li>
                    <a target="_blank" href="http://dl.flipkart.com/dl/womens-clothing/ethnic-wear/sarees/pr?pincode=560034&p%5B%5D=facets.brand%255B%255D%3DFashionoma&p%5B%5D=sort%3Dpopularity&affid=erjaldipg&filterNone=true&sid=2oq%2Cc1r%2C3pj%2C7od&aff_sub={USERID}">
                    
        <img src="http://pocketmall.in/images/banners/8168flipkart sarees.jpg" alt="" >
        </a>        
        </li>
               </ul>
</div>
      <section class="top-w-deal">
      <div class="welcome-line">
        <div class="container"> <span>Welcome</span>New to Pocketmall.in? Earn Cashbacks over and above the amazing Discounts! <a href="signup.php">Sign up & Earn</a> </div>
      </div>
      <div class="container"> 
        
        <!--======= TITTLE =========-->
        <div class="tittle">
          <h3>Featured Stores</h3>
        </div>
        <ul class="row">
          
          <!--======= WEEK DEAL 1 =========-->
                    <li>
            <div class="w-deal-in"> <img class="img-responsive" src="http://pocketmall.in/images/retailer/48331mg.jpg" alt="" height="50" > 
              <!--======= HOVER DETAL =========-->
              <div class="w-over">
                <p>Up to 40 Rs. Cashback & 1 More Offers</p>
                <a href="http://pocketmall.in/store/1mg">Show Offers</a> </div>
            </div>
          </li>
                    <li>
            <div class="w-deal-in"> <img class="img-responsive" src="http://pocketmall.in/images/retailer/1406dominos.jpg" alt="" height="50" > 
              <!--======= HOVER DETAL =========-->
              <div class="w-over">
                <p>Up to 15 Rs. Cashback & 5 More Offers</p>
                <a href="http://pocketmall.in/store/dominos">Show Offers</a> </div>
            </div>
          </li>
                    <li>
            <div class="w-deal-in"> <img class="img-responsive" src="http://pocketmall.in/images/retailer/9590ebay.png" alt="" height="50" > 
              <!--======= HOVER DETAL =========-->
              <div class="w-over">
                <p>Up to 4% Cashback & 7 More Offers</p>
                <a href="http://pocketmall.in/store/ebay">Show Offers</a> </div>
            </div>
          </li>
                    <li>
            <div class="w-deal-in"> <img class="img-responsive" src="http://pocketmall.in/images/retailer/8730fc_logo.jpg" alt="" height="50" > 
              <!--======= HOVER DETAL =========-->
              <div class="w-over">
                <p>Up to 4% Cashback & 19 More Offers</p>
                <a href="http://pocketmall.in/store/firstcry-com">Show Offers</a> </div>
            </div>
          </li>
                    <li>
            <div class="w-deal-in"> <img class="img-responsive" src="http://pocketmall.in/images/retailer/7282flipkart master logo_rgb-1.png" alt="" height="50" > 
              <!--======= HOVER DETAL =========-->
              <div class="w-over">
                <p>Up to 4% Cashback & 5 More Offers</p>
                <a href="http://pocketmall.in/store/flipkart">Show Offers</a> </div>
            </div>
          </li>
                    <li>
            <div class="w-deal-in"> <img class="img-responsive" src="http://pocketmall.in/images/retailer/7288freecharge.jpg" alt="" height="50" > 
              <!--======= HOVER DETAL =========-->
              <div class="w-over">
                <p>Up to 1% Cashback & 17 More Offers</p>
                <a href="http://pocketmall.in/store/freecharge">Show Offers</a> </div>
            </div>
          </li>
                    <li>
            <div class="w-deal-in"> <img class="img-responsive" src="http://pocketmall.in/images/retailer/5618greendust.png" alt="" height="50" > 
              <!--======= HOVER DETAL =========-->
              <div class="w-over">
                <p>Up to 500 Rs. Cashback & 14 More Offers</p>
                <a href="http://pocketmall.in/store/greendust">Show Offers</a> </div>
            </div>
          </li>
                    <li>
            <div class="w-deal-in"> <img class="img-responsive" src="http://pocketmall.in/images/retailer/3979healthkart.gif" alt="" height="50" > 
              <!--======= HOVER DETAL =========-->
              <div class="w-over">
                <p>Up to 4% Cashback & 24 More Offers</p>
                <a href="http://pocketmall.in/store/healthkart">Show Offers</a> </div>
            </div>
          </li>
                    <li>
            <div class="w-deal-in"> <img class="img-responsive" src="http://pocketmall.in/images/retailer/5388homeshop18.jpg" alt="" height="50" > 
              <!--======= HOVER DETAL =========-->
              <div class="w-over">
                <p>Up to 5% Cashback & 36 More Offers</p>
                <a href="http://pocketmall.in/store/homeshop18">Show Offers</a> </div>
            </div>
          </li>
                    <li>
            <div class="w-deal-in"> <img class="img-responsive" src="http://pocketmall.in/images/retailer/7104industrybuying.jpg" alt="" height="50" > 
              <!--======= HOVER DETAL =========-->
              <div class="w-over">
                <p>Up to 5% Cashback & 15 More Offers</p>
                <a href="http://pocketmall.in/store/industrybuying">Show Offers</a> </div>
            </div>
          </li>
                  </ul>
      </div>
    </section>
    <div class="coupon-section">
      <div class="container"> 
        
        <!--======= TITTLE =========-->
        
        <div class="coupon">
          <div class="tittle">
            <h3>Today&acute;s Top Deals & Offers</h3>
          </div>
          <ul class="row">
            
            <!--======= COUPEN DEALS =========--> 
            <!--======= COUPEN DEALS =========-->
                        <li class="col-sm-4 coupon-width"   >
              <div class="coupon-inner">
                                <div class="c-img">
                  <div class="couponimg"> <img class="img-responsive" src="http://pocketmall.in/SampleImage.php?src=http://pocketmall.in/images/retailer/7285lino perros.jpg&h=143&q=40&zc=0" alt=""  > </div>
                  <a class="head" href="http://pocketmall.in/coupon/free-movie-voucher-worth-inr-250-of-bookmyshow-on-all-prepaid-orders-of-rs-995-amp-above">Free movie voucher worth INR 250 of Bookmyshow on All Prep</a>
                  <p>Experies On : Jun 30, 2016</p>
                  <div class="text-center">
                                        <a target="_blank" href="http://pocketmall.in/doRedirect.php?cid=705" class="btn">Get Cashback</a>
                                      </div>
                </div>
              </div>
            </li>
                        <li class="col-sm-4 coupon-width"   >
              <div class="coupon-inner">
                                <div class="c-img">
                  <div class="couponimg"> <img class="img-responsive" src="http://pocketmall.in/SampleImage.php?src=http://pocketmall.in/images/retailer/4411snapdeal-logo.jpg&h=143&q=40&zc=0" alt=""  > </div>
                  <a class="head" href="http://pocketmall.in/coupon/le1s-gold-32gb">Letv Le1S (32gb Gold)-Refurbished at Only Rs. 9950 + Up to</a>
                  <p>Experies On : Jul 31, 2016</p>
                  <div class="text-center">
                                        <a target="_blank" href="http://pocketmall.in/doRedirect.php?cid=531" class="btn">Get Cashback</a>
                                      </div>
                </div>
              </div>
            </li>
                        <li class="col-sm-4 coupon-width"   >
              <div class="coupon-inner">
                                <div class="c-img">
                  <div class="couponimg"> <img class="img-responsive" src="http://pocketmall.in/SampleImage.php?src=http://pocketmall.in/images/retailer/4411snapdeal-logo.jpg&h=143&q=40&zc=0" alt=""  > </div>
                  <a class="head" href="http://pocketmall.in/coupon/snapdeal-footwear">Min 30% off on men's footwear + Upto 4% PM Cashback</a>
                  <p>Experies On : Dec 31, 2016</p>
                  <div class="text-center">
                                        <a target="_blank" href="http://pocketmall.in/doRedirect.php?cid=530" class="btn">Get Cashback</a>
                                      </div>
                </div>
              </div>
            </li>
                        <li class="col-sm-4 coupon-width"   >
              <div class="coupon-inner">
                                <div class="c-img">
                  <div class="couponimg"> <img class="img-responsive" src="http://pocketmall.in/SampleImage.php?src=http://pocketmall.in/images/retailer/7282flipkart master logo_rgb-1.png&h=143&q=40&zc=0" alt=""  > </div>
                  <a class="head" href="http://pocketmall.in/coupon/flipkart-laptop">Upto 10000 Rs. Off on laptop+Up to 2% Additional cashback</a>
                  <p>Experies On : Jun 30, 2016</p>
                  <div class="text-center">
                                        <a target="_blank" href="http://pocketmall.in/doRedirect.php?cid=416" class="btn">Get Cashback</a>
                                      </div>
                </div>
              </div>
            </li>
                      </ul>
        </div>
      </div>
    </div>
    <div class="how-it-work-section">
      <div class="container">
        <div class="tittle">
          <h3>Join Any and Earn Money with 3 Easy Steps</h3>
        </div>
        <div class="how-it-work">
          <ul>
            <li> <span class="browse-icon">&nbsp;</span>
              <h3>Join/Browse</h3>
              <p>Find great deals on your favourite<br>
                online stores.</p>
            </li>
            <li> <span class="shop-icon">&nbsp;</span>
              <h3>Shop</h3>
              <p>Click via our link and shop as<br>
                usual.</p>
            </li>
            <li> <span class="earn-icon">&nbsp;</span>
              <h3>Earn</h3>
              <p>Sit back and relax. We will track your purchase<br>
                and add cash to your account. </p>
            </li>
          </ul>
        </div>
      </div>
    </div>
    
    
    
  </section>
  <section class="testimonial-main" style="display:none;">
    <div class="container">
      <div class="tittle">
        <h3>Our Customers Words</h3>
      </div>
      <div class="testimonials">
                <div class="homeactive active item last">
          <blockquote>
            <h2 class="testimonial_title">Nice Website</h2>
            <div class="customersstars">
                            <label class="cstar cstar-1>"></label>
                            <label class="cstar cstar-2>"></label>
                            <label class="cstar cstar-3>"></label>
                            <label class="cstar cstar-4>"></label>
                            <label class="cstar cstar-5>"></label>
                          </div>
            <p>Great website for online shopping!</p>
          </blockquote>
          <div class="carousel-info">
                        <img class="pull-left"  src="http://pocketmall.in/images/user-image.jpg"     />
                        <div class="pull-left"> <span class="testimonials-name">Akshat Vaghela</span> <span class="testimonials-post">Member Since 18 May, 2016</span> </div>
          </div>
        </div>
              </div>
    </div>
  </section>
  <section class="account-section-main">
    <div class="container">
      <div class="account-section">
        <p>Join us for <span>Free</span> and start getting cashback on your purchases!<br>Refer your friend and get 10% of your referral's cashback <br>
          Our special launch offer for smart shoppers</p>
        <a href="signup.php">Join Us For Free Now!</a> </div>
    </div>
  </section>
  <div class="usp-points-section">
      <div class="container">
        <div class="usp-points">
          <ul>
            <li><a href="http://pocketmall.in/page/how-it-works"><span class="browse-icon">&nbsp;</span><h3 style="color:#d32828;">How Pocketmall Works</h3></a></li>
            <li><a href="http://pocketmall.in/testimonial"><span class="shop-icon">&nbsp;</span><h3 style="color:#3bb149;">Testimonials</h3></a></li>
            <li><a href="refernearn-page.php"><span class="earn-icon">&nbsp;</span><h3 style="color:#2ab6a7;">Refer and Earn</h3></a></li>
          </ul>
        </div>
      </div>
    </div>
  <div class="newsletter-section-main">
    <div class="container">
      <div class="tittle">
        <h3>Subscribe Deals & offers !</h3>
        <p>We never SPAM. Remove yourself at anytime.</p>
      </div>
      <div class="newsletter-section">
        <div class="search">
          <form action="http://pocketmall.in/pocketmall/search.php" id="frmsearch" name="frmsearch">
            <input placeholder="Enter your keyword..." name="q" value="" id="search" class="form-control">
            <button type="submit" class="subscribe">SEARCH</button>
          </form>
        </div>
      </div>
    </div>
  </div>

<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="//pocketmall.us13.list-manage.com/subscribe/post?u=8df3a3952f87fa617f05ed4f8&amp;id=c4c39212bf" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_8df3a3952f87fa617f05ed4f8_c4c39212bf" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup-->
  
  <!--======= FOOTER =========-->
  <footer>
<div class="container">
	<div class="footer-links">
    	<ul>
        	<li class="title">Shop</li>
            <li><a href="category.php">Shop by Category</a></li>
            <li><a href="http://pocketmall.in/stores">Shop by Stores</a></li>
            <li><a href="http://pocketmall.in/coupons">Coupons</a></li>
        </ul>
    </div>
    <div class="footer-links">
    	<ul>
        	<li class="title">PocketMall.in</li>
            <li><a href="http://pocketmall.in/page/how-it-works">How it Works</a></li>
            <li><a href="http://pocketmall.in/page/about-us">About Us</a></li>
            <li><a href="http://pocketmall.in/contact">Contact us</a></li>
        </ul>
    </div>
    <div class="footer-links">
    	<ul>
        	<li class="title">Misc</li>
            <li><a href="http://pocketmall.in/page/privacy-policy">Privacy Policy</a></li>
            <li><a href="http://pocketmall.in/page/terms-and-condition">Terms and Condition</a></li>
        </ul>
    </div>
    <div class="social-mdeia-links">
    	<ul>
        	<li class="title">Follow us on Social Media</li><br />
            <li><a href="http://www.facebook.com/pocketmall.in" target="_blank" class="facebook">&nbsp;</a></li>
       <!-- <li><a href="" target="_blank" class="linkdin">&nbsp;</a></li> -->
            <li><a href="http://www.twitter.com/pocketmall_in" class="twitter" target="_blank">&nbsp;</a></li>
       <!-- <li><a href="#" class="google">&nbsp;</a></li> -->
            <li><a href="https://www.pinterest.com/pocketmallin" class="pintrest" target="_blank">&nbsp;</a></li>
            <li><a href="https://www.instagram.com/pocketmall.in/" class="instagram" target="_blank">&nbsp;</a></li>
        </ul>
    </div>
    <div class="download-app">
    	<a href="https://play.google.com/store/apps/details?id=com.shoppingcart.pocketmall">&nbsp;</a>
    </div>
    </div>

          <div class="rights">
      <p> </p>
<p>2016-2017 Pocketmall.in All right reserved.</p>
      <!--<ul class="social_icons">
        <li class="facebook"> <a href="http://www.facebook.com/pocketmall.in"><i class="fa fa-facebook"></i> </a></li>
        <li class="twitter"> <a href="http://www.twitter.com/pocketmall_in"><i class="fa fa-twitter"></i> </a></li>                                                                                                            
      </ul>-->
      
    </div>
  </footer></div>
<script src="http://pocketmall.in/js/jquery-1.11.0.min.js"></script> 
<script src="http://pocketmall.in/js/wow.min.js"></script> 
<script src="http://pocketmall.in/js/bootstrap.min.js"></script> 
<script src="http://pocketmall.in/js/jquery.stellar.js"></script> 
<script src="http://pocketmall.in/js/jquery.isotope.min.js"></script> 
<script src="http://pocketmall.in/js/jquery.flexslider-min.js"></script> 
<script src="http://pocketmall.in/js/owl.carousel.min.js"></script> 
<script src="http://pocketmall.in/js/jquery.sticky.js"></script> 
<script src="http://pocketmall.in/js/own-menu.js"></script> 
<script src="http://pocketmall.in/js/main.js"></script>
<script src="http://pocketmall.in/js/jquery.validate.js"></script> 
<script type="text/javascript" src="http://pocketmall.in/js/jquery.leanModal.min.js"></script>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">-->

<script type="text/javascript">
   base_url="http://pocketmall.in/";
   
    $("#modal_trigger").leanModal({top : 100, overlay : 0.6, closeButton: ".modal_close" });
    $("#modal_trigger1").leanModal({top : 100, overlay : 0.6, closeButton: ".modal_close" });
     $("#modal_testimonial").leanModal({top : 100, overlay : 0.6, closeButton: ".modal_close" });
    $(".btncashback").leanModal({top : 100, overlay : 0.6, closeButton: ".modal_close" });
     $(".btncashbackstore").leanModal({top : 100, overlay : 0.6, closeButton: ".modal_close" });
    $(function(){
        // Calling Login Form
        $("#login_form").click(function(){
            $(".social_login").hide();
             $(".user_register").hide(); 
            $(".user_login").show();
            return false;
        });

        // Calling Register Form
        $("#register_form").click(function(){
            $(".user_register").show();
            $(".user_login").hide();     
            $(".social_login").hide();
            $(".user_register").show();
            $(".header_title").text('Register');
            return false;
        });     
        
        $(".register_form").click(function(){
            $(".social_login").hide();
             $(".user_login").hide();     
            $(".user_register").show();
            $(".header_title").text('Register');
            $('.cashback-link').hide();  
            return false;
        });
        
        $("#modal_trigger").click(function(){
            $(".social_login").show();
            $(".user_register").hide();
            $(".header_title").text('Login');                                     
            $('.cashback-link').hide();  
            return false;
        });

        // Going back to Social Forms
        $(".back_btn").click(function(){
            $(".user_login").hide();
            $(".user_register").hide();
            $(".social_login").show();
            $(".header_title").text('Login');
            return false;
        });
        
         $(".btncashback").click(function(){
              $(".social_login").show();
            $(".user_register").hide();
            $(".header_title").text('Login'); 
             cid=this.id;
              coupon_id=cid.replace('cashbackbtn-','');
              redirectUrl=base_url+"doRedirect.php?cid="+coupon_id;
              $('#cashback-link').attr('href',redirectUrl);
              $('.cashback-link').show();
                     
        });
        
       $( "body" ).on( "click", ".btncashback", function() {
               
               $(".social_login").show();
            $(".user_register").hide();
            $(".header_title").text('Login'); 
             cid=this.id;
              coupon_id=cid.replace('cashbackbtn-','');
              redirectUrl=base_url+"doRedirect.php?cid="+coupon_id;
              $('#cashback-link').attr('href',redirectUrl);
              $('.cashback-link').show();
            }); 
            
          $( "body" ).on( "click", ".btncashbackstore", function() {
               
               $(".social_login").show();
            $(".user_register").hide();
            $(".header_title").text('Login'); 
             sid=this.id;
              store_id=sid.replace('cashbackbtn-','');
              redirectUrl=base_url+"doRedirect.php?sid="+store_id;
              $('#cashback-link').attr('href',redirectUrl);
              $('.cashback-link').show();
            });    
            
            
        

    })       
                   
    
</script>
<script type="text/javascript">
 $(function(){
    $( "#search" ).autocomplete({
        source: 'getsearch.php' ,
         minLength: 3,
           select: function(event, ui) { 
        $("#search").val(ui.item.label);
        $("#frmsearch").submit(); }
    });
});  
</script>
</body>
</html>