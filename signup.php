<?php include_once 'user_field.php';
   //$enable_otp=0; 
if(isLoggedIn())
{
    header("location: ".WEB_ROOT."myaccount.php");
}

$crumbs[]=__('Sign Up');
?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
 <BASE href="<?php echo WEB_ROOT;?>">  
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('Sign Up');?> - <?php echo SITE_NAME;?></title>
                                           
<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" /> 
<!--MAIN STYLE-->
    <?php include 'script.php';?>  

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>
<div class="se-pre-con"></div>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="sign-up">
    <div class="container">
     <?php include_once 'breadcrumb.php';?>
      <div class="row">
        <div class="col-sm-6">
          <h4><?php echo('Get more free coupons and deals');?> 
            </h4>
          <img class="img-responsive" src="images/sign-up-img.png" alt="" > </div>
        
        <!--======= SIGN UP FORM =========-->
         <?php if($facebook_login=="On" || $google_login=="On"): ?>      
                
                   <?php if($facebook_login=="On"): ?>
                   <a href="<?php echo WEB_ROOT; ?>login.php?provider=Facebook">
                          <img src="<?php echo WEB_ROOT?>images/facebooklogin.png" alt="" width="200">
                        
                    </a>
                    <?php endif; ?>
                      <?php if($google_login=="On"): ?>
                    <a href="<?php echo $authUrl;?>" class="social_box google">
                                 <img src="<?php echo WEB_ROOT?>images/googlelogin.png" alt="">
                    </a>
                     <?php endif; ?> 
                   
             
         <?php endif;?>
        
      <h2 align="center">OR</h2>  
        
        <div class="col-sm-6">
          <form method="post" name="frm" id="userfrm">
          <input type="hidden" name="txtMode" value="add" >
          
            <ul class="row">
              <li class="col-md-6">
                <div class="form-group">
                  <label for=""><?php echo('Name');?>  *
                    <input type="text" class="form-control" id="name" placeholder="" name="name">
                  </label>
                </div>
              </li>
              <li class="col-md-6">
                <div class="form-group">
                  <label for=""><?php echo('City');?>   *
                    <input type="text" class="form-control" id="city" placeholder="" name="city">
                  </label>
                </div>
              </li>
                      
               <li class="col-md-6">
                <div class="form-group">
                  <label for=""><?php echo('Phone#');?> *
                    <input type="text" class="form-control" id="phone" placeholder="" name="phone" <?php if($enable_otp): ?> onchange="sendCode1()" <?php endif;?> >
                  </label>
                </div>
              </li>
               <?php if($enable_otp): ?>        
                <li class="col-md-6">
                <div class="form-group">
                  <label for=""><?php echo('OTP');?> <a id="resendotp1" style="display: none" href="javascript:;" onclick="sendCode1()"  />(Resend OTP)</a> *
                    <input type="text" class="form-control" id="verification_code" placeholder="" name="verification_code" >
                  </label>
                </div>
              </li> 
              <?php endif;?>
              <li class="col-md-6">
                <div class="form-group">
                  <label for=""><?php echo('E-mail Address');?> *
                    <input type="email" class="form-control" id="email" placeholder="" name="email">
                  </label>
                </div>
              </li>
              
             <?php if(ENABLE_REF_CODE): ?> 
             <li class="col-md-6">
                <div class="form-group">
                  <label for=""><?php echo('Referral Code');?>
                    <input type="text" class="form-control" id="ref_code" placeholder="" name="ref_code">
                  </label>
                </div>
              </li> 
             <?php endif;?> 
             
              <?php if(ENABLE_MOBILE_REF_CODE): ?> 
             <li class="col-md-6">
                <div class="form-group">
                  <label for=""><?php echo('Reference Mobile#');?>
                    <input type="text" class="form-control" id="mobile_ref_code" placeholder="" name="mobile_ref_code">
                  </label>
                </div>
              </li> 
             <?php endif;?> 
             
            
              <li class="col-md-6">
                <div class="form-group">
                  <label for=""><?php echo('Password');?> 
                    <input type="password" class="form-control" id="password" placeholder="" name="password">
                  </label>
                </div>
              </li>
                    
               <li >
                <div class="form-group" style="display:none;">
                
                        <input id="send_updates" name="newsletter" value="1" checked="checked" class="form-control" type="checkbox" style="margin-left: 0px;margin-right:0px; position:relative;" />
                        <label for="send_updates"><?php echo('Send me occasional email updates');?></label>
                    
                </div>
              </li>
             
              <li class="col-md-6">
                <button type="submit" class="btn"><?php echo('create an account');?></button>
              </li>
            </ul>
          </form>
          <div class="policy">
            <p><?php echo('By creating an account, you accept our');?> <a href="<?php echo WEB_ROOT;?>page/privacy-policy"><?php echo('Privacy Policy');?> </a> <?php echo('and our');?> <a href="<?php echo WEB_ROOT ?>page/terms-conditions"><?php echo('Terms and Conditions');?></a></p>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
    <?php include 'js.php';?>      
<script src="<?php echo WEB_ROOT;?>js/jquery.validate.js"></script> 
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#userfrm").validate({
                 rules: {
                name: "required",
                 city: "required",  
                 country: "required",  
                  phone: "required",     
                password: {
                    required: true,
                    minlength: 5
                },
                     
                email: {
                    required: true,
                    email: true
                },
                     
                
            },
                         
            
        });                                     
          
    });
    
function sendCode1()
 {
     
     phone=$('#userfrm #phone').val();
     
      if($.trim(phone)=="")
        {
            
           $('#userfrm #phone').val('');
           $('#userfrm #phone').attr("placeholder","Please enter mobile number");
           $('#userfrm #phone').addClass('errorbox');
            return;          
        }
        else
        {
           $('#userfrm #phone').attr("placeholder","");
           $('#userfrm #phone').removeClass('errorbox')                                                      
        }                
        
        if(phone.length<10)
        {
           $('#userfrm #phone').val('');
           $('#userfrm #phone').attr("placeholder","Please enter valid mobile number");
           $('#userfrm #phone').addClass('errorbox');
            return;          
        }
        else
        {
           $('#userfrm #phone').attr("placeholder","");
           $('#userfrm #phone').removeClass('errorbox')                                                      
        }                
                   
         
      $.post(base_url+'signup.php', {mobile:phone,txtMode:'sendcode','ajax':1}, function(result){
         if(result=="dupmobile")
          {
             var msg="Mobile number already exist";
           $('#userfrm #phone').val('');
           $('#userfrm #phone').attr("placeholder",msg);
           $('#userfrm #phone').addClass('errorbox');    
              
          }
          else      
             $('#resendotp1').show();                            
         
    });
     
     
 }    
    
    
    
    </script>
</body>
</html>