<?php include_once 'user_field.php';?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('Forgot Password');?> - <?php echo SITE_NAME;?></title>
<meta name="keywords" content="" >
<meta name="description" content="">
<link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" />  

<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/main.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>
<div class="se-pre-con"></div>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="sign-up">
    <div class="container">
     <?php include_once 'breadcrumb.php';?>
      <div class="row">
        <div class="col-sm-6">
          <h4><?php echo('Forgot Password');?><br>
            </h4>
  </div>
          <?php
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg'];     
        ?>
         <?php echo getMessage($msgcode);?>
        <!--======= SIGN UP FORM =========-->
        <div class="col-sm-6">
        
          <form method="post" name="frm">
          <input type="hidden" name="txtMode" value="forgotpassword" >
          
            <ul class="row">
              <li class="col-md-6" style="float: none;">
                <div class="form-group">
                  <label for=""><?php echo('Email Id');?>*
                    <input type="email" class="form-control" id="" placeholder="" name="username">
                  </label>
                </div>
              </li>      
                <li class="col-md-6">
                <button type="submit" class="btn"><?php echo('Submit Request');?></button>
              </li>
               
            </ul>
          </form>
            
        </div>
      </div>
    </div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
   <?php include 'js.php';?>      
</body>
</html>