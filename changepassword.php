<?php include_once 'user_field.php';
 checkSiteUser();       
$crumbs[]= __('Change Password');  
?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('Change Password') ;?> - <?php echo SITE_NAME;?></title>
        
 

<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
      <?php include 'script.php';?>  
         

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
  <script type="text/javascript">
    $(document).ready(function(){
        $(".ui-tabs").tabs();
    });
</script>
</head>
<body>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="myaccount">
     <div class="container">
      <?php include_once 'breadcrumb.php';?>
     <div class="ui-tabs">
         <div class="col-md-3"><?php include 'custmenu.php';?></div>
         <div class="col-md-9 white-body ticketform">
    <div id="tabs-1" class="ui-tabs-panel">
        <h4><?php echo('Change Password');?>.</h4>
           <?php   $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg'];
             echo getMessage($msgcode);?>
           
             
          <form method="post" name="frm" enctype="multipart/form-data" >
          <input type="hidden" name="txtMode" value="changepwd" >
           
            <ul class="row">
              <li class="col-md-6" style="float: none;">
                <div class="form-group">
                  <label for=""><?php echo('Old Password');?> *
                    <input type="password" class="form-control" id=""  placeholder="" name="oldpassword">
                  </label>
                </div>
              </li>
              <li class="col-md-6"  style="float: none;">
                <div class="form-group">
                  <label for=""><?php echo('New Password');?> *
                    <input type="password" class="form-control" id=""   placeholder="" name="newpassword">
                  </label>
                </div>
              </li>
               <li class="col-md-6"  style="float: none;">
                <div class="form-group">
                  <label for=""> <?php echo('Confirm New Password');?>*
                    <input type="password" class="form-control" id=""   placeholder="" name="confirm_newpassword">
                  </label>
                </div>
              </li>           
              <li class="col-md-6"  style="float: none;">
                <button type="submit" class="btn"><?php echo('Change Password');?></button>
              </li>
            </ul>
          </form>                   
        
        
    </div>
    </div>
        
</div>
     
     
     </div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
    <?php include 'js.php';?>      
</body>
</html>
