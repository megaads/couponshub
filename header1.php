 <!--======= HEADER =========-->
 <!-- Navbar will come here -->
<nav class="navbar navbar-info navbar-fixed-top navbar-color-on-scroll">
	<div class="container-fluid">
        <div class="navbar-header">
	    	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-index" id="nav-icon0" style="margin: 5px 28px;">
	        	<i class="material-icons">account_circle</i>
	    	</button>
	    	<a href="<?php echo WEB_ROOT;?>">
	        	<div class="logo-container">
				<img class="img-responsive" src="<?php echo SITE_LOGO;?>" alt="<?php echo SITE_NAME;?>" >
	              
				</div>
	      	</a>
	    </div>

	    <div class="collapse navbar-collapse" id="navigation-index">
	    	<ul class="nav navbar-nav navbar-right">
                  <li>
                 <div id="custom-search-input">
                  
                       <form name="frmsearch" id="frmsearch" action="<?php echo WEB_ROOT?>search.php" >
                <div class="input-group">
                     <input type="text"  class="form-control input-lg" id="search" value="<?php echo (isset($_GET['q']))?$_GET['q']:"";?>" name="q" placeholder="<?php echo('Search product or website name...'); ?>..."/>
        
                    <span class="input-group-btn">
                        <button class="btn btn-info" type="submit">
                           <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                    </span>
                           </div>
                    </form>

            </div>
                </li> 
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle text-center" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <span><i class="material-icons">notifications_active</i></span>
                    <span class='badgeAlert'>2</span>
                    <span class="caret"></span></a>
                  <ul class="list-notificacao dropdown-menu">
                    <li id='item_notification_1'>
                        <div class="media">
                           <div class="media-left"> 
                              <a href="#"> 
                              <img alt="64x64" class="media-object" data-src="holder.js/64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNWZhMWJmZmI3MCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1ZmExYmZmYjcwIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy40Njg3NSIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true"> </a> 
                           </div>
                           <div class="media-body">
                              <div class='exclusaoNotificacao'><button class='btn btn-danger btn-xs button_exclusao' id='1' onclick='excluirItemNotificacao(this)'>x</button>
                              </div>
                              <h4 class="media-heading">Notification 1</h4>
                              <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio.</p>
                           </div>
                        </div>
                     </li>    
                     <li id='item_notification_2'>
                        <div class="media">
                           <div class="media-left"> 
                              <a href="#"> 
                              <img alt="64x64" class="media-object" data-src="holder.js/64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNWZhMWJmZmI3MCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1ZmExYmZmYjcwIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy40Njg3NSIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true"> </a> 
                           </div>
                           <div class="media-body">
                              <div class='exclusaoNotificacao'><button class='btn btn-danger btn-xs' id='2' onclick='excluirItemNotificacao(this)'>x</button>
                              </div>
                              <h4 class="media-heading">Notification 2</h4>
                              <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.</p>
                           </div>
                        </div>
                     </li>
                  </ul>
               </li>
               <span style="display:flex; justify-content: center;">
				<?php if(isset($_SESSION['USER_ID'])):?> 
             <div class="dropdown mega-dropdown" style="margin-top:13%;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" style="color:#fff;">Welcome, <?php echo $_SESSION['USER_NAME'];?> <span class="fa fa-angle-down"></span>
                </a>
                  <ul class="dropdown-menu account-top-dropdown " aria-labelledby="navbarDrop1">
                    <li>
                      <a href="<?php echo WEB_ROOT?>myearning.php"><i class="fa fa-money" aria-hidden="true" style="padding: 5px;"></i>My Earnings</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>payment.php"><i class="fa fa-inr" aria-hidden="true" style="padding: 5px;"></i>Payment</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>myprofile.php"><i class="fa fa-cog" aria-hidden="true" style="padding: 5px;"></i>Settings</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>tickets.php"><i class="fa fa-question" aria-hidden="true" style="padding: 5px;"></i>Missing Cashback</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>refernearn.php"><i class="fa fa-users" aria-hidden="true" style="padding: 5px;"></i>Invite Freinds</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>logout.php"><i class="fa fa-sign-out" aria-hidden="true" style="padding: 5px;"></i>Logout</a>
                    </li>
                  </ul>
				   <button type="button" id="hamburger_menu" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar bar_one"></span>
                <span class="icon-bar bar_two"></span>
                <span class="icon-bar bar_three"></span>
              </button>
              </div>             
                     <?php else: ?>  
                   <li>
                    <a data-toggle="modal" href="#loginModal" class="btn btn_account">
                   <i class="fa fa-user-o" aria-hidden="true"></i>&nbsp; Log In
                    </a>
				</li>
                <li>
                    <a data-toggle="modal"  data-toggle="modal" href="#signupModal" class="btn btn_account">
                   <i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp; Sign Up
                    </a>
				</li>
                  
                  <?php endif; ?>   
                  
                </span>
                
				
                
	    	</ul>
	    </div>
	</div>
</nav>
<!-- end navbar -->
<div class="wrapper">
	<div class="header">
        <div id="cat">
        <div class="continer-fluid">
                    <nav class="navbar cat_back">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#category" id="nav-icon0">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
								</button>
                                <a href="#" class="hidden-sm hidden-md hidden-lg cat_style"><i class="material-icons">select_all</i></a>
							</div>
           
				<div class="collapse navbar-collapse" id="category">                
                <ul class="nav navbar-nav justify-content-end mov_view">    
                <li>
                 <a class="btn btn-sm" style="padding: 12px;margin: 3px 20px; font-size: 22px;font-weight: 900; " onclick="openNav()">&#9776; </a>
                </li>
                <div id="mySidenav" class="sidenav">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                
                    <a href="#" data-toggle="collapse" data-target="#cat_menu">Categories  <b class="caret"></b></a>
                       <!-- <ul class="collapse text-center" id="cat_menu">-->
                        <?php echo getCategoriesMenu();?>  
                       <!-- </ul>-->
                  <a href="<?php  echo WEB_ROOT.'store/'.$rt['identifier'];?>"><?php echo('Shop'); ?></a>
                    <a href="<?php echo WEB_ROOT;?>"><?php echo('Featured'); ?></a>
                    <a href="<?php echo WEB_ROOT;?>"><?php echo('Blog'); ?></a>
                    <a href="<?php echo WEB_ROOT;?>"><?php echo('Contact Us'); ?></a>
                    </div>

    <li class="dropdown mega-dropdown">
        <a href="#" class="dropdown-toggle">
            <i class="fa fa-bolt" aria-hidden="true"></i> Categories <span class="caret"></span>
        </a>
            <ul class="dropdown-menu mega-dropdown-menu row" aria-labelledby="dropdownMenu1" role="menu">
            <li class="col-md-4" id="icon_area" role="presentation">
                  
           <ul class="categories-icon-left">
                <li class="dropdown-header text-center">Today's Popular Categories</li>
                  <br />
               <?php echo getCategoriesMenuwithimg();?>  
             <!--    <ul class="inline_icon">
                    <li><a href="javascript:void(0);"><i class="fa fa-mobile fa-4x" aria-hidden="true"></i><span>Mobile</span></a></li>  
                    <li><a href="javascript:void(0);"><i class="fa fa-ticket fa-4x" aria-hidden="true"></i><span>Movie Tickets</span></a></li>  
                    <li><a href="javascript:void(0);"><i class="fa fa-suitcase fa-4x" aria-hidden="true"></i><span>Travel</span></a></li>  
                </ul>
                  <br />
                  <ul class="inline_icon">
                <li><a href="javascript:void(0);"><i class="fa fa-child fa-4x" aria-hidden="true"></i><span>Dress</span></a></li>  
                <li><a href="javascript:void(0);"><i class="fa fa-cutlery fa-4x" aria-hidden="true"></i><span>Food</span></a></li>  
                <li><a href="javascript:void(0);"><i class="fa fa-plug fa-4x" aria-hidden="true"></i><span>Electronics</span></a></li>  
                </ul>-->
                  <br />
                  <div class="row" align="center">
                     <li class="btn_more"><a href="view_more_page.html">View all Collection</a></li>
                </div>
              </ul>
            </li>
            <li class="col-md-8">
              <ul>
                <li class="dropdown-header">Trending Categories</li>
                			<div class="card card-nav-tabs card-plain text-center" id="trend_category">
							<div class="header">
								<!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
								<div class="nav-tabs-navigation" >
									<div class="nav-tabs-wrapper">
									 <?php echo getCategoriesMenuTabs();?>  
							
                                        </div>
                                        </div>
                                        </div>
                                        </ul>
                                        </li>
                                        </ul>
                    </li>
                    
        <li class="dropdown mega-dropdown">
            <a href="#pablo" class="dropdown-toggle">
              <i class="fa fa-bullhorn" aria-hidden="true"></i> Top Stores <span class="caret"></span>
            </a>

            <ul class="dropdown-menu mega-dropdown-menu2" aria-labelledby="dropdownMenu2" role="menu">
            <li class="col-md-10 col-md-offset-1" role="presentation">
                
               <ul>
                   <?php
         $t =0;
      foreach($featuredstores as $data ){
            $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
          
          if($t>=12) continue;
            if($t!=0 && $t%6==0)
            {
                   ?> 
              </ul> 
             
                 <ul>
          <?php   } ?>
          
        
          
     <li><div class="col-md-2 col-xs-6 col-sm-6"><a href="<?php echo WEB_ROOT?>store/<?php echo $rec['identifier']; ?>"><div class="panel panel-default"><div class="seprat_view" align="center">
               <img src="<?php echo RETAILER_IMAGE_PATH_SHOW.$data['logo'];?>"  width="100">
                </div>
                <p class="text-center"><?php echo $data['retailer_name'] ?></p>
                    <div class="green"> Upto Rs.<?php echo str_replace($good,$bad,$data['cashback'])?> </div>
                    <div class="popular_store_offer"><?php echo $data['total_coupon']?> Offers</div>
                </div>
                </a>
                </div>   
                </li>
             
        <?php     $t++;
          
      }?>
            </ul>
                
                
                
         
                <ul>
                       <li class="btn_more"><a href="#">View all Collection</a></li>
                </ul>
            </li>
            </ul>
            </li>
		                            <li class="dropdown mega-dropdown">
		                                <a href="#pablo" class="dropdown-toggle">
											<i class="fa fa-gift" aria-hidden="true"></i> Best Offers <span class="caret"></span>
		                                </a>
                <ul class="dropdown-menu mega-dropdown-menu3" aria-labelledby="dropdownMenu3" role="menu">
                    <li class="col-md-12" role="presentation">
                        <ul>
                			<div class="card card-nav-tabs card-plain text-center" id="best_offer">
							<div class="header">
								<!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
								<div class="nav-tabs-navigation">
									<div class="nav-tabs-wrapper">
										<ul class="nav nav-tabs" data-tabs="tabs" id="lb-tabs">
											<li class="active"><a href="#Fashion_tab" data-toggle="tab">Fashion</a></li>
											<li><a href="#Food_Dining" data-toggle="tab">Food & Dining</a></li>
											<li><a href="#Travel_tab" data-toggle="tab">Travel</a></li>
											<li><a href="#Mobiles_Tablets" data-toggle="tab">Mobiles & Tablets</a></li>
											<li><a href="#Beauty_Health" data-toggle="tab">Beauty & Health</a></li>
											<li><a href="#Computers_Laptops_Gaming" data-toggle="tab">Computers, Laptops & Gaming</a></li>
											<li><a href="#Recharge" data-toggle="tab">Recharge</a></li>
											<li><a href="#Appliances" data-toggle="tab">Appliances</a></li>
											<li><a href="#Home_Furnishing_Decor" data-toggle="tab">Home Furnishing Decor</a></li>
											<li><a href="#Cameras_Accessories" data-toggle="tab"> Cameras & Accessories</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="content">
								<div class="tab-content text-center">
                                        <div class="tab-pane active" id="Fashion_tab">
                                        <div class="col-md-12 col-xs-12 col-sm-12 null">
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                       <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                           
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
            
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="row" align="center">
                                            <ul>
                                            <li class="view_more btn_more"><a href="#" class="more_btn">See More Fashion Offers</a>
                                            </ul>
                                            </div>
                                        </div>
                                        </div>
                                    
                                        <div class="tab-pane" id="Food_Dining">
                                        <div class="col-md-12 col-xs-12 col-sm-12 null">
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                       <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                           
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
            
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="row" align="center">
                                            <ul>
                                            <li class="view_more btn-more"><a href="#" class="more_btn">See More Food & Dining Offers</a>
                                            </ul>
                                            </div>
                                        </div>
                                        </div>
                                    
                                        <div class="tab-pane" id="Travel_tab">
                                        <div class="col-md-12 col-xs-12 col-sm-12 null">
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                       <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                           
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
            
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="row" align="center">
                                            <ul>
                                            <li class="view_more btn_more"><a href="#" class="more_btn">See More Travel Offers</a>
                                            </ul>
                                            </div>
                                        </div>
                                        </div>
                                    
                                    <div class="tab-pane" id="Mobiles_Tablets">
                                        <div class="col-md-12 col-xs-12 col-sm-12 null">
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                       <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                           
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
            
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="row" align="center">
                                            <ul>
                                            <li class="view_more btn_more"><a href="#" class="more_btn">See More Travel Offers</a>
                                            </ul>
                                            </div>
                                        </div>
                                        </div>
                                    
                                        <div class="tab-pane" id="Beauty_Health">
                                        <div class="col-md-12 col-xs-12 col-sm-12 null">
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                       <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                           
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
            
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="row" align="center">
                                            <ul>
                                            <li class="view_more btn-more"><a href="#" class="more_btn">See More Travel Offers</a>
                                            </ul>
                                            </div>
                                        </div>
                                        </div>
                                    
                                        <div class="tab-pane" id="Computers_Laptops_Gaming">
                                        <div class="col-md-12 col-xs-12 col-sm-12 null">
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                       <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                           
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
            
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="row" align="center">
                                            <ul>
                                            <li class="view_more btn_more"><a href="#" class="more_btn">See More Travel Offers</a>
                                            </ul>
                                            </div>
                                        </div>
                                        </div>
                                     <div class="tab-pane" id="Recharge">
                                        <div class="col-md-12 col-xs-12 col-sm-12 null">
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                       <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                           
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
            
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="row" align="center">
                                            <ul>
                                            <li class="view_more btn_more"><a href="#" class="more_btn">See More Travel Offers</a>
                                            </ul>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="tab-pane" id="Appliances">
                                        <div class="col-md-12 col-xs-12 col-sm-12 null">
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                       <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                           
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
            
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="row" align="center">
                                            <ul>
                                            <li class="view_more btn_more"><a href="#" class="more_btn">See More Travel Offers</a>
                                            </ul>
                                            </div>
                                        </div>
                                        </div>
                                     <div class="tab-pane" id="Home_Furnishing_Decor">
                                        <div class="col-md-12 col-xs-12 col-sm-12 null">
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                       <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                           
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
            
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="row" align="center">
                                            <ul>
                                            <li class="view_more btn_more"><a href="#" class="more_btn">See More Travel Offers</a>
                                            </ul>
                                            </div>
                                        </div>
                                        </div>
                                      <div class="tab-pane" id="Cameras_Accessories">
                                        <div class="col-md-12 col-xs-12 col-sm-12 null">
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                       <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                           
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                            <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                        </a>
                                        </div>
                                         <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
            
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            
                                        </a>
                                        </div>
                                        <div class="col-md-2 col-xs-6 col-sm-6 hover_effect">
                                        <a href="#">
                                        <img src="assets/img/amazon.png" width="80">
                                        <p class="text-center">Amazon</p>
                                        <div class="banner_offer">CD Exclusive: Flat 8% Off on All Products</div>
                                            <div class="green">Upto 7.8% Voucher Rewards</div>
                                            
                                        </a>
                                        </div>
                                            <div class="row" align="center">
                                            <ul>
                                            <li class="view_more btn_more"><a href="#" class="more_btn">See More Travel Offers</a>
                                            </ul>
                                            </div>
                                        </div>
                                        </div>
                                    
                                        </div>
                                        </div>
                                        </div>
                                        </ul>
                                        </li>
                                        </ul>
                                        </li>
                    
                                    <li>
		                                <a href="#pablo">
											<i class="fa fa-rss" aria-hidden="true"></i> Blog
		                                </a>
		                            </li>
                                    <li>
		                                <a href="#pablo">
											<i class="fa fa-info-circle" aria-hidden="true"></i> How Cashback Works?
		                                </a>
		                            </li>
                                    <li>
		                                <a href="refer_and_earn.html">
											<i class="fa fa-angellist" aria-hidden="true"></i> Amazback Giveways
		                                </a>
		                            </li>
								</ul>
                            </div>
                            </nav>
                            </div>
                            </div>
                            </div>
    
    
                          
</div>