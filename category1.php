<?php include_once 'category_field.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php echo $catName;?> - <?php echo SITE_NAME;?></title>
<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" />
<!--MAIN STYLE-->
   <?php include 'script.php';?>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Page Wrap ===========================================-->
<div id="wrap">

  <!--======= TOP BAR =========-->
     <?php  include 'top.php'?>
     <?php  include 'header.php'?>




  <!--======= SIGN UP =========-->
  <section class="stores">
    <div class="container">
      <?php include_once 'breadcrumb.php';?>
      <!--======= TITTLE =========-->
      <div class="tittle">
        <h3><?php echo $catName;?></h3>

        <!--======= FILTERS LETTERS =========-->
                </div>

     <section class="top-w-deal">
    <div class="container">

     <?php

                                      echo getCategoriesDropdown($catId);


                                     // echo funcCombo($catlist,"parent_id","parent_id","parent_id",$rec['make'],$Extra);
                                   ?>

                </div>




  </section>




            </div>
  </section>
  <section class="great-deals">
    <div class="container">

      <div class="coupon" style="width: 100%;">
           <ul class="row">

        <!--======= WEEK DEAL 1 =========-->
        <?php foreach($retailerlistbycat as $rec):
            $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
        ?>
        <li>
          <div class="w-deal-in"> <img class="img-responsive" src="<?php echo RETAILER_IMAGE_PATH_SHOW.$rec['logo'];?>" alt="" height="50" >
            <p>Upto <?php echo $rec['cashback']?><?php echo str_replace($good,$bad,$rec['fixed_percent'])?> Cashback &
              <?php echo $rec['total_coupon']?> More Offers</p>

            <!--======= HOVER DETAL =========-->
            <div class="w-over"> <a href="<?php echo WEB_ROOT?>store/<?php echo $rec['identifier']; ?>"><?php echo('Show Offers'); ?></a> </div>
          </div>
        </li>
        <?php endforeach;?>


      </ul>
        <!--======= PAGINATION =========-->
          <div id="pagination" style="text-align: center;display: none;">
               <img src="<?php echo WEB_ROOT?>images/loading.gif">
          </div>
      </div>

            </div>
  </section>

 <input type="hidden" name="pagenumber" id="pagenumber" value="2"  />
  <input type="hidden" name="total_page" id="total_page" value="<?php echo $totalPage;?>"  />
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
  <?php include 'js.php';?>
<script type="text/javascript">
function GotoLocation(val)
{
   window.location=val;

}
</script>


</body>
</html>
