<?php
	if (!defined('WEB_ROOT')) {
		echo "You Cannot directly access this page";
		exit;
	}  

	$msg = isset($_GET['msg'])?$_GET['msg']:'';
	$msgcode=(isset($msg) && $msg != "")?$msg:'';
	
	$CardCount = count($imgrecords);
	if(isset($_GET['view']) && $_GET['view']=="under") {
		$type = $_GET['view'];
	} else {
		$type = "over";
	}
	
?>
<style>
.upload{
	
	font-weight:bold; font-size:1.3em;
	font-family:Arial, Helvetica, sans-serif;
	text-align:center;
	background:#f2f2f2;
	color:#3366cc;
	border:1px solid #ccc;
	width:150px;
	cursor:pointer !important;
	-moz-border-radius:5px; -webkit-border-radius:5px;
}
.darkbg{
	background:#ddd !important;
}
#status{
	font-family:Arial; padding:5px;
}
ul#files{ list-style:none; padding:0; margin:0; }
ul#files li{ padding:10px; margin-bottom:2px; width:200px; float:left; margin-right:10px;}
ul#files li img{ max-width:180px; max-height:150px; }
.success{ background:#99f099; border:1px solid #339933; }
.error{ background:#f0c6c3; border:1px solid #cc6622; }
</style>
<script type="text/javascript" src="<?php echo WEB_ROOT?>admin/js/jquery-1.3.2.js" ></script>
<script type="text/javascript" src="<?php echo WEB_ROOT?>admin/js/ajaxupload.3.5.js" ></script>
<script type="text/javascript" src="<?php echo WEB_ROOT; ?>admin/js/tooltip.jquery.js"></script>
<script type="text/javascript">
			$(document).ready(function(){
			 
				$('.tooltip5').tooltip({width: '100px'});
			 				
			});
		</script> 
<script>
function uploadImage(serverpath,sku){
	var btnUpload=$('#upload'+sku);
	 
	var status=$('#status'+sku);
	
	new AjaxUpload(btnUpload, {
		action: serverpath+'upload-file.php?sku='+sku,
		//Name of the file input box
		name: 'uploadfile',
		onSubmit: function(file, ext){
			if (! (ext && /^(jpg|jpeg)$/.test(ext))){
                  // check for valid file extension
				alert("Only JPG files are allowed")
				//status.text('Only JPG files are allowed');
				return false;
			}
			status.text('Uploading...');
		},
		onComplete: function(file, response){
			//On completion clear the status
			status.text(response+' Images Uploaded');
			  
			//Add uploaded file to list
			if(response==="success"){
				
			}
		}
	});
}

</script>
<table cellspacing="0" cellpadding="0" width="100%" border="0" height="100%">
  <tr>
    <td class="contantpart" height="100%" valign="top">
      <!--Middle Start-->
      <table cellspacing="0" cellpadding="0" border="0" width="100%" style="height: 100%;">
        <tbody>
          <tr>
            <td valign="top" colspan="3">
              <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tbody>
                  <tr>
                    <td class="bottomborder">
                      <h1>
                        <div> <span id="ctl00_MainContent_lblMainTitle"><?php echo $Title;?></span> </div>
                      </h1>
                    </td>
                    <td align="right" valign="bottom" class="bottomborder">
                      <div> </div>
                    </td>
                  </tr>
                  <tr>
                    <td align="right" colspan="2">
                      <div>
                        <?php
							echo getMessage($msgcode);
						?>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td height="10"></td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <!--	Left Start	-->
             
            <!--	Left End	-->
            <td width="24"> </td>
            <!--	Right Start	-->
            <td valign="top">
              <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr>
                  <td class="dashbordboxhead"><?php echo $Title;?></td>
                </tr>
                <tbody>
                  <tr>
                    <td valign="top" class="tableborder">
                      <!--Middle Part start-->
                      <div>
                        <div >
                          <form name="frm" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            <table cellspacing="1" cellpadding="2" border="0" align="center" width="98%">
                              <tbody>
							  
							  <tr>
							  	<td colspan="2"><strong><a href="<?php echo WEB_ROOT;?>admin/modules/<?php echo MODULE_PAGE;?>/index.php?view=over" style="color:#990033;">Over $5</a> | <a href="<?php echo WEB_ROOT;?>admin/modules/<?php echo MODULE_PAGE;?>/index.php?view=under" style="color:#990033">Under $5</a>  <a href="<?php echo WEB_ROOT;?>admin/modules/<?php echo MODULE_PAGE;?>/export_report.php?fn=sku_report&ty=<?php echo $type;?>" target="_self" ><br /><u>Export Data</u></a></strong></td>
                                  <td align="right" colspan="4">
                                    <input type="submit" class="button" id="btnSave1" onClick="return checkForm(this.form)" value="Save" name="btnSave1">
                                    &nbsp;
                                    <input type="reset" class="button" value="Clear" id="Reset1" name="Reset1">
                                  </td>
                                </tr>
								
							  	<tr>
                                  <td width="5%" style="border-bottom:#999999 1px dashed;" align="right" class="labelclass">&nbsp;</td>
								  <td width="15%" style="border-bottom:#999999 1px dashed;" align="right" class="labelclass">SKU</td>
								  <td width="15%" style="border-bottom:#999999 1px dashed;" align="right" class="labelclass">Price ($)</td>
                                  <td width="30%" style="border-bottom:#999999 1px dashed;" align="left" valign="middle" class="labelclass">Main Image</td>
								    <td colspan="2" width="30%" style="border-bottom:#999999 1px dashed;" align="left" valign="middle" class="labelclass">Additional Image</td>
                                </tr>
							<?php                                 
                                for($i=0; $i < $CardCount; $i++) {
									//echo $imgrecords[$i]['itemID'];
							?>
                                <tr>
                                  <td width="5%" align="right" class="labelclass"> <?php echo $i+1;?> </td>
								  <td width="15%" align="right" class="labelclass"><?php echo $imgrecords[$i]['sku'];?>
								  </td>
								  <td width="15%" align="right" class="labelclass"><?php echo $imgrecords[$i]['sellprice'];?>
								  </td>
                                  <td width="30%" align="left" valign="middle">
                                    <input type="file"  class="re-input" name="imgfile<?php echo $imgrecords[$i]['itemID'];?>">
                                  </td>
								  <td>
								  
								  
								  <div class="upload" id="upload<?php echo $i ?>"  ><span>Upload Image<span></div>
								  <script type="text/javascript" >
								  
									$(function(){
										var btnUpload<?php echo $i?>=$('#upload'+"<?php echo $i?>");
										sku="<?php echo  str_replace("/","_",$imgrecords[$i]['sku']);?>";
										serverpath="<?php echo WEB_ROOT?>admin/modules/upload_multi_images/"; 
										var status<?php echo $i?>=$('#status'+"<?php echo $i?>");
										
										new AjaxUpload(btnUpload<?php echo $i?>, {
											action: serverpath+'upload-file.php?sku='+sku,
											//Name of the file input box
											name: 'uploadfile',
											onSubmit: function(file, ext){
												if (! (ext && /^(jpg|jpeg)$/.test(ext))){
													  // check for valid file extension
													alert("Only JPG files are allowed")
													//status.text('Only JPG files are allowed');
													return false;
												}
												status<?php echo $i?>.text('Uploading...');
											},
											onComplete: function(file, response){
												//On completion clear the status
												msg="";
												msg='<a href="javascript:;"  title="Click Here for View Images"   onclick="window.open(\''+serverpath+'showimage.php?sku=<?php echo  str_replace("/","_",$imgrecords[$i]['sku']);?>\',\'\',\'width=800,height=600,scrollbars=yes\')">'+ response +' Images Uploaded</a>'
												//status.innerHtml=msg
												//document.getElementById('#status'+"<?php echo $i?>").innerHTML='<a href="javascript:;">'+ response +' Images Uploaded</a>';
												//alert(msg)
												 status<?php echo $i?>.html(msg);
												  
												//Add uploaded file to list
												if(response==="success"){
													
												}
											}
										});
										
									});
								</script>
		
								  
								   
								  </td>
								  <td align="right">
								  <span id="status<?php echo $i ?>" >
								  <?php 
								  $allowed = array('jpg','JPG'); 
								  $uploaddir=DOC_ROOT."sku_addional/".str_replace("/","_",$imgrecords[$i]['sku'])."/"; 
									$images=getFiles($uploaddir,$allowed);
									 
									$cntimages= count($images); 
								  	if(!empty($images) && $cntimages>0)
									 { ?>
									<a href="javascript:;" title="Click Here for View Images" onclick="window.open('<?php echo WEB_ROOT?>admin/modules/upload_multi_images/showimage.php?sku=<?php echo str_replace("/","_",$imgrecords[$i]['sku']);?>','','width=800,height=600,scrollbars=yes')"><?php echo $cntimages ?> Images Uploaded</a> 
									<?php }
								  ?>
								  </span>
								  </td>
                                </tr>
							<?php } ?>                                
                                <tr>
                                  <td align="right" style="border-top:#999999 1px dashed;" colspan="6">
                                    <input type="submit" class="button" id="btnSave2" onClick="return checkForm(this.form)" value="Save" name="btnSave2">
                                    &nbsp;
                                    <input type="reset" class="button" value="Clear" id="Reset2" name="Reset2">
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" colspan="2">&nbsp; </td>
                                </tr>
                              </tbody>
                            </table>
                          </form>
                        </div>
                      </div>
                      <!--Middle Part End-->
                    </td>
                  </tr>
                  <tr>
                    <td height="25" class="fieldheader"> <?php /*?><img hspace="5" align="absmiddle" vspace="8" title="Back" alt="Back" src="<?php echo WEB_ROOT;?>admin/images/back-arw.gif"> <a href="<?php echo WEB_ROOT;?>admin/modules/<?php echo MODULE_PAGE?>/index.php?view=list<?php echo isset($_GET['PageNum'])?"&PageNum=".$_GET['PageNum']:""?>">Back</a<?php */?></td>
                  </tr>
                </tbody>
              </table>
            </td>
            <!--	Right End	-->
          </tr>
        </tbody>
      </table>
      <!--Middle End-->
    </td>
  </tr>
</table>
<div id="data_tooltip_5" style="display:none;">
<strong>Click Here to View Images.</strong>
</div>