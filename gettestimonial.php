<?php include_once 'testimonial_field.php';?>  
<?php foreach($records as $rec): ?>
                <div class="active item">
                  <blockquote>
                  <h2 class="testimonial_title"><?php echo $rec['title']?></h2>
                   <div class="customersstars">                               
                 <?php for($i=1;$i<=$rec['rating'];$i++): ?>
                  <label class="cstar cstar-<?php echo $i; ?>>"></label>
                 <?php endfor;?>  
                     </div>         
                  
                  
                  <p><?php echo $rec['description']?></p></blockquote>
                  <div class="carousel-info">
                    
<?php
                 if($rec['user_image']!="")
                 {
                     if(file_exists(USER_IMAGE_PATH_UPLOAD.$rec['user_image']))
                     {
                         $rec['user_image']= USER_IMAGE_PATH_SHOW.$rec['user_image'];
                     }      
                     
                     ?> 
               <img  class="pull-left" src="<?php echo $rec['user_image']?>"  />   
                    
                    <?php
                 }
                 else
                 {
                     ?>
                      <img class="pull-left"  src="<?php echo WEB_ROOT?>images/user-image.jpg"     />    
                     <?php
                 } 
                 
                 ?>      
                    <div class="pull-left">
                      <span class="testimonials-name"><?php echo $rec['name']?></span>
                      <span class="testimonials-post">Member Since  <?php echo dateFormat($rec['member_since'],"d M, Y"); ?></span>
                    </div>
                  </div>
                </div>
                <?php endforeach; ?>