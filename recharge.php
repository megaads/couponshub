<?php include_once 'recharge_field.php';?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('Withdraw Money');?> - <?php echo SITE_NAME;?></title>
  

<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" /> 
<!--MAIN STYLE-->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/main.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
         

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
     
</head>
<body>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="myaccount">
     <div class="container">
      <?php include_once 'breadcrumb.php';?>
     <div class="ui-tabs">
            <?php include 'custmenu.php';?>        
        
</div>       
<h2>Recharge History</h2>
<div class="table-responsive">
                  <table class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th><?php echo('Date');?></th>            
                        <th><?php echo('Mobile');?></th>                         
                        <th><?php echo('Recharge Type');?></th> 
                        <th><?php echo('Operator');?></th>                        
                        <th><?php echo('Recharge Amount');?></th>
                        <th><?php echo('Status');?></th>                                          
                      </tr>
                    </thead>
                   <tbody>
                   <?php foreach($rechargehistory as $rh):?>
                   <tr>
                   <td><?php echo date('M d,Y',strtotime($rh['created_date']))?></td>     
                     <td><?php echo $rh['mobile'];?></td>      
                     <td><?php echo $rh['recharge_type'];?></td>      
                     <td><?php echo getOperatorName($rh['operator']);?></td>      
                   <td> <?php echo $currency; ?><?php echo $rh['amount'];?></td>
                    <td><?php echo $rh['recharge_status'];?></td>      
                 
                   
                   </tr>
                   
                   <?php endforeach;?>
                   
                   </tbody>       
                  </table>
                    <ul class="pagination">
            <?php echo $paging;?>
             
        </ul>
                </div>


    
     </div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
 
  
        <?php include 'js.php';?>      
</body>
</html>
