<?php include_once 'coupon_field.php';?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
         <meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo ($coupons['meta_title']!="")?$coupons['meta_title']:$coupons['name'] ?> </title>
<meta name="keywords" content="<?php echo $coupons['meta_keyword'] ?>" >
<meta name="description" content="<?php echo $coupons['meta_description'] ?>">


<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
     <?php  include 'script.php'?>     

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
           <style>
           .storeheader {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #ddd;
    box-shadow: 0 0 7px #cacaca;
    float: left;
    padding: 10px;
    width: 100%;
    margin-bottom: 20px;
}
.coupon-code {
    width: 250px;
    padding: 10px;
    text-align: center;
    border: 3px dashed #ccc;
margin-left:30%}

.right-side{
float :right;
}
.ribbon {
  position: absolute;
  right: -5px; top: -5px;
  z-index: 1;
  overflow: hidden;
  width: 75px; height: 75px;
  text-align: right;
}
.ribbon span {
  font-size: 10px;
  font-weight: bold;
  color: #FFF;
  text-transform: uppercase;
  text-align: center;
  line-height: 20px;
  transform: rotate(45deg);
  -webkit-transform: rotate(45deg);
  width: 100px;
  display: block;
  background: #79A70A;
  background: linear-gradient(#F70505 0%, #8F0808 100%);
  box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
  position: absolute;
  top: 19px; right: -21px;
}
.ribbon span::before {
  content: "";
  position: absolute; left: 0px; top: 100%;
  z-index: -1;
  border-left: 3px solid #8F0808;
  border-right: 3px solid transparent;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #8F0808;
}
.ribbon span::after {
  content: "";
  position: absolute; right: 0px; top: 100%;
  z-index: -1;
  border-left: 3px solid transparent;
  border-right: 3px solid #8F0808;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #8F0808;
}
.lbl-work {
    position: relative;
    display: inline-block;
   
}

.lbl-work .tooltiptext {
    visibility: hidden;
    width: auto;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;

    /* Position the tooltip */
    position: absolute;
    z-index: 1;
}

.lbl-work:hover .tooltiptext {
    visibility: visible;
}
button.accordion {
    background-color: #2d3e52;
    color: #ffffff;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

button.accordion.active, button.accordion:hover {
    background-color:  #e74f1d;
}

button.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}

button.accordion.active:after {
    content: "\2212";
}

div.panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
}
           </style>
</head>
<body>

<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="great-deals">
  <div class="sp-header">
  <div class="img-head">
 <?php include_once 'breadcrumb.php';?> 
 </div>
  </div>
    <div class="container"> 
                                                               
          
      <?php
       $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
              if($coupons['coupon_imageurl']!="")
            {
                $coupons['coupon_image']=$coupons['coupon_imageurl'];
            }
            else
            {
                 $coupons['coupon_image']=COUPON_IMAGE_PATH_SHOW.(($coupons['coupon_image']!="")?$coupons['coupon_image']:'no-image.jpg'); 
                 $coupons['coupon_image']= WEB_ROOT."SampleImage.php?src=".$coupons['coupon_image']."&h=150&q=40&zc=0";        
            }
                    
               
          $coupons['retailer_logo']=RETAILER_IMAGE_PATH_SHOW.(($coupons['retailer_logo']!="")?$coupons['retailer_logo']:'no-image.jpg');
              
               $good=array("percent","fixed");
        $bad=array("%"," ".$currency);                                           
      ?>
      <div class="coupon" style="margin-top:-80px;">
          <div class="coupon-inner-content-section bgfff">
		
          <div class="coupon-side-img">
          <div class="col-md-3">
		   <div class="ribbon"><span><?php echo $coupons['price'];?></span></div>
     <!---     <img class="img-responsive" src="<?php //echo $coupons['coupon_image']?>" alt="<?php //echo $store['name'];?> |<?php //echo $coupons['name']?>"   >  ---> 
  <img class="img-responsive" src="<?php echo $coupons['retailer_logo']?>"   alt="<?php echo $store['name'];?> coupon & offers"> 	 
          </div>
          </div>
      <!---    <div class="right-side">
          <img class="img-responsive" src="<?php //echo $coupons['retailer_logo']?>"  height="40" alt="<?php //echo $store['name'];?>"> 
          </div>  ---->

          <div class="coupon-side-title">
          <div class="col-md-9">
         <div class="text-left social-share">
         <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
    <a class="a2a_button_facebook a2a_counter"></a>
    <a class="a2a_button_pinterest a2a_counter"></a>
    <a class="a2a_button_linkedin a2a_counter"></a>
    <a class="a2a_button_tumblr a2a_counter"></a>
    <a class="a2a_button_reddit a2a_counter"></a>
    <a class="a2a_dd a2a_counter" href="https://www.addtoany.com/share"></a>
</div>

<script async src="https://static.addtoany.com/menu/page.js"></script>
</div>

          <h1><?php echo $coupons['name']?></h1>
                                          <div class="percentage-title"><span class="lbl-work">+ Get Upto <?php echo str_replace($good,$bad,$coupons['cashback'])?>  Cashback&nbsp <i class="fa fa-info-circle" aria-hidden="true"></i>
										  <span class="tooltiptext">Login or Signup to earn rewards (real money!) into your CouponsHub account when you make a transaction at <?php echo $store['name'];?> </span></span>
          </div>
		  <div class="text-center" style="padding-bottom: 15px;"> 
          
           <div class="col-md-6">
           <div class="coupon-code">
           <?php if($coupons['coupon_code']): ?>
           <?php echo $coupons['coupon_code']?>
           <?php else: ?>
           <?php echo('No Coupon Required');?>
           <?php endif;?>
           </div> 
           </div>
           <div class="col-md-6 cashback-cta-section">
          
                <a target="_blank" href="<?php echo WEB_ROOT ?>doRedirect.php?cid=<?php echo $coupons['coupon_id']; ?>" class="btn"><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp<?php echo('Shop Now & Get Cashback');?></a>
                          
         </div>
         <div class="clear"></div>
           </div>
           </div>
          </div>
                     <div class="clear"></div>
           </div>

     <div class="storeheader how-it-works-section" >
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Rsponsive_couponpage -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-4120507368931039"
     data-ad-slot="3213329700"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
        <div> <h3 style="line-height: 1.5em;">  <?php echo nl2br($coupons['description']);?></h3></hr></div></div>
      <div class="storeheader how-it-works-section" style="width: 75%;">           
      <h2>How it Works? <i class="fa fa-cog" aria-hidden="true"></i></h2>  
      <ul class="howitworks">
      <li><?php echo('Login / Sign Up to');?> <?php echo SITE_NAME;?> </li>
      <li><?php echo('Click on the Offer you want, This will redirect you to');?> <?php echo $store['name'];?>'s Website </li> 
       <li><?php echo('Shop Normally at');?> <?php echo $store['name'];?></li>
       <li><?php echo('Your Cashback will automatically track and be added within 48 hrs in your');?> <?php echo SITE_NAME;?> account </li>  
      </ul>
    </div>    
	
    <?php include 'sidebar.php';?>        
    <div class="coupon" style="width: 75%;">
      <div class="tittle" style="text-align: left; margin-bottom: 5px;">
        <h3><?php echo('Related Deals');?></h3>
        <hr width="60%">
      </div>
        <ul class="row">
          
          <!--======= COUPEN DEALS =========-->
                    <!--======= COUPEN DEALS =========-->
          <?php foreach($relatedcoupons as $rec):
                    if($rec['coupon_imageurl']!="")
            {
                $rec['coupon_image']=$rec['coupon_imageurl'];
            }
            else
            {
                 $rec['coupon_image']=COUPON_IMAGE_PATH_SHOW.(($rec['coupon_image']!="")?$rec['coupon_image']:'no-image.jpg'); 
                 $rec['coupon_image']= WEB_ROOT."SampleImage.php?src=".$rec['coupon_image']."&h=143&q=40&zc=0";        
            }
                    
               
          $rec['retailer_logo']=RETAILER_IMAGE_PATH_SHOW.(($rec['retailer_logo']!="")?$rec['retailer_logo']:'no-image.jpg');
            
           ?>
               <li class="col-sm-6 col-md-4" style="">
            <div class="coupon-inner">
               <?php if(strtotime(TODAY)==strtotime($rec['end_date'])): ?> 
              <div class="top-tag">                          
              <span class="ribn-pur"><span><?php echo('Expiring Today');?></span></span></div>
              <?php endif;?>
              <div class="c-img"> <div>   <img class="img-responsive" src="<?php echo $rec['retailer_logo']?>"" alt="<?php echo $rec['identifier']; ?>"  >
               </div>

               <div class="coupon-heading"><a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>"><?php echo substr($rec['name'],0,58)?><?php if (str_replace($good,$bad,$rec['cashback'])==0):?>
											 <div style="font-weight: bold; color: green;">Cashback Not Available </div>
											 <?php else: ?>
                                                <div style="font-weight: bold; color: green;">+upto <?php echo str_replace($good,$bad,$rec['cashback'])?> Extra Cashback</div>
                                             <?php endif; ?></a></div>
                <div class="coupon-expiry"><?php echo('Experies On');?> : <?php echo date("M d, Y",strtotime($rec['end_date']))?></div>
                     
                <div class="text-center" style="padding-bottom: 15px;">
                 
               
                 <?php if($rec['coupon_code']==NULL): ?>   
                <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn btn-blue btn-take-coupon showcoupon" ><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp<?php echo('Get Deal');?></a>
                <?php else: ?>
                <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn btn-green btn-take-coupon showcoupon" ><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp<?php echo('Get Code');?></a>
                <?php endif; ?>
                
                </div>
              </div>
                
            </div>
          </li>
         <?php endforeach;?>                     
          
                                         
        </ul>
<h2>FAQ's</h2>
  <button class="accordion">How do I get Cashback?</button>
<div class="panel">
  <p>You can earn Cashback by making a valid purchase from the partner stores via Couponshub website.
Simple steps to get paid:
<ul>
<li>--> Sign in to your Couponshub account.</li>
<li>--> Select your favourite offer from any retailer or category.</li>
<li>--> Complete your purchase via couponshub link.</li>
<li>--> Complete your billing transaction.</li>
<li>--> That's it now you are eligible for cashback.</li></ul></p>
</div>

<button class="accordion">Why am I getting cashback for my own shopping?</button>
<div class="panel">
  <p>The affiliate marketing technique makes shopping all more fun for customers. It involves cashback site (Couponshub) passing on the commission they receive from the merchant to the users. The cashback is valid when a successful sale is generated through Couponshub's website. PS: The cashback is paid by the coupon site and not the merchant.</p>
</div>

<button class="accordion">How Long it takes to get my cashback?</button>
<div class="panel">
  <p>It takes about 60-90 days from each merchant to confirm your sale. Once we receive the sale confirmation from the merchant's end, the cashback amount will get credited to your account.</p>
</div>
<button class="accordion">How can I redeem earned cashback?</button>
<div class="panel">
  <p>You can redeem your cashback in Couponshub's account through any of the following ways:
Paytm Wallet : When you reached minimum amount of withdrawal you can simply wihtdraw your earned cashback into Paytm Wallet .
Mobikwik Wallet : When you reached minimum amount of withdrawal you can simply wihtdraw your earned cashback into Mobikwik Wallet.</p>
</div>
<button class="accordion">What if I change or cancel my order?</button>
<div class="panel">
  <p>In case you place an order but later on cancel it or make a change, you are not entitled to get the cashback. Suppose you purchased 3 items from a retailer (say, Jabong), and decided to return one of the items. You will not be entitled to receive any cashback (not even on the other two items).</p>
</div>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  }
}
</script>

      </div>

         </div>     
    
       
    </div>
  </section> 
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
  <?php include 'js.php';?>
   <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "8ba5fe01-2ba4-4fdf-9658-2e23473606d9", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
</body>
</html>
