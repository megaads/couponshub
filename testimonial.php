<?php include_once 'testimonial_field.php';
$crumbs[]=__('Testimonial');
?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
 <BASE href="<?php echo WEB_ROOT;?>">  
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('Testimonial');?> - <?php echo SITE_NAME;?></title>
                                           
<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" /> 
<!--MAIN STYLE-->
    <?php include 'script.php';?>  

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
  
</head>
<body>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
  <input type="hidden" name="pagenumber" id="pagenumber" value="2"  />
  <input type="hidden" name="total_page" id="total_page" value="<?php echo $totalPage;?>"  />       
       
  <!--======= SIGN UP =========-->
  <section class="sign-up">
  <div class="container"> <?php include_once 'breadcrumb.php';?>
     <div class="container content">
    <div class="row">
          <?php
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg'];     
        ?>
         <?php echo getMessage($msgcode);?>
        <div class="col-md-6">
         <h3>User's Testimonials</h3>  
            <div class="testimonials">
                <?php foreach($records as $rec): ?>
                <div class="active item">
                  <blockquote>
                  <h2 class="testimonial_title"><?php echo $rec['title']?></h2>
                   <div class="customersstars">                               
                 <?php for($i=1;$i<=$rec['rating'];$i++): ?>
                  <label class="cstar cstar-<?php echo $i; ?>>"></label>
                 <?php endfor;?>  
                     </div>         
                  
                  
                  <p><?php echo $rec['description']?></p></blockquote>
                  <div class="carousel-info">
                    
<?php
                 if($rec['user_image']!="")
                 {
                     if(file_exists(USER_IMAGE_PATH_UPLOAD.$rec['user_image']))
                     {
                         $rec['user_image']= USER_IMAGE_PATH_SHOW.$rec['user_image'];
                     }      
                     
                     ?> 
               <img  class="pull-left" src="<?php echo $rec['user_image']?>"  />   
                    
                    <?php
                 }
                 else
                 {
                     ?>
                      <img class="pull-left"  src="<?php echo WEB_ROOT?>images/user-image.jpg"     />    
                     <?php
                 } 
                 
                 ?>      
                    <div class="pull-left">
                      <span class="testimonials-name"><?php echo $rec['name']?></span>
                      <span class="testimonials-post">Member Since  <?php echo dateFormat($rec['member_since'],"d M, Y"); ?></span>
                    </div>
                  </div>
                </div>
                <?php endforeach; ?>
                <div id="pagination" style="text-align: center;display: none;">
               <img src="<?php echo WEB_ROOT?>images/loading.gif">
          </div> 
                
            </div>
        </div>
        <div class="col-sm-6">
          <form method="post" name="frm" id="userfrm">
          <input type="hidden" name="txtMode" value="add" >
            <h3>Write a Testimonial</h3>
            <ul class="row">
              <li class="col-md-6"  style="width:100%">
                <div class="form-group">
                  <label for=""><?php echo('Title');?>  *
                    <input type="text" class="form-control" id="" placeholder="" name="title">
                  </label>
                </div>
              </li>
              <li class="col-md-6"  style="width:100%">
                <div class="form-group">
                  <label for=""><?php echo('Testimonial');?>   *
                    <textarea style="height:100px;" class="form-control" id="description" placeholder="" name="description"> </textarea>
                  </label>
                </div>
              </li>
               <li class="col-md-6"  style="width:100%">
                <div class="form-group">
                  <label for=""><?php echo('Rating');?> *
                    
                  </label>
                   <div class="stars">
     
              <input class="star star-5" id="star-5" type="radio" value="5" name="rating"/>
              <label class="star star-5" for="star-5"></label>
              <input class="star star-4" id="star-4" type="radio" value="4" name="rating"/>
              <label class="star star-4" for="star-4"></label>
              <input class="star star-3" id="star-3" type="radio" value="3" name="rating"/>
              <label class="star star-3" for="star-3"></label>
              <input class="star star-2" id="star-2" type="radio" value="2" name="rating"/>
              <label class="star star-2" for="star-2"></label>
              <input class="star star-1" id="star-1" type="radio" value="1" name="rating"/>
              <label class="star star-1" for="star-1"></label>
     
                 </div>         
                </div>
              </li>
                     
              <li class="col-md-6"  style="width:100%">
                 <?php if($_SESSION['USER_ID']): ?>  
                <button type="submit" class="btn"><?php echo('Submit');?></button>
                <?php else: ?>
               
                  <a id="modal_testimonial" href="#modal" class="btn"><?php echo('Submit');?></a>  
                
                <?php endif;?>
              </li>
            </ul>
          </form>
              
        </div>
        
    </div>
</div>
     
        </div> 
     
        </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
    <?php include 'js.php';?>      
<script src="<?php echo WEB_ROOT;?>js/jquery.validate.js"></script> 
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#userfrm").validate({
                 rules: {
                name: "required",
                 city: "required",  
                 country: "required",  
                  phone: "required",     
                password: {
                    required: true,
                    minlength: 5
                },
                     
                email: {
                    required: true,
                    email: true
                },
                     
                
            },
                         
            
        });                                     
          
    });
    </script>
    
 <script type="text/javascript">
var url="<?php echo WEB_ROOT."gettestimonial.php"?>";
var docroot="<?php echo DOC_ROOT."images/"?>";
   function lastmsg_funtion(pagenumber){
    $('#pagination').show();                                              
    $.post(url, {PageNum: pagenumber}, function(result){
    $('#pagination').hide();                                              
      $('.testimonials').append(result);
      
    });
} 

$(window).scroll(function(){

if ($(window).scrollTop() == $(document).height() - $(window).height()){
       
  var pagenumber=eval($('#pagenumber').val());
    
   $('#pagenumber').val(pagenumber+1); 
   var total_page=$('#total_page').val();   
   
  if(pagenumber<=total_page) 
  lastmsg_funtion(pagenumber);
  
   
}

});

</script>                                               
    
    
</body>
</html>