<?php include_once 'search_field.php';?>  
<?php //include_once 'store_field.php'; ?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('Search');?> - <?php echo SITE_NAME;?></title>
   

<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" /> 
<!--MAIN STYLE-->
<?php include 'script.php';?>

           
</head>
<body>

<!-- Page Wrap ===========================================-->
      <?php // include 'top.php'?>
     <?php  include 'header.php'?> 
 
   <div class="">
        <div class="container-fluid">
            <div class="row">
                <div class="store_page col-md-12">
                    <div class="store_logo">
                    <img  src="<?php echo RETAILER_IMAGE_PATH_SHOW.$store['logo']?>" class="img-responsive" />
                    </div>
                    <h2>Amazon Offers: Upto <?php echo $store['cashback']."".(($store['fixed_percent']=="percent")?"%":$currency);?> CashKaro Rewards on top of Upto 90% Off Amazon Deals</h2>
                    <div class="why">
                    <a href="#">WHAT ARE AMAZBACK REWARDS?</a>
                    </div>
                    <span><button type="button" class="btn btn-reward">ACTIVATE REWARDS</button></span>
                  <span><button type="button" class="btn btn-warning btn-subscribe">Subscribe</button></span>
                    <p>Not a Member Yet! <a href="#">Sign Up Here</a></p>
                </div>     
            </div>
                             
        </div>
    </div>
    
    <div class="section">
        <div class="container">
            
            <div class="row">
               <div class="col-md-12 col-sm-12" style="margin-top:2%;margin-bottom:3%;text-align:center;">
                      <span style="font-size: 24px; margin-top:10%;">Search results for '<?php echo $_GET['q'];?>'</span>   
                </div>
               <div class="col-md-12 col-sm-12">
   
                   <div class="col-md-3 col-sm-4">
                                       <div class="panel panel-default hidden-xs">
                                                      <?php include 'sidebar.php';?> 
                          <div class="cashbackstats">
                                <h6>Cashback Status</h6>
                              <p>Estimated Payment Date &nbsp;<span><a href="javascript:void(0);" data-toggle="tooltip" data-placement="right" title="If you shop today, we expect your rewards will be credited to your CouponDunia account around 12 Apr, 2018."><i class="material-icons">info</i></a></span></p>
                              <div class="cashbackdate">12 Apr, 2018</div>
                              <hr />
                              <p>Tracking Speed &nbsp;<span><a href="javascript:void(0);" data-toggle="tooltip" data-placement="right" title="It takes approximately 1 hour to track your rewards when shopping at Flipkart."><i class="material-icons">info</i></a></span></p>
                              <div class="cashbackdate">1 hour</div>
                              <hr />
                              <p>Rewards Claim Time &nbsp;<span><a href="javascript:void(0);" data-toggle="tooltip" data-placement="right" title="In case your rewards does not track automatically, you can file a missing rewards claim request that will take 60 days to resolve."><i class="material-icons">info</i></a></span></p>
                              <div class="cashbackdate">60 days</div>
                            </div>
                        </div>
                        <div class="panel panel-default hidden-xs">
                          <div class="cashbackstats">
                                <h6 class="text-center">Similar Stores</h6>
                              <div style="display: inline-flex;">
                                  
                                 <span><a href="#"><img src="assets/img/amazon.png" width="80" /><div class="store_name">Amazon</div></a></span>
                                  &nbsp;&nbsp;<span><a href="#"><img src="assets/img/amazon.png" width="80" /><div class="store_name">Amazon</div></a></span>
                               </div>
                            </div>
                        </div>
                        <div class="panel panel-default null hidden-xs">
                            <a href="#"><div class="add_image"> <?php
                               // echo getConfiguration('sidebar_ads');?>
                                </div></a>
                        </div>
                   </div>
                   <div class="col-md-9 col-sm-8 set_top">
                    				<div class="card card-nav-tabs card-plain">
							<div class="header header-success my">
								<!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
								<div class="nav-tabs-navigation">
									<div class="nav-tabs-wrapper">
										<ul class="nav nav-tabs" data-tabs="tabs">
											<li class="active"><a href="#all" data-toggle="tab">All</a></li>
											<li><a href="#coupons" data-toggle="tab">Coupons (1)</a></li>
											<li><a href="#deals" data-toggle="tab">Deals (48)</a></li>
										</ul>
									</div>
								</div>
							</div>
    
  <section>
     <div class="grid_frame page-content">    
      <div class="container_grid"> 
                              <?php //include_once 'breadcrumb.php';?>

    <div class="storeheader">    
	
           
                <div class="layout-2cols clearfix">
                    <div class="grid_8 content">
                        <div class="mod-coupons-code">   
        <div class="mod-grp-coupon block clearfix">
            	<div class="content">
      <div class="tab-content">
     <div class="tab-pane active" id="all">
        <ul class="row storepage wrap-list" id="couponlist">
          
          
          <!--======= COUPEN DEALS =========-->
             <?php foreach($coupons as $rec): 
 $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
            if($rec['coupon_imageurl']!="")
            {
                $rec['coupon_image']=$rec['coupon_imageurl'];
            }
           else
            {
                 $rec['coupon_image']=COUPON_IMAGE_PATH_SHOW.(($rec['coupon_image']!="")?$rec['coupon_image']:'no-image.jpg');
                   $rec['coupon_image']= WEB_ROOT."SampleImage.php?src=".$rec['coupon_image']."&h=135&q=40&zc=0";           
            }
                
               $rec['retailer_logo']=RETAILER_IMAGE_PATH_SHOW.(($rec['retailer_logo']!="")?$rec['retailer_logo']:'no-image.jpg');                                                                                                                     
                       
            
           
            
          ?>
          <li>
		  <div class="coupons-code-item right-action flex">
		  
                         
							
                                       <div class="col-md-12 panel">
										<div class="panel-default">
                                          <div class="col-md-2 text-center">
                                            <div class="coupan_border">
                                                <a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>" class="ver_container">  <img class="img-responsive" src="<?php echo $rec['retailer_logo']?>" alt="<?php echo $rec['identifier']; ?>" height="135px" width="143px" ></a>
                                              </div>
                                            </div>
                                            <div class="col-md-7">
                                            <div class="marks row text-center">
                                                <div style="float: left; font-size: 11px;"><i class="fa fa-check-square-o" aria-hidden="true"></i> Verified yesterday</div>
                                                <div style="font-size: 11px;"><i class="fa fa-users" aria-hidden="true"></i> Verified yesterday</div>
                                                </div>
                                                <div class="coupan_title row">
                                                    <h4><a href="#"><?php echo substr($rec['name'],0,58)?></a></h4>
                                                    <p style="color: green; font-size: 12px;">+upto <?php echo str_replace($good,$bad,$rec['cashback'])?> Extra Cashback &nbsp;<span style="font-size: 11px;"><a href="#">(What's this?)</a></span> </p>
                                                </div>
                                                <div class="divider"></div>
                                                <div class="coupan_desc">
                                                <ul>
                                                <li> <?php echo substr($rec['description'],0,58); ?> </li>
                                                <li> <p class="rs save-price"><a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>"><?php echo substr($rec['name'],0,58)?></a></li>
                                                <div id="more_des" class="collapse">
                                              
                                                </div>
                                                </ul>
                                                    </div>
                                                    
                                            </div>
                                            <div class="col-md-3 coupan_btn">
                                                <i class="fa fa-star" aria-hidden="true"></i> Editor's Choice
                                            
                                                   <?php if($rec['coupon_code']==NULL): ?> 
                <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn btn-warning showcoupon" ><?php echo('Get Cashback');?></a>
                <?php else: ?>
                  <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn btn-warning showcoupon" ><?php echo('Get Code');?></a>
                <?php endif; ?>
                                            </div>
                   
                                   
              
             <div class="right-content flex-body">
			 <p class="rs save-price"></p>
               
               
<p class="rs coupon-desc"></p>
 <div class="bottom-action">
                                           
                </div>
              </div>
                
            </div>
              </div>
              
              
           
								
              </div>
          </li>
         <?php endforeach;?> 
              
           </ul>
      
        <button type="button" class="btn btn-warning loadMore">Load More<div class="ripple-container"></div></button>
                                           </div>
              	<div class="tab-pane" id="coupons">
                 
                    
									 <ul class="row storepage wrap-list" id="couponlist">
          
          
          <!--======= COUPEN DEALS =========-->
                                         
             <?php if(!empty($coupons_cp)){
                                         
                                         foreach($coupons_cp as $rec): 
                                               ?>
                            
                            
                        <!--     <li>
		  <div class="coupons-code-item right-action flex">
		   <div class="col-md-12 panel">
               <div class="panel-default">
                   <span><?php //echo('No Deals Available'); ?></span>
                </div>                    
              </div></div></li>-->
                                 
                                 
                <?php       
 $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
            if($rec['coupon_imageurl']!="")
            {
                $rec['coupon_image']=$rec['coupon_imageurl'];
            }
           else
            {
                 $rec['coupon_image']=COUPON_IMAGE_PATH_SHOW.(($rec['coupon_image']!="")?$rec['coupon_image']:'no-image.jpg');
                   $rec['coupon_image']= WEB_ROOT."SampleImage.php?src=".$rec['coupon_image']."&h=135&q=40&zc=0";           
            }
                
               $rec['retailer_logo']=RETAILER_IMAGE_PATH_SHOW.(($rec['retailer_logo']!="")?$rec['retailer_logo']:'no-image.jpg');                                                                                                                     
                       
            
           
            
          ?>
          <li>
		  <div class="coupons-code-item right-action flex">
		  
                         
							
                                       <div class="col-md-12 panel">
										<div class="panel-default">
                                          <div class="col-md-2 text-center">
                                            <div class="coupan_border">
                                                <a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>" class="ver_container">  <img class="img-responsive" src="<?php echo $rec['retailer_logo']?>" alt="<?php echo $rec['identifier']; ?>" height="135px" width="143px" ></a>
                                              </div>
                                            </div>
                                            <div class="col-md-7">
                                            <div class="marks row text-center">
                                                <div style="float: left; font-size: 11px;"><i class="fa fa-check-square-o" aria-hidden="true"></i> Verified yesterday</div>
                                                <div style="font-size: 11px;"><i class="fa fa-users" aria-hidden="true"></i> Verified yesterday</div>
                                                </div>
                                                <div class="coupan_title row">
                                                    <h4><a href="#"><?php echo substr($rec['name'],0,58)?></a></h4>
                                                    <p style="color: green; font-size: 12px;">+upto <?php echo str_replace($good,$bad,$rec['cashback'])?> Extra Cashback &nbsp;<span style="font-size: 11px;"><a href="#">(What's this?)</a></span> </p>
                                                </div>
                                                <div class="divider"></div>
                                                <div class="coupan_desc">
                                                <ul>
                                                <li> Get upto Rs. <?php echo str_replace($good,$bad,$rec['cashback'])?> cashback to MakeMyTrip wallet on domestic flight bookings.</li>
                                                <li> <p class="rs save-price"><a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>"><?php echo substr($rec['name'],0,58)?></a></li>
                                                <div id="more_des" class="collapse">
                                              
                                                </div>
                                                </ul>
                                                    </div>
                                                    
                                            </div>
                                            <div class="col-md-3 coupan_btn">
                                                <i class="fa fa-star" aria-hidden="true"></i> Editor's Choice
                                                <?php if($rec['coupon_code']==NULL): ?> 
                <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn btn-warning showcoupon" ><?php echo('Get Cashback');?></a>
                <?php else: ?>
                  <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn btn-warning showcoupon" ><?php echo('Get Code');?></a>
                <?php endif; ?>
                                            </div>
                   
                                   
              
             <div class="right-content flex-body">
			 <p class="rs save-price"></p>
               
               
<p class="rs coupon-desc"></p>
 <div class="bottom-action">
                                           
                </div>
              </div>
                
            </div>
              </div>
              
              
           
								
              </div>
          </li>
         <?php endforeach; }  else{ ?>
                                            <li>
		  <div class="coupons-code-item right-action flex">
		   <div class="col-md-12 panel">
               <div class="panel-default">
                   <span><?php echo('No Coupons Available'); ?></span>
                </div>                    
              </div></div></li> <?php  }  ?> 
              
           </ul>
                                     
									</div>
									<div class="tab-pane" id="deals">
									 <ul class="row storepage wrap-list" id="couponlist">
          
          
          <!--======= COUPEN DEALS =========-->
             <?php if(!empty($coupons_deals)){
                                         
                                         
                foreach($coupons_deals as $rec): 
                                        
                         ?>
                            
                            
                          
                                 
                                 
                <?php        
                                        
 $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
            if($rec['coupon_imageurl']!="")
            {
                $rec['coupon_image']=$rec['coupon_imageurl'];
            }
           else
            {
                 $rec['coupon_image']=COUPON_IMAGE_PATH_SHOW.(($rec['coupon_image']!="")?$rec['coupon_image']:'no-image.jpg');
                   $rec['coupon_image']= WEB_ROOT."SampleImage.php?src=".$rec['coupon_image']."&h=135&q=40&zc=0";           
            }
                
               $rec['retailer_logo']=RETAILER_IMAGE_PATH_SHOW.(($rec['retailer_logo']!="")?$rec['retailer_logo']:'no-image.jpg');                                                                                                                     
                       
            
           
            
          ?>
          <li>
		  <div class="coupons-code-item right-action flex">
		  
                         
							
                                       <div class="col-md-12 panel">
										<div class="panel-default">
                                          <div class="col-md-2 text-center">
                                            <div class="coupan_border">
                                                <a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>" class="ver_container">  <img class="img-responsive" src="<?php echo $rec['retailer_logo']?>" alt="<?php echo $rec['identifier']; ?>" height="135px" width="143px" ></a>
                                              </div>
                                            </div>
                                            <div class="col-md-7">
                                            <div class="marks row text-center">
                                                <div style="float: left; font-size: 11px;"><i class="fa fa-check-square-o" aria-hidden="true"></i> Verified yesterday</div>
                                                <div style="font-size: 11px;"><i class="fa fa-users" aria-hidden="true"></i> Verified yesterday</div>
                                                </div>
                                                <div class="coupan_title row">
                                                    <h4><a href="#"><?php echo substr($rec['name'],0,58)?></a></h4>
                                                    <p style="color: green; font-size: 12px;">+upto <?php echo str_replace($good,$bad,$rec['cashback'])?> Extra Cashback &nbsp;<span style="font-size: 11px;"><a href="#">(What's this?)</a></span> </p>
                                                </div>
                                                <div class="divider"></div>
                                                <div class="coupan_desc">
                                                <ul>
                                                <li> Get upto Rs. <?php echo str_replace($good,$bad,$rec['cashback'])?> cashback to MakeMyTrip wallet on domestic flight bookings.</li>
                                                <li> <p class="rs save-price"><a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>"><?php echo substr($rec['name'],0,58)?></a></li>
                                                <div id="more_des" class="collapse">
                                              
                                                </div>
                                                </ul>
                                                    </div>
                                                    
                                            </div>
                                            <div class="col-md-3 coupan_btn">
                                                <i class="fa fa-star" aria-hidden="true"></i> Editor's Choice
                                               <?php if($rec['coupon_code']==NULL): ?> 
                <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn btn-warning showcoupon" ><?php echo('Get Cashback');?></a>
                <?php else: ?>
                  <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn btn-warning showcoupon" ><?php echo('Get Code');?></a>
                <?php endif; ?>
                                            </div>
                   
                                   
              
             <div class="right-content flex-body">
			 <p class="rs save-price"></p>
               
               
<p class="rs coupon-desc"></p>
 <div class="bottom-action">
                                           
                </div>
              </div>
                
            </div>
              </div>
              
              
           
								
              </div>
          </li>
         <?php   endforeach; }
                                         else{ ?>
                                            <li>
		  <div class="coupons-code-item right-action flex">
		   <div class="col-md-12 panel">
               <div class="panel-default">
                   <span><?php echo('No Deals Available'); ?></span>
                </div>                    
              </div></div></li> <?php  }  ?> 
              
           </ul>
                                     
									</div>
								</div>
							</div>
              
		    </div>
        </div>
                        </div>
                  
		
                      
         <div id="pagination" style="text-align: center;display: none;">
               <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
          </div>
      </div>
     
          </div></div></div>
           </section> 
                       </div></div></div>
        </div></div>
  <!--======= FOOTER =========-->
    <?php include 'footer.php';?> </div>
  <?php include 'js.php';?>      
</body>
</html>