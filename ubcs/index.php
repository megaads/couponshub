<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" href="font.css" tppabs="http://envato.megadrupal.com/html/couponday/css/font.css"/>
    <link rel="stylesheet" href="font-awesome.css" tppabs="http://envato.megadrupal.com/html/couponday/css/font-awesome.css"/>
    <link rel="stylesheet" href="normalize.css" tppabs="http://envato.megadrupal.com/html/couponday/css/normalize.css"/>
    <!--css plugin-->
    <link rel="stylesheet" href="flexslider.css" tppabs="http://envato.megadrupal.com/html/couponday/css/flexslider.css"/>
    <link rel="stylesheet" href="jquery.nouislider.css" tppabs="http://envato.megadrupal.com/html/couponday/css/jquery.nouislider.css"/>
    <link rel="stylesheet" href="jquery.popupcommon.css" tppabs="http://envato.megadrupal.com/html/couponday/css/jquery.popupcommon.css"/>

    <link rel="stylesheet" href="style.css" tppabs="http://envato.megadrupal.com/html/couponday/css/style.css"/>
    
    
    <!--[if IE 9]>
    <link rel="stylesheet" href="ie9.css" tppabs="http://envato.megadrupal.com/html/couponday/css/ie9.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="ie8.css" tppabs="http://envato.megadrupal.com/html/couponday/css/ie8.css"/>
    <![endif]-->

    <link rel="stylesheet" href="res-menu.css" tppabs="http://envato.megadrupal.com/html/couponday/css/res-menu.css"/>
    <link rel="stylesheet" href="responsive.css" tppabs="http://envato.megadrupal.com/html/couponday/css/responsive.css"/>
    <!--[if lte IE 8]>
        <script type="text/javascript" src="html5.js" tppabs="http://envato.megadrupal.com/html/couponday/js/html5.js"></script>
    <![endif]-->

</head>
<body class=""><!--<div class="alert_w_p_u"></div>-->
<div class="container-page">
    <div class="mp-pusher" id="mp-pusher">
        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
        <nav id="mp-menu" class="mp-menu alternate-menu">
            <div class="mp-level">
                <h2>Menu</h2>
                <ul>
                    <li><a href="index.html" tppabs="http://envato.megadrupal.com/html/couponday/light/index.html">Home</a></li>
                    <li><a href="coupon.html" tppabs="http://envato.megadrupal.com/html/couponday/light/coupon.html">Coupons</a></li>
                    <li class="has-sub">
                        <a href="coupon-code.html" tppabs="http://envato.megadrupal.com/html/couponday/light/coupon-code.html">Coupons Code</a>
                        <div class="mp-level">
                            <h2>Coupons Code</h2>
                            <a class="mp-back" href="#">back</a>
                            <ul>
                                <li><a href="coupon-code.html" tppabs="http://envato.megadrupal.com/html/couponday/light/coupon-code.html">Coupons Code 1</a></li>
                                <li><a href="coupon-code-2.html" tppabs="http://envato.megadrupal.com/html/couponday/light/coupon-code-2.html">Coupons Code 2</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="has-sub">
                        <a href="brand-list.html" tppabs="http://envato.megadrupal.com/html/couponday/light/brand-list.html">Brands</a>
                        <div class="mp-level">
                            <h2>Brands</h2>
                            <a class="mp-back" href="#">back</a>
                            <ul>
                                <li><a href="brand-detail-1.html" tppabs="http://envato.megadrupal.com/html/couponday/light/brand-detail-1.html">Brand Detail 1</a></li>
                                <li><a href="brand-detail-2.html" tppabs="http://envato.megadrupal.com/html/couponday/light/brand-detail-2.html">Brand Detail 2</a></li>
                                <li><a href="brand-detail-3.html" tppabs="http://envato.megadrupal.com/html/couponday/light/brand-detail-3.html">Brand Detail 3</a></li>
                                <li><a href="brand-detail-4.html" tppabs="http://envato.megadrupal.com/html/couponday/light/brand-detail-4.html">Brand Detail 4</a></li>
                                <li><a href="brand-detail-5.html" tppabs="http://envato.megadrupal.com/html/couponday/light/brand-detail-5.html">Brand Detail 5</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="blog.html" tppabs="http://envato.megadrupal.com/html/couponday/light/blog.html">Blog</a></li>
                    <li><a href="my-coupon.html" tppabs="http://envato.megadrupal.com/html/couponday/light/my-coupon.html">My coupons(12)</a></li>
                    <li><a href="login.html" tppabs="http://envato.megadrupal.com/html/couponday/light/login.html">Login</a></li>
                </ul>
            </div>
        </nav><!--end: .mp-menu -->
        <div class="top-area">
            <div class="mod-head-slide">
                <div class="grid_frame">
                    <div class="wrap-slide">
                        <p class="ta-c"><img src="ajax-loader.gif" tppabs="http://envato.megadrupal.com/html/couponday/images/ajax-loader.gif" alt="loading"></p>
                        <div id="sys_head_slide" class="head-slide flexslider">
                            <ul class="slides">
                                <li>
                                    <img src="01_banner.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_banner.jpg" alt=""/>
                                </li>
                                <li>
                                    <img src="02_banner.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/02_banner.jpg" alt=""/>
                                </li>
                                <li>
                                    <img src="03_banner.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/03_banner.jpg" alt=""/>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!---  <div id="sys_mod_filter" class="mod-filter">
            <div class="grid_frame">
                <div class="container_grid clearfix">
                    <div class="grid_12">
                        <div class="lbl-search">
                            <input class="txt-search" id="sys_txt_search" type="search" placeholder="Search"/>
                            <input type="submit" class="btn-search" value=""/>
                        </div>
                        <div class="select-cate">
                            <div id="sys_selected_val" class="show-val">
                                <span data-cate-id="0">All type</span>
                                <i class="pick-down"></i>
                            </div>
                            <div id="sys_list_dd_cate" class="dropdown-cate">
                                <div class="first-lbl">All Categories</div>
                                <div class="wrap-list-cate clearfix">
                                    <a href="#" data-cate-id="1">Baby &amp; Toddler</a>
                                    <a href="#" data-cate-id="2">Automotive </a>
                                    <a href="#" data-cate-id="3">Beverages</a>
                                    <a href="#" data-cate-id="4">Books &amp; Magazines</a>
                                    <a href="#" data-cate-id="5">Foods </a>
                                    <a href="#" data-cate-id="6">Health Care</a>
                                    <a href="#" data-cate-id="7">Home Entertainment</a>
                                    <a href="#" data-cate-id="8">Personal Care </a>
                                    <a href="#" data-cate-id="9">Pet Care </a>
                                    <a href="#" data-cate-id="10">Professional Services </a>
                                    <a href="#" data-cate-id="11">Toys and Games</a>
                                    <a href="#" data-cate-id="12">Coupon Codes</a>
                                    <a href="#" data-cate-id="13">Recipes</a>
                                    <a href="#" data-cate-id="14">Household </a>
                                </div>
                            </div>
                        </div><!--end: .select-cate -->
              <!--          <div class="range-days-left">
                            <span class="lbl-day">Days left</span>
                            <span id="sys_min_day" class="min-day"></span>
                            <div id="sys_filter_days_left" class="filter-days"></div>
                            <span id="sys_max_day" class="max-day"></span>
                        </div><!--end: .range-days-left -->
              <!--          <input id="sys_apply_filter" class="btn btn-red type-1 btn-apply-filter" type="button" value="Apply Filter">
                    </div>
                </div>
            </div>
        </div>  <!--end: .mod-filter -->
        <div class="grid_frame page-content">
            <div class="container_grid">
                <div class="mod-grp-coupon block clearfix">
                    <div class="grid_12">
                        <h3 class="title-block has-link">
                            New Coupons
                            <a href="#" class="link-right">See all <i class="pick-right"></i></a>
                        </h3>
                    </div>
                    <div class="block-content list-coupon clearfix">
                        <div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_01.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_01.jpg" alt="$COUPON_TITLE"></a>
                                    </div>
                                </div>
                                <div class="coupon-price">$2.00 Off</div>
                                <div class="coupon-brand">Wallmart</div>
                                <div class="coupon-desc">Find Parts for All Major Brands at Sears PartsDirect </div>
                                <div class="time-left">9 days 4 hours left</div>
                                <a class="btn btn-blue btn-take-coupon" href="#">Take Coupon</a>
                            </div>
                            <i class="stick-lbl hot-sale"></i>
                        </div><!--end: .coupon-item -->
                        <div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_02.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_02.jpg" alt="$COUPON_TITLE"></a>
                                    </div>
                                </div>
                                <div class="coupon-price">Save $1.50 on two</div>
                                <div class="coupon-brand">Lindt Chocolate</div>
                                <div class="coupon-desc">Find Parts for All Major Brands at Sears PartsDirect </div>
                                <div class="time-left">9 days 4 hours left</div>
                                <a class="btn btn-blue btn-take-coupon" href="#">Take Coupon</a>
                            </div>
                        </div><!--end: .coupon-item -->
                        <div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_03.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_03.jpg" alt="$COUPON_TITLE"></a>
                                    </div>
                                </div>
                                <div class="coupon-price">$5.00 Off</div>
                                <div class="coupon-brand">Lindt Chocolate</div>
                                <div class="coupon-desc">Find Parts for All Major Brands at Sears PartsDirect </div>
                                <div class="time-left">2 days 14 hours left</div>
                                <a class="btn btn-blue btn-take-coupon dismiss" href="#">Dismiss Coupon</a>
                            </div>
                        </div><!--end: .coupon-item -->
                        <div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_04.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_04.jpg" alt="$COUPON_TITLE"></a>
                                    </div>
                                </div>
                                <div class="coupon-price">$7.00 Off</div>
                                <div class="coupon-brand">Wallmart</div>
                                <div class="coupon-desc">During the Red Star Spectacular Sale going on now get an extra 20% off</div>
                                <div class="time-left">12 days 1 hour left</div>
                                <a class="btn btn-blue btn-take-coupon" href="#">Take Coupon</a>
                            </div>
                            <i class="stick-lbl hot-sale"></i>
                        </div><!--end: .coupon-item -->
                    </div>
                </div><!--end block: New Coupons-->
                <div class="mod-grp-coupon block clearfix">
                    <div class="grid_12">
                        <h3 class="title-block has-link">
                            Featured Coupons
                            <a href="#" class="link-right">See all <i class="pick-right"></i></a>
                        </h3>
                    </div>
                    <div class="block-content list-coupon clearfix">
                        <div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_02.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_02.jpg" alt="$COUPON_TITLE"></a>
                                    </div>
                                </div>
                                <div class="coupon-price">$12.00 Off</div>
                                <div class="coupon-brand">Wallmart</div>
                                <div class="coupon-desc">Find Parts for All Major Brands at Sears PartsDirect </div>
                                <div class="time-left">9 days 4 hours left</div>
                                <a class="btn btn-blue btn-take-coupon" href="#">VIEW COUPON CODE</a>
                            </div>
                            <i class="stick-lbl hot-sale"></i>
                        </div><!--end: .coupon-item -->
                        <div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_03.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_03.jpg" alt="$COUPON_TITLE"></a>
                                    </div>
                                </div>
                                <div class="coupon-price">$17.50 off</div>
                                <div class="coupon-brand">Lindt Chocolate</div>
                                <div class="coupon-desc">Find Parts for All Major Brands at Sears PartsDirect </div>
                                <div class="time-left">9 days 4 hours left</div>
                                <a class="btn btn-blue btn-take-coupon dismiss" href="#">17GH0097</a>
                            </div>
                            <i class="stick-lbl trust-brand-y"></i>
                        </div><!--end: .coupon-item -->
                        <div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_01.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_01.jpg" alt="$COUPON_TITLE"></a>
                                    </div>
                                </div>
                                <div class="coupon-price">$3.00 Off</div>
                                <div class="coupon-brand">Lindt Chocolate</div>
                                <div class="coupon-desc">Find Parts for All Major Brands at Sears PartsDirect </div>
                                <div class="time-left">2 days 14 hours left</div>
                                <a class="btn btn-blue btn-take-coupon" href="#">Take Coupon</a>
                            </div>
                            <i class="stick-lbl trust-brand-b"></i>
                        </div><!--end: .coupon-item -->
                        <div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_04.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_04.jpg" alt="$COUPON_TITLE"></a>
                                    </div>
                                </div>
                                <div class="coupon-price">$7.00 Off</div>
                                <div class="coupon-brand">Wallmart</div>
                                <div class="coupon-desc">During the Red Star Spectacular Sale going on now get an extra 20% off</div>
                                <div class="time-left">12 days 1 hour left</div>
                                <a class="btn btn-blue btn-take-coupon" href="#">Take Coupon</a>
                            </div>
                        </div><!--end: .coupon-item -->
                        <div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_04.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_04.jpg" alt="$COUPON_TITLE"></a>
                                    </div>
                                </div>
                                <div class="coupon-price">$2.00 Off</div>
                                <div class="coupon-brand">Lindt Chocolate</div>
                                <div class="coupon-desc">Find Parts for All Major Brands at Sears PartsDirect </div>
                                <div class="time-left">9 days 4 hours left</div>
                                <a class="btn btn-blue btn-take-coupon" href="#">Take Coupon</a>
                            </div>
                        </div><!--end: .coupon-item -->
                        <div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_01.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_01.jpg" alt="$COUPON_TITLE"></a>
                                    </div>
                                </div>
                                <div class="coupon-price">$11.50 off</div>
                                <div class="coupon-brand">Lindt Chocolate</div>
                                <div class="coupon-desc">Find Parts for All Major Brands at Sears PartsDirect </div>
                                <div class="time-left">9 days 4 hours left</div>
                                <a class="btn btn-blue btn-take-coupon" href="#">Take Coupon</a>
                            </div>
                        </div><!--end: .coupon-item -->
                        <div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_03.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_03.jpg" alt="$COUPON_TITLE"></a>
                                    </div>
                                </div>
                                <div class="coupon-price">$12.00 Off</div>
                                <div class="coupon-brand">SunMart</div>
                                <div class="coupon-desc">Find Parts for All Major Brands at Sears PartsDirect </div>
                                <div class="time-left">2 days 14 hours left</div>
                                <a class="btn btn-blue btn-take-coupon dismiss" href="#">Dismiss Coupon</a>
                            </div>
                        </div><!--end: .coupon-item -->
                        <div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_02.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_02.jpg" alt="$COUPON_TITLE"></a>
                                    </div>
                                </div>
                                <div class="coupon-price">$4.5 Off</div>
                                <div class="coupon-brand">Wallmart</div>
                                <div class="coupon-desc">During the Red Star Spectacular Sale going on now get an extra 20% off</div>
                                <div class="time-left">12 days 1 hour left</div>
                                <a class="btn btn-blue btn-take-coupon" href="#">Take Coupon</a>
                            </div>
                        </div><!--end: .coupon-item -->
                    </div>
                    <a class="grid_6 btn btn-orange btn-load-more" href="#">Load more coupon</a>
                </div><!--end block: Featured Coupons-->
                <div class="mod-email-newsletter clearfix">
                    <div class="grid_12">
                        <div class="wrap-form clearfix">
                            <div class="left-lbl">
                                <div class="big-lbl">newsletter</div>
                                <div class="sml-lbl">Don't miss a chance!</div>
                            </div>
                            <div class="wrap-email">
                                <label for="sys_email_newsletter">
                                    <input type="email" id="sys_email_newsletter" placeholder="Enter your email here"/>
                                </label>
                            </div>
                            <button class="btn btn-orange btn-submit-email" type="submit">SUBSCRIBE NOW</button>
                        </div>
                    </div>
                </div><!--end: .mod-email-newsletter-->
                <div class="mod-brands block clearfix">
                    <div class="grid_12">
                        <h3 class="title-block has-link">
                            POPULAR BRANDS (129)
                            <a href="#" class="link-right">See all <i class="pick-right"></i></a>
                        </h3>
                    </div>
                    <div class="block-content list-brand clearfix">
                        <div class="brand-item grid_4">
                            <div class="brand-content">
                                <div class="brand-logo">
                                    <div class="wrap-img-logo">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_07.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_07.jpg" alt="$BRAND_TITLE"></a>
                                    </div>
                                </div>
                            </div>
                        </div><!--end: .brand-item -->
                        <div class="brand-item grid_4">
                            <div class="brand-content">
                                <div class="brand-logo">
                                    <div class="wrap-img-logo">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_07.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_07.jpg" alt="$BRAND_TITLE"></a>
                                    </div>
                                </div>
                            </div>
                        </div><!--end: .brand-item -->
                        <div class="brand-item grid_4">
                            <div class="brand-content">
                                <div class="brand-logo">
                                    <div class="wrap-img-logo">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_07.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_07.jpg" alt="$BRAND_TITLE"></a>
                                    </div>
                                </div>
                            </div>
                        </div><!--end: .brand-item -->
                        <div class="brand-item grid_4">
                            <div class="brand-content">
                                <div class="brand-logo">
                                    <div class="wrap-img-logo">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_07.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_07.jpg" alt="$BRAND_TITLE"></a>
                                    </div>
                                </div>
                            </div>
                        </div><!--end: .brand-item -->
                        <div class="brand-item grid_4">
                            <div class="brand-content">
                                <div class="brand-logo">
                                    <div class="wrap-img-logo">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_07.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_07.jpg" alt="$BRAND_TITLE"></a>
                                    </div>
                                </div>
                            </div>
                        </div><!--end: .brand-item -->
                        <div class="brand-item grid_4">
                            <div class="brand-content">
                                <div class="brand-logo">
                                    <div class="wrap-img-logo">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="01_07.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/01_07.jpg" alt="$BRAND_TITLE"></a>
                                    </div>
                                </div>
                            </div>
                        </div><!--end: .brand-item -->
                    </div>
                </div><!--end: .mod-brand -->
            </div>
        </div>
        <footer class="mod-footer">
            <div class="footer-top">
                <div class="grid_frame">
                    <div class="container_grid clearfix">
                        <div class="grid_3">
                            <div class="company-info">
                                <img src="logo-gray.png" tppabs="http://envato.megadrupal.com/html/couponday/images/logo-gray.png" alt="CouponDay"/>
                                <p class="rs">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud</p>
                                <p class="rs">
                                    1200 Balh Blah Avenue <br />
                                    Hanoi, Vietnam 12137
                                </p>
                            </div>
                        </div>
                        <div class="grid_3">
                            <div class="block social-link">
                                <h3 class="title-block">Follow us</h3>
                                <div class="block-content">
                                    <ul class="rs">
                                        <li>
                                            <i class="fa fa-facebook-square fa-2x"></i>
                                            <a href="#" target="_blank">Our Facebook page</a>
                                        </li>
                                        <li>
                                            <i class="fa fa-twitter-square fa-2x"></i>
                                            <a href="#" target="_blank">Follow our Tweets</a>
                                        </li>
                                        <li>
                                            <i class="fa fa-pinterest-square fa-2x"></i>
                                            <a href="#" target="_blank">Follow our Pin board</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--end: Follow us -->
                        <div class="grid_3">
                            <div class="block intro-video">
                                <h3 class="title-block">Intro Video</h3>
                                <div class="block-content">
                                    <div class="wrap-video" id="sys_wrap_video">
                                        <div class="lightbox-video">
                                                <a class="html5lightbox" href="javascript:if(confirm('http://player.vimeo.com/video/36932496  \n\nThis file was not retrieved by Teleport Pro, because it is addressed on a domain or path outside the boundaries set for its Starting Address.  \n\nDo you want to open it from the server?'))window.location='http://player.vimeo.com/video/36932496'" tppabs="http://player.vimeo.com/video/36932496" title=""><i class="btn-play"></i><img src="video-img.png" tppabs="http://envato.megadrupal.com/html/couponday/images/video-img.png" alt=""></a>     
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--end: Intro Video -->
                        <div class="grid_3">
                            <div class="block blog-recent">
                                <h3 class="title-block">Latest blog</h3>
                                <div class="block-content">
                                    <div class="entry-item flex">
                                        <a class="thumb-left" href="#">
                                            <img src="04-15.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/04-15.jpg" alt="$TITLE"/>
                                        </a>
                                        <div class="flex-body"><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing eli</a></div>
                                    </div>
                                    <div class="entry-item flex">
                                        <a class="thumb-left" href="#">
                                            <img src="04-16.jpg" tppabs="http://envato.megadrupal.com/html/couponday/images/ex/04-16.jpg" alt="$TITLE"/>
                                        </a>
                                        <div class="flex-body"><a href="#">Ut wisi enim ad minim veniam, quis nostrud</a></div>
                                    </div>
                                </div>
                            </div>
                        </div><!--end: blog-recent -->
                    </div>
                </div>
            </div><!--end: .foot-top-->
            <div class="foot-copyright">
                <div class="grid_frame">
                    <div class="container_grid clearfix">
                        <div class="left-link">
                            <a href="#">Home</a>
                            <a href="#">Term of conditions</a>
                            <a href="#">Privacy</a>
                            <a href="#">Support</a>
                            <a href="#">Contact</a>
                        </div>
                        <div class="copyright">
                            Copyright &copy; 2014 by www.couponday.com
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>

<script type="text/javascript" src="jquery-1.10.2.js" tppabs="http://envato.megadrupal.com/html/couponday/js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="jquery.flexslider-min.js" tppabs="http://envato.megadrupal.com/html/couponday/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="jquery.nouislider.js" tppabs="http://envato.megadrupal.com/html/couponday/js/jquery.nouislider.js"></script>
<script type="text/javascript" src="jquery.popupcommon.js" tppabs="http://envato.megadrupal.com/html/couponday/js/jquery.popupcommon.js"></script>
<script type="text/javascript" src="html5lightbox.js" tppabs="http://envato.megadrupal.com/html/couponday/js/html5lightbox.js"></script>
<!--//js for responsive menu-->
<script type="text/javascript" src="modernizr.custom.js" tppabs="http://envato.megadrupal.com/html/couponday/js/modernizr.custom.js"></script>
<script type="text/javascript" src="classie.js" tppabs="http://envato.megadrupal.com/html/couponday/js/classie.js"></script>
<script type="text/javascript" src="mlpushmenu.js" tppabs="http://envato.megadrupal.com/html/couponday/js/mlpushmenu.js"></script>

<script type="text/javascript" src="script.js" tppabs="http://envato.megadrupal.com/html/couponday/js/script.js"></script>

<!--[if lte IE 9]>
<script type="text/javascript" src="jquery.placeholder.js" tppabs="http://envato.megadrupal.com/html/couponday/js/jquery.placeholder.js"></script>
<script type="text/javascript" src="create.placeholder.js" tppabs="http://envato.megadrupal.com/html/couponday/js/create.placeholder.js"></script>
<![endif]-->
<!--[if lte IE 8]>
<script type="text/javascript" src="ie8.js" tppabs="http://envato.megadrupal.com/html/couponday/js/ie8.js"></script>
<![endif]-->

</body>
</html>