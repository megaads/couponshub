<!--- OGP --->
<meta property="og:title" content="<?php echo  getConfiguration('home_page_title');  ?>" />
<meta property="og:description" content="Get latest Coupons ,Deals and Discounts of India's top online stores" />
<meta property="og:image" content="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" />
<meta property="og:url" content="<?php echo WEB_ROOT;?>" />
<!---End OGP --->

<link href="<?php echo WEB_ROOT;?>css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/main.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/style.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/animate.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/responsive.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
<?php include 'ga.php';?>
<link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" />
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TTK3LL');</script>
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1236143123117479');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1236143123117479&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<!---- Notifications ----->
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async='async'></script>
  <script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(["init", {
      appId: "c6297183-cb3b-49dc-8cbb-80d04bd1d6f8",
      autoRegister: true, /* Set to true to automatically prompt visitors */
      subdomainName: 'couponshub-co.onesignal.com',   
      notifyButton: {
          enable: true /* Set to false to hide */
      }
    }]);
  </script>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-4120507368931039",
    enable_page_level_ads: true
  });
</script>
<script src="//my.hellobar.com/28a9669ae77398bf48da80325b3199cdd4021cb3.js" type="text/javascript" charset="utf-8" async="async"></script>
