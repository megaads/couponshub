<?php include_once 'coupons_field.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="google-site-verification" content="JkII0lEUb72JmF9am31fcH0nH-sXjbgyi_pb4khD5zk" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo  getConfiguration('home_page_title');  ?> </title>
<meta name="keywords" content="<?php echo  getConfiguration('home_page_meta_keyword');   ; ?>" >
<meta name="description" content="<?php echo  getConfiguration('home_page_meta_description');   ; ?>">
<link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" />
<link rel="canonical" href="<?php echo WEB_ROOT ?>"/>

<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
<?php include 'script.php';?>
<script async src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script async src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style>
h3.mbr-section-title.display-2 {
    font-size: 35px;
    font-family: inherit;
    text-shadow: 2px 2px #ffffff;
}
.mbr-table-cell.col-md-5.text-xs-center.text-md-left {
    padding-top: 30px;
}
.mbr-table-cell.mbr-right-padding-md-up.col-md-5.text-xs-center.text-md-right {
    padding-top: 30px;
}
.lead p {
    color: #000000;
}
section.header-how-it-works h1 {
    text-align: -webkit-center;
    padding-top: 45px;
    font-family: sans-serif;
    color: #fff;
    font-weight: 700;
    text-shadow: 0 5px 7px #000;
}
section.header-how-it-works {
    height: 150px;
    background: #41a85f;
}
section#msg-box5-a {
    transform: rotate(-5deg);
}
#wrap{
	background: red; /* For browsers that do not support gradients */
  background: -webkit-linear-gradient(left top, red, yellow); /* For Safari 5.1 to 6.0 */
  background: -o-linear-gradient(bottom right, red, yellow); /* For Opera 11.1 to 12.0 */
  background: -moz-linear-gradient(bottom right, red, yellow); /* For Firefox 3.6 to 15 */
  background: linear-gradient(to bottom right, red, yellow); /* Standard syntax */
}
</style>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TTK3LL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Page Wrap ===========================================-->

<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
     <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
	 <section class="header-how-it-works">
	 <h1><i class="fa fa-money" aria-hidden="true"></i> &nbsp&nbspHow Cashback works in couponshub ?</h1>
	 </section>
	 
	 <section class="mbr-section" id="msg-box5-9" style="background-color: rgb(243, 198, 73); padding-top: 0px; padding-bottom: 0px;">

    
    <div class="container">
        <div class="row">
            <div class="mbr-table-md-up">

              <div class="mbr-table-cell mbr-right-padding-md-up mbr-valign-top col-md-7">
                  <div class="mbr-figure"><a href="http://207.246.98.232:83/payment.php" target="_blank"><img class="img-responsive" src="http://207.246.98.232:83/images/category/Couponshub-howitworks-1-min.png" alt="Couponshub- How-it-works" title="Get cashback on your every purchase via Couponshub - Check how Couponshub Works"></a></div>
              </div>

              


              <div class="mbr-table-cell col-md-5 text-xs-center text-md-left">
                  <h3 class="mbr-section-title display-2">Join 207.246.98.232:83 for FREE</h3>
                  <div class="lead">

                    <p>Join/Browse in Couponshub &amp; Find great deals from your favorite stores and earn extra cashback over and above the discounts from couponshub ..</p>

                  </div>

                  <div><a class="btn btn-primary" href="http://207.246.98.232:83">Join Now&nbsp;</a></div>
              </div>


              

            </div>
        </div>
    </div>

</section>
<section class="mbr-section" id="msg-box5-a" style="background-color: rgb(0, 168, 133); padding-top: 0px; padding-bottom: 0px;">
        <div class="container">
        <div class="row">
            <div class="mbr-table-md-up">

              

              <div class="mbr-table-cell mbr-right-padding-md-up col-md-5 text-xs-center text-md-right">
                  <h3 class="mbr-section-title display-2">Select your favorite offer &nbsp;&amp; start shopping via us</h3>
                  <div class="lead">

                    <p>Your savings starts from here.! &nbsp;Select your offers &amp; click on Get cashback &amp; apply COUPON CODE (if any at Checkout ).&nbsp;<br>We will track your purchase and &nbsp;add cashback in to your Couponshub account</p>

                  </div>

                  <div><a class="btn btn-primary" href="http://207.246.98.232:83/coupons">Shop Now</a></div>
              </div>


              


              <div class="mbr-table-cell mbr-valign-top col-md-7">
                  <div class="mbr-figure"><a href="http://207.246.98.232:83/stores" target="_blank"><img class="img-responsive" src="http://207.246.98.232:83/images/category/Couponshub-howitworks-2-min.png" alt="Couponshub-Cashback-how-it-works" title="Now get cashback on your every purchase from couponshub - Learn how couponshub cashback works"></a></div>
              </div>

            </div>
        </div>
    </div>

</section>
<section class="mbr-section" id="msg-box5-9" style="background-color: rgb(243, 198, 73); padding-top: 0px; padding-bottom: 0px;">

    
    <div class="container">
        <div class="row">
            <div class="mbr-table-md-up">

              <div class="mbr-table-cell mbr-right-padding-md-up mbr-valign-top col-md-7">
                  <div class="mbr-figure"><a href="http://207.246.98.232:83/payment.php" target="_blank"><img class="img-responsive" src="http://207.246.98.232:83/images/category/Couponshub-howitworks-3-min.png" alt="Couponshub- How-it-works" title="Get cashback on your every purchase via Couponshub - Check how Couponshub Works"></a></div>
              </div>

              


              <div class="mbr-table-cell col-md-5 text-xs-center text-md-left">
                  <h3 class="mbr-section-title display-2">Check your Cashback transactions</h3>
                  <div class="lead">

                    <p>Once you complete with shopping come back and check your dashboard.The cashback will be added in your couponshub account within 48hrs.</p>

                  </div>

                  <div><a class="btn btn-primary" href="http://207.246.98.232:83/myaccount.php">Check your transactions</a></div>
              </div>


              

            </div>
        </div>
    </div>

</section>
<section class="mbr-section" id="msg-box5-a" style="background-color: rgb(0, 168, 133); padding-top: 0px; padding-bottom: 0px;">
        <div class="container">
        <div class="row">
            <div class="mbr-table-md-up">

              

              <div class="mbr-table-cell mbr-right-padding-md-up col-md-5 text-xs-center text-md-right">
                  <h3 class="mbr-section-title display-2">Withdraw Cashback</h3>
                  <div class="lead">

                    <p>Time to withdraw your cashback once it confirms.You can withdraw in to your banks or into your digital wallets like Paytm or in Mobikwik</p>

                  </div>

                  <div><a class="btn btn-primary" href="http://207.246.98.232:83/payment.php">Withdraw Cashback</a></div>
              </div>


              


              <div class="mbr-table-cell mbr-valign-top col-md-7">
                  <div class="mbr-figure"><a href="http://207.246.98.232:83/stores" target="_blank"><img class="img-responsive" src="http://207.246.98.232:83/images/category/Couponshub-howitworks-4-min.png" alt="Couponshub-Cashback-how-it-works" title="Now get cashback on your every purchase from couponshub - Learn how couponshub cashback works"></a></div>
              </div>

            </div>
        </div>
    </div>

</section>
<section class="mbr-section" id="msg-box4-5" style="background-color: rgb(242, 242, 242); padding-top: 70px; padding-bottom: 70px;">

    
    <div class="container">
        <div class="row">
            <div class="mbr-table-md-up">

              <div class="mbr-table-cell mbr-right-padding-md-up mbr-valign-top col-md-7">
                  <div class="mbr-figure"><iframe class="mbr-embedded-video" src="https://www.youtube.com/embed/bEw_u7jfnVs?rel=0&amp;amp;showinfo=0&amp;autoplay=0&amp;loop=0" width="620" height="350" frameborder="0" allowfullscreen></iframe></div>
              </div>

              


              <div class="mbr-table-cell col-md-5 text-xs-center text-md-left">
                  <h3 class="mbr-section-title display-2">Couponshub <br>Intro Video</h3>
                  <div class="lead">

                    <p></p><p>CouponsHub aims to provide our visitors and members the latest coupon codes promotional codes from leading retailers/ merchants in INDIA. The CouponsHub team works with 1000s of retailers to collate the latest offers that can save you some money. SAVE BIG WITH COUPONSHUB Coupon codes are very easy and simple to use.</p><p></p>

                  </div>

                  
              </div>


              

            </div>
        </div>
    </div>

</section>

	 
	
	  <?php include 'footer.php';?>
   </div>
     <?php include 'js.php';?>  
	 </body>
	 </html>