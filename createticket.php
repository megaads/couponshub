<?php include_once 'tickets_field.php';
$crumbs[]="<a href='".WEB_ROOT."tickets.php'>Missing Cashback</a>";
$crumbs[]=  __('Raise Ticket');
?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('Create Ticket');?> - <?php echo SITE_NAME;?></title>
                                               
<link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" /> 
<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->

<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/main.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
     
</head>
<body>

<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
   <section class="myaccount">
     <div class="container">
      <?php include_once 'breadcrumb.php';?>
     <div class="ui-tabs">
      <div class="col-md-3"><?php include 'custmenu.php';?></div>  
 <div class="col-md-9 white-body"> 	  
    <div id="tabs-1" class="ui-tabs-panel1">
        <h4><?php echo('Create Missing Cashback Ticket');?>.</h4>
                   <?php   $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg'];
             echo getMessage($msgcode);?>
          <form method="post" name="frm" enctype="multipart/form-data" >
          <input type="hidden" name="txtMode" value="add" >
           
            <ul class="row">
              <li class="col-md-6" style="float: none;">
                <div class="form-group">
                  <label for=""><?php echo('Transaction Date');?> *
                    <input type="text" class="form-control" id="transaction_date" value="<?php echo (isset($rec['transaction_date']))?$rec['transaction_date']:"";?>" placeholder="" name="transaction_date" required>
                  </label>
                </div>
              </li>
              <li class="col-md-6"  style="float: none;">
                <div class="form-group">
                  <label for=""><?php echo('Retailer Name');?>*
                   <div id="retailer"> <select class="form-control"  name="retailer_id">
                    <option value=""><?php echo('Please select');?></option>
                    </select>  <div>
                  </label>
                </div>
              </li>
               <li class="col-md-6"  style="float: none;">
                <div class="form-group">
                  <label for=""><?php echo('Transaction Amount');?> *
                    <input type="text" class="form-control" id="" value="<?php echo (isset($rec['transaction_amount']))?$rec['transaction_amount']:"";?>" placeholder="" name="transaction_amount" required>
                  </label>
                </div>
              </li>
               <li class="col-md-6"  style="float: none;">
                <div class="form-group">
                  <label for=""><?php echo('Description');?> *
                     <textarea name="description"class="form-control" required><?php echo (isset($rec['description']))?$rec['description']:""?></textarea>
                  </label>
                </div>
              </li>
                  
              <li class="col-md-6"  style="float: none;">
                <div class="form-group">
                  <label for=""><?php echo('Reference Image');?> 
                    <input type="file"  id="" placeholder="" name="ticket_image" required>
                              
                  </label>
                </div>
              </li>              
              <li class="col-md-6"  style="float: none;">
                <button type="submit" class="btn"><?php echo('Create');?></button>
              </li>
            </ul>
          </form>                   
        
     </div>
    </div>
    </div>
        
</div>
     
     
     </div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
     <?php include 'js.php';?>      
<script language="JavaScript" type="text/javascript" src="<?php echo WEB_ROOT;?>js/jquery-ui.js"></script>
<link href="<?php echo WEB_ROOT;?>css/jquery-ui.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
 $(function() {
    $( "#transaction_date" ).datepicker();
         
  });
 
 $("#transaction_date").change(function(){
           var  tr_date=$('#transaction_date').val();
         
      $.post("getretailer.php", {transaction_date:tr_date}, function(result){
        $("#retailer").html(result);
    });                          
});       
  
  
</script>         
</body>
</html>