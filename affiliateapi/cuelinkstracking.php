<?php

ini_set("display_errors",1);
define('TABLE_NAME','tbl_transaction');
require_once("../admin/system/constant.php"); 
require_once("../admin/system/databaseLayer.php"); 
require_once("../admin/functions/common.php"); 
require_once("../admin/functions/listfunction.php"); 
require_once("../admin/functions/commonfun.php"); 
                      
 $referral_level=getConfiguration('referral_level'); 
      $referral_commision=getConfiguration('referral_commision');        
function getConversion($affiliate,$start_date,$end_date)
      {      
            
          $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "https://www.cuelinks.com/api/v2/transactions.json?start_date=2017-04-01&end_date=2017-05-31&page=1&per_page=50");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              "Authorization: Token token=".$affiliate['token'],
              "Content-Type: application/json"
            ));

              $response = curl_exec($ch);
            curl_close($ch);
           return json_decode($response);
      }
                      
                  
     function fetchPayment($affiliate)
   {
        global $db;   
                 
       $start_date=date("Y-m-d", strtotime("-10 day"));
       $end_date=date("Y-m-d"); 
        
       $records=getConversion($affiliate,$start_date,$end_date) ;
          
        
       foreach($records->transactions as $rec)
       {  
           if($rec->id=="") continue;
           
           $sqlextra="";
           $cols2="reference_id";
           $vals2="'".$rec->id."'";  
            
            $resChk = isDuplicate("transaction_id",$cols2,$vals2,TABLE_NAME, $sqlextra);
                  
            if(!empty($resChk) && $resChk > 0) {
               continue;
            }     
              
           $sql="select * from tbl_retailer where name='".$rec->store_name."' and affiliate_id=".$affiliate['affiliate_id']." and 

status=1";
          $store =$db->objSelect($sql, "ROW") ; 
          if(!isset($store['retailer_id'])) continue;     
          
 //if($store['retailer_id']!=562) continue;
        if($rec->aff_sub=="") continue; 
        
        $sql="select user_id from tbl_user where user_id='".$rec['Stat']['affiliate_info1']."' and status=1";
        
          $user =$db->objSelect($sql, "ROW") ; 
          if(!isset($user['user_id'])) continue;                                   
        
          $data['user_id']=$rec->aff_sub;                                                     
          $data['retailer_id']=$store['retailer_id'];
          $data['sale_amount']=$rec->sale_amount;
          $data['payment_type']=$rec->id;
          $data['earned_amount']=$rec->user_commission;
          
         if($rec->status=="rejected") 
            $data['payment_status']="Declined";
         else
            $data['payment_status']="Pending";
          
          $revenueshare= getConfiguration('cashback_share');
          
          
          $data['amount']= floatval(($data['earned_amount']*$revenueshare/100));
             
                                                                          
          $data['reference_id']=$rec->id;

          importRecord($data,$affiliate);                                                    
           
       } 
       
       
   }  
   
        
   function importRecord($data,$affiliate)
   {
         global $db;
         global $referral_commision;  
         global $referral_level;
        $fields=array();
        $fields=array

('user_id','retailer_id','payment_type','sale_amount','earned_amount','amount','payment_status','exp_confirm_date','reference_id');
        
        $rec['user_id']= $data['user_id'];
        $rec['retailer_id'] = $data['retailer_id'];
        $rec['payment_type']=$data['payment_type'];
        $rec['sale_amount']=$data['sale_amount'];
        $rec['earned_amount']=$data['earned_amount'];
        $rec['amount']=$data['amount'];

        $rec['payment_status']=$data['payment_status'];                                   
        $date = new DateTime();
        $date->modify("+60 days");
        $rec['exp_confirm_date']= $date->format("Y-m-d");                                                       
        $rec['reference_id']=$data['reference_id'];               
         
            
         $cols =implode(",",$fields).',created_date'; 
         $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec))."',NOW()";
     
        $pm1_id =$db->objInsert(TABLE_NAME,$cols,$vals,"LASTID"); 
          
        if($pm1_id>0)
        {  
             $enable_cashback_message = getConfiguration('enable_cashback_message');  
            
            if($enable_cashback_message)
            {
                $cashback_message = getConfiguration('cashback_message');   
                $userinfo=getUserInfo($rec['user_id']);
                
                $bad=array('{name}','{cashback_amount}');
                $good=array($userinfo['name'],$rec['amount']);
                
                $message=str_replace($bad,$good,$cashback_message); 
               
                $mobile=$userinfo['phone'];
                sendMessage($mobile,$message);    
                
            }
            
             $level=1;        
            $level++;
            
            if($level<=$referral_level)
            {       
            
              $sqlref="select ref_id from tbl_user where user_id=".$rec['user_id'];
                                
              $refuser =$db->objSelect($sqlref, "ROW");
                
                
              if($refuser && $referral_commision>0)
              {  
                 addrefferalcashback($refuser['ref_id'],$rec['amount'],$pm_id,$level,$affiliate) ; 
              } 
        
             }         
              
        }
                      
   }        
                  
                  
  function addrefferalcashback($userid,$amount,$pm_id,$level,$affiliate)
    {
        global $db;
        global $msg;
        
        global $referral_level;
        global $referral_commision; 
        // Insert into Horse table
        $fields=array();
        $fields=array('user_id','retailer_id','payment_type','amount','payment_status','parent_trans_id','exp_confirm_date','reference_id');
        
        $rec['user_id']= $userid;
        $rec['retailer_id'] =0;
        $rec['payment_type']="Referal Commision";
        $rec['amount']=floatval($amount*$referral_commision/100) ;
        $rec['payment_status']=$_POST['payment_status'];
        $rec['parent_trans_id']= $pm_id;                                
        $date = new DateTime();
        $date->modify("+60 days"); 
        $rec['exp_confirm_date']= $date->format("Y-m-d");                                                       
          $rec['reference_id']=GenerateReferenceID();               
            
         $cols =implode(",",$fields).',created_date'; 
         $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec))."',NOW()";
     
        $pm1_id =$db->objInsert(TABLE_NAME,$cols,$vals,"LASTID"); 
           
        if($pm1_id>0)
        {  
            $level++;
            
            if($level<=$referral_level)
            {       
            
              $sqlref="select ref_id from tbl_user where user_id=".$rec['user_id'];
                                
              $refuser =$db->objSelect($sqlref, "ROW");
                
              if($refuser && $referral_commision>0)
              {  
                 addrefferalcashback($refuser['ref_id'],$rec['amount'],$pm_id,$level,$affiliate) ; 
              } 
        
             }         
              
        }
          
          
    }
    
    
    $sql="select * from tbl_affiliate where name='Cuelinks' and status=1";    

$affiliates = $db->objSelect($sql,"ASSOC");
    
foreach($affiliates as $affiliate) {
     fetchPayment($affiliate);       
}                      
                               
 

?>
