<?php
ini_set("display_errors",1);
define('TABLE_NAME','tbl_retailer');
require_once("../admin/system/constant.php"); 
require_once("../admin/system/databaseLayer.php"); 
require_once("../admin/functions/common.php"); 
require_once("../admin/functions/listfunction.php"); 
require_once("../admin/functions/commonfun.php"); 

$revenueshare= getConfiguration('cashback_share');        

 function getOffers($affiliate,$page){
       $ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://www.cuelinks.com/api/v2/campaigns.json?page=".$page."&per_page=100");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HEADER, FALSE);

curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  "Authorization: Token token=".$affiliate['token'],
  "Content-Type: application/json"
));

$response = curl_exec($ch);
curl_close($ch);
return json_decode($response);

 }        

function multiexplode ($delimiters,$string) {
   
    $ready = str_replace($delimiters, $delimiters[0], $string);
    $launch = explode($delimiters[0], $ready);
    return  $launch;
}
   

function getImage($affiliate,$image) {
    
    $imageUrl=strstr($image, '?', true);
                     
      $imageName=basename($imageUrl);
     
     $imageString = file_get_contents($imageUrl);
     $save = file_put_contents(RETAILER_IMAGE_PATH_UPLOAD.$imageName,$imageString);
       
  return $imageName; 
    
    
}


function addRetailers($affiliate) {
      global $db;
        global $msg;
        global $rec ;
        global $revenueshare; 
  $flag=true;  
  $page=1;
   while($flag)
   {  
     $retailers = getOffers($affiliate,$page); 
     echo "<pre>";
    
        if(count($retailers->campaigns)>0)
        {
            setRetailer($affiliate,$retailers); 
            $page++;
        }
        else
            $flag=false;    
   } 
     
}

function setRetailer($affiliate,$retailers)
{
     global $db;
        global $msg;
        global $rec ;
        global $revenueshare; 
    $i=1;           
        foreach($retailers->campaigns as $offer) {
            
           // $delimiters=array('CPL','CPA','CPS','CPC','(CPL)','(CPA)','(CPS)','(CPC)','(CPI)');                                                    
                        
          //  $name = $offer->name;//multiexplode($delimiters,$offer);       
            $data['name']= $offer->name;
             
             
             
             
            $identifier=$data['name'];
            $identifier=createSlug($identifier);                                           
             
            $sqlextra=" and status <> 2";
            $cols1="identifier";
            $vals1="'".$identifier."'";  

            $resChk = isDuplicate("retailer_id",$cols1,$vals1,TABLE_NAME, $sqlextra);
             
            if(!empty($resChk) && $resChk > 0) {
                 continue;
            }          
            
            $sqlextra=" and affiliate_id='".$affiliate['affiliate_id']."'  and status <> 2";
            $cols1="merchant_id";
            $vals1="'".$offer->id."'";  

            $resChk = isDuplicate("retailer_id",$cols1,$vals1,TABLE_NAME, $sqlextra);
             
            if(!empty($resChk) && $resChk > 0) {
                 continue;
            }  
                     
              $data['identifier']= $identifier;
             $data['retailer_domain']= $offer->domain;
             $data['affiliate_id']=$affiliate['affiliate_id'];
             $data['merchant_id']=$offer->id;
             $data['url']="https://linksredirect.com/?pub_id={AFF_ID}&subid={USERID}&source=linkkit&url=".urlencode($offer->url);
                       
             
             $maxpayout=0;
             foreach($offer->payout_categories as $payout)
             {
                 if($payout->payout>$maxpayout)
                 {
                     $maxpayout=$payout->payout;
                 }     
             }
              $data['commission']=$maxpayout;
              
               $data['cashback']= $data['commission']*$revenueshare/100; 
           if($offer->payout_type=="Per Sale(%)")
             {       
                  
                   $data['fixed_percent']='percent';
             }
             else
             {  
                    $data['fixed_percent']='fixed';   
             }  
             
             
                  
                 
                   $data['description']="";
                    $data['expiry_date']=dateFormat("2020-05-05","Y-m-d"); 
                    $data['logo']=getImage($affiliate,$offer->image);
                 
              echo $i;
              add($affiliate,$data);
             $i++;
            
        }
    
    
}


function add($affiliate,$data)
    {
        global $db;
        global $msg;
        global $rec ;
        global $revenueshare; 
        // Insert into Horse table
        $fields=array();
        $fields=array('name','identifier','retailer_domain','affiliate_id','merchant_id','url','commission','cashback','fixed_percent','description','expiry_date','logo');
               
          
         $cols =implode(",",$fields).',created_date'; 
         $vals ="'".str_replace("*@@*","','",implode("*@@*",$data))."',NOW()";
     
          $retailer_id =$db->objInsert(TABLE_NAME,$cols,$vals,"LASTID"); 
            
        if($retailer_id>0)
        {    
             
            // addPayoutGroup($affiliate,$retailer_id,$data['merchant_id'],$data['affiliate_id']);
                   
        }
            
    }
       
    
$sql="select * from tbl_affiliate where name='Cuelinks' and status=1";      

$affiliates = $db->objSelect($sql,"ASSOC");

foreach($affiliates as $affiliate) {
    
    addRetailers($affiliate);
    
}          


?>