<?php include 'index_field.php';?>
<?php $userid=(isset($_SESSION['USER_ID']))?$_SESSION['USER_ID']:"Guest";  ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="google-site-verification" content="JkII0lEUb72JmF9am31fcH0nH-sXjbgyi_pb4khD5zk" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Flipkart Products,Flipkart Deals,Flipkart coupons,Flipkart discounts</title>
<meta name="keywords" content="<?php echo  getConfiguration('home_page_meta_keyword');   ; ?>" >
<meta name="description" content="<?php echo  'Get the lates flipkart deals and discounts offers and cashback deals from couponshub.Get the cashback on your every purchase via couponshub' ; ?>">
<link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" />
<link rel="canonical" href="<?php echo WEB_ROOT ?>"/>

<!-- FONTS ONLINE -->

 <style>
 #app-instoll-bar{background:url(/front/images/footer-new-bg.png) repeat;float:left;margin-bottom:-10px;padding:20px 0 0;width:100%}
.app-down-btn{width:100%;float:left;text-align:center}
.app-down-btn a img{margin:0 auto}

.image-link-grid {
  font-size: 0;
  margin-left: -5px !important;
  margin-right: -5px !important;
}
.image-link-grid.p > * {
  margin-bottom: 0;
}
.image-link-grid > * {
  box-sizing: border-box;
  display: inline-block;
  vertical-align: top;
  width: calc(25% - 0.5px);
  padding-left: 5px !important;
  padding-right: 5px !important;
  font-size: 15px;
  margin-bottom: 10px;
}
@media (max-width: 768px) {
  .image-link-grid {
    display: block;
    margin-left: 0 !important;
    margin-right: 0 !important;
  }
  .image-link-grid > * {
    display: block;
    width: 100% !important;
    padding-left: 0 !important;
    padding-right: 0 !important;
    margin-bottom: 10px !important;
  }
}
@media (max-width: 992px) {
  .image-link-grid > * {
    width: calc(50% - 0.5px);
  }
}
@media (max-width: 992px) {
  .image-link-grid > * {
    width: calc(50% - 0.5px);
  }
}
.image-link-grid .image-link {
  height: 300px;
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  position: relative;
}
.image-link-grid .image-link::before {
  content: "";
  position: absolute;
  left: 10px;
  top: 10px;
  right: 10px;
  bottom: 10px;
  background-color: rgba(0, 0, 0, 0.3);
  -webkit-transition: background-color 0.4s ease;
  -khtml-transition: background-color 0.4s ease;
  -moz-transition: background-color 0.4s ease;
  -ms-transition: background-color 0.4s ease;
  transition: background-color 0.4s ease;
}
.image-link-grid .image-link a {
  position: absolute;
  color: #ffffff;
  text-transform: uppercase;
  font-weight: 700;
  text-align: center;
  top: 50%;
  left: 50%;
  width: 80%;
  -webkit-transform: translate(-50%, -50%);
  -khtml-transform: translate(-50%, -50%);
  -moz-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}
.image-link-grid .image-link:hover::before {
  background-color: rgba(255, 102, 56, 0.9);
}
.grid_3 {
    width: 28%;
    display: inline;
    float: left;
    height: 400px;
    margin-left: 1.388888888888889%;
    margin-right: 1.388888888888889%;
}
.img-box img {
    display: block;
    height: 150px;
}

.grid_11 {
    width: 28%;
    display: inline;
    float: left;
    margin-left: 1.388888888888889%;
    margin-right: 1.388888888888889%;
	height:100px;
}
.tittle h3 {
    text-align: -webkit-center;
    color: #ffffff;
    font-size: xx-large;
    text-shadow: 2px 2px 4px #f3f3f3;
}
.tittle p{
	color: #ffffff;
}
.ribbon {
  position: absolute;
  right: -5px; top: -5px;
  z-index: 1;
  overflow: hidden;
  width: 75px; height: 75px;
  text-align: right;
}
.ribbon span {
  font-size: 10px;
  font-weight: bold;
  color: #FFF;
  text-transform: uppercase;
  text-align: center;
  line-height: 20px;
  transform: rotate(45deg);
  -webkit-transform: rotate(45deg);
  width: 100px;
  display: block;
  background: #79A70A;
  background: linear-gradient(#F70505 0%, #8F0808 100%);
  box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
  position: absolute;
  top: 19px; right: -21px;
}
.ribbon span::before {
  content: "";
  position: absolute; left: 0px; top: 100%;
  z-index: -1;
  border-left: 3px solid #8F0808;
  border-right: 3px solid transparent;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #8F0808;
}
.ribbon span::after {
  content: "";
  position: absolute; right: 0px; top: 100%;
  z-index: -1;
  border-left: 3px solid transparent;
  border-right: 3px solid #8F0808;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #8F0808;
}
.menu li {
    padding: 10px;
    border-bottom: 1px solid #c3c3c3;
    text-align: center;
}
	</style> 
  
  

<!--MAIN STYLE-->
<?php include 'script.php';?>



<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TTK3LL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Page Wrap ===========================================-->

<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
     <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
	 
<?php
/**
 * Demo Code
 * PHP Wrapper for Flipkart API (unofficial)
 * GitHub: https://github.com/xaneem/flipkart-api-php
 * Demo: http://www.clusterdev.com/flipkart-api-demo
 * License: MIT License
 *
 * @author Saneem (@xaneem, xaneem@gmail.com)
 */

//This is basic example code, and is not intended to be used in production.


//Don't forget to use a valid Affiliate Id and Access Token.

//Include the class.
include "clusterdev.flipkart-api.php";

//Replace <affiliate-id> and <access-token> with the correct values
$flipkart = new \clusterdev\Flipkart("srisrinad1", "4aed217c81f74024abd52f4e0db403fd", "json");


$dotd_url = 'https://affiliate-api.flipkart.net/affiliate/offers/v1/dotd/json';
$topoffers_url = 'https://affiliate-api.flipkart.net/affiliate/offers/v1/top/json';



//To view category pages, API URL is passed as query string.
$url = isset($_GET['url'])?$_GET['url']:false;
if($url){
	//URL is base64 encoded to prevent errors in some server setups.
	$url = base64_decode($url);

	//This parameter lets users allow out-of-stock items to be displayed.
	$hidden = isset($_GET['hidden'])?false:true;

	//Call the API using the URL.
	$details = $flipkart->call_url($url);

	if(!$details){
		echo 'Error: Could not retrieve products list.';
		exit();
	}

	//The response is expected to be JSON. Decode it into associative arrays.
	$details = json_decode($details, TRUE);

	//The response is expected to contain these values.
	$nextUrl = $details['nextUrl'];
	$validTill = $details['validTill'];
	$products = $details['productInfoList'];
	
$home = $flipkart->api_home();

//Make sure there is a response.
if($home==false){
	echo 'Error: Could not retrieve API homepage';
	exit();
}

//Convert into associative arrays.
$home = json_decode($home, TRUE);

$list = $home['apiGroups']['affiliate']['apiListings'];

echo '<div class="sp-header">
 <div class="img-head">
 <div class="tittle"><h3>Flipkart Products</h3>
 <p>Click on a category link to show available products from that category</p><br><br></div></div></div>';

//Create the tabulated view for different categories.
echo ' <section class="stores category-store"><div class="container"><div class="grid_frame page-content">
	<div class="left-bar">
            <div class="left-menu-store">
		<div class="left-title">Shop By Category</div>';
$count = 0;
$end = 1;
 echo "<ul class='menu'>";
foreach ($list as $key => $data) {
	$count++;
	$end = 0;

	//To build a 3x3 table.
	/* if($count%3==1)
		echo '<tr><td>';
	else if($count%3==2)
		echo '</td><td>';
	else{
		echo '</td><td>';
		$end =1;
	}
*/
	echo '<li><a href="?url='.base64_encode($data['availableVariants']['v0.1.0']['get']).'"><strong>'.$key.'</strong></a></li>';
	//echo "<br>";
	//URL is base64 encoded when sent in query string.
	
}


if($end!=1)
	echo '</div></div>';

	//The navigation buttons.
	echo '<h3 style="text-align: center;"><a href="?" style="color: black; text-shadow: 2px 2px 4px #030303;">HOME</a> | <a href="?url='.base64_encode($nextUrl).'" style="color: black; text-shadow: 2px 2px 4px #030303;">NEXT >></a></h3>';

	//Message to be displayed if out-of-stock items are hidden.
	/*if($hidden)
		echo 'Products that are out of stock are hidden by default.<br><a href="?hidden=1&url='.base64_encode($url).'">SHOW OUT-OF-STOCK ITEMS</a><br><br>'; */

	//Products table
	echo '<div class="right-bar">
    
    
            <div class="container_grid" > 
    
        <ul class="row storepage" id="couponslist">';
	$count = 0;
	$end = 1;

	//Make sure there are products in the list.
	if(count($products) > 0){
		foreach ($products as $product) {

			//Hide out-of-stock items unless requested.
			$inStock = $product['productBaseInfo']['productAttributes']['inStock'];
			if(!$inStock && $hidden)
				continue;
			
			//Keep count.
			$count++;

			//The API returns these values nested inside the array.
			//Only image, price, url and title are used in this demo
			$productId = $product['productBaseInfo']['productIdentifier']['productId'];
			$title = $product['productBaseInfo']['productAttributes']['title'];
			$productDescription = $product['productBaseInfo']['productAttributes']['productDescription'];

			//We take the 200x200 image, there are other sizes too.
			$productImage = array_key_exists('200x200', $product['productBaseInfo']['productAttributes']['imageUrls'])?$product['productBaseInfo']['productAttributes']['imageUrls']['200x200']:'';
			$sellingPrice = $product['productBaseInfo']['productAttributes']['sellingPrice']['amount'];
			$maximumRetailPrice = $product['productBaseInfo']['productAttributes']['maximumRetailPrice']['amount'];
			$productUrl = $product['productBaseInfo']['productAttributes']['productUrl'];
			$productBrand = $product['productBaseInfo']['productAttributes']['productBrand'];
			$color = $product['productBaseInfo']['productAttributes']['color'];
			$productUrl = $product['productBaseInfo']['productAttributes']['productUrl'];
			$discountPercentage= $product['productBaseInfo']['productAttributes']['discountPercentage'];
			//Setting up the table rows/columns for a 3x3 view.
			$end = 0;
	/*		if($count%3==1)
				echo '<tr><td>';
			else if($count%3==2)
				echo '</td><td>';
			else{
				echo '</td><td>';
				$end =1;
			}
*/
			echo ' <li><div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
									<div class="img-box">
                                       <a class="ver_container" target="_blank" href="'.$productUrl.'&aff_id='.$userid.'"><img class="img-responsive" src="'.$productImage.'" style="max-width:200px; max-height:200px;"/>
										</div><div class="c-img text-center"> 
              <div class="top-brand-img text-center">
              
              </div><div class="coupon-desc">'.$title."</div></a>";
			  echo 'Rs'.$sellingPrice.' &nbsp;&nbsp;&nbsp;<del>Rs. '.$maximumRetailPrice.'</del>';
			  echo '<div style="font-weight: bold; color: green;">+upto 8% Extra Cashback from Couponshub</div>';
			  echo '<div class="ribbon"><span>'.$discountPercentage.'% off</span></div>';
									
										
									   
									   
			echo '<div class="text-center" style="padding-bottom: 15px;"> 
                  
                <a target="_blank" href="'.$productUrl.'&aff_id='.$userid.'" class="btn btn-blue btn-take-coupon showcoupon">GET CASHBACK</a>
                
                </div>';
			if($end)
				echo '   </div></div>
              </div>
                
            </div></li> </ul> </section>';
				//echo '</td></tr>';

		}
	}

	//A message if no products are printed.	
	if($count==0){
		echo '<tr><td>The retrieved products are not in stock. Try the Next button or another category.</td><tr>';
	}

	//A hack to make sure the tags are closed.	
	if($end!=1)
		echo '</td></tr>';

//	echo '</table>';

	//Next URL link at the bottom.
	echo '<h2><a href="?url='.base64_encode($nextUrl).'">NEXT >></a></h2>';

	//That's all we need for the category view.
	exit();
}


//Deal of the Day DOTD and Tops offers
$offer = isset($_GET['offer'])?$_GET['offer']:false;
if($offer){

	if($offer == 'dotd'){
		//Call the API using the URL.
		$details = $flipkart->call_url($dotd_url);

		if(!$details){
			echo 'Error: Could not retrieve DOTD.';
			exit();
		}

		//The response is expected to be JSON. Decode it into associative arrays.
		$details = json_decode($details, TRUE);

		$list = $details['dotdList'];

		//The navigation buttons.
		echo '<h2 style="text-align: center;"><a href="?" style="color: black; text-shadow: 2px 2px 4px #030303;">HOME</a> | Deal of the day Offers | <a href="?offer=topoffers" style="color: black; text-shadow: 2px 2px 4px #030303;">Top Offers</a></h2>';

		//Show table
		echo "<table border=2 cellpadding=10 cellspacing=1 style='text-align:center'>";
		$count = 0;
		$end = 1;

		//Make sure there are products in the list.
		if(count($list) > 0){
			foreach ($list as $item) {
				//Keep count.
				$count++;

				//The API returns these values
				$title = $item['title'];
				$description = $item['description'];
				$url = $item['url'];
				$imageUrl = $item['imageUrls'][0]['url'];
				$availability = $item['availability'];

				//Setting up the table rows/columns for a 3x3 view.
				$end = 0;
				if($count%3==1)
					echo '<tr><td>';
				else if($count%3==2)
					echo '</td><td>';
				else{
					echo '</td><td>';
					$end =1;
				}

				echo '<a target="_blank" href="'.$url.'"><img src="'.$imageUrl.'" style="max-width:200px; max-height:200px;"/><br>'.$title."</a><br>".$description;

				if($end)
					echo '</td></tr>';

			}
		}
		//A message if no products are printed.	
		if($count==0){
			echo '<tr><td>No DOTDs returned.</td><tr>';
		}

		//A hack to make sure the tags are closed.	
		if($end!=1)
			echo '</td></tr>';

		echo '</table>';

		//That's all we need for the category view.
		exit();
	}else if($offer == 'topoffers'){

		//Call the API using the URL.
		$details = $flipkart->call_url($topoffers_url);

		if(!$details){
			echo 'Error: Could not retrieve Top Offers.';
			exit();
		}

		//The response is expected to be JSON. Decode it into associative arrays.
		$details = json_decode($details, TRUE);

		$list = $details['topOffersList'];

		//The navigation buttons.
		echo '<h2 style="text-align: center;"><a href="?" style="color: black; text-shadow: 2px 2px 4px #030303;">HOME</a> | <a href="?offer=dotd" style="color: black; text-shadow: 2px 2px 4px #030303;">Deal of the day Offers</a> | Top Offers</h2>';

		//Show table
		echo "<table border=2 cellpadding=10 cellspacing=1 style='text-align:center'>";
		$count = 0;
		$end = 1;

		//Make sure there are products in the list.
		if(count($list) > 0){
			foreach ($list as $item) {
				//Keep count.
				$count++;

				//The API returns these values
				$title = $item['title'];
				$description = $item['description'];
				$url = $item['url'];
				$imageUrl = $item['imageUrls'][0]['url'];
				$availability = $item['availability'];

				//Setting up the table rows/columns for a 3x3 view.
				$end = 0;
				if($count%3==1)
					echo '<tr><td>';
				else if($count%3==2)
					echo '</td><td>';
				else{
					echo '</td><td>';
					$end =1;
				}

				echo '<a target="_blank" href="'.$url.'"><img src="'.$imageUrl.'" style="max-width:200px; max-height:200px;"/><br>'.$title."</a><br>".$description;

				if($end)
					echo '</td></tr>';

			}
		}
		//A message if no products are printed.	
		if($count==0){
			echo '<tr><td>No Top Offers returned.</td><tr>';
		}

		//A hack to make sure the tags are closed.	
		if($end!=1)
			echo '</td></tr>';

		echo '</table>';

		//That's all we need for the category view.
		exit();

	}else{
		echo 'Error: Invalid offer type.';
		exit();
	}

}


//If the control reaches here, the API directory view is shown.

//Query the API
$home = $flipkart->api_home();

//Make sure there is a response.
if($home==false){
	echo 'Error: Could not retrieve API homepage';
	exit();
}

//Convert into associative arrays.
$home = json_decode($home, TRUE);

$list = $home['apiGroups']['affiliate']['apiListings'];

echo '<div class="sp-header">
 <div class="img-head">
 <div class="tittle"><h3>Flipkart Products</h3>
 <p>Click on a category link to show available products from that category</p><br><br></div></div></div>';

//Create the tabulated view for different categories.
echo ' <section class="stores category-store"><div class="container"><div class="grid_frame page-content">
    
    
            <div class="container_grid" > 
    
        
		';
$count = 0;
$end = 1;
 echo '<ul class="row storepage" id="couponslist">';
foreach ($list as $key => $data) {
	$count++;
	$end = 0;

	//To build a 3x3 table.
/*	if($count%3==1)
		echo '<tr><td>';
	else if($count%3==2)
		echo '</td><td>';
	else{
		echo '</td><td>';
		$end =1;
	}
*/ 
	echo '<li><div class="coupon-item grid_11">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    
									<div class="img-box"><a href="?url='.base64_encode($data['availableVariants']['v0.1.0']['get']).'">
	<div class="coupon-desc" style="text-align: center;color: black;font-size: large;padding-top: 15px;border: 2px solid grey;"><strong>'.$key.' - Deals</strong></div></a></li>';
	//echo "<br>";
	//URL is base64 encoded when sent in query string.
	//echo 'View Products &raquo;';
}


if($end!=1)
	echo '</div></div></section>';
//	echo '</td></tr>';
//echo '</table>';

//This was just a rough example created in limited time.
//Good luck with the API.
?>
 <?php include 'footer.php';?> </div>
  <?php include 'js.php';?>   
</body>
</html>