<?php include_once 'myaccount_field.php';?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('My Account');?> - <?php echo SITE_NAME;?></title>
        
 

<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
   <?php include 'script.php';?>  
         

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
  <script type="text/javascript">
    $(document).ready(function(){
        $(".ui-tabs").tabs();
    });
</script>
</head>
<body>

<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="myaccount">
     <div class="container">
      <?php include_once 'breadcrumb.php';?>

     <div class="ui-tabs">
             <div class="col-md-3"><?php include 'custmenu.php';?></div>
             <div class="col-md-9 myaccount-dashboard">
    <div id="tabs-1" class="ui-tabs-panel">
       <h4><?php echo('Dashboard');?></h4>
          <ul class="list-group">
                  <li class="list-group-item">
                    <span class="badge">
                   <?php echo $currency; ?><?php echo (isset($paymentstatus['Pending']))?$paymentstatus['Pending']:'0.00';?>
                    </span>
                    <?php echo('Pending Cashback');?>
                  </li>
                   
                  <li class="list-group-item">
                    <span class="badge"><?php echo $currency; ?><?php echo $balance;?></span>
                    <?php echo('Available Cashback');?> 
                  </li>
                   <li class="list-group-item">
                    <span class="badge"><?php echo $currency; ?><?php echo (isset($paymentstatus['Declined']))?$paymentstatus['Declined']:'0.00';?></span>
                    <?php echo('Declined  Cashback');?>
                  </li> 
                  <li class="list-group-item">
                    <span class="badge"><?php echo $currency; ?><?php echo (isset($paymentstatus['Requested']))?$paymentstatus['Requested']:'0.00';?></span>
                    <?php echo('Cash Out Requested');?>  
                  </li> 
                  <li class="list-group-item">
                    <span class="badge"><?php echo $currency; ?><?php echo (isset($paymentstatus['Paid']))?$paymentstatus['Paid']:'0.00';?></span>
                    <?php echo('Cash Out Processed');?> 
                  </li> 
          <li class="list-group-item">
                    <span class="badge"><?php echo $currency; ?><?php echo  $lifttimecashback;?></span>
                    <?php echo('Lifetime Cashback');?>  
                  </li>
                </ul>
      <div>
      </div>
      
      
      </div>
       
       
    </div>
        
</div>
     
     
     </div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
      <?php include 'js.php';?>      
</body>
</html>