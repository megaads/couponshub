<?php 
        require_once("admin/system/constant.php"); 
		require_once("admin/system/databaseLayer.php"); 
		require_once("admin/functions/common.php"); 
		require_once("admin/functions/listfunction.php"); 
		require_once("admin/functions/commonfun.php"); 
		include_once 'category_field.php';
		                         
      $retailerlist= getRetailers();
      $couponslist= getCoupons(); 
      $categorieslist = getCategoriesMenu();	  
      $data= '<?xml version="1.0" encoding="UTF-8"?> 
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
      
      foreach($retailerlist as $rec)
      {
          $url=WEB_ROOT."store/".$rec['identifier'];
          $date=$rec['created_date'];
          $data.='
            <url>
            <loc>'.$url.'</loc>
            <lastmod>'.$date.'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
            </url>
            '; 
          
          
      }
      
       foreach($couponslist as $crec)
      {
          $url=WEB_ROOT."coupon/".$crec['identifier'];
          $date=$crec['created_date'];
          $data.='
            <url>
            <loc>'.$url.'</loc>
            <lastmod>'.$date.'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
            </url>
            '; 
          
          
      }
	   foreach($categorieslist as $catrec)
      {
          $url=WEB_ROOT."category/".$catrec['identifier'];
          $date=$catrec['created_date'];
          $data.='
            <url>
            <loc>'.$url.'</loc>
            <lastmod>'.$date.'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
            </url>
            '; 
          
          
      }
      
      
      
       $data.= '</urlset>'; 
      
       $handle=fopen("sitemap1.xml","w");
       
       fwrite( $handle,$data);
       
      fclose($fp);    
		
?>