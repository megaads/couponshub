<?php include_once 'user_field.php';
$crumbs[]= __('Refer and Earn');

  checkSiteUser();

  $RecPerPage=10;  
         
 $PageNum = 1;
if(isset($_GET['PageNum']) && $_GET['PageNum'] != "") {
     $PageNum = $_GET['PageNum'];
}
 $offset = ($PageNum - 1) * $RecPerPage;    

$cond=" ref_id=".$_SESSION['USER_ID'];     
$referedfriendslist= getReferredFriends($_SESSION['USER_ID'],$offset,$RecPerPage);

 $query="select count(user_id) as totRec from tbl_user where ".$cond."";
$path=WEB_ROOT.'refernearn.php';     
 $paging=  getAccountPagination($query,$RecPerPage,$PageNum,$path);




?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('Refer and Earn');?> - <?php echo SITE_NAME;?></title>
        
 

<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
     
       
  <link href="<?php echo WEB_ROOT;?>css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/main.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/style.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/animate.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/responsive.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
<?php include 'ga.php';
 
 
?>   

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
  <script type="text/javascript">
    $(document).ready(function(){
        $(".ui-tabs").tabs();
    });
</script>
</head>
<body>
<div class="se-pre-con"></div>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="myaccount">
     <div class="container">
      <?php include_once 'breadcrumb.php';?>
     <div class="ui-tabs">
         <div class="col-md-3"><?php include 'custmenu.php';?></div>  
         <div class="col-md-9 white-body">      
    <div id="tabs-1" class="ui-tabs-panel">
        <h4><?php echo('Refer Friends & Earn');?>  <?php echo getConfiguration('referral_commision');?>% <?php echo('Cashback forever');?>!</h4>
           <?php   $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg'];
             echo getMessage($msgcode);?>
           
       <div class="col-md-12">      
          <form method="post" name="frm" enctype="multipart/form-data" >
          <input type="hidden" name="txtMode" value="changepwd" >
            <ul class="pad0">
              <li>
                <div class="form-group">
                  <label for=""><?php echo('Your unique referral link');?>: *                  </label>
                    <input type="text" class="form-control" size="40" id="" value="<?php echo WEB_ROOT.'signup?ref='.$_SESSION['USER_ID'];?>"  placeholder="" name="referallink">
                </div>
              </li>
                      
           </ul>
          </form>  
       </div>
       <div class="col-md-12 color-white">
       <div class="inner-share-section">
       <h5><?php echo('Invite Via Social Media');?></h5>
       
       <a href="javascript:;" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php echo WEB_ROOT.'signup.php?ref='.$_SESSION['USER_ID'];?>','Facebook Share','width=600')"><i class="fa fa-facebook" aria-hidden="true"></i></a>
       <?php
       $text=urlencode("I have Joined ".SITE_NAME." and earned cashback");
       ?>
       <a href="javascript:;" onclick="window.open('http://twitter.com/share?text=<?php echo $text; ?>&url=<?php echo WEB_ROOT.'signup.php?ref='.$_SESSION['USER_ID'];?>','Twitter Share','width=600')"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        <a href="javascript:;" onclick="window.open('http://twitter.com/share?text=<?php echo $text; ?>&url=<?php echo WEB_ROOT.'signup.php?ref='.$_SESSION['USER_ID'];?>','Twitter Share','width=600')"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
        <a href="javascript:;" onclick="window.open('http://twitter.com/share?text=<?php echo $text; ?>&url=<?php echo WEB_ROOT.'signup.php?ref='.$_SESSION['USER_ID'];?>','Twitter Share','width=600')"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
       </div>
       </div>                    
     <div class="col-md-12 clear">
     
      <h4><?php echo('My Referral Network');?></h4>    
<div class="table-responsive">
                  <table class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th><?php echo('Date Joined');?></th>
                        <th><?php echo('Referral Name');?></th>
                        <th><?php echo('Referral Account Status');?></th>                                    
                      </tr>
                    </thead>
                   <tbody>
                   <?php foreach($referedfriendslist as $friend):?>
                   <tr>
                   <td><?php echo date('M d,Y',strtotime($friend['created_date']))?></td>
                   <td><?php echo $friend['name'];?></td>
                   <td><?php echo ($friend['verified']==1)?"Active":"InActive";?></td>             
                   </tr>       
                   <?php endforeach;?>                       
                   </tbody>       
                  </table>
                    <ul class="pagination">
            <?php echo $paging;?>
             
        </ul>
                </div>
     
     </div>   
      
        
        
        
    </div>
    </div>
        
</div>
  

     
     
     </div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
    <?php include 'js.php';?>      
</body>
</html>
