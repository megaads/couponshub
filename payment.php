<?php include_once 'payment_field.php';?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('Withdraw Money');?> - <?php echo SITE_NAME;?></title>
  

<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" /> 
<!--MAIN STYLE-->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/main.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
         

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style>
@media only screen and (max-width: 450px){
.grid_3 {
    display: inline;
    float: left;
    margin-left: 1.388888888888889%;
    margin-right: 1.388888888888889%;
    width: 100%;
}
}
</style>     
</head>
<body>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="myaccount">
     <div class="container">
      <?php include_once 'breadcrumb.php';?>
     <div class="ui-tabs">
          <div class="col-md-3"><?php include 'custmenu.php';?></div>     
    <div class="col-md-9 white-body">       
  
    <div id="tabs-1" class="ui-tabs-panel"> 
     <?php if($_GET['msg']!='addsuc'):?>
   <?php   $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg'];
             echo getMessage($msgcode);?>       
    <?php endif;?>
       <h3 style="margin-bottom: 25px;"><?php echo('Withdraw Money');?></h3>
       <?php if($_GET['msg']=='addsuc'):?>
    <?php echo('Congratulation! We have Received Your Payment Request.Your request will be processed soon!');?>       
     <?php else:?> 
     
     <?php if($minpay<$availablecashback): ?>
	 
	 <div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                       <a href="javascript:showOption('wallet')"><img src="<?php echo WEB_ROOT?>images/walletcashback.png" width="120" alt="wallet"></a>
                                    </div>
                                </div>
                                
                                <div class="coupon-desc">You can withdraw into Wallets Paytm or Mobikwik </div>
                                <div class="time-left">Your amount will be added within 24hrs</div>
                                <a class="btn btn-blue btn-take-coupon" href="javascript:showOption('wallet')">Withdraw into Wallets</a>
                            </div>
                            <i class="stick-lbl hot-sale">Wallets</i>
       </div>
	 <?php if($enable_mobilerecharge): ?>  
	   <div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                      <a href="javascript:showOption('recharge')"><img src="<?php echo WEB_ROOT?>images/rechargecashback.png" width="120" alt="recharge"></a> 
                                    </div>
                                </div>
                                
                                <div class="coupon-desc">You can use your cashback for Mobile recharge </div>
                                <div class="time-left"></div>
                                <a class="btn btn-blue btn-take-coupon" href="javascript:showOption('recharge')">Make a mobile recharge</a>
                            </div>
                            <i class="stick-lbl hot-sale">Recharge</i>
       </div>
       <?php endif;?>
	  
     
      
       
      <div style="clear:both"></div>
        <form method="post" name="frm" id="frmwithdraw" style="display: none;">
          <input type="hidden" name="txtMode" value="withdraw" > 
    
     
     
     <table style="padding: 25px;">
      <tr>
     <td ><?php echo('Cashback available for withdraw');?></td>
     <td width="50" align="center">:</td>
     <td>
        <span></span><?php echo $currency; ?><?php echo $availablecashback;?>     
       <input type="hidden" name="avail_balance" id="avail_balance" value="<?php echo $availablecashback;?>">
     </td>
     </tr>
     <tr>
     <td><?php echo('Payment Mehtod');?></td>
     <td width="50" align="center">:</td>
     <td>
     <select name="payment_method" id="payment_method" onchange="showHide(this.value)">
     <option value=""><?php echo('Select Payment Method');?></option>
     <?php foreach($paymentmethods as $pmtmthod):
     if($pmtmthod['name']=="Mobile Recharge") continue;
     ?>
     <option value="<?php echo $pmtmthod['pm_id']?>"><?php echo $pmtmthod['name']?></option>
     <?php endforeach;?>
     </select>
     
     </td>
     </tr>
        
     <tr>
     <td></td>
     <td></td>
     <td>
     <?php foreach($paymentmethods as $pmtmthod):?>
     <div class="boxdesc" style="display: none;" id="box-<?php echo $pmtmthod['pm_id']?>"><?php echo nl2br($pmtmthod['description']) ?></div>
     <?php endforeach;?> 
     
     
     </td>
     </tr>
     <tr id="pmtdetail" style="display: none;">
     <td ><?php echo('Payment Details');?></td>
     <td width="50" align="center">:</td>
     <td><textarea cols="" rows="" name="payment_details"></textarea></td>
     </tr>  
     
     <tr>
     <td><?php echo('Amount');?></td>
     <td width="50" align="center">:</td>
     <td>
       <input type="text" class="form-control" name="amount" id="amount" required />      
     
     </td>
     </tr>
     
         
      <tr>
     <td></td>
     <td></td>
     <td>
      <button type="submit" onclick="return doSubmit(this.form)" class="btn"><?php echo('Withdraw');?></button> 
     
     
     </td>
     </tr>
               
     </table>   
     </form>    
    
     
     <?php
    
      if($enable_mobilerecharge):
      
     ?>
     <div id="mobile-recharge"  style="display: none;">
      <h2>Recharge Now</h2>
      
      
      <form name="frmrecharge" id="frmrecharge" method="post">
       <input type="hidden" name="avail_balance" id="avail_balance" value="<?php echo $availablecashback;?>">
       <input type="hidden" name="txtMode" value="recharge" >   
      <table style="padding: 25px;">     
       <tr>
     <td><?php echo('Mobile Number');?></td>
     <td width="50" align="center">:</td>
     <td>
       <input type="text" class="form-control" name="mobile_number" id="mobile_number" maxlength="10">      
     
     </td>
     </tr>
      <tr>
     <td><?php echo('Recharge or Pay Bill');?></td>
     <td width="50" align="center">:</td>
     <td>
       <input type="radio" onclick="changeoperator('prepaid_operator')" checked="checked" name="recharge_type" id="recharge_type1" value="prepaid" > Prepaid
        <input type="radio" onclick="changeoperator('postpaid_operator')" name="recharge_type" id="recharge_type2" value="postpaid" >Postpaid
     
     </td>
     </tr>
     
     <tr class="operatortd" id="prepaid_operator">
     <td><?php echo('Operator');?></td>
     <td width="50" align="center">:</td>
     <td>
           <select name="prepaidoperator" id="prepaidoperator">
                <option value="">Choose</option>
                <option value="AT">Airtel</option>
                <option value="AL">Aircel</option>
                <option value="BS">BSNL</option>
                <option value="BSS">BSNL Special</option>
                <option value="IDX">Idea</option>
                <option value="VF">Vodafone</option>
                    
                <option value="TD">Docomo GSM</option>
                <option value="TDS">Docomo GSM Special</option>
                <option value="TI">Docomo CDMA (Indicom)</option>
                <option value="RG">Reliance GSM</option>
                <option value="RL">Reliance CDMA</option>
                <option value="MS">MTS</option>
                <option value="UN">Uninor</option>
                <option value="UNS">Uninor Special</option>
                <option value="VD">Videocon</option>
                <option value="VDS">Videocon Special</option>
                <option value="MTM">MTNL Mumbai</option>
                <option value="MTMS">MTNL Mumbai Special</option>
                <option value="MTD">MTNL Delhi</option>
                <option value="MTDS">MTNL Delhi Special</option>
                <option value="VG">Virgin GSM</option>
                <option value="VGS">Virgin GSM Special</option>
                <option value="VC">Virgin CDMA</option>
                <option value="T24">T24</option>
                <option value="T24S">T24 Special</option>
                </select>

     
     </td>
     </tr>
     
     <tr class="operatortd" id="postpaid_operator" style="display: none;">
     <td><?php echo('Operator');?></td>
     <td width="50" align="center">:</td>
     <td>
           <select name="postpaidoperator" id="postpaidoperator">
                <option value="">Choose</option>
                <option value="APOS">Airtel</option>
                
                <option value="BPOS">BSNL</option>
                
                <option value="IPOS">Idea</option>
                <option value="VPOS">Vodafone</option>                                      
                   
                <option value="RGPOS">Reliance GSM</option>
                <option value="RCPOS">Reliance CDMA</option>
                <option value="DGPOS">TATA DOCOMO GSM</option>
                <option value="DCPOS">TATA   INDICOM (CDMA)</option>
                <option value="CPOS">AIRCEL</option>
                           
                </select>

     
     </td>
     </tr>
     
     
     
     
     <tr>
     <td><?php echo('Amount');?></td>
     <td width="50" align="center">:</td>
     <td>
       <input type="text" class="form-control" name="amount" id="recharge_amount">      
     
     </td>
     </tr>
      <tr>
     <td></td>
     <td></td>
     <td>
      <button type="submit" onclick="return doRecharge(this.form)" class="btn"><?php echo('Recharge');?></button> 
     (<a href="<?php echo WEB_ROOT?>recharge.php">Recharge History</a>)
     
     </td>
     </tr>
      </table> 
      
      
      
      </form>
      </div>
      <?php endif;?>
       <?php else : ?>
        <?php echo('You have to reach min. payout '.$minpay.' to withdraw money!');?>       
       <?php endif; ?> 
      <?php endif;?> 
      
          
    </div>
   </div>     
</div>       
<h2>Withdraw Request History</h2>
<div class="table-responsive">
                  <table class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th><?php echo('Date');?></th>                                    
                        <th><?php echo('Amount');?></th>
                        <th><?php echo('Status');?></th>                                         
                        <th><?php echo('# Ref. ID');?></th>
                      </tr>
                    </thead>
                   <tbody>
                   <?php foreach($withdrawhistory as $wh):?>
                   <tr>
                   <td><?php echo date('M d,Y',strtotime($wh['created_date']))?></td>     
                    
                   <td> <?php echo $currency; ?><?php echo $wh['amount'];?></td>
                    <td><?php echo $wh['payment_status'];?></td>      
                 
                   <td><?php echo $wh['reference_id'];?></td>
                   </tr>
                   
                   <?php endforeach;?>
                   
                   </tbody>       
                  </table>
                    <ul class="pagination">
            <?php echo $paging;?>
             
        </ul>
                </div>


    
     </div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
<script type="text/javascript">
function showHide(val)
{
    
    $('#pmtdetail').show();
    $('.boxdesc').hide();
    $('#box-'+val).show();    
}

function doSubmit(frm)
{
     if(eval($('#avail_balance').val())<eval($('#amount').val()))
     {
         alert("You cannot withdraw more than available balance");
         return false;              
     }
    
    return true;
    
}

function changeoperator(operator)
{
   $('.operatortd').hide(); 
   $('#'+operator).show();
    
    
}


function doRecharge(frm)
{
     if(eval($('#avail_balance').val())<eval($('#recharge_amount').val()))
     {
         alert("You cannot recharge more than available balance");
         return false;              
     }
     
     mobile_number=$('#frmrecharge #mobile_number').val();           
      if($.trim(mobile_number)=="")
        {
           $('#frmrecharge #mobile_number').val('');
           $('#frmrecharge #mobile_number').attr("placeholder","Please enter mobile number");
           $('#frmrecharge #mobile_number').addClass('errorbox');
             return false;                        
        }
        else
        {
           $('#frmrecharge #mobile_number').attr("placeholder","");
           $('#frmrecharge #mobile_number').removeClass('errorbox')                                                      
        }                
        
        if(mobile_number.length<10)
        {
           $('#frmrecharge #mobile_number').val('');
           $('#frmrecharge #mobile_number').attr("placeholder","Please enter valid mobile number");
           $('#frmrecharge #mobile_number').addClass('errorbox');
            return false;              
        }
        else
        {
           $('#frmrecharge #mobile_number').attr("placeholder","");
           $('#frmrecharge #mobile_number').removeClass('errorbox')                                                      
        }                
                                 
     if($('#recharge_type1').is(":checked"))
     {
        operatorcode='prepaidoperator';   
     }    
     else
        operatorcode='postpaidoperator';   
         
         if($('#'+operatorcode).val()=="")
         {
             alert("Please select operator");
             return false;              
         }
     
     if($('#recharge_amount').val()=="" || eval($('#recharge_amount').val())<10)
     {
         alert("Please enter recharge amount minimum 10 rs.");
         return false;              
     }
     
     
    
    return true;
    
}

function showOption(value)
{
    
    if(value=="wallet")
    {
        $("#mobile-recharge").hide();
        $("#frmwithdraw").show(); 
        $("#payment_method").val('1'); 
        
    }
    if(value=="banktransafer")
    {
        $("#mobile-recharge").hide();
        $("#frmwithdraw").show(); 
        $("#payment_method").val('2'); 
        
    }
      if(value=="recharge")
    {
        $("#mobile-recharge").show();
        $("#frmwithdraw").hide(); 
       
        
    }
    
}

</script>  
  
        <?php include 'js.php';?>      
</body>
</html>
