<?php 
ini_set("display_errors",1);
        require_once("admin/system/constant.php"); 
        
        require_once("admin/system/databaseLayer.php"); 
        require_once("admin/functions/common.php"); 
        
        require_once("admin/functions/listfunction.php"); 
        require_once("admin/functions/commonfun.php"); 
        
         $minpay=getConfiguration('min_payout');
           $enable_mobilerecharge= getConfiguration('enable_mobilerecharge');   
        $RecPerPage=10;  
         
          checkSiteUser(); 
         
         $PageNum = 1;
        if(isset($_GET['PageNum']) && $_GET['PageNum'] != "") {
             $PageNum = $_GET['PageNum'];
        }
         $offset = ($PageNum - 1) * $RecPerPage;      
         
         
        $action = isset($_POST['txtMode']) ? $_POST['txtMode'] : '';
         checkSiteUser();
       $cashback=getUserBalance($_SESSION['USER_ID']);      
                            
         $availablecashback=$cashback;//($cashback)?$cashback[0]:0;      
         
         
         
    switch ($action) {
        
        case 'withdraw' :
            withdraw();
            break;                  
            
        case 'recharge' :
            recharge();
            break;                       
            
         
    }  
    
    
    
           
        
        function withdraw()
    {
        global $db;
        global $msg;
        global $rec;
          $user_id=$_SESSION['USER_ID'];
        $payment_details = addslashes(trim($_POST['payment_details'])); 
         $payment_method = addslashes(trim($_POST['payment_method']));      
          $amount = addslashes(trim($_POST['amount']));       
         $payment_status='Requested';
           $cashout_ref_id=GenerateReferenceID();
            $payment_type="Withdraw";
                                                                                    
         
       $cols="reference_id,user_id,payment_type,payment_method,payment_details,amount,payment_status,created_date";
       $vals="'".$cashout_ref_id."','".$user_id."','".$payment_type."','".$payment_method."','".$payment_details."','$amount','$payment_status','".CURRENT_DATETIME."'";
         
       $trans_id =$db->objInsert('tbl_transaction',$cols,$vals,"LASTID");
                     
         
        if($trans_id)
        {
            
            if(ENABLE_CASHBACK_EMAIL && $payment_status=="Requested")
            {
               $sql_query="select * from tbl_emailtemplate where template_code='user-email-withdraw-request' and status=1";
               $templaterec =$db->objSelect($sql_query, "ROW") ;
                
                $cashback_message =$templaterec['content'];
                $userinfo=getUserInfo($user_id);
                
                 $site_login_url=WEB_ROOT.'login.php';
                $bad=array('{{SITE_NAME}}','{{SITE_LOGIN_URL}}','{{name}}','{{amount}}');  
                $good=array(SITE_NAME,$site_login_url,$userinfo['name'],$rec['amount'].' '.CURRENCY);
                
                $Message=str_replace($bad,$good,$cashback_message); 
                $email_subject=str_replace($bad,$good,$templaterec['email_subject']);    
                
               $adminEmail=getConfiguration('admin_email');   
               sendMail($userinfo['email'],$adminEmail,$Message,$email_subject);                                                          
               
               // Mail to admin
                $cashback_message="";
               $sql_query="select * from tbl_emailtemplate where template_code='admin-email-withdraw-request' and status=1";
               $templaterec =$db->objSelect($sql_query, "ROW") ;
                
                $cashback_message =$templaterec['content'];                                              
                
                $site_login_url=WEB_ROOT.'login.php';
                $bad=array('{{SITE_NAME}}','{{SITE_LOGIN_URL}}','{{name}}','{{amount}}');  
                $good=array(SITE_NAME,$site_login_url,$userinfo['name'],$rec['amount'].' '.CURRENCY);
                
                $Message=str_replace($bad,$good,$cashback_message); 
                $email_subject=str_replace($bad,$good,$templaterec['email_subject']);    
                
               $adminEmail=getConfiguration('admin_email');   
               sendMail($adminEmail,$userinfo['email'],$Message,$email_subject);                                                          
                                                                                                                                                     
                
            }                
            
            
             addNotification("New Withdraw Request","cashouts",$cashout_ref_id,"money"); 
             header("location: payment.php?msg=addsuc");
             exit;
        }
          
          
    } 
    
    
     function recharge()
    {
        global $db;
        global $msg;
        global $rec;
        global $availablecashback;
        $user_id=$_SESSION['USER_ID'];
        $mobile_number=addslashes(trim($_POST['mobile_number']));       
         $operator=($_POST['recharge_type']=="prepaid")?addslashes(trim($_POST['prepaidoperator'])):addslashes(trim($_POST['postpaidoperator']));       ;       
        $payment_details = $_POST['recharge_type']."$operator:$mobile_number"; 
         $payment_method = getConfiguration('mobie_recharge_pmid');
          $amount = addslashes(trim($_POST['amount']));       
         $payment_status='Paid';
         $cashout_ref_id=GenerateReferenceID();
         $payment_type="Withdraw";
                
                                                                      
         $params['mobile'] =$mobile_number;
         
         $params['operator']=$operator;
         $params['amount']=$amount;
          $txtstatus= rechargeMobile($params);
                  
         if($availablecashback<$amount)
         {
              header("location: payment.php?msg=minthreshold");
              exit;
         }
         
         
         
       if($txtstatus=="SUCCESS")
       {
         
           $cols="reference_id,user_id,payment_type,payment_method,payment_details,amount,payment_status,created_date";
           $vals="'".$cashout_ref_id."','".$user_id."','".$payment_type."','".$payment_method."','".$payment_details."','$amount','$payment_status','".CURRENT_DATETIME."'";
             
           $trans_id =$db->objInsert('tbl_transaction',$cols,$vals,"LASTID");
           
           
           $cols="transaction_id,user_id,recharge_type,operator,mobile,amount,recharge_status,created_date";
           $vals="$trans_id,$user_id,'".$_POST['recharge_type']."','".$operator."','".$mobile_number."','".$amount."','".$txtstatus."','".CURRENT_DATETIME."'";
             
           $recharge_id =$db->objInsert('tbl_recharge',$cols,$vals,"LASTID");
            
           
           
            if($recharge_id)
            {
                 addNotification("Recharge Done","rechargereports",$recharge_id,"money"); 
                 header("location: payment.php?msg=rechargesuc");
                 exit;
            }
           
           
           
       } 
       
       if($txtstatus=="PENDING")
       {
           $payment_status="Requested";     
           $cols="reference_id,user_id,payment_type,payment_method,payment_details,amount,payment_status,created_date";
           $vals="'".$cashout_ref_id."','".$user_id."','".$payment_type."','".$payment_method."','".$payment_details."','$amount','$payment_status','".CURRENT_DATETIME."'";
             
           $trans_id =$db->objInsert('tbl_transaction',$cols,$vals,"LASTID");
           
           $cols="transaction_id,user_id,recharge_type,operator,mobile,amount,recharge_status,created_date";
           $vals="$trans_id,$user_id,'".$_POST['recharge_type']."','".$operator."','".$mobile_number."','".$txtstatus."','".CURRENT_DATETIME."'";
             
           $recharge_id =$db->objInsert('tbl_recharge',$cols,$vals,"LASTID");
             
           
           
           
           addNotification("Recharge Done","rechargereports",$recharge_id,"money"); 
           
           header("location: payment.php?msg=rechargepending");
             exit;
           
       }       
          
       if($txtstatus=="FAILED")
       {
           header("location: payment.php?msg=rechargefail");
           exit;            
       }    
          
    }        
        
    
      $paymentmethods= getPaymentMethods();
                  
      $paymentstatus="Confirmed";            
     // $cashback= getMyCashback($_SESSION['USER_ID'],$paymentstatus);
     
          
     /* if($availablecashback['total_amount']<$minpay)
      {               
             header("location: myearning.php?msg=minthreshold");
             exit;
              
      }  */
      
      $withdrawhistory=getMyWithdrawRequest($_SESSION['USER_ID'],$offset,$RecPerPage);
      
      
      
         $retailerlist= getRetailersMenu();    
        $crumbs[]=  __('My Payment');           
        
        
        
        
        
        
                     
               
        
?>