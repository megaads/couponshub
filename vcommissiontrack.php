<?php

ini_set("display_errors",1);
define('TABLE_NAME','tbl_transaction');
require_once("admin/system/constant.php"); 
require_once("admin/system/databaseLayer.php"); 
require_once("admin/functions/common.php"); 
require_once("admin/functions/listfunction.php"); 
require_once("admin/functions/commonfun.php"); 
                      
        
function getConversion($aff_id,$start_date,$end_date)
      {      
          
          $affrec=getAffilliates($aff_id);
                  
          define('HASOFFERS_API_URL', 'https://api.hasoffers.com/Apiv3/json');
    
              // Specify method arguments
    $args = array(
        'NetworkId' => $affrec[0]['access_key'],
        'Target' => 'Affiliate_Report',
        'Method' => 'getConversions',
        'api_key' => $affrec[0]['token'],
        'fields' => array(
            'Stat.sale_amount',
            'Stat.offer_id', 
            'Stat.goal_id',
            'Stat.id',
            'Stat.approved_payout',
            'Stat.conversion_status',
            'PayoutGroup.id',
            'Stat.affiliate_info1',
            'Stat.ad_id'
        ),
        'data_start' =>$start_date,
        'data_end' => $end_date
    );
      
     // Initialize cURL
    $curlHandle = curl_init();
 
    // Configure cURL request
    curl_setopt($curlHandle, CURLOPT_URL, HASOFFERS_API_URL . '?' . http_build_query($args));
 
    // Make sure we can access the response when we execute the call
    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
 
    // Execute the API call
     $jsonEncodedApiResponse = curl_exec($curlHandle);
       
    // Ensure HTTP call was successful
    if($jsonEncodedApiResponse === false) {
        throw new \RuntimeException(
            'API call failed with cURL error: ' . curl_error($curlHandle)
        );
    }
 
    // Clean up the resource now that we're done with cURL
    curl_close($curlHandle);
 
    // Decode the response from a JSON string to a PHP associative array
    $apiResponse = json_decode($jsonEncodedApiResponse, true);
 
    // Make sure we got back a well-formed JSON string and that there were no
    // errors when decoding it
    $jsonErrorCode = json_last_error();
    if($jsonErrorCode !== JSON_ERROR_NONE) {
        throw new \RuntimeException(
            'API response not well-formed (json error code: ' . $jsonErrorCode . ')'
        );
    }
 
    // Print out the response details
    if($apiResponse['response']['status'] === 1) {
        // No errors encountered
        return $apiResponse['response']['data'];
    }
    else {
        // An error occurred
        echo 'API call failed (' . $apiResponse['response']['errorMessage'] . ')';
        echo PHP_EOL;
        echo 'Errors: ' . print_r($apiResponse['response']['errors'], true);
        echo PHP_EOL;
    }
                  
      }
                      
                  
     function fetchPayment($aff_id)
   {
        global $db;   
       //$aff_id=5;//$_POST['affiliate_id'];
     //  echo $aff_id;exit;
       $start_date=date("Y-m-d", strtotime("-10 day"));
       $end_date=date("Y-m-d"); 
        
       $records=getConversion($aff_id,$start_date,$end_date) ;
        
       foreach($records['data'] as $rec)
       {  
           if($rec['Stat']['ad_id']=="") continue;
           
           $sqlextra="";
           $cols2="payment_type";
           $vals2="'".$rec['Stat']['ad_id']."'";  
            
            $resChk = isDuplicate("transaction_id",$cols2,$vals2,TABLE_NAME, $sqlextra);
                  
            if(!empty($resChk) && $resChk > 0) {
               continue;
            }     
              
           $sql="select * from tbl_retailer where merchant_id='".$rec['Stat']['offer_id']."' and affiliate_id=$aff_id and 

status=1";
          $store =$db->objSelect($sql, "ROW") ; 
          if(!isset($store['retailer_id'])) continue;     
          
 //if($store['retailer_id']!=562) continue;
        if($rec['Stat']['affiliate_info1']=="") continue; 
        
        $sql="select user_id from tbl_user where user_id='".$rec['Stat']['affiliate_info1']."' and status=1";
        
          $user =$db->objSelect($sql, "ROW") ; 
          if(!isset($user['user_id'])) continue;                                   
        
          $data['user_id']=$rec['Stat']['affiliate_info1'];                                                     
          $data['retailer_id']=$store['retailer_id'];
          $data['sale_amount']=$rec['Stat']['sale_amount'];
          $data['payment_type']=$rec['Stat']['ad_id'];
          $data['earned_amount']=$rec['Stat']['approved_payout'];
          
         if($rec['Stat']['conversion_status']=="rejected") 
            $data['payment_status']="Declined";
         else
            $data['payment_status']="Pending";
          
          $revenueshare= getConfiguration('cashback_share');;
          
          
          $data['amount']= floatval(($rec['Stat']['approved_payout']*$revenueshare/100));
             
                                                                          
          $data['reference_id']=$rec['Stat']['id'];

          importRecord($data);                                                    
           
       } 
       
       
   }  
   
        
   function importRecord($data)
   {
         global $db;
         global $referral_commision;  
        
        $fields=array();
        $fields=array

('user_id','retailer_id','payment_type','sale_amount','earned_amount','amount','payment_status','exp_confirm_date','reference_id');
        
        $rec['user_id']= $data['user_id'];
        $rec['retailer_id'] = $data['retailer_id'];
        $rec['payment_type']=$data['payment_type'];
        $rec['sale_amount']=$data['sale_amount'];
        $rec['earned_amount']=$data['earned_amount'];
        $rec['amount']=$data['amount'];

        $rec['payment_status']=$data['payment_status'];                                   
        $date = new DateTime();
        $date->modify("+12 week");
        $rec['exp_confirm_date']= $date->format("Y-m-d");                                                       
        $rec['reference_id']=$data['reference_id'];               
         
            
         $cols =implode(",",$fields).',created_date'; 
         $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec))."',NOW()";
     
        $pm1_id =$db->objInsert(TABLE_NAME,$cols,$vals,"LASTID"); 
          
        if($pm1_id>0)
        {  
             $enable_cashback_message = getConfiguration('enable_cashback_message');  
            
            if($enable_cashback_message)
            {
                $cashback_message = getConfiguration('cashback_message');   
                $userinfo=getUserInfo($rec['user_id']);
                
                $bad=array('{name}','{cashback_amount}');
                $good=array($userinfo['name'],$rec['amount']);
                
                $message=str_replace($bad,$good,$cashback_message); 
               
                $mobile=$userinfo['phone'];
                sendMessage($mobile,$message);    
                
            }
            
            
            $level++;
            
            if($level<=$referral_level)
            {       
            
              $sqlref="select ref_id from tbl_user where user_id=".$rec['user_id'];
                                
              $refuser =$db->objSelect($sqlref, "ROW");
                
                
              if($refuser && $referral_commision>0)
              {  
                 addrefferalcashback($refuser['ref_id'],$rec['amount'],$pm_id,$level) ; 
              } 
        
             }         
              
        }
                      
   }        
                  
                  
  function addrefferalcashback($userid,$amount,$pm_id,$level)
    {
        global $db;
        global $msg;
        
        global $referral_level;
        global $referral_commision; 
        // Insert into Horse table
        $fields=array();
        $fields=array('user_id','retailer_id','payment_type','amount','payment_status','parent_trans_id','exp_confirm_date','reference_id');
        
        $rec['user_id']= $userid;
        $rec['retailer_id'] =0;
        $rec['payment_type']="Referal Commision";
        $rec['amount']=floatval($amount*$referral_commision/100) ;
        $rec['payment_status']=$_POST['payment_status'];
        $rec['parent_trans_id']= $pm_id;                                
        $date = new DateTime();
        $date->modify("+12 week");
        $rec['exp_confirm_date']= $date->format("Y-m-d");                                                       
          $rec['reference_id']=GenerateReferenceID();               
            
         $cols =implode(",",$fields).',created_date'; 
         $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec))."',NOW()";
     
        $pm1_id =$db->objInsert(TABLE_NAME,$cols,$vals,"LASTID"); 
           
        if($pm1_id>0)
        {  
            $level++;
            
            if($level<=$referral_level)
            {       
            
              $sqlref="select ref_id from tbl_user where user_id=".$rec['user_id'];
                                
              $refuser =$db->objSelect($sqlref, "ROW");
                
              if($refuser && $referral_commision>0)
              {  
                 addrefferalcashback($refuser['ref_id'],$rec['amount'],$pm_id,$level) ; 
              } 
        
             }         
              
        }
          
          
    }        
                               
$affiliates=array(3,5);
foreach($affiliates as $affid)
    fetchPayment($affid);
                         

?>
