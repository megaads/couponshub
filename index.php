<?php include_once 'index_field.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="google-site-verification" content="JkII0lEUb72JmF9am31fcH0nH-sXjbgyi_pb4khD5zk" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo  getConfiguration('home_page_title');  ?> </title>
<meta name="keywords" content="<?php echo  getConfiguration('home_page_meta_keyword');   ; ?>" >
<meta name="description" content="<?php echo  getConfiguration('home_page_meta_description');   ; ?>">
<link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" />
<link rel="canonical" href="<?php echo WEB_ROOT ?>"/>

<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
<link href='http://207.246.98.232:83/css/style.css' rel='stylesheet' type='text/css'>
<!--MAIN STYLE-->
<?php include 'script.php';?>
<style>

a.right {
    float: right;
}
a.right:link, a.right:visited {
    background-color: #e74f1d;
    color: white;
    padding: 10px 20px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    border-radius : 10px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}


a.right:hover, a:active {
    background-color: red;
}
.tittle {
    text-align: -webkit-auto;
}
div.img {
    border: 1px solid #ccc;
}

div.img:hover {
    border: 1px solid #777;
}

div.img img {
    width: 100%;
    height: auto;
}

div.desc {
    padding: 15px;
    text-align: center;
}

* {
    box-sizing: border-box;
}

.responsive {
    padding: 0 6px;
    float: left;
    width: 24.99999%;
}

@media only screen and (max-width: 700px){
    .responsive {
        width: 49.99999%;
        margin: 6px 0;
    }
}

@media only screen and (max-width: 500px){
    .responsive {
        width: 100%;
    }
}

.clearfix:after {
    content: "";
    display: table;
    clear: both;
}
#mobile-sec-bg2{background:rgba(0,0,0,0) url(images/how-works-bg.jpg) repeat fixed 0 0}
.store-left-fb #loginSocialMediaFBU{width:100%}
.store-left-gp #loginSocialMediaGmail{border-radius:0}
#mobile-sec-bg2{text-align:center;margin:0px 0 0 0;float:left;width:100%;}
#mobile-sec-bg2 h1{font-size:26px;color:#f45336;margin:10px 0 0;font-family:roboto}
#mobile-sec-bg2 h3{font-size:26px;color:#4396c4;margin:10px 0;font-family:roboto}
#mobile-sec-bg2 h2{font-size:18px;color:#ccc;margin:10px 0 40px}
#mobile-sec-bg2 .mobile-area-content h1{color:#333;font-size:22px}
#mobile-sec-bg2 .mobile-area-content h3{color:#fff;font-size:22px;font-weight:700}
#mobile-sec-bg2 .mobile-area-content p{color:#ccc;font-size:14px;line-height:22px}
#app-instoll-bar{background:url(images/coupons-bg.png) repeat;float:left;margin-bottom:-10px;padding:20px 0 0;width:100%}
.addtech-header{margin-top:5em;width:100%;float:left}
.addtech-header p{color:#fff;font-size:16px;font-weight:400;line-height:27px;margin-top:1em;text-align:center}
.addtech-header a img{margin:0 auto;margin-top:20px}
.viewmore {
    padding-left: 45%;
    padding-top: 50px;
    position: absolute;
}
</style>
<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Product",
  "name": "Upto 70% Off on Branded Clothing For Men",
 
  "image": "https://i1.sdlcdn.com/static/img/marketing-mailers/mailer/2016/Ankita/EOSS_WEBHEADER31-7-2016.jpg",
  "description": "Use this Offer and get Upto 70% Off on Branded Clothing For Men from Snapdeal. Products from all leading brands like Arrow, UCB are on offer. This Offer is redeemable for all customers. Offer applicable on products listed on landing page. Coupon code not required to redeem this offer. To visit landing page click on Get Offer. Hurry! Grab this coupon soon - See more at: http://207.246.98.232:83/coupon/upto-70-off-on-branded-clothing-for-men-1980431851#sthash.q3Fqj78Y.dpuf",
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "4.5",
    "reviewCount": "276",
    "bestRating": "5",
    "worstRating": "1"
  }
 
}
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TTK3LL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Page Wrap ===========================================-->

<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
     <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
 
  <!--======= BANNER =========-->
                 
  <!--======= BANNER =========-->
     
            <?php  include 'banner.php'?>   
         
    
      <!--======= TITTLE =========-->
      <section class="top-w-deal bgecf padtop_padbot">
    <div class="container"> 
    
      <!--======= TITTLE =========-->
      <div class="tittle col-md-12 col-sm-12 col-xs-12 col-lg-12 text-center">
        <h2><?php echo('Top online stores');?>
         <a href="<?php echo WEB_ROOT?>stores" class="right"><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp&nbsp View All </a>
        
        </h2>
      </div>
	  
      <ul class="row">
        
        <!--======= WEEK DEAL 1 =========-->
        <?php foreach($featuredstores as $rec):                                           
        $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
        
        ?>
        <li>
          <div class="w-deal-in"> <img class="img-responsive" src="<?php echo RETAILER_IMAGE_PATH_SHOW.$rec['logo'];?>" alt="<?php echo $rec['identifier']; ?>" height="50" alt="<?php echo $rec['identifier']; ?>">
            <p>Up to <?php echo str_replace($good,$bad,$rec['cashback'])?> Cashback &
              <?php echo $rec['total_coupon']?> More Offers</p>
            
            <!--======= HOVER DETAL =========-->
            <div class="w-over"> <a href="<?php echo WEB_ROOT?>store/<?php echo $rec['identifier']; ?>"><?php echo('Show Offers');?></a> </div>
          </div>
        </li>
        <?php endforeach;?>
        
                                           
      </ul>
    </div>
  </section>           
    <section class="top-coupons bgf3f padtop_padbot">
      <div class="container text-center">
        <div class="row cleafix">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <h2>Today's top coupons, deals & offers
 <a href="<?php echo WEB_ROOT?>coupons" class="right"><i class="fa fa-tags" aria-hidden="true"></i>&nbsp&nbsp View All </a>
</h2>
          </div>
        </div>
        <div class="row clearfix padtop70">
         <?php $t=0; foreach($coupons as $rec):
            
            if($t>=12) continue;
            
            if($t!=0 && $t%3==0)
            {
                echo " </div> ";
                echo ' <div class="row clearfix padtop70"> ';
            }
            
            
            if($rec['coupon_imageurl']!="")
            {
                $rec['coupon_image']=$rec['coupon_imageurl'];
            }
            else
            {
                 $rec['coupon_image']=COUPON_IMAGE_PATH_SHOW.(($rec['coupon_image']!="")?$rec['coupon_image']:'no-image.jpg'); 
                 $rec['coupon_image']= WEB_ROOT."SampleImage.php?src=".$rec['coupon_image']."&h=150&q=40&zc=0";        
            }
                    
               
          $rec['retailer_logo']=RETAILER_IMAGE_PATH_SHOW.(($rec['retailer_logo']!="")?$rec['retailer_logo']:'no-image.jpg');                                                                                                                     
               
              
           ?>         
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="top-coupon-box bgfff">
             <div class="top-brand-img">
              
              </div>
              <div class="coupon-img">
               <img src="<?php echo $rec['retailer_logo']?>" alt="<?php echo $rec['identifier']; ?>" width="200">
              </div>
              <div class="coupon-status text-uppercase">
                <span class="status">featured</span>
                
              </div>
              <div class="clear"></div>
              <div class="coupon-heading">
                 <a class="head" href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>"><?php echo substr($rec['name'],0,58)?></a><div style="font-weight: bold; color: green;">+upto <?php echo str_replace($good,$bad,$rec['cashback'])?> Extra Cashback</div> </a> 

              </div>
              <div class="coupon-expiry">
                Valid till <?php echo date("d/M/Y",strtotime($rec['end_date']))?> 
              </div>
              <div class="coupon-cta cta-btn mt20">
                
                <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn-nrml showcoupon"><?php echo('get coupon code');?><i class="fa fa-scissors" aria-hidden="true"></i></a>
                
              </div>
            </div>
          </div>
          
           <?php $t++; endforeach;?>       
          </div>                    
           </div>
		  
		   
    </section> 
	<section id="mobile-sec-bg2">
	<div class="container-fluid" style="padding:30px 0;background:rgba(0,0,0,0.9)">
		<div class="container no-padding">
			<div class="row">
				<div class="col-md-12 no-padding">
					<h1>Free Online Shopping Coupons &amp; Extra Cashback Deals</h1>
					<h2>Join today &amp; we&#39;ll add a Rs.50 Bonus to your account</h2>
				</div>
			</div>
			<div class="row">
								<div class="col-md-3 col-sm-6">
				
				
					<div class="star-area">
						<div class="star-area">
														<a href="http://207.246.98.232:83/coupons"><img width="123" height="123" alt="Browse Coupon Codes &amp; Deals" src="http://207.246.98.232:83/images/browse.png"></a>
													</div>
					</div>
					<div class="mobile-area-content">
						<h3>Browse</h3>
						<p>Explore couponshub for amazing coupons &amp; cashback deals across categories or type your favourite store in the search box.</p>
					</div>
				</div>
								<div class="col-md-3 col-sm-6">
				
				
					<div class="star-area">
						<div class="star-area">
														<a href="http://207.246.98.232:83/stores"><img width="123" height="123" alt="Shop using Coupon Codes &amp; Deals" src="http://207.246.98.232:83/images/cart-icon.png"></a>
													</div>
					</div>
					<div class="mobile-area-content">
						<h3>Shop</h3>
						<p>Get exclusive coupons and cashback deals &amp; continue shopping by clicking through 'Get Deal' or 'Get Code' link.</p>
					</div>
				</div>
								<div class="col-md-3 col-sm-6">
				
				
					<div class="star-area">
						<div class="star-area">
														<a href="http://207.246.98.232:83/myearning.php"><img width="123" height="123" alt="Earn Cashback using Coupon Codes &amp; Deals" src="http://207.246.98.232:83/images/cashback.png"></a>
													</div>
					</div>
					<div class="mobile-area-content">
						<h3>Earn Cashback</h3>
						<p>Once your order is completed, we track your transaction within few hours and add the cashback we earn to your account.</p>
					</div>
				</div>
								<div class="col-md-3 col-sm-6">
				
				
					<div class="star-area">
						<div class="star-area">
														<a href="http://207.246.98.232:83/myearning.php"><img width="123" height="123" alt="Fill Wallet using Coupon Codes &amp; Deals" src="http://207.246.98.232:83/images/wallet-withdraw.png"></a>
													</div>
					</div>
					<div class="mobile-area-content">
						<h3>Withdraw Wallet</h3>
						<p>Once cashback is confirmed, it can be transferred to your bank account &amp; existing e-wallets or used for mobile recharge.</p>
					</div>
				</div>
							</div>
		</div>
	</div>
</section>
 <section id="app-instoll-bar">
	<div class="container-fluid">
		<div class="container">
			<div class="row">
			<div class="col-md-12">
			<div class="col-md-1"></div>
				<div class="col-md-5 phone-img-add download-app-area-left"><img width="575" height="auto" src="http://207.246.98.232:83/images/app.png" alt="Download APP"> </div>
				
				<div class="col-md-6 download-app-area-right">
					<div class="addtech-header" style="text-align:center;">
					<p style="text-align:center;color:#333;margin:0px;padding:0px;font-weight:bold; text-transform:uppercase;font-size:30px;font-family: Rockwell;line-height:30px;">Shop Smartly..Save More.!!</p>
						<p> Download the couponshub cashback app &amp; discover the best cashback and discount offers on online stores.<br> Download Now and start Saving </p>
						<div class="app-down-btn" style="margin:0 auto;width:100%;"><a target="_blank" href="https://play.google.com/store/apps/details?id=com.motionappsworld.couponshub"><img width="180" height="auto" src="http://207.246.98.232:83/images/google-play-img.png" alt="Download the couponshub app &amp; discover the best offers on online stores">  </a>
						</div>
						
					</div>
				</div>
				
				</div>
			</div>
		</div>
	</div>
</section>       
                       
          <section class="testimonial color-white">     
      <div class="container-fluid text-center">
        <div class="row clearfix">
            <div class="col-md-12 full-width-col">
                <div class="col-md-8 col-sm-7 bg2d3 padtop_padbot height" data-mh="height">
                  <h2>What people are saying about us</h2>
                    <div id="testimonial_carousel" class="owl-carousel text-center">
                <?php $c=1;   $totrec=count($testimonials); foreach($testimonials as $rec): 
   
   ?>
                
                <div class="item">
                  <div class="author-details">
                    <div class="author-avatar">
                    
                    <?php
                 if($rec['user_image']!="")
                 {
                     if(file_exists(USER_IMAGE_PATH_UPLOAD.$rec['user_image']))
                     {
                         $rec['user_image']= USER_IMAGE_PATH_SHOW.$rec['user_image'];
                     }      
                     
                     ?> 
                      <img class="img-responsive" src="<?php echo $rec['user_image']?>" class="img-circle" height="100px" width="100px" alt="couponshub-user">                                                                    
             
                    
                    <?php
                 }
                 else
                 {
                     ?>
                       <img class="img-responsive" src="images/testimonial/1.png" alt="couponshub-user">       
                     <?php
                 } 
                 
                 ?>    
                    </div>
                    <div class="author-name">
                     <?php echo $rec['name']?>
                    </div>
                    <div class="border"></div>
                    <div class="author-content">
                     <?php echo $rec['description']?>
                    </div>
                  </div>
                </div>
              <?php $c++; endforeach; ?>   
                
                
                              </div>                 </div>
                <div class="col-md-4 col-sm-5 bg272 height" data-mh="height">
                  <div class="coupon-earn-cta">
                    New to Couponshub?<br>
                    earn cashback
                    <div class="cta mt20">
                      <a href="<?php echo WEB_ROOT?>signup" class="btn-nrml">sign up & earn</a>
                    </div>
					<img src="http://207.246.98.232:83/images/earn.png"  alt="coupons"style="height: 200px;">
                  </div>
                </div>
            </div>
        </div>
      </div>
 
    </section>
 
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?>
   </div>
     <?php include 'js.php';?>      
</body>
</html>