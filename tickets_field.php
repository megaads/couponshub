<?php 
        require_once("admin/system/constant.php"); 
        require_once("admin/system/databaseLayer.php"); 
        require_once("admin/functions/common.php"); 
        require_once("admin/functions/listfunction.php"); 
        require_once("admin/functions/commonfun.php"); 
         
        checkSiteUser();   
         
      $action = isset($_POST['txtMode']) ? $_POST['txtMode'] : ''; 
      
      switch ($action) {
        
        case 'add' :
            add();
            break;              
    } 
      
               
         
       function add()
    {
        global $db;
        global $msg;
        global $rec ;
         
        // Insert into Horse table
        $fields=array();
        $fields=array('transaction_date','retailer_id','transaction_amount','description');
            
         //exit;
        foreach($_POST as $key=>$val)
        { 
            if(in_array($key,$fields))
            {
                if(substr_count($key,"date")>0)
                {  
                  if($val!="" && $val!="mm-dd-yyyy")
                      $rec[$key] = dateFormat($val,"Y-m-d");
                  else
                          $rec[$key]="";    
                }
                else    
                {   
                  $rec[$key] = addslashes(trim($val));
                }
            }    
        } 
       $user_id=$_SESSION['USER_ID']; 
        
        
          if($_FILES['ticket_image']['name']!="")
         {   
             $allowed=array('jpg','JPG','png','PNG'); 
             $Pre="";
              $ticket_image= upload_file("ticket_image",TICKET_IMAGE_PATH_UPLOAD, MAX_FILE_SIZE, $allowed,$Pre);
             
            if($_FILES['ticket_image']['name']!="" && !$ticket_image)
            {
             $msg="imgerr";
                return;
            }    
        }      
                 
          
           $cols =implode(",",$fields).',created_date,ticket_image,user_id'; 
         
           $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec))."','".CURRENT_DATETIME."','".$ticket_image."','".$user_id."'";
            
          $ticket_id =$db->objInsert('tbl_ticket',$cols,$vals,"LASTID"); 
              
        if($ticket_id>0)
        {      
             addNotification("New Ticket Created","tickets",$ticket_id,"envelope");
             header("location: ".WEB_ROOT."tickets.php?view=list&msg=addsuc");
             exit;
        }
          
          
    }         
         
         
         
         $RecPerPage=10;  
         
         $PageNum = 1;
        if(isset($_GET['PageNum']) && $_GET['PageNum'] != "") {
             $PageNum = $_GET['PageNum'];
        }
         $offset = ($PageNum - 1) * $RecPerPage;    
        
       $cond=" user_id=".$_SESSION['USER_ID'];     
        $tickets= getMyTickets($_SESSION['USER_ID'],$offset,$RecPerPage);
        
         $query="select count(ticket_id) as totRec from tbl_ticket t left join tbl_retailer r on t.retailer_id=r.retailer_id where ".$cond."";
        $path=WEB_ROOT.'tickets.php';     
         $paging=  getAccountPagination($query,$RecPerPage,$PageNum,$path);
         
     
         $retailerlist= getRetailersMenu();    
                
              
?>