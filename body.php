<!DOCTYPE html>
<html>
<head>
  <!-- Site made with Mobirise Website Builder v3.9.2, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v3.9.2, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="assets/images/logo.png" type="image/x-icon">
  <meta name="description" content="">
  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic&amp;subset=latin">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
  <link rel="stylesheet" href="assets/bootstrap-material-design-font/css/material.css">
  <link rel="stylesheet" href="assets/et-line-font-plugin/style.css">
  <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/animate.css/animate.min.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/mobirise-gallery/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
  
  
  
</head>
<body>
<section class="mbr-section mbr-section__container article" id="header3-8" style="background-color: rgb(255, 255, 255); padding-top: 20px; padding-bottom: 20px;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="mbr-section-title display-2">Top Coupon Categories</h3>
                <small class="mbr-section-subtitle"></small>
            </div>
        </div>
    </div>
</section>

<section class="engine"><a rel="external" href="https://mobirise.com">best web site maker software</a></section><section class="mbr-gallery mbr-section mbr-section-nopadding mbr-slider-carousel" id="gallery1-1" data-filter="false" style="background-color: rgb(255, 255, 255); padding-top: 0rem; padding-bottom: 0rem;">
    <!-- Filter -->
    

    <!-- Gallery -->
    <div class="mbr-gallery-row container">
        <div class=" mbr-gallery-layout-default">
            <div>
                <div>
                    <div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p0" data-tags="Awesome" data-video-url="false">
                       
                            
                            

                           <a href="http://207.246.98.232:83" target="_blank" ><img alt="" src="assets/images/fashion-coupons-2000x1296-90-2000x1296-81.jpg"></a>
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">FASHION COUPONS</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p0" data-tags="Responsive" data-video-url="false">
                        <div href="#lb-gallery1-1" data-slide-to="1" data-toggle="modal">
                            
                            

                            <img alt="" src="assets/images/electronics-2000x1296-5-2000x1296-65.jpg">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">ELECTRONICS COUPONS</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p0" data-tags="Creative" data-video-url="false">
                        <div href="#lb-gallery1-1" data-slide-to="2" data-toggle="modal">
                            
                            

                            <img alt="" src="assets/images/recharge-coupons-2000x1296-53-2000x1296-51.jpg">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">RECHARGE COUPONS</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p0" data-tags="Animated" data-video-url="false">
                        <div href="#lb-gallery1-1" data-slide-to="3" data-toggle="modal">
                            
                            

                            <img alt="" src="assets/images/travel-coupons-2000x1296-59-2000x1296-59.jpg">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">TRAVEL COUPONS</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p0" data-tags="Awesome" data-video-url="false">
                        <div href="#lb-gallery1-1" data-slide-to="4" data-toggle="modal">
                            
                            

                            <img alt="" src="assets/images/homedecor-2000x1296-40-2000x1296-58.jpg">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">HOME &amp; DECOR COUPONS</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p0" data-tags="Beautiful" data-video-url="false">
                        <div href="#lb-gallery1-1" data-slide-to="5" data-toggle="modal">
                            
                            

                            <img alt="" src="assets/images/food-coupons-2000x1296-55-2000x1296-74.jpg">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">FOOD &amp; BEVERAGES COUPONS</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p0" data-tags="Responsive" data-video-url="false">
                        <div href="#lb-gallery1-1" data-slide-to="6" data-toggle="modal">
                            
                            

                            <img alt="" src="assets/images/beauty-coupons-2000x1296-18-2000x1296-76.jpg">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">BEAUTY &amp; PERSONAL CARE COUPONS</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p0" data-tags="Animated" data-video-url="false">
                        <div href="#lb-gallery1-1" data-slide-to="7" data-toggle="modal">
                            
                            

                            <img alt="" src="assets/images/giftscoupons-2000x1296-43-2000x1296-56.jpg">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">GIFTS &amp; OTHERS COUPONS</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- Lightbox -->
    
</section>

<section class="mbr-cards mbr-section mbr-section-nopadding" id="features7-2" style="background-color: rgb(239, 239, 239);">

    

    <div class="mbr-cards-row row">
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img iconbox"><a href="http://207.246.98.232:83" target="_blank" class="mbri-desctop mbr-iconfont mbr-iconfont-features7" style="color: rgb(255, 255, 255);"></a></div>
                    <div class="card-block">
                        <h4 class="card-title">Join/Browse</h4>
                        
                        <p class="card-text">Logon to couponshub and find best deals and offers from you favourite stores.</p>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img iconbox"><a href="http://207.246.98.232:83" target="_blank" class="mbri-cart-add mbr-iconfont mbr-iconfont-features7" style="color: rgb(255, 255, 255);"></a></div>
                    <div class="card-block">
                        <h4 class="card-title">Shop</h4>
                        
                        <p class="card-text">Click on the link provided to visit your favourite stores and shop normally<br>and apply coupon code at checkout.</p>
                        
                    </div>
                </div>
          </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img iconbox"><a href="http://207.246.98.232:83" target="_blank" class="mbri-cash mbr-iconfont mbr-iconfont-features7" style="color: rgb(255, 255, 255);"></a></div>
                    <div class="card-block">
                        <h4 class="card-title">Earn</h4>
                        
                        <p class="card-text">Give us some time till we track yur purchase and transferthe cashback to your couponshub account.</p>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img iconbox"><a href="http://207.246.98.232:83" target="_blank" class="mbri-gift mbr-iconfont mbr-iconfont-features7" style="color: rgb(255, 255, 255);"></a></div>
                    <div class="card-block">
                        <h4 class="card-title">Withdraw</h4>
                        
                        <p class="card-text">Withdraw your cashback into wallets,banks or use as a mobile recharge.</p>
                        
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
</section>

<section class="mbr-info mbr-info-extra mbr-section mbr-section-md-padding mbr-parallax-background" id="msg-box1-3" style="background-image: url(assets/images/the-life-of-a-2000x1046-24.png); padding-top: 90px; padding-bottom: 90px;">

    
    <div class="container">
        <div class="row">


            


            <div class="mbr-table-md-up">
                <div class="mbr-table-cell mbr-right-padding-md-up col-md-8 text-xs-center text-md-left">
                    <h2 class="mbr-info-subtitle mbr-section-subtitle"></h2>
                    <h3 class="mbr-info-title mbr-section-title display-2"></h3>
                </div>

                <div class="mbr-table-cell col-md-4">
                    <div class="text-xs-center"><a class="btn btn-primary" href="https://mobirise.com">Download Now</a></div>
                </div>
            </div>


        </div>
    </div>
</section>

<section class="mbr-section" id="msg-box4-5" style="background-color: rgb(242, 242, 242); padding-top: 120px; padding-bottom: 120px;">

    
    <div class="container">
        <div class="row">
            <div class="mbr-table-md-up">

              <div class="mbr-table-cell mbr-right-padding-md-up mbr-valign-top col-md-7">
                  <div class="mbr-figure"><iframe class="mbr-embedded-video" src="https://www.youtube.com/embed/bEw_u7jfnVs?rel=0&amp;amp;showinfo=0&amp;autoplay=0&amp;loop=0" width="1280" height="720" frameborder="0" allowfullscreen></iframe></div>
              </div>

              


              <div class="mbr-table-cell col-md-5 text-xs-center text-md-left">
                  <h3 class="mbr-section-title display-2">Couponshub <br>Intro Video</h3>
                  <div class="lead">

                    <p></p><p>CouponsHub aims to provide our visitors and members the latest coupon codes promotional codes from leading retailers/ merchants in INDIA. The CouponsHub team works with 1000s of retailers to collate the latest offers that can save you some money. SAVE BIG WITH COUPONSHUB Coupon codes are very easy and simple to use.</p><p></p>

                  </div>

                  
              </div>


              

            </div>
        </div>
    </div>

</section>


  <script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/SmoothScroll.js"></script>
  <script src="assets/viewportChecker/jquery.viewportchecker.js"></script>
  <script src="assets/masonry/masonry.pkgd.min.js"></script>
  <script src="assets/imagesloaded/imagesloaded.pkgd.min.js"></script>
  <script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="assets/jarallax/jarallax.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/mobirise-gallery/player.min.js"></script>
  <script src="assets/mobirise-gallery/script.js"></script>
  
  
  
  </body>
</html>