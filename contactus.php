<?php include_once 'contactus_field.php';?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('Contact Us');?> - <?php echo SITE_NAME;?></title>
           
<link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" /> 

<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
   <?php include 'script.php';?>  

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>
<div class="se-pre-con"></div>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="sign-up">
    <div class="container">
     <?php include_once 'breadcrumb.php';?>
      <div class="row">
        <div class="col-sm-6">
          <h4><?php echo('Contact Us');?> 
         </h4>
          <img class="img-responsive" src="<?php echo WEB_ROOT;?>images/contact3.jpg" alt="" width="300" > </div>
          <?php echo getMessage($msg);?>
        <!--======= SIGN UP FORM =========-->
        <div class="col-sm-6">
          <form method="post" name="frm" id="userfrm">
          <input type="hidden" name="txtMode" value="contact" >
          
            <ul class="row">
              <li class="col-md-6">
                <div class="form-group">
                  <label for=""><?php echo('Name');?> *
                    <input type="text" class="form-control" id="" placeholder="" name="name">
                  </label>
                </div>
              </li>
                    
               <li class="col-md-6">
                <div class="form-group">
                  <label for=""><?php echo('Phone#');?> *
                    <input type="text" class="form-control" id="" placeholder="" name="phone">
                  </label>
                </div>
              </li>
              <li class="col-md-6">
                <div class="form-group">
                  <label for=""><?php echo('E-mail Address');?> *
                    <input type="email" class="form-control" id="" placeholder="" name="email">
                  </label>
                </div>
              </li>
               <li class="col-md-6">
                <div class="form-group">
                  <label for=""><?php echo('Message');?> *
                    <textarea cols="" rows="" class="form-control" name="message"></textarea>
                  </label>
                </div>
              </li>
                  
              <li class="col-md-6">
                <button type="submit" class="btn"><?php echo('Send');?></button>
              </li>
            </ul>
          </form>
             
        </div>
      </div>
    </div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
   <?php include 'js.php';?>      
<script src="<?php echo WEB_ROOT ?>js/jquery.validate.js"></script>  
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#userfrm").validate({
                 rules: {
                name: "required",                  
                  phone: "required", 
                   message: "required",                
                email: {
                    required: true,
                    email: true
                },
                     
                
            },
                         
            
        });                                     
          
    });
    </script>
</body>
</html>
