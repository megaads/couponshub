<style>
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
</style>


<?php include_once 'user_field.php';
$crumbs[]= __('Refer and Earn');

  
  $RecPerPage=10;  
         
 $PageNum = 1;
if(isset($_GET['PageNum']) && $_GET['PageNum'] != "") {
     $PageNum = $_GET['PageNum'];
}
 $offset = ($PageNum - 1) * $RecPerPage;    

$cond=" ref_id=".$_SESSION['USER_ID'];     
$referedfriendslist= getReferredFriends($_SESSION['USER_ID'],$offset,$RecPerPage);

 $query="select count(user_id) as totRec from tbl_user where ".$cond."";
$path=WEB_ROOT.'refernearn.php';     
 $paging=  getAccountPagination($query,$RecPerPage,$PageNum,$path);




?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('Refer and Earn');?> - <?php echo SITE_NAME;?></title>
 

<!-- FONTS ONLINE -->
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
      <?php include 'script.php';?>  
         

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
  
</head>
<body>
<div class="se-pre-con"></div>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="myaccount">
     <div class="container">
      <?php include_once 'breadcrumb.php';?>
     <div class="ui-tabs">   
    <div id="tabs-1" class="ui-tabs-panel" style="width:100%;">
        <h4><?php echo('Refer your Friends ');?> and get <?php echo getConfiguration('referral_commision');?>Rs. FREE CASHBACK!    </h4>   
           <?php   $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg'];
             echo getMessage($msgcode);?>
          <div class="referandearn">
    <!---  <img src="images/refer-and-earn.png" width="630" height="185"> ---> 
          <form method="post" name="frm" enctype="multipart/form-data" >
          <input type="hidden" name="txtMode" value="changepwd" >
           
            <ul class="row">
              <li class="col-md-6" style="float: none;">
                <div class="form-group">
                  <label for=""><?php echo('Your unique referral link');?>: *
                    <input type="text" class="form-control" size="40" id="" value="<?php echo WEB_ROOT.'signup/?ref='.$_SESSION['USER_ID'];?>"  placeholder="" name="referallink">
                  </label>
                </div>
              </li>
                      
           </ul>
          </form> 
<button id="myBtn">Read details of refer and earn</button>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">Close</span>
    <h3>Conditions </h3>
    <p>Refer a Friend you'll get 25rs and your friend wil get 50rs signup bonus.</p>
<p>Your referal amount will be credited as a pending amount.</p>
<p>Once your friend starts earning of couponshub shopping bonus you're pending referal amount wil be credited into your main balance.</p>
<p>Your referal friend should earn minimum amount of shopping bonus Rs 25 as confirmed to make available of your pending bonus. </p>
<p>Note** :If your friend signup with facebook or google the referal amount will not be credited </p>
  </div> 
       </div>
       <div class="refer-socialmedia" style="border-bottom:none; padding-bottom:10px;">
       <h4><?php echo('Invite Via Social Media');?></h4>
       
       <a href="javascript:;" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php echo WEB_ROOT.'signup.php?ref='.$_SESSION['USER_ID'];?>','Facebook Share','width=600')"><img src="<?php echo WEB_ROOT?>images/facebook.jpg" /></a>
       <?php
       $text=urlencode("I have Joined ".SITE_NAME." and earned cashback");
       ?>
       <a href="javascript:;" onclick="window.open('http://twitter.com/share?text=<?php echo $text; ?>&url=<?php echo WEB_ROOT.'signup.php?ref='.$_SESSION['USER_ID'];?>','Twitter Share','width=600')"><img src="<?php echo WEB_ROOT?>images/twitter.jpg" /></a>
       
       </div>                    
     <div class="earn-point">
     
      <h4><?php echo('My Referral Network');?></h4>    
<div class="table-responsive">
                  <table class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th><?php echo('Date Joined');?></th>
                        <th><?php echo('Referral Name');?></th>
                        <th><?php echo('Referral Account Status');?></th>                                    
                      </tr>
                    </thead>
                   <tbody>
                   <?php foreach($referedfriendslist as $friend):?>
                   <tr>
                   <td><?php echo date('M d,Y',strtotime($friend['created_date']))?></td>
                   <td><?php echo $friend['name'];?></td>
                   <td><?php echo ($friend['verified']==1)?"Active":"InActive";?></td>             
                   </tr>       
                   <?php endforeach;?>                       
                   </tbody>       
                  </table>
                    <ul class="pagination">
            <?php echo $paging;?>
             
        </ul>
                </div>
     
     </div>   
      
        
        
        
   
        
</div>
  

     
     
     </div>
  </section>
  <script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
    <?php include 'js.php';?>      
</body>
</html>
