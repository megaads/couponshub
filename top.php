<script>
window.fbAsyncInit = function() {
    // FB JavaScript SDK configuration and setup
    FB.init({
      appId      : '<?php echo $facebookappId;?>', // FB App ID
      cookie     : true,  // enable cookies to allow the server to access the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });
    
    // Check whether the user already logged in
   /* FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            //display user data
            getFbUserData();
        }
    });     */
};

// Load the JavaScript SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Facebook login with JavaScript SDK
function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getFbUserData();
        } else {
           // document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, {scope: 'email'});
}

// Fetch the user profile data from facebook
function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function (response) {
        saveUserData(response);
       // document.getElementById('fbLink').setAttribute("onclick","fbLogout()");
     //   document.getElementById('fbLink').innerHTML = 'Logout from Facebook';
      //  document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.first_name + '!';
     //   document.getElementById('userData').innerHTML = '<p><b>FB ID:</b> '+response.id+'</p><p><b>Name:</b> '+response.first_name+' '+response.last_name+'</p><p><b>Email:</b> '+response.email+'</p><p><b>Gender:</b> '+response.gender+'</p><p><b>Locale:</b> '+response.locale+'</p><p><b>Picture:</b> <img src="'+response.picture.data.url+'"/></p><p><b>FB Profile:</b> <a target="_blank" href="'+response.link+'">click to view profile</a></p>';
    });
}

function saveUserData(response){
     $('.fb-sign').html("<img src='"+base_url+"images/loading.gif'> "); 
    $.post(base_url+'signup.php', {id:response.id,given_name:response.first_name+" "+response.last_name,email:response.email,picture:response.picture.data.url,outh_provider:'facebook',newsletter:1,txtMode:'fblogin','ajax':1}, function(result){
       if(result=="Login Success")                                    
           window.location.reload();
       
    });
        
    }
             
    
</script>  
<?php 

 setcookie("ref_id",$_GET['ref'], time()+43200);  
 
  $facebook_login=getConfiguration('facebook_login'); 
  $google_login=getConfiguration('google_login');     
                                                  
 ?>
 

 <div id="loginModal" class="modal fade in">
      <div class="modal-dialog">
        <div class="modal-content bgefe">
            <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
                <h4 class="modal-title">Log in to Couponshub</h4>
            </div>
            <div class="modal-body" id="login">
              <div class="row">
                <div class="col-md-5 col-sm-5 col-xs-12">
                  <div class="sign-in-area">
                    <span>Existing Users, Login</span>
                    
                    <?php if($facebook_login=="On" || $google_login=="On"): ?>   
                     <div class="social-sign-btn"> 
                            <?php if($facebook_login=="On"): ?>  
                          <div class="fb-sign">
                       <a href="javascript:;" onclick="fbLogin()"><i class="fa fa-facebook"></i>          
                          <span class="social-login-title">Facebook</span>  </a>
                      </div>           
                              <?php endif; ?>    
                            <?php if($google_login=="On"): ?>   
                          <div class="google-sign">
                       <a href="<?php echo $authUrl;?>">  <i class="fa fa-google-plus"></i>
                          <span class="social-login-title">Google</span>  </a> 
                      </div>
 
                             <?php endif; ?>
                      </div>            
                                 
                         <?php endif; ?>      
                                         
                  </div>
                </div>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <form class="modalform" id="frmlogin" method="POST" action="<?=$_SERVER['PHP_SELF'];?>">     
                 
                    <input type="text"   name="username" placeholder="Email" value="<?php echo (isset($_COOKIE['username']))?$_COOKIE['username']:"";?>" id="username" />
                    <input type="password"  placeholder="Password" name="password" id="password" value="<?php echo (isset($_COOKIE['password']))?$_COOKIE['password']:"";?>" />     
                   <input id="rememberme" type="checkbox" <?php echo (isset($_COOKIE['remember']) && $_COOKIE['remember']==1)?"checked":""?> value="1"   />       
                                Remember login 
                    <div class="forgot-password"> <a href="<?php echo WEB_ROOT; ?>forgotpassword.php" class="forgot-pass">Forgot Password?</a></div>
                 <button type="button" onclick="login()" >Login</button>  
				 
				  
                  </form>
                </div>
              </div> 
                <div class="cashback-link">Or <a id="cashback-link" href="#"><?php //echo('Continue Without Cashback');?></a></div> 
            </div>
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dalog -->
    </div>  
 
 
 <div id="signupModal" class="modal fade in">
      <div class="modal-dialog">
        <div class="modal-content bgefe">
            <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
                <h4 class="modal-title">Join in Couponshub</h4>
            </div>
            <div class="modal-body" id="login">
              <div class="row">
                <div class="col-md-5 col-sm-5 col-xs-12">
                  <div class="sign-in-area">
                    <span>Existing Users, Login</span>
                    
                    <?php if($facebook_login=="On" || $google_login=="On"): ?>   
                     <div class="social-sign-btn"> 
                            <?php if($facebook_login=="On"): ?>  
                          <div class="fb-sign">
                      <a href="javascript:;" onclick="fbLogin()"><i class="fa fa-facebook"></i>          
                          <span class="social-login-title">Facebook</span>  </a>
                      </div>           
                              <?php endif; ?>    
                            <?php if($google_login=="On"): ?>   
                          <div class="google-sign">
                       <a href="<?php echo $authUrl;?>">  <i class="fa fa-google-plus"></i>
                          <span class="social-login-title">Google</span>  </a> 
                      </div>
 
                             <?php endif; ?>
                      </div>            
                                 
                         <?php endif; ?>      
                                         
                  </div>
                </div>
                <div class="col-md-7 col-sm-7 col-xs-12">
 <div class="errormsgreg"></div>  
                   <form id="frmreg" class="modalform">
                 
                      <input type="text"  placeholder="Full Name" name="name"  id="name"/> 
                       <input type="text"  placeholder="Email" name="email" id="email" /> 
                       <?php if(ENABLE_REF_CODE): ?>  
                    
                        <input type="text" name="ref_code"  id="ref_code" placeholder="Referal Code(If any )"/>
                        
                        <?php endif;?>   
                     
                    <input type="text" name="phone" placeholder="Mobile Number" maxlength="10" id="phone" <?php if($enable_otp): ?> onchange="sendCode()" <?php endif;?>/>
                   
                   <?php if($enable_otp): ?>       
                      <a id="resendotp" style="display: none" href="javascript:;" onclick="sendCode()">(Resend OTP)</a> 
                    <input type="text" placeholder="OTP" name="verification_code" id="verification_code" />
                    
                    <?php endif;?>
                    
                    <input type="password" placeholder="Password" name="password" id="password" />
                        
                    
                   <button type="button" onclick="register()" >Register</button>  
                  </form> 
                </div>
              </div> 
                
            </div>
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dalog -->
    </div>
 
 
   <!-- /.modal -->                                                   
 
 
       
<!-- Show Coupon popup-->
<div id="couponbox" class="popupContainercoupon" style="display: block;position: fixed;z-index: 11000;/* left: 50%; */margin-left: -90px;top: 100px;opacity: 1;">                
</div>



		            



    
    
    
    
    
<script type="text/javascript">
var base_url = "<?php echo WEB_ROOT; ?>";

   function login(){
          
        username=$('#frmlogin #username').val();
        password=$('#frmlogin #password').val(); 
         
        if($('#rememberme').is(":checked"))
        {
            remember=1;
        }
        else
            remember=0;
        
        if(!isEmail(username))
        {
            
            $('#frmlogin #username').val('');
            $('#frmlogin #username').attr("placeholder","Please enter valid email address");
             $('#frmlogin #username').addClass('errorbox')                                                     
             return;
        }
        else
        {
           $('#frmlogin #username').attr("placeholder","");
           $('#frmlogin #username').removeClass('errorbox')                                                      
        }
         
        if(password=="")
        {
           $('#frmlogin #password').val('');
           $('#frmlogin #password').attr("placeholder","Please enter password");
           $('#frmlogin #password').addClass('errorbox');
            return;          
        }
        else
        {
           $('#frmlogin #password').attr("placeholder","");
           $('#frmlogin #password').removeClass('errorbox')                                                      
        }
                                        
                                                        
       $.post(base_url+'login.php', {username:username,password: password,remember:remember,txtMode:'login','ajax':1}, function(result){
       if(result=="Login Success")  
       {
         if(($('.cashback-link').is(":visible"))) {
             url= $('#cashback-link').attr('href');                                          
                   
             location.reload();
             
         }
         else
         {
             window.location=base_url;      
         }
         
          
       }    
       else
          $('.errormsglogin').html(result);
                                                
         
    });      
} 

 function register(){
        name=$('#frmreg > #name').val();
         email=$('#frmreg > #email').val();
        password=$('#frmreg > #password').val(); 
        phone=$('#frmreg > #phone').val();
        if($('#frmreg > #verification_code'))
          verification_code=$('#frmreg > #verification_code').val();
        else
           verification_code="";  
        if($('#send_updates').is(":checked"))
        {
            newsletter=1;
        }
        else
            newsletter=0;
            
            
        if($.trim(name)=="")
        {
           $('#frmreg > #name').val('');
           $('#frmreg > #name').attr("placeholder","Please enter name");
           $('#frmreg > #name').addClass('errorbox');
            return;          
        }
        else
        {
           $('#frmreg > #name').attr("placeholder","");
           $('#frmreg > #name').removeClass('errorbox')                                                      
        }      
            
            
        if(!isEmail(email))
        {
            $('#frmreg > #email').val('');
            $('#frmreg > #email').attr("placeholder","Please enter valid email address");
             $('#frmreg > #email').addClass('errorbox')                                                     
             return;
        }
        else
        {
           $('#frmreg > #email').attr("placeholder","");
           $('#frmreg > #email').removeClass('errorbox')                                                      
        }
        
        if($.trim(password)=="")
        {
           $('#frmreg > #password').val('');
           $('#frmreg > #password').attr("placeholder","Please enter password");
           $('#frmreg > #password').addClass('errorbox');
            return;          
        }
        else
        {
           $('#frmreg > #password').attr("placeholder","");
           $('#frmreg > #password').removeClass('errorbox')                                                      
        }                
        
        if(password.length<6)
        {
           $('#frmreg > #password').val('');
           $('#frmreg > #password').attr("placeholder","Password must be atleast 6 characters");
           $('#frmreg > #password').addClass('errorbox');
            return;          
        }
        else
        {
           $('#frmreg > #password').attr("placeholder","");
           $('#frmreg > #password').removeClass('errorbox')                                                      
        }                
        
        
                                        
                                                     
       $.post(base_url+'signup.php', {name:name,email:email,password:password,newsletter:newsletter,phone:phone,verification_code:verification_code,txtMode:'add','ajax':1}, function(result){
       if(result=="Registration Success")                                    
           window.location=base_url+'thanks.php';
       else
       {
           
           if(result=="duperr")
           var msg="Email already Exist";
           
           if(result=="invalidotp")
           var msg="Invalid OTP";
           
           
          $('.errormsgreg').html(msg); 
       }
          
                                                
         
    });
 }
 
 
 function sendCode()
 {
     phone=$('#frmreg > #phone').val();
      if($.trim(phone)=="")
        {
           $('#frmreg > #phone').val('');
           $('#frmreg > #phone').attr("placeholder","Please enter mobile number");
           $('#frmreg > #phone').addClass('errorbox');
            return;          
        }
        else
        {
           $('#frmreg > #phone').attr("placeholder","");
           $('#frmreg > #phone').removeClass('errorbox')                                                      
        }                
        
        if(!$.isNumeric(phone) || phone.length<10)
        {
           $('#frmreg > #phone').val('');
           $('#frmreg > #phone').attr("placeholder","Please enter valid mobile number");
           $('#frmreg > #phone').addClass('errorbox');
            return;          
        }
        else
        {
           $('#frmreg > #phone').attr("placeholder","");
           $('#frmreg > #phone').removeClass('errorbox')                                                      
        }                
                   
     
      $.post(base_url+'signup.php', {mobile:phone,txtMode:'sendcode','ajax':1}, function(result){
        
          if(result=="dupmobile")
          {
             var msg="Mobile number already exist";
           $('#frmreg > #phone').val('');
           $('#frmreg > #phone').attr("placeholder",msg);
           $('#frmreg > #phone').addClass('errorbox');
              
          }
          else      
             $('#resendotp').show();         
         
    });
     
     
 }
 
 
    function showCoupon(coupon_id){
        
       $.post(base_url+'showcoupon.php?cid='+coupon_id, {'ajax':1}, function(result){
            
          $('#couponbox').html(result);
                                                
         
    });      
    } 
    
    
    function showCouponbyUrl(url){
         
       $.post(url, {'ajax':1}, function(result){
           
          $('#model').html(result);
                                                
         
    });      
    } 
 
 
 

</script>
