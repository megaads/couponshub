<?php include_once 'category_field.php';
?>  


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="google-site-verification" content="JkII0lEUb72JmF9am31fcH0nH-sXjbgyi_pb4khD5zk" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo  getConfiguration('home_page_title');  ?> </title>
<meta name="keywords" content="<?php echo  getConfiguration('home_page_meta_keyword');   ; ?>" >
<meta name="description" content="<?php echo  getConfiguration('home_page_meta_description');   ; ?>">
<link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" />
<link rel="canonical" href="<?php echo WEB_ROOT ?>"/>

<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
<?php include 'script.php';?>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TTK3LL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- Page Wrap ===========================================-->

  
  <!--======= TOP BAR =========-->
             <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
 <div class="section" id="view_more_page">
        <div class="container">
            <div class="row">
               <div class="col-md-12 panel panel-default">
                    <div class="panel-heading">
                    <h4>ALL CATEGORIES &amp; SUB-CATEGORIES</h4>
                   </div>
                   <div class="panel-body">
                  <!-- <div class="row row_size">
                        <div class="col-md-2"><a href="#">Fashion:</a></div>
                        <div class="col-md-2"><a href="#">Food &amp; Dining:</a></div>
                        <div class="col-md-2"><a href="#">Travel:</a></div>
                        <div class="col-md-2"><a href="#">Mobiles &amp; Tablets:</a></div>
                        <div class="col-md-2"><a href="#">Beauty &amp; Health:</a></div>
                        <div class="col-md-2"><a href="#">Computers, Laptops &amp; Gaming:</a></div>
                       </div>
                       <div class="row row_size">
                        <div class="col-md-2"><a href="#">Fashion:</a></div>
                        <div class="col-md-2"><a href="#">Food &amp; Dining:</a></div>
                        <div class="col-md-2"><a href="#">Travel:</a></div>
                        <div class="col-md-2"><a href="#">Mobiles &amp; Tablets:</a></div>
                        <div class="col-md-2"><a href="#">Beauty &amp; Health:</a></div>
                        <div class="col-md-2"><a href="#">Computers, Laptops &amp; Gaming:</a></div>
                       </div>
                       <div class="row row_size">
                        <div class="col-md-2"><a href="#">Fashion:</a></div>
                        <div class="col-md-2"><a href="#">Food &amp; Dining:</a></div>
                        <div class="col-md-2"><a href="#">Travel:</a></div>
                        <div class="col-md-2"><a href="#">Mobiles &amp; Tablets:</a></div>
                        <div class="col-md-2"><a href="#">Beauty &amp; Health:</a></div>
                        <div class="col-md-2"><a href="#">Computers, Laptops &amp; Gaming:</a></div>
                       </div>
                        <div class="row row_size">
                        <div class="col-md-2"><a href="#">Fashion:</a></div>
                        <div class="col-md-2"><a href="#">Food &amp; Dining:</a></div>
                        <div class="col-md-2"><a href="#">Travel:</a></div>
                        <div class="col-md-2"><a href="#">Mobiles &amp; Tablets:</a></div>
                        <div class="col-md-2"><a href="#">Beauty &amp; Health:</a></div>
                        <div class="col-md-2"><a href="#">Computers, Laptops &amp; Gaming:</a></div>
                       </div>
                        <div class="row row_size">
                        <div class="col-md-2"><a href="#">Fashion:</a></div>
                        <div class="col-md-2"><a href="#">Food &amp; Dining:</a></div>
                        <div class="col-md-2"><a href="#">Travel:</a></div>
                        <div class="col-md-2"><a href="#">Mobiles &amp; Tablets:</a></div>
                        <div class="col-md-2"><a href="#">Beauty &amp; Health:</a></div>
                        <div class="col-md-2"><a href="#">Computers, Laptops &amp; Gaming:</a></div>
                       </div>-->
                 <?php 
    print_r($allCat); ?>
                   </div>
                </div>
            </div>
        </div>
    </div>
      
  <input type="hidden" name="pagenumber" id="pagenumber" value="2"  />
  <input type="hidden" name="total_page" id="total_page" value="<?php echo $totalPage;?>"  />         
  <!--======= SIGN UP =========-->
    
     
  
        
        <!--======= FOOTER =========-->
  <?php include 'footer.php';?> 
             <?php include 'js.php';?>  
                              
  <script type="text/javascript">
var url="<?php echo WEB_ROOT."getcategorycoupons.php"?>";  
var docroot="<?php echo DOC_ROOT."images/"?>";
var cat="<?php echo $catId;?>"; 
   function lastmsg_funtion(pagenumber){
    $('#pagination').show();                                              
    $.post(url, {catid:cat,PageNum: pagenumber}, function(result){
    $('#pagination').hide();                                              
      $('#couponlist').append(result);
      $(".btncashback").leanModal({top : 100, overlay : 0.6, closeButton: ".modal_close" });  
       $(".showcoupon").leanModal({top :100 , overlay : 0.6, closeButton: ".modal_close" }); 
    });
} 
     

</script>                                          



