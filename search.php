<?php include_once 'search_field.php';?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('Search');?> - <?php echo SITE_NAME;?></title>
   

<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" /> 
<!--MAIN STYLE-->
<?php include 'script.php';?>

<link rel="stylesheet" href="offers_styles/font.css" />
    <link rel="stylesheet" href="offers_styles/font-awesome.css" />
    <link rel="stylesheet" href="offers_styles/normalize.css" />
    <!--css plugin-->
    <link rel="stylesheet" href="offers_styles/flexslider.css" />
    <link rel="stylesheet" href="offers_styles/jquery.nouislider.css"/>
    <link rel="stylesheet" href="offers_styles/jquery.popupcommon.css" />

    <link rel="stylesheet" href="offers_styles/style.css"/>
    
    

    <link rel="stylesheet" href="offers_styles/res-menu.css" />
    <link rel="stylesheet" href="offers_styles/responsive.css"/>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
           <style>
           .storeheader {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #ddd;
    box-shadow: 0 0 7px #cacaca;
    float: left;
    padding: 10px;
    width: 100%;
    margin-bottom: 20px;
}
           </style>
</head>
<body>
<div class="se-pre-con"></div>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
     
  <section class="top-w-deal">  
    <div class="container"> 
      <?php include_once 'breadcrumb.php';?>
       <span style="font-size: 24px;">Search results for '<?php echo $_GET['q'];?>'</span>   
      <!--======= TITTLE =========-->
          
      <ul class="row">
        
        <!--======= WEEK DEAL 1 =========-->
        <?php foreach($storeslist as $rec):                                           
        $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
        
        ?>
        <li>
          <div class="w-deal-in"> <img class="img-responsive" src="<?php echo RETAILER_IMAGE_PATH_SHOW.$rec['logo'];?>" alt="" height="50" >
            <p>Up to <?php echo str_replace($good,$bad,$rec['cashback'])?><?php echo str_replace($good,$bad,$rec['fixed_percent'])?> Cashback &
              <?php echo $rec['total_coupon']?> More Offers</p>
            
            <!--======= HOVER DETAL =========-->
            <div class="w-over"> <a href="<?php echo WEB_ROOT?>store/<?php echo $rec['identifier']; ?>"><?php echo('Show Offers');?></a> </div>
          </div>
        </li>
        <?php endforeach;?>
        
                                           
      </ul>
    </div>
  </section>                  
  <!--======= SIGN UP =========-->
  <section class="great-deals">
    <div class="container-page" style="background: white;">
<div class="mp-pusher" id="mp-pusher">

                          
      <div class="grid_frame page-content">

            <div class="container_grid">
<?php include_once 'breadcrumb.php';?>   
             
                   
                  
        
                <div class="mod-grp-coupon block clearfix">
                    <div class="grid_12">
                        <h3 class="title-block has-link">
                            COUPONS 
                            
                        </h3>
                    </div>
                    <div class="block-content list-coupon clearfix">

          <ul class="row" id="couponslist">
          <!--======= COUPEN DEALS =========-->
          <?php foreach($coupons as $rec): 
 $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
            if($rec['coupon_imageurl']!="")
            {
                $rec['coupon_image']=$rec['coupon_imageurl'];
            }
           else
            {
                 $rec['coupon_image']=COUPON_IMAGE_PATH_SHOW.(($rec['coupon_image']!="")?$rec['coupon_image']:'no-image.jpg');
                   $rec['coupon_image']= WEB_ROOT."SampleImage.php?src=".$rec['coupon_image']."&h=143&q=40&zc=0";           
            }
                
               $rec['retailer_logo']=RETAILER_IMAGE_PATH_SHOW.(($rec['retailer_logo']!="")?$rec['retailer_logo']:'no-image.jpg');                                                                                                                     
                       
            
           
            
          ?>
          <li><div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                  <a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>" class="ver_container"><img class="img-responsive" src="<?php echo $rec['coupon_image']?>" alt="<?php echo $rec['identifier']; ?>" height="143px" width="143px"   ></a>
           <?php if(strtotime(TODAY)==strtotime($rec['end_date'])): ?> 
              <div class="top-tag">                          
              <span class="ribn-pur"><span><?php echo('Expiring Today');?></span></span></div>
              <?php elseif(strtotime(TODAY)>strtotime($rec['end_date'])): ?>
              <div class="top-tag">                          
              <span class="ribn-red"><span><?php echo('Expired');?></span></span></div>
              
              <?php endif;?>
              
              <div class="c-img text-center"> 
              <div class="top-brand-img text-center">
              
              </div>
               
               <div class="coupon-desc"> <a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>"><?php echo substr($rec['name'],0,58)?><div style="font-weight: bold; color: green;">+upto <?php echo str_replace($good,$bad,$rec['cashback'])?> Extra Cashback</div></a></div>

                <div class="time-left"><?php echo('Expiry Date');?> : <?php echo date("M d, Y",strtotime($rec['end_date']))?></div>
                     
                <div class="text-center" style="padding-bottom: 15px;"> 
                  
                <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn showcoupon" ><?php echo('Get Cashback');?></a>
                
                </div>
              </div>
                
            </div>
            <i class="stick-lbl hot-sale"><img class="img-responsive" src="<?php echo $rec['retailer_logo']?>" height="20" alt="Coupons and Cashback discounts"   ></i>
          </li>
         <?php endforeach;?> 
              
           </ul>       
        
        <!--======= PAGINATION =========-->
        <ul class="pagination">
            <?php echo $paging;?>
             
        </ul>
      </div>
      
            </div>
  </section> 
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
  <?php include 'js.php';?>      
</body>
</html>