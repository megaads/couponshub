<?php include_once 'user_field.php';
$crumbs[]= __('Contact List');

  checkSiteUser();

  $RecPerPage=10;  
         
 $PageNum = 1;
if(isset($_GET['PageNum']) && $_GET['PageNum'] != "") {
     $PageNum = $_GET['PageNum'];
}
 $offset = ($PageNum - 1) * $RecPerPage;    

$cond=" user_id=".$_SESSION['USER_ID'];     
$contactslist= getContactList($_SESSION['USER_ID'],$offset,$RecPerPage);

 $query="select count(contact_id) as totRec from tbl_contactlist where ".$cond."";
$path=WEB_ROOT.'mycontacts.php';     
 $paging=  getAccountPagination($query,$RecPerPage,$PageNum,$path);




?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('Contact List');?> - <?php echo SITE_NAME;?></title>
        
 

<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
 <link rel="shortcut icon" type="image/ico" href="<?php echo LOGO_IMAGE_PATH_SHOW.getConfiguration('website_favicon');?>" /> 
<!--MAIN STYLE-->
      <?php include 'script.php';?>  
         

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
  <script type="text/javascript">
    $(document).ready(function(){
        $(".ui-tabs").tabs();
    });
</script>
</head>
<body>
<div class="se-pre-con"></div>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="myaccount">
     <div class="container">
      <?php include_once 'breadcrumb.php';?>
     <div class="ui-tabs">
         <?php include 'custmenu.php';?>        
    <div id="tabs-1" class="ui-tabs-panel">
        <h4><?php echo('Add New Contact');?>!</h4>
           <?php   $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg'];
             echo getMessage($msgcode);?>
           
       <div style="float: left; width: 50%;">      
          <form method="post" name="frm" id="fromcontact" enctype="multipart/form-data" >
          <input type="hidden" name="txtMode" value="addcontact" >
           
                   <ul class="row">
                   
              <?php for($i=1;$i<=10;$i++):?>     
              <li class="col-md-6">
                <div class="form-group">
                  <label for=""><?php echo('Name');?> *
                    <input type="text" class="form-control" id="name<?php echo $i?>"  placeholder="" name="name[]">
                  </label>
                </div>
              </li>
              <li class="col-md-6" >
                <div class="form-group">
                  <label for=""><?php echo('Mobile');?> *
                    <input type="text" class="form-control" id="mobile<?php echo $i?>" maxlength="10"   placeholder="" name="mobile[]">
                  </label>
                </div>
              </li>
             <?php endfor;?> 
                         
              <li class="col-md-6">
                <button type="submit" class="btn"><?php echo('Invite');?></button>
              </li>
            </ul>
          </form>  
       </div>
       <div style="clear:both"></div>
                          
     <div>
     
      <h4><?php echo('My Contacts');?></h4>    
<div class="table-responsive">
                  <table class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th><?php echo('Last Invite Date');?></th>
                        <th><?php echo('Name');?></th>
                        <th><?php echo('Mobile');?></th>                                    
                         
                      </tr>
                    </thead>
                   <tbody>
                   <?php foreach($contactslist as $friend):?>
                   <tr>
                   <td><?php echo date('M d,Y',strtotime($friend['created_date']))?></td>
                   <td><?php echo $friend['name'];?></td>
                   <td><?php echo $friend['mobile'];?></td>
                   </tr>       
                   <?php endforeach;?>                       
                   </tbody>       
                  </table>
                    <ul class="pagination">
            <?php echo $paging;?>
             
        </ul>
                </div>
     
     </div>   
      
        
        
        
    </div>
        
</div>
  

     
     
     </div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
    <?php include 'js.php';?>   
 <script src="<?php echo WEB_ROOT;?>js/jquery.validate.js"></script> 
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#fromcontact").validate({
                 rules: {
                name: "required",
                 mobile: "required",                 
                
            },
                         
            
        });                                     
          
    });
</script>   
    
       
</body>
</html>
