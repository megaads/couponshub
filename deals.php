<?php include_once 'coupons_field.php';?>  

<div class="content-wrap">
<section id="section-underline-1" class="great-deals" name="coupons">
 
                    <div class="block-content list-coupon clearfix">

          <ul class="row" id="couponslist">
          <!--======= COUPEN DEALS =========-->
          <?php foreach($coupons as $rec): 
 $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
            if($rec['coupon_imageurl']!="")
            {
                $rec['coupon_image']=$rec['coupon_imageurl'];
            }
           else
            {
                 $rec['coupon_image']=COUPON_IMAGE_PATH_SHOW.(($rec['coupon_image']!="")?$rec['coupon_image']:'no-image.jpg');
                   $rec['coupon_image']= WEB_ROOT."SampleImage.php?src=".$rec['coupon_image']."&h=135&q=40&zc=0";           
            }
                
               $rec['retailer_logo']=RETAILER_IMAGE_PATH_SHOW.(($rec['retailer_logo']!="")?$rec['retailer_logo']:'no-image.jpg');                                                                                                                     
                       
            
           
            
          ?>
          <li><div class="coupon-item grid_3">
                            <div class="coupon-content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                <a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>" class="ver_container">  <img class="img-responsive" src="<?php echo $rec['coupon_image']?>" alt="<?php echo $rec['identifier']; ?>" height="135px" width="143px" ></a>
       <!----   <?php if(strtotime(TODAY)==strtotime($rec['end_date'])): ?>    
              <div class="top-tag">                          
              <span class="ribn-pur"><span><?php echo('Expiring Today');?></span></span></div>
              <?php elseif(strtotime(TODAY)>strtotime($rec['end_date'])): ?>
              <div class="top-tag">                          
              <span class="ribn-red"><span><?php echo('Expired');?></span></span></div>  
              
              <?php endif;?>   ---->
              
              <div class="c-img text-center"> 
              <div class="top-brand-img text-center">
              
              </div>
               
               <div class="coupon-desc"> <a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>"><?php echo substr($rec['name'],0,58)?><div style="font-weight: bold; color: green;">+upto <?php echo str_replace($good,$bad,$rec['cashback'])?> Extra Cashback</div></a></div>

                <div class="time-left"><?php echo('Expiry Date');?> : <?php echo date("M d, Y",strtotime($rec['end_date']))?></div>
				
                     
                <div class="text-center" style="padding-bottom: 15px;"> 
                  
                <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn showcoupon" ><?php echo('Get Cashback');?></a>
                
                </div>
              </div>
                
            </div>
            <i class="stick-lbl hot-sale"><img class="img-responsive" src="<?php echo $rec['retailer_logo']?>" height="20" alt=""   ></i>
          </li>
         <?php endforeach;?> 
              
           </ul>       
        
        <!--======= PAGINATION =========-->
                 <button class="loadMore" style="
    display: inline-block;
    padding: 15px 25px;
    font-size: 24px;
    cursor: pointer;
    text-align: center;
    text-decoration: none;
    outline: none;
    color: #fff;
    background-color:#e74f1d;
    border: none;
    border-radius: 15px;
    box-shadow: 0 9px #999;
">Load More</button>
         <div id="pagination" style="text-align: center;display: none;">
             <!----  <img src="<?php echo WEB_ROOT?>images/loading.gif">  --->
   <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
<span class="sr-only">Loading...</span>
          </div>
      </div>
      
            </div>
			</div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> 
             <?php include 'js.php';?>  
                              
  <script type="text/javascript">
var url="<?php echo WEB_ROOT."getcoupons.php"?>";
var docroot="<?php echo DOC_ROOT."images/"?>";
   function lastmsg_funtion(pagenumber){
    $('#pagination').show();                                              
    $.post(url, {PageNum: pagenumber}, function(result){
    $('#pagination').hide();                                              
      $('#couponslist').append(result);
      $(".btncashback").leanModal({top : 100, overlay : 0.6, closeButton: ".modal_close" });  
       $(".showcoupon").leanModal({top :100 , overlay : 0.6, closeButton: ".modal_close" }); 
    });
} 

$(window).scroll(function(){

if ($(window).scrollTop() == $(document).height() - $(window).height()){
  
   $('.loadMore').fadeIn(200);
  
   
}

});

$('.loadMore').click(function () {
    
   var pagenumber=eval($('#pagenumber').val());
    
   $('#pagenumber').val(pagenumber+1); 
   var total_page=$('#total_page').val();   
   
   if(pagenumber<=total_page) 
{
lastmsg_funtion(pagenumber);

}
 else
jQuery('button.loadMore').attr('disabled', 'disabled').text('No More Coupons ');
  

});

</script>                                          
</body>
</html>