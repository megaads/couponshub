<?php include_once 'tickets_field.php';
$crumbs[]="Missing Cashback";
?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('My Tickets');?> - <?php echo SITE_NAME;?></title>
                               
<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
 
  
  <link href="<?php echo WEB_ROOT;?>css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/main.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/style.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/animate.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/responsive.css" rel="stylesheet" type="text/css">
<link href="<?php echo WEB_ROOT;?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
<?php include 'ga.php';
 
 
?>
         

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
  <script type="text/javascript">
    $(document).ready(function(){
        $(".ui-tabs").tabs();
    });
</script>
</head>
<body>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="myaccount">
     <div class="container">
      <?php include_once 'breadcrumb.php';?>
     <div class="ui-tabs">
                <div class="col-md-3"><?php include 'custmenu.php';?></div>    
                <div class="col-md-9 white-body">    
    <div id="tabs-1" class="ui-tabs-panel">
        <h4><?php echo('Missing Cashback');?></h4>
        <?php
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg'];     
        ?>
         <?php echo getMessage($msgcode);?>
         <button type="button" onclick="window.location='<?php echo WEB_ROOT?>createticket.php'" class="btn"><?php echo('Raise Ticket');?></button>  
        
       <div class="table-responsive">
                  <table class="table table-striped table-hover">
                    <thead>
                      <tr>
                       <th><?php echo('Ticket');?>#</th>
                        <th><?php echo('Date of Query');?></th>
                        <th><?php echo('Retailer');?></th>
                        <th><?php echo('Transaction Amount');?></th>
                        <th><?php echo('Transaction Date');?></th>
                        <th><?php echo('Status');?></th>                          
                      </tr>
                    </thead>
                   <tbody>
                   <?php foreach($tickets as $ticket):?>
                   <tr>
                   <td>#<?php echo $ticket['ticket_id'];?></td>
                   <td><?php echo date('M d,Y',strtotime($ticket['created_date']))?></td>
                   <td><?php echo (isset($ticket['retailer_name']) && $ticket['retailer_name']!="")?$ticket['retailer_name']:"";?></td>
                   <td> <?php echo $currency; ?><?php echo $ticket['transaction_amount'];?></td>                                                                                  
                   <td><?php echo date('M d,Y',strtotime($ticket['transaction_date']))?></td>
                   <td><?php echo $ticket['ticket_status'];?></td>
                   </tr>
                   
                   <?php endforeach;?>
                   
                   </tbody>       
                  </table>
                    <ul class="pagination">
            <?php echo $paging;?>
             
        </ul>
                </div>
    </div>
    </div>
        
</div>
     
     
     </div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
    <?php include 'js.php';?>      
</body>
</html>