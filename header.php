<!--======= HEADER =========-->

    <header class="bg223">
  <div class="fl fw light_eff"><div class="fw light_effect"><div class="fw light_effect_on"></div></div>
      <div class="container">
        <div class="row clearfix">
          <div class="col-md-3 col-sm-4 col-lg-4 col-xs-12 logo-section">
             <a href="<?php echo WEB_ROOT;?>"><img src="<?php echo SITE_LOGO;?>" alt="<?php echo SITE_NAME;?>" ></a>
          </div>
           <div class="col-md-4 col-sm-2 col-lg-3 col-xs-12"></div>
         
          <div class="col-md-5 col-sm-6 col-lg-5 col-xs-12 text-right">
            <div class="search-form">
              <form name="frmsearch" id="frmsearch" action="<?php echo WEB_ROOT?>search.php" >
                <input type="text" placeholder="Search for Flipkart, Amazon, Pizza etc." id="search" value="<?php echo (isset($_GET['q']))?$_GET['q']:"";?>" name="q" placeholder="<?php //echo('Enter your keyword'); ?>..."/>
                <button type="submit" class="btn btn-primary btn-sm text-uppercase">
                  Search
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>  
    </header>

<div class="fixed-header">
    <nav class="navbar menu-bar bg2d3">
      <div class="container">
        <div class="row clearfix">
          <div class="col-md-9 col-sm-9 col-xs-12 col-lg-8">                                     
            <div class="navbar-header">
			<?php if(!isset($_SESSION['USER_ID'])):?>  
              <button type="button" id="hamburger_menu" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar bar_one"></span>
                <span class="icon-bar bar_two"></span>
                <span class="icon-bar bar_three"></span>
              </button>
			 <?php endif; ?>   
               <div class="login-area-for-mobile hidden-sm hidden-md hidden-lg">
                <div class="login-section">
                   <?php if(isset($_SESSION['USER_ID'])):?>    
              
              <div class="dropdown mega-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> </i><?php echo $_SESSION['USER_NAME'];?> <span class="fa fa-angle-down"></span>
                </a>
                  <ul class="dropdown-menu account-top-dropdown" aria-labelledby="navbarDrop1">
                    <li>
                      <a href="<?php echo WEB_ROOT?>myearning.php"><i class="fa fa-money" aria-hidden="true"></i>My Earnings</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>payment.php"><i class="fa fa-inr" aria-hidden="true"></i>Payment</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>myprofile.php"><i class="fa fa-cog" aria-hidden="true"></i>Settings</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>tickets.php"><i class="fa fa-question" aria-hidden="true"></i>Missing Cashback</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>refernearn.php"><i class="fa fa-users" aria-hidden="true"></i>Invite Freinds</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                    </li>
                  </ul>
				
              </div> 
                 <button type="button" id="hamburger_menu" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar bar_one"></span>
                <span class="icon-bar bar_two"></span>
                <span class="icon-bar bar_three"></span>
              </button>            
                    <?php else: ?>  
                  <a data-toggle="modal" href="#loginModal"><i class="fa fa-unlock-alt" aria-hidden="true"></i> Login</a>
                  <a data-toggle="modal"  data-toggle="modal" href="#signupModal"><i class="fa fa-user" aria-hidden="true"></i>Register</a>
                  
                  <?php endif; ?>      
                  
                </div>
               </div>
            </div>
            <div id="navbar" class="navbar-collapse collapse menu-bar">
              <ul class="nav navbar-nav text-uppercase">
                <li><a href="<?php echo WEB_ROOT;?>"><i class="fa fa-home" aria-hidden="true"></i><?php echo('Home'); ?></a>
             
          </li>
                <li><a href="<?php echo WEB_ROOT;?>coupons"><i class="fa fa-tags" aria-hidden="true"></i><?php echo('Coupons'); ?></a> </li>    
               <li class="dropdown mega-dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                     Categories  <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                   <?php echo getCategoriesMenu();?>     
				
                   </li>  
                <li class="dropdown mega-dropdown store-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                     Store  <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                  <div class="dropdown-menu store-dropdown">
                    <div class="clearfix pb20">                                          
                      <?php
                      
                       $i=0; foreach($retailerlist as $rt):
                       if($i>7) break;
                       
                           if($i!=0 && $i%4==0)
                           echo '</div><div class="clearfix pd20 pdb20">';
                       ?>
                      <div class="col-md-3 col-sm-3 col-xs-12 text-center">
                       <a href="<?php echo WEB_ROOT.'store/'.$rt['identifier'];?>" title="<?php echo $rt['name'];?>"> <img alt="<?php echo $rt['name'];?>"   src="<?php echo RETAILER_IMAGE_PATH_SHOW.$rt['menu_logo'];?>" height="30px" width="55px"></a>
                      
					  </div>
                      <?php $i++; endforeach;?>
                         
                    </div> 
                    <div style="text-align: right; margin-top: 10px; margin-right: 16px;"><a style="color:#000" href="<?php echo WEB_ROOT?>stores">View All</a></div> 
                  </div>
                </li>
                 <li><a href="<?php echo WEB_ROOT;?>how-it-works.php"><i class="fa fa-wrench" aria-hidden="true"></i><?php echo('How it works'); ?></a> 
             
           </li>
                <li><span class="rh-menu-label rh-menu-label-blue" style="background: red;">NEW</span><a href="<?php echo WEB_ROOT;?>bestprice" target="_blank"><i class="fa fa-tags" aria-hidden="true"></i><?php echo('Price Compare'); ?></a> </li>
                </ul>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12 col-lg-4 text-right hidden-xs">
            <div class="login-section login-mobile">
                <?php if(isset($_SESSION['USER_ID'])):?>    

              <div class="dropdown mega-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <?php echo $_SESSION['USER_NAME'];?> <span class="fa fa-angle-down"></span>
                </a>
                  <ul class="dropdown-menu account-top-dropdown" aria-labelledby="navbarDrop1">
                    <li>
                      <a href="<?php echo WEB_ROOT?>myearning.php"><i class="fa fa-money" aria-hidden="true"></i>My Earnings</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>payment.php"><i class="fa fa-inr" aria-hidden="true"></i>Payment</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>myprofile.php"><i class="fa fa-cog" aria-hidden="true"></i>Settings</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>tickets.php"><i class="fa fa-question" aria-hidden="true"></i>Missing Cashback</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>refernearn.php"><i class="fa fa-users" aria-hidden="true"></i>Invite Freinds</a>
                    </li>
                    <li>
                      <a href="<?php echo WEB_ROOT?>logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                    </li>
                  </ul>
              </div>             
                    <?php else: ?>  
              
              <a data-toggle="modal" href="#loginModal"><i class="fa fa-unlock-alt" aria-hidden="true"></i> Login</a>
              <a  data-toggle="modal" href="#signupModal"><i class="fa fa-user" aria-hidden="true"></i> Register</a>
                <?php endif; ?>      
              
            </div>
          </div>
        </div>
      </div>
    </nav>
<!---<a href="https://play.google.com/store/apps/details?id=com.motionappsworld.couponshub" target="_blank"><img class="img-responsive" src="<?php echo WEB_ROOT;?>images/couponshub_banner.png" alt="<?php echo SITE_NAME;?> cashback android app" ></a> --->
</div>