<?php

     require_once("../admin/system/constant.php"); 
     require_once("../admin/system/databaseLayer.php"); 
     require_once("../admin/functions/common.php"); 
     require_once("../admin/functions/listfunction.php"); 
     require_once("../admin/functions/commonfun.php");
     require_once("../config.php");                                     
     header('Content-type: application/json');
     
     //print_r($_REQUEST);exit;
      $inputJSON = file_get_contents('php://input');
     $input= json_decode( $inputJSON, TRUE ); //convert JSON into array
    
     
     if(isset($input))
     {
         $apiuser=$input['api_user'];
         $apisecret=$input['api_secret'];
         $username=$input['username'];
         $password=$input['password'];
         $android_reg_id=$input['android_reg_id']; 
           
         if(empty($apiuser) || empty($apisecret))
         {
                $data['error']= "Missing Required Paramters";
                echo json_encode($data);
                exit;
         }             
         
         
         $sql_query="select user_id,name from tbl_user where email='".$apiuser."' and password='".$apisecret."' and  

user_level =1 and status=1 limit 1 ";
      
         $Record =$db->objSelect($sql_query, "ROW");     
          
        if(!$Record)
        {
            $data['error']= "Invalid API Credential";
            echo json_encode($data);
            exit;
        }        
          
        
        if($input['outh_provider']=='facebook')
        {
              loginwithFacebookapi($input);
        } 
        else if($input['outh_provider']=='google')
        {
              loginwithGoogleapi($input);
        }
        else
        {
             if(empty($username) || empty($password))
               {
                $data['error']= "Missing Required Paramters";
                echo json_encode($data);
                exit;
               }             
            
           $sql_query="select user_id,name,user_image,email,city,country,phone from tbl_user where email='".$username."' 

and password='".$password."' and  user_level =0 and verified=1 and status=1 limit 1 ";
            
         $Record =$db->objSelect($sql_query, "ROW");  
         
         if($Record['user_image']!="")
         {
             if(file_exists(USER_IMAGE_PATH_UPLOAD.$Record['user_image']))
             {
                 $Record['user_image']= USER_IMAGE_PATH_SHOW.$Record['user_image'];
             }   
         
         }         
         
         
          if($Record)
         {
             
             $sql="select android_reg_id from tbl_user where user_id=".$Record['user_id']." and 

android_reg_id='$android_reg_id'";
             $reccnt=$db->objSelect($sql,"ROWCNT");
             
             if($reccnt<=0)
             {
                 $cols="android_reg_id";
                 $vals="'".$android_reg_id."'";
                 $Cond=" where user_id= ".$Record['user_id'];
                 $res =$db->objUpdate("tbl_user",$cols,$vals,$Cond);                                                       
                 
             }     
             
             echo json_encode($Record);
             exit;  
         }
         else
         {
               $data['error']= "Invalid Username or Password";
               echo json_encode($data);
               exit;
         }                          
            
            
            
        }    
               
         
     }
     
     function loginwithFacebookapi($userProfile)
    {
        global $db;
        
        $userProfile['outh_provider']='facebook';  
         $outh_id= $userProfile['outh_id'];
        $android_reg_id= addslashes(trim($userProfile['android_reg_id'])); 
        
         $query="select * from tbl_user where email='".$userProfile['username']."' and  status <> 2 " ;
        
        $res = $db->objSelect($query,"ROW");
         
        if(!empty($res) && count($res) > 0) {                                  
           
              $Record= $res;
              
               $sql="select android_reg_id from tbl_user where user_id=".$Record['user_id']." and 

android_reg_id='$android_reg_id'";
             $reccnt=$db->objSelect($sql,"ROWCNT");
             
             if($reccnt<=0)
             {
                 $cols="android_reg_id";
                 $vals="'".$android_reg_id."'";
                 $Cond=" where user_id= ".$Record['user_id'];
                 $res =$db->objUpdate("tbl_user",$cols,$vals,$Cond);                                                       
                 
             }     
              
              
                          
             echo json_encode($Record);
             exit;        
            
        } 
        else
        {
            $Record= addUserapi($userProfile); 
            
            
             echo json_encode($Record);
             exit;
            
        }
                   
        
    } 
               
      
   
function loginwithGoogleapi($userProfile)
    {
        global $db;
        $outh_id= $userProfile['outh_id'];
        $userProfile['outh_provider']='google';  
         $android_reg_id= addslashes(trim($userProfile['android_reg_id'])); 
        $user_image=  $userProfile['picture'];                                                    
        $query="select * from tbl_user where email='".$userProfile['username']."'  and  status <> 2 " ;
        
        $res = $db->objSelect($query,"ROW");
         
        if(!empty($res) && count($res) > 0) {                                  
           
           $Record= $res; 
           
                $sql="select android_reg_id from tbl_user where user_id=".$Record['user_id']." and 

android_reg_id='$android_reg_id'";
             $reccnt=$db->objSelect($sql,"ROWCNT");
             
             if($reccnt<=0)
             {
                 $cols="android_reg_id";
                 $vals="'".$android_reg_id."'";
                 $Cond=" where user_id= ".$Record['user_id'];
                 $res =$db->objUpdate("tbl_user",$cols,$vals,$Cond);                                                       
                 
             }                     
             echo json_encode($Record);
             exit; 
        } 
        else
        {
            $Record= addUserapi($userProfile);
            
             echo json_encode($Record);
             exit;
             
        }
                   
        
    }                  
       function addUserapi($userProfile)
    {
        global $db;
        global $msg;
        global $rec;
        
        $rec['name']=$name =$userProfile['name'];
        $rec['newsletter']=$newsletter = 1;                                     
        $rec['email']=$email = $userProfile['username']; 
        $rec['android_reg_id']=$android_reg_id = addslashes(trim($userProfile['android_reg_id']));
        $rec['outh_provider']=$outh_provider = $userProfile['outh_provider'];
        $rec['outh_uid']=$outh_uid = $userProfile['outh_id'];   
         $rec['user_image']=$user_image=  $userProfile['user_image'];                                                  
         $rec['status']=$status=1;
          $rec['verified']=$verified=1;
         $user_level=0;      
         $signup_bonus=getConfiguration('signup_bonus');      
          if(isset($userProfile['ref_id'])) {
           $ref_id=$userProfile['ref_id'];
        }
        else
        {
            $ref_id=0;
        }                
         
          $cols="name,email,user_level,created_date,status,verified,ref_id,newsletter,user_image,outh_provider,outh_uid,android_reg_id";
         $vals="'".$name."','".$email."','".$user_level."','".CURRENT_DATETIME."',$status,'$verified',$ref_id,

$newsletter,'$user_image','$outh_provider','$outh_uid','$android_reg_id'";
         
              $user_id =$db->objInsert('tbl_user',$cols,$vals,"LASTID"); 
                
        
        if($signup_bonus!="" && $signup_bonus!="0")
         { 
             $fields=array

('user_id','retailer_id','payment_type','amount','payment_status','exp_confirm_date','reference_id'); 
             
             $rec1['user_id']= $user_id ;
             $rec1['retailer_id']= 0;
             $rec1['payment_type']= "Sign Up Bonus";
               $rec1['amount']= $signup_bonus;     
               $rec1['payment_status']= "Confirmed";             
                 $rec1['exp_confirm_date']= date("Y-m-d");
               $rec1['reference_id']=GenerateReferenceID();               
               
              $cols =implode(",",$fields).',created_date'; 
             $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec1))."','".CURRENT_DATETIME."'";
         
            $pm_id =$db->objInsert("tbl_transaction",$cols,$vals,"LASTID");     
         }                                                                 
      
       // Email for Registration Email
       
       $sql_query="select * from tbl_emailtemplate where template_id=1 and status=1";
       $templaterec =$db->objSelect($sql_query, "ROW") ; 
         
       $site_login_url=WEB_ROOT.'login.php';
       
        $emailverifylink=WEB_ROOT."verify.php?v=".$email_verify_code;  
             
        $bad=array('{{SITE_NAME}}','{{name}}','{{SITE_LOGIN_URL}}','{{username}}','{{password}}','{{activation_link}}');
        $good=array(SITE_NAME,$name,$site_login_url,$email,$password,$emailverifylink);  
          
         $Message=str_replace($bad,$good,$templaterec['content']);                                                         

        
         $email_subject=str_replace($bad,$good,$templaterec['email_subject']);
         
       $adminEmail=getConfiguration('admin_email');   
     //  sendMail($email,$adminEmail,$Message,$email_subject);   
             
       if($ref_id)
         {   
               // Referal Bonus
            $fields=array

('user_id','retailer_id','payment_type','amount','payment_status','exp_confirm_date','reference_id');   
               
             $ref_bonus = getConfiguration('refer_bonus');
             $rec1['user_id']=$ref_id ;
             $rec1['retailer_id']= 0;
             $rec1['payment_type']= "Referral Bonus";
               $rec1['amount']= $ref_bonus;     
               $rec1['payment_status']= "Confirmed";             
                 $rec1['exp_confirm_date']= date("Y-m-d");
               $rec1['reference_id']=GenerateReferenceID();               
               
              $cols =implode(",",$fields).',created_date'; 
             $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec1))."',NOW()";
         
            $pm_id =$db->objInsert("tbl_transaction",$cols,$vals,"LASTID");                                                

                        
             
         }
          
           $query="select * from tbl_user where user_id='".$user_id."'  and  status <> 2 " ;
        
        $result = $db->objSelect($query,"ROW");                            
             
       addNotification("New User Registration","users",$user_id); 
       return $result;  
    }   
   
     
     
     
      
     
     
     
     
     
     
     
     
     
?>