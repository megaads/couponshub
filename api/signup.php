<?php

     require_once("../admin/system/constant.php"); 
     require_once("../admin/system/databaseLayer.php"); 
     require_once("../admin/functions/common.php"); 
     require_once("../admin/functions/listfunction.php"); 
     require_once("../admin/functions/commonfun.php");
     define('ENABLE_REF_CODE',$clientId = trim(getConfiguration('enable_referral_code'))) ;   
     header('Content-type: application/json');
     //print_r($_REQUEST);exit;
      $inputJSON = file_get_contents('php://input');
     $input= json_decode( $inputJSON, TRUE ); //convert JSON into array
                    
         
     if(isset($input)) 
     {
         $apiuser=$input['api_user'];
         $apisecret=$input['api_secret'];
          
         
         if(empty($apiuser) || empty($apiuser))
         {
                $data['error']= "Missing Required Paramters";
                echo json_encode($data);
                exit;
         }
         
         $sql_query="select user_id,name from tbl_user where email='".$apiuser."' and password='".$apisecret."' and  user_level =1 and status=1 limit 1 ";
      
         $Record =$db->objSelect($sql_query, "ROW");     
          
        if(!$Record)
        {
            $data['error']= "Invalid API Credential";
            echo json_encode($data);
            exit;
        }          
         
         
         add();    
         
     }
     
     
      function add()
    {
       
        global $db;
        global $msg;
        global $rec;
        global $input; 
       
        $signup_email_activation="Off";//getConfiguration('signup_email_activation');     
        $rec['name']=$name = addslashes(trim($input['name']));
        $rec['newsletter']=$newsletter = (isset($input['newsletter']))?addslashes(trim($input['newsletter'])):0;                                                                     
        $rec['email']=$email = addslashes(trim($input['email']));  
        $rec['phone']=$phone = addslashes(trim($input['phone'])); 
        $rec['android_reg_id']=$android_reg_id = addslashes(trim($input['android_reg_id']));                                                            
        $rec['password']=$password = addslashes(trim($input['password']));
        $ref_id = (isset($input['ref_id']))?addslashes(trim($input['ref_id'])):0;
        $rec['status']=$status=1;
        $user_level=0; 
        $signup_bonus=getConfiguration('signup_bonus');  
        $ref_code=GenerateRandString1(4); 
        $ref_id=($ref_id!="")?$ref_id:0;
         
        if(ENABLE_REF_CODE):
            $referencecode=  addslashes(trim($input['ref_code']));   
            if($referencecode!="")
            {
               $sql="select * from tbl_user where status=1 and verified=1 and ref_code='$referencecode'";
               $refrec =$db->objSelect($sql,"ROW");
               if(isset($refrec['user_id']))
                  $ref_id=$refrec['user_id'];                                 
            }
        endif; 
        
         
         
        ///
        $sqlextra=" and status <> 2";
        $cols="email";
         $vals="'".$email."'";  
        
       $resChk = isDuplicate("user_id",$cols,$vals,'tbl_user', $sqlextra);
             
       if(!empty($resChk) && $resChk > 0)
        {
               $data['error']= "Account Already Exist with this email";
               echo json_encode($data);
               exit;
        }  
        
          ///
        $sqlextra=" and status <> 2";
        $cols="phone";
         $vals="'".$phone."'";  
        
            $resChk = isDuplicate("user_id",$cols,$vals,'tbl_user', $sqlextra);
             
       if(!empty($resChk) && $resChk > 0)
        {
               $data['error']= "Account Already Exist with this mobile number";
               echo json_encode($data);
               exit;
        }  
        
               
        
        if($signup_email_activation=="Off")
        {
           $verified=1;
           $email_verify_code=""; 
           $template_id=4;
        }
        else
        {
            $email_verify_code=GenerateReferenceID();
             $verified=0; 
             $template_id=1;     
        }     
         
         
         $cols="name,email,phone,android_reg_id,password,user_level,created_date,status,email_verified_code,ref_id,newsletter,verified,ref_code";
         $vals="'".$name."','".$email."','".$phone."','".$android_reg_id."','".$password."','".$user_level."',NOW(),$status,'$email_verify_code',$ref_id,$newsletter,$verified,'$ref_code'";
         
        $user_id =$db->objInsert('tbl_user',$cols,$vals,"LASTID"); 
            
         $fields=array('user_id','retailer_id','payment_type','amount','payment_status','exp_confirm_date','reference_id'); 
         
        if($signup_bonus!="" && $signup_bonus!="0")
         { 
             $rec1['user_id']= $user_id ;
             $rec1['retailer_id']= 0;
             $rec1['payment_type']= "Sign Up Bonus";
               $rec1['amount']= $signup_bonus;     
               $rec1['payment_status']= "Confirmed";             
                 $rec1['exp_confirm_date']= date("Y-m-d");
               $rec1['reference_id']=GenerateReferenceID();               
               
              $cols =implode(",",$fields).',created_date'; 
             $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec1))."',NOW()";
         
            $pm_id =$db->objInsert("tbl_transaction",$cols,$vals,"LASTID");                                                                    
         }  
       // Email for Registration Email
           
       $sql_query="select * from tbl_emailtemplate where template_id=1 and status=1";
       $templaterec =$db->objSelect($sql_query, "ROW") ; 
         
       $site_login_url=WEB_ROOT.'login.php';
       
        $emailverifylink=WEB_ROOT."verify.php?v=".$email_verify_code;  
             
        $bad=array('{{SITE_NAME}}','{{name}}','{{SITE_LOGIN_URL}}','{{username}}','{{password}}','{{activation_link}}');
        $good=array(SITE_NAME,$name,$site_login_url,$email,$password,$emailverifylink);  
          
         $Message=str_replace($bad,$good,$templaterec['content']);                                                                 
         $email_subject=str_replace($bad,$good,$templaterec['email_subject']);
         
       $adminEmail=getConfiguration('admin_email');   
       sendMail($email,$adminEmail,$Message,$email_subject);   
             
         if($signup_email_activation=="Off" && $ref_id)
         {   
               // Referal Bonus
            $fieldsref=array('user_id','retailer_id','payment_type','amount','payment_status','exp_confirm_date','reference_id');   
               
             $ref_bonus = getConfiguration('refer_bonus');
             $rec1['user_id']=$ref_id ;
             $rec1['retailer_id']= 0;
             $rec1['payment_type']= "Referral Bonus";
               $rec1['amount']= $ref_bonus;     
               $rec1['payment_status']= "Pending";             
                 $rec1['exp_confirm_date']= date("Y-m-d");
               $rec1['reference_id']=GenerateReferenceID();               
               
              $cols =implode(",",$fieldsref).',created_date'; 
             $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec1))."',NOW()";
         
            $pm_id =$db->objInsert("tbl_transaction",$cols,$vals,"LASTID");                                                                        
             
         }
        
        if($user_id)
        {
             addNotification("New User Registration","users",$user_id,"user");  
             $data['success']= "success";
             $data['user_id']=$user_id;
             echo json_encode($data);
             exit;
        }    
          
    }
      
     
     
     
     
     
     
     
     
     
?>