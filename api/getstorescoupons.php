<?php

     require_once("../admin/system/constant.php"); 
     require_once("../admin/system/databaseLayer.php"); 
     require_once("../admin/functions/common.php"); 
     require_once("../admin/functions/listfunction.php"); 
     require_once("../admin/functions/commonfun.php");

     header('Content-type: application/json');
     //print_r($_REQUEST);exit;
     $inputJSON = file_get_contents('php://input');
     $input= json_decode( $inputJSON, TRUE ); //convert JSON into array 
     
     
     
      if(isset($input))   
     {
         $apiuser=$input['api_user'];
         $apisecret=$input['api_secret'];
          
          $type=$input['type'];   
         if(empty($apiuser) || empty($apiuser))
         {
                $data['error']= "Missing Required Paramters";
                echo json_encode($data);
                exit;
         }
         
         $sql_query="select user_id,name from tbl_user where email='".$apiuser."' and password='".$apisecret."' and  user_level =1 and status=1 limit 1 ";
      
         $Record =$db->objSelect($sql_query, "ROW");     
          
        if(!$Record)
        {
            $data['error']= "Invalid API Credential";
            echo json_encode($data);
            exit;
        }          
     
     
     
     
     
     
     $RecPerPage=(isset($input['recPerPage']) && $input['recPerPage']!="")?$input['recPerPage']:10;                
          
         
        $sid =$input['retailer_id'];
      $retailer= getRetailers($sid);
      
      if(!isset($retailer[0]))
      {
          $data['error']="Invalid Store ID";
             echo json_encode($data);   
          exit;
      }
      
       
        $store=$retailer[0];
       
       
          
          
      $cashbacklist= getCashbackList($store['retailer_id']);   
                                
      $PageNum = 1;
        if(isset($input['PageNum']) && $input['PageNum'] != "") {
             $PageNum = $input['PageNum'];
        }
         $offset = ($PageNum - 1) * $RecPerPage;    
      $coupons=getCoupons($store['retailer_id'],"","","",$offset,$RecPerPage,"",$type);  
      
       if($store['retailer_id']!="")
      $cond.=" and store_id=".$store['retailer_id'];
      
      if($type=="coupons")
      {
         $cond.=" and coupon_code!=''"; 
          
      }
       if($type=="deals")
      {
         $cond.=" and (coupon_code='' || coupon_code is NULL)"; 
          
      }                       
      
      
      
       $query="select count(coupon_id) as totRec from tbl_coupon where  status=1 $cond";
       
      $totalPage=getTotalPages($query,$RecPerPage);
      
        $data['retailer']=$store; 
        $data['cashback_detail']=getCashbackList($store['retailer_id']);   
       $data['coupons']=$coupons;
       $data['total_pages']=$totalPage;
       $data['PageNum']= $PageNum;
       $data['coupon_image_path']= COUPON_IMAGE_PATH_SHOW; 
       $data['retailer_image_path']= RETAILER_IMAGE_PATH_SHOW;
       echo json_encode($data);   
     }      
     
?>