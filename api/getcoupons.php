<?php

     require_once("../admin/system/constant.php"); 
     require_once("../admin/system/databaseLayer.php"); 
     require_once("../admin/functions/common.php"); 
     require_once("../admin/functions/listfunction.php"); 
     require_once("../admin/functions/commonfun.php");

     header('Content-type: application/json');
     //print_r($_REQUEST);exit;
     $inputJSON = file_get_contents('php://input');
     $input= json_decode( $inputJSON, TRUE ); //convert JSON into array
                    
     
      if(isset($input))     
     {
         $apiuser=$input['api_user'];
         $apisecret=$input['api_secret'];
          
         
         if(empty($apiuser) || empty($apiuser))
         {
                $data['error']= "Missing Required Paramters";
                echo json_encode($data);
                exit;
         }
         
         $sql_query="select user_id,name from tbl_user where email='".$apiuser."' and password='".$apisecret."' and  user_level =1 and status=1 limit 1 ";
      
         $Record =$db->objSelect($sql_query, "ROW");     
          
        if(!$Record)
        {
            $data['error']= "Invalid API Credential";
            echo json_encode($data);
            exit;
        }          
     
     
     
     
     $RecPerPage=(isset($input['recPerPage']) && $input['recPerPage']!="")?$input['recPerPage']:10;                
      
         
        $PageNum = 1;
               
        if(isset($input['PageNum']) && $input['PageNum'] != "") {
             $PageNum = $input['PageNum'];
        }
          $offset = ($PageNum - 1) * $RecPerPage;    
          
       $coupons=getCoupons("","","","",$offset,$RecPerPage);       
                        
      
       $query="select count(coupon_id) as totRec from tbl_coupon where status=1";
      
         $totalPage=getTotalPages($query,$RecPerPage); 
     
       $data['coupons']=$coupons;
       $data['total_pages']=$totalPage;
       $data['PageNum']= $PageNum;
       $data['coupon_image_path']= COUPON_IMAGE_PATH_SHOW; 
       $data['retailer_image_path']= RETAILER_IMAGE_PATH_SHOW;
       echo json_encode($data);   
     }        
     
?>