<?php
       
     require_once("../admin/system/constant.php"); 
     require_once("../admin/system/databaseLayer.php"); 
     require_once("../admin/functions/common.php"); 
     require_once("../admin/functions/listfunction.php"); 
     require_once("../admin/functions/commonfun.php");

     header('Content-type: application/json');
     //print_r($_REQUEST);exit;
     $inputJSON = file_get_contents('php://input');
     $input= json_decode( $inputJSON, TRUE ); //convert JSON into array
              
       
     if(isset($input)) 
     {
          $apiuser=$input['api_user'];
         $apisecret=$input['api_secret'];
          
         
         if(empty($apiuser) || empty($apiuser))
         {
                $data['error']= "Missing Required Paramters";
                echo json_encode($data);
                exit;
         }
         
         $sql_query="select user_id,name from tbl_user where email='".$apiuser."' and password='".$apisecret."' and  user_level =1 and status=1 limit 1 ";
      
         $Record =$db->objSelect($sql_query, "ROW");     
          
        if(!$Record)
        {
            $data['error']= "Invalid API Credential";
            echo json_encode($data);
            exit;
        }          
         
         
         withdraw();    
         
     }
     
     
         
        function withdraw()
    {
        global $db;
        global $msg;
        global $rec;
        global $input;
            $minpay=getConfiguration('min_payout');  
            $user_id=addslashes(trim($input['user_id']));  
        $payment_details = addslashes(trim($input['payment_details'])); 
         $payment_method = addslashes(trim($input['payment_method']));      
          $amount = addslashes(trim($input['amount']));       
         $payment_status='Requested';
           $cashout_ref_id=GenerateReferenceID();
            $payment_type="Withdraw";
          
           $cashback=getUserBalance($user_id);
         
         if($minpay>$cashback)
         {
              $Record['error']='You have to reach min. payout '.$minpay.' to withdraw money!';
                 echo json_encode($Record);
                 exit;  
         }     
         
          if($amount>$cashback)
         {
              $Record['error']='You cannot create withdraw request for more amount then available cashback';
                 echo json_encode($Record);
                 exit;  
         }     
         
       $cols="reference_id,user_id,payment_type,payment_method,payment_details,amount,payment_status,created_date";
       $vals="'".$cashout_ref_id."','".$user_id."','".$payment_type."','".$payment_method."','".$payment_details."','$amount','$payment_status','".CURRENT_DATETIME."'";
         
         $trans_id =$db->objInsert('tbl_transaction',$cols,$vals,"LASTID");
            
         
        if($trans_id)
        {
             addNotification("New Withdraw Request","cashouts",$cashout_ref_id,"money"); 
              $data['success']= "success";
             $data['trans_id']=$trans_id;
              $cashback=getUserBalance($user_id);
           $data['balance']=$cashback;   
             echo json_encode($data);
             exit;
        }
            
          
    } 
     
     
     
     
     
     
     
     
     
?>