<?php

     require_once("../admin/system/constant.php"); 
     require_once("../admin/system/databaseLayer.php"); 
     require_once("../admin/functions/common.php"); 
     require_once("../admin/functions/listfunction.php"); 
     require_once("../admin/functions/commonfun.php");
     require_once("../config.php");                                     
     header('Content-type: application/json');
     
     //print_r($_REQUEST);exit;
      $inputJSON = file_get_contents('php://input');
     $input= json_decode( $inputJSON, TRUE ); //convert JSON into array
                    
     
     if(isset($input))
     {
         $apiuser=$input['api_user'];
         $apisecret=$input['api_secret'];
                   
         if(empty($apiuser) || empty($apisecret))
         {
                $data['error']= "Missing Required Paramters";
                echo json_encode($data);
                exit;
         }             
         
         
         $sql_query="select user_id,name from tbl_user where email='".$apiuser."' and password='".$apisecret."' and  

user_level =1 and status=1 limit 1 ";
      
         $Record =$db->objSelect($sql_query, "ROW");     
          
        if(!$Record)
        {
            $data['error']= "Invalid API Credential";
            echo json_encode($data);
            exit;
        }  
       recharge($input);        
         
     }
     
            
      
      
    function recharge($input)
    {
        global $db;
        global $msg;
        global $rec;                      
         
        $user_id=$input['user_id'];
        
        $cashback=getUserBalance($user_id);      
                            
         $availablecashback=$cashback;//($cashback)?$cashback[0]:0;        
        
        $mobile_number=addslashes(trim($input['mobile']));       
         $operator=($input['recharge_type']=="prepaid")?addslashes(trim($input['operator'])):addslashes(trim($input['operator']));       ;       
        $payment_details = $input['recharge_type']."$operator:$mobile_number"; 
         $payment_method = getConfiguration('mobie_recharge_pmid');
          $amount = addslashes(trim($input['amount']));       
         $payment_status='Paid';
         $cashout_ref_id=GenerateReferenceID();
         $payment_type="Withdraw";
                
                                                                      
         $params['mobile'] =$mobile_number;
         
         $params['operator']=$operator;
         $params['amount']=$amount;
          $txtstatus= rechargeMobile($params);
                  
         if($availablecashback<$amount)
         {
              $Record['error']="You cannot recharge more amount then available cashback";
                 echo json_encode($Record);
                 exit;  
         }
         
         
         
       if($txtstatus=="SUCCESS")
       {
         
           $cols="reference_id,user_id,payment_type,payment_method,payment_details,amount,payment_status,created_date";
           $vals="'".$cashout_ref_id."','".$user_id."','".$payment_type."','".$payment_method."','".$payment_details."','$amount','$payment_status','".CURRENT_DATETIME."'";
             
           $trans_id =$db->objInsert('tbl_transaction',$cols,$vals,"LASTID");
           
           
           $cols="transaction_id,user_id,recharge_type,operator,mobile,amount,recharge_status,created_date";
           $vals="$trans_id,$user_id,'".$_POST['recharge_type']."','".$operator."','".$mobile_number."','".$amount."','".$txtstatus."','".CURRENT_DATETIME."'";
             
           $recharge_id =$db->objInsert('tbl_recharge',$cols,$vals,"LASTID");
            
           
           
            if($recharge_id)
            {
                 addNotification("Recharge Done","rechargereports",$recharge_id,"money"); 
                 $Record['success']="Recharge Done";
                  $cashback=getUserBalance($user_id);
           $Record['balance']=$cashback;   
                 echo json_encode($Record);
                 exit;  
            }      
           
       } 
       
       if($txtstatus=="PENDING")
       {
           $payment_status="Requested";     
           $cols="reference_id,user_id,payment_type,payment_method,payment_details,amount,payment_status,created_date";
           $vals="'".$cashout_ref_id."','".$user_id."','".$payment_type."','".$payment_method."','".$payment_details."','$amount','$payment_status','".CURRENT_DATETIME."'";
             
           $trans_id =$db->objInsert('tbl_transaction',$cols,$vals,"LASTID");
           
           $cols="transaction_id,user_id,recharge_type,operator,mobile,amount,recharge_status,created_date";
           $vals="$trans_id,$user_id,'".$_POST['recharge_type']."','".$operator."','".$mobile_number."','".$txtstatus."','".CURRENT_DATETIME."'";
             
           $recharge_id =$db->objInsert('tbl_recharge',$cols,$vals,"LASTID");                                                                        
           
           addNotification("Recharge Done","rechargereports",$recharge_id,"money"); 
           
             $Record['success']="Recharge Pending";
              $cashback=getUserBalance($user_id);
           $Record['balance']=$cashback;   
                 echo json_encode($Record);
                 exit;  
           
       }       
          
       if($txtstatus=="FAILED")
       {
          $Record['error']="Recharge Failed Please try after some time";
           $cashback=getUserBalance($user_id);
           $Record['balance']=$cashback;   
                 echo json_encode($Record);
                 exit;  
       }    
          
    }              
            
     
     
      
     
     
     
     
     
     
     
     
     
?>