<?php include_once 'store_field.php';?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo ($store['meta_title']!="")?$store['meta_title']:$store['name']; ?></title>
<meta name="keywords" content="<?php echo $store['meta_keyword'] ?>" >
<meta name="description" content="<?php echo $store['meta_description'] ?>">


<!-- FONTS ONLINE -->
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
   <?php include 'script.php';?> 

 <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/font.css" />
    <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/font-awesome.css" />
    <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/normalize.css" />
    <!--css plugin-->
    <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/flexslider.css" />
    <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/jquery.nouislider.css"/>
    <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/jquery.popupcommon.css" />

    <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/style.css" type="text/css"/>
     <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/res-menu.css" />
    <link rel="stylesheet" href="http://207.246.98.232:83/offers_styles/responsive.css" />
    

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

   

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
           <style>
           .storeheader {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #ddd;
    box-shadow: 0 0 7px #cacaca;
    float: left;
    padding: 10px;
    width: 100%;
    margin-bottom: 20px;
}
span.ver_hold {
    color: white;
}
.coupons-code-item .center-img a img {
    max-height: 80%;
    max-width :60%;
}


.ribbon {
  position: absolute;
  right: -5px; top: -5px;
  z-index: 1;
  overflow: hidden;
  width: 75px; height: 75px;
  text-align: right;
}
.ribbon span {
  font-size: 10px;
  font-weight: bold;
  color: #FFF;
  text-transform: uppercase;
  text-align: center;
  line-height: 20px;
  transform: rotate(45deg);
  -webkit-transform: rotate(45deg);
  width: 100px;
  display: block;
  background: #79A70A;
  background: linear-gradient(#F70505 0%, #8F0808 100%);
  box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
  position: absolute;
  top: 19px; right: -21px;
}
.ribbon span::before {
  content: "";
  position: absolute; left: 0px; top: 100%;
  z-index: -1;
  border-left: 3px solid #8F0808;
  border-right: 3px solid transparent;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #8F0808;
}
.ribbon span::after {
  content: "";
  position: absolute; right: 0px; top: 100%;
  z-index: -1;
  border-left: 3px solid transparent;
  border-right: 3px solid #8F0808;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #8F0808;
}
.tittle h3 {
    text-align: -webkit-center;
    color: #ffffff;
    font-size: xx-large;
    text-shadow: 2px 2px 4px #f3f3f3;
	margin-top: -50px;
}
.lbl-work {
    position: relative;
    display: inline-block;
   
}

.lbl-work .tooltiptext {
    visibility: hidden;
    width: 200px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;

    /* Position the tooltip */
    position: absolute;
    z-index: 100;
}

.lbl-work:hover .tooltiptext {
    visibility: visible;
}
.lbl-work .tooltiptext {
    top: -5px;
    right: 105%; 
}
.sci {
   
    margin-left: 75%;
}
i.fa.fa-scissors {
    z-index: 999;
    position: absolute;
    -webkit-transform: rotate(-75deg);
    -moz-transform: rotate(-75deg);
    -o-transform: rotate(-75deg);
    -ms-transform: rotate(-75deg);
    transform: rotate(-75deg);
}
button.accordion {
    background-color: #2d3e52;
    color: #ffffff;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

button.accordion.active, button.accordion:hover {
    background-color:  #e74f1d;
}

button.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}

button.accordion.active:after {
    content: "\2212";
}

div.panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
}
.card {
    margin-top: 10px;
    border-radius: 5px;
    padding: 10px;
    box-shadow: 0 8px 10px 1px rgba(0,0,0,.14), 0 3px 14px 2px rgba(0,0,0,.12), 0 5px 5px -3px rgba(0,0,0,.2);
}
        </style>
</head>
<body>


<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
  <input type="hidden" name="pagenumber" id="pagenumber" value="2"  />
  <input type="hidden" name="total_page" id="total_page" value="<?php echo $totalPage;?>"  />         
  <!--======= SIGN UP =========-->
   <section>
   <div class="sp-header">
 <div class="img-head">
 <?php include_once 'breadcrumb.php';?> 
 <div class="tittle">
 <h3><?php echo $store['name'];?> Coupons ,Deals & Discount Offers </h3>
 </div>
 </div>
 </div>	
     <div class="grid_frame page-content">  
      <div class="container_grid"> 
    <div class="storeheader"  style="margin-top: -50px;">
<!----<div class="storelogo"><img  src="<?php echo RETAILER_IMAGE_PATH_SHOW.$store['featured']?>" /> ---->
         <div class="storelogo"><img  src="<?php echo RETAILER_IMAGE_PATH_SHOW.$store['logo']?>" width="150" />  
         <br>
         <?php if($store['cashback_disable']==1):?>
         <span style="font-size:16px;font-weight:bold; margin-top: 10px;"><?php echo('Currently no Cashback is available for this store');?></span> 
         <?php else: ?>
         <span style="font-size:24px;font-weight:bold; margin-top: 10px;">Upto <?php echo $store['cashback']."".(($store['fixed_percent']=="percent")?"%":$currency);?> Cashback</span> 
         <?php endif;?>
         </div>
         <div class="storedesc">
             <?php echo $store['description'];?>

<br/>
 
                <a target="_blank" href="<?php echo WEB_ROOT ?>doRedirect.php?sid=<?php echo $store['retailer_id']; ?>" class="btn"><?php echo('Shop & Earn Cashback');?></a>
                                                 
 
</div>
    </div>
    
    <?php if($store['banner']!=NULL) : ?>
		<a target="_blank" href="<?php echo WEB_ROOT ?>doRedirect.php?sid=<?php echo $store['retailer_id']; ?>"><img src="<?php echo $store['banner']?>" alt="<?php $store['name']?>" width="728px" height="90px"/></a>
		<?php endif;?>
    
    
   
        <?php include 'sidebar.php';?> 
          
	<!---		<div class="tabs tabs-style-underline" style="margin-top:-50px;">
			<nav>
						<ul>
							<li><a href="#section-underline-1" ><i class="fa fa-gift" aria-hidden="true"></i><span>Deals</span></a></li>
							<li><a href="#section-underline-2"><i class="fa fa-tags" aria-hidden="true"></i><span>Coupons</span></a></li>
							
						</ul>
					</nav>
					</div> -->
					
  
           
                <div class="layout-2cols clearfix">
                    <div class="grid_8 content">
                        <div class="mod-coupons-code">   
        <div class="mod-grp-coupon block clearfix">
        <ul class="row storepage wrap-list" id="couponlist">
      <div class="coupons-code-item right-action flex">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-f3+i+3a-ol+y9"
     data-ad-client="ca-pub-4120507368931039"
     data-ad-slot="1912954302"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>    
          
          <!--======= COUPEN DEALS =========-->
             <?php foreach($coupons as $rec): 
 $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
            if($rec['coupon_imageurl']!="")
            {
                $rec['coupon_image']=$rec['coupon_imageurl'];
            }
           else
            {
                 $rec['coupon_image']=COUPON_IMAGE_PATH_SHOW.(($rec['coupon_image']!="")?$rec['coupon_image']:'no-image.jpg');
                   $rec['coupon_image']= WEB_ROOT."SampleImage.php?src=".$rec['coupon_image']."&h=135&q=40&zc=0";           
            }
                
               $rec['retailer_logo']=RETAILER_IMAGE_PATH_SHOW.(($rec['retailer_logo']!="")?$rec['retailer_logo']:'no-image.jpg');                                                                                                                     
                       
            
           
            
          ?>
          <li>
		  <div class="coupons-code-item right-action flex">
		  
                           <div class="brand-logo thumb-left">
                                        <div class="wrap-logo">
                                            <div class="center-img">
                                                <span class="ver_hold"></span>
                <a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>" class="ver_container">  <img class="img-responsive" src="<?php echo $rec['retailer_logo']?>" alt="<?php echo $rec['identifier']; ?>" height="135px" width="143px" ></a>
       </div>
                                        </div>
										 <div class="wrap-logo">
                                            <div class="center-img">
                                              <?php if ($rec['price']!=NULL) : ?>
                                                <span class="ver_hold"><?php echo $rec['price'] ?></span>
											<?php else: ?>
												<span class="ver_hold"><?php echo 'Best Deal' ?></span>
												<?php endif; ?>
       </div>
                                        </div>
                                    </div>
									 
                                       
                                   
              
             <div class="right-content flex-body">
			 <p class="rs save-price"><a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>"><?php echo substr($rec['name'],0,58)?></a></p>
               
               
<p class="rs coupon-desc"><?php echo substr($rec['description'],0,100)?></p>
<p><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp Expiry Date : <?php echo date("M d, Y",strtotime($rec['end_date']))?> &nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-check-square-o" aria-hidden="true"></i> Verified</p>
 <div class="bottom-action">

                                            <div class="left-vote">
											<?php if (str_replace($good,$bad,$rec['cashback'])==0):?>
											 <span class="lbl-work">Cashback Not Available </span>
											 <?php else: ?>
                                                <span class="lbl-work">+upto <?php echo str_replace($good,$bad,$rec['cashback'])?> Extra Cashback&nbsp <i class="fa fa-info-circle" aria-hidden="true"></i>
												<span class="tooltiptext">Login or Signup to earn rewards (real money!) into your CouponsHub account when you make a transaction </span></span>
                                             <?php endif; ?>
                                            </div>
                
                <?php if($rec['coupon_code']==NULL): ?>   
                <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn btn-blue btn-take-coupon showcoupon" ><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp<?php echo('Get Deal');?></a>
                <?php else: ?>
                <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn btn-green btn-take-coupon showcoupon" ><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp<?php echo('Get Code');?></a>
                <?php endif; ?>
                </div>
              </div>
               <div class="sci">
				<i class="fa fa-scissors fa-2x" aria-hidden="true"></i>
				</div>  
            </div>
            
          </li>
         <?php endforeach;?> 
              <div class="coupons-code-item right-action flex">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-f3+i+3a-ol+y9"
     data-ad-client="ca-pub-4120507368931039"
     data-ad-slot="1912954302"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
           </ul>
		    </div>
        </div>
		
                     <button class="loadMore btn btn-blue btn-take-coupon" style="width:100%;font-size:20px;padding:10px 10px" >&nbsp&nbsp&nbspLoad More&nbsp&nbsp&nbsp<i class="fa fa-angle-double-down "></i></button>
         <div id="pagination" style="text-align: center;display: none;">
               <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
          </div>
	<div class="card">	  
		  <h2>FAQ's</h2>
  <button class="accordion">How do I get Cashback?</button>
<div class="panel">
  <p>You can earn Cashback by making a valid purchase from the partner stores via Couponshub website.
Simple steps to get paid:
<ul>
<li>--> Sign in to your Couponshub account.</li>
<li>--> Select your favourite offer from any retailer or category.</li>
<li>--> Complete your purchase via couponshub link.</li>
<li>--> Complete your billing transaction.</li>
<li>--> That's it now you are eligible for cashback.</li></ul></p>
</div>

<button class="accordion">Why am I getting cashback for my own shopping?</button>
<div class="panel">
  <p>The affiliate marketing technique makes shopping all more fun for customers. It involves cashback site (Couponshub) passing on the commission they receive from the merchant to the users. The cashback is valid when a successful sale is generated through Couponshub's website. PS: The cashback is paid by the coupon site and not the merchant.</p>
</div>

<button class="accordion">How Long it takes to get my cashback?</button>
<div class="panel">
  <p>It takes about 60-90 days from each merchant to confirm your sale. Once we receive the sale confirmation from the merchant's end, the cashback amount will get credited to your account.</p>
</div>
<button class="accordion">How can I redeem earned cashback?</button>
<div class="panel">
  <p>You can redeem your cashback in Couponshub's account through any of the following ways:
Paytm Wallet : When you reached minimum amount of withdrawal you can simply wihtdraw your earned cashback into Paytm Wallet .
Mobikwik Wallet : When you reached minimum amount of withdrawal you can simply wihtdraw your earned cashback into Mobikwik Wallet.</p>
</div>
<button class="accordion">What if I change or cancel my order?</button>
<div class="panel">
  <p>In case you place an order but later on cancel it or make a change, you are not entitled to get the cashback. Suppose you purchased 3 items from a retailer (say, Jabong), and decided to return one of the items. You will not be entitled to receive any cashback (not even on the other two items).</p>
</div>
</div>
      </div>
	  

       <div class="grid_4 sidebar" id="sidebar">
	   
                        
                        <div class="mod-list-store block">
                            <h3 class="title-block">Popular store</h3>
                            <div class="block-content">
                                <div class="wrap-list-store clearfix">
								 <ul class="row">
								  <?php foreach($featuredstores as $rec):                                           
        $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
        
        ?>
		<li>
                                    <a class="brand-logo" href="<?php echo WEB_ROOT?>store/<?php echo $rec['identifier']; ?>" >
                                        <span class="wrap-logo">
                                            <span class="center-img">
                                                <span class="ver_hold"><p>Up to <?php echo str_replace($good,$bad,$rec['cashback'])?> Cashback &
              <?php echo $rec['total_coupon']?> More Offers</p></span>
                                                <span class="ver_container"><img src="<?php echo RETAILER_IMAGE_PATH_SHOW.$rec['logo'];?>" alt="<?php echo $rec['identifier']; ?>"></span>
                                            </span>
											
                                        </span>
										
                                    </a>
									</li>
        <?php endforeach;?>

                                  
                                </div>
                            </div>
                        </div><!--end: .mod-list-store -->
						<div class="ads" >
					 <div data-WRID="WRID-146521500164064543" data-widgetType="searchWidget" data-class="affiliateAdsByFlipkart" height="250" width="300" ></div><script async src="//affiliate.flipkart.com/affiliate/widgets/FKAffiliateWidgets.js"></script>
	  <script charset="utf-8" type="text/javascript">
amzn_assoc_ad_type = "responsive_search_widget";
amzn_assoc_tracking_id = "coupo00e-21";
amzn_assoc_marketplace = "amazon";
amzn_assoc_region = "IN";
amzn_assoc_placement = "";
amzn_assoc_search_type = "search_widget";
amzn_assoc_width = "auto";
amzn_assoc_height = "auto";
amzn_assoc_default_search_category = "";
amzn_assoc_default_search_key = "";
amzn_assoc_theme = "light";
amzn_assoc_bg_color = "FFFFFF";
</script>
<script src="//z-in.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&Operation=GetScript&ID=OneJS&WS=1&MarketPlace=IN"></script>
<a href="http://tracking.vcommission.com/aff_c?offer_id=2672&aff_id=51050&file_id=115838" target="_blank"><img src="http://media.vcommission.com/brand/files/vcm/2672/Jabong_CPA_The_Winter_WRAP_Minimum_30_Off_Women_300x250.jpg" width="300" height="250" border="0" /></a><img src="http://tracking.vcommission.com/aff_i?offer_id=2672&file_id=115838&aff_id=51050" width="1" height="1" />
<div data-WRID="WRID-147529734468183576" data-widgetType="Push Content"  data-class="affiliateAdsByFlipkart" height="250" width="300"></div><script async src="//affiliate.flipkart.com/affiliate/widgets/FKAffiliateWidgets.js"></script>
<div data-SDID="1558367504"  data-identifier="SnapdealAffiliateAds" data-height="300" data-width="250" ></div><script async id="snap_zxcvbnhg" src="https://affiliate-ads.snapdeal.com/affiliate/js/snapdealAffiliate.js"></script>
    </div>  </div>
	 
	
            </div>
			
  </section> 
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
                               <?php include 'js.php';?>
 
                               
<script type="text/javascript">    
var url="<?php echo WEB_ROOT."getstore.php"?>";
var id="<?php echo $store['retailer_id'];?>";
   function lastmsg_funtion(pagenumber){
        $('#pagination').show();                                              
       $.post(url, {id:id,PageNum: pagenumber}, function(result){
         $('#pagination').hide();        
                                        
     $('#couponlist').append(result); 
       $(".btncashback").leanModal({top : 100, overlay : 0.6, closeButton: ".modal_close" }); 
         $(".showcoupon").leanModal({top :100 , overlay : 0.6, closeButton: ".modal_close" });  
                      
                                                                                                  
    });
} 

$(window).scroll(function(){

if ($(window).scrollTop() == $(document).height() - $(window).height()){
  
   $('.loadMore').fadeIn(200);
  
   
}

});

$('.loadMore').click(function () {
    
   var pagenumber=eval($('#pagenumber').val());
    
   $('#pagenumber').val(pagenumber+1); 
   var total_page=$('#total_page').val();   
   
   if(pagenumber<=total_page) 
{
lastmsg_funtion(pagenumber);

}
 else
jQuery('button.loadMore').attr('disabled', 'disabled').text('No More Coupons ');
  

});

$(window).scroll(function(){

if ($(window).scrollTop()  == $(document).height() - $(window).height() ){
  
  var pagenumber=eval($('#pagenumber').val());
    
   $('#pagenumber').val(pagenumber+1); 
   var total_page=$('#total_page').val();   
   
  if(pagenumber<=total_page) 
  lastmsg_funtion(pagenumber);
  
   
}
});
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  }
}

</script>                               
                               
</body>
</html>