<div id="content" class="span10">
            
            
            <?php include 'breadcrumb.inc';?>
                                                                                          
            <div class="row-fluid">
                
                <div class="span3 statbox purple clickstoday" onTablet="span6" onDesktop="span3">
                    <div class="boxchart">5,6,7,2,0,4,2,4,8,2,3,3,2</div>
                    <div class="number"><?php echo $todayclick = getTodaysClick();?>
                    <?php if(getYesterdayClick() <= $todayclick): ?>
                    <i class="icon-arrow-up"></i> 
                    <?php else: ?>
                    <i class="icon-arrow-down"></i>
                    <?php endif;?>
                    </div>
                    <div class="title">Clicks Today</div>
                    <div class="footer">
                        <a href="<?php echo WEB_ROOT?>admin/module/clickhistorys"> read full report</a>
                    </div>    
                </div>
                <div class="span3 statbox green userstoday" onTablet="span6" onDesktop="span3">
                    <div class="boxchart">1,2,6,4,0,8,2,4,5,3,1,7,5</div>
                    <div class="number"><?php echo $usertoday= getTodaysClick();?>
                    <?php if(getYesterdayUsers() <= $usertoday): ?>
                    <i class="icon-arrow-up"></i> 
                    <?php else: ?>
                    <i class="icon-arrow-down"></i>
                    <?php endif;?>
                    </div>
                    <div class="title">Users Today</div>
                    <div class="footer">
                        <a href="<?php echo WEB_ROOT?>admin/module/users"> read full report</a>
                    </div>
                </div>
                <div class="span3 statbox blue noMargin clickyesterday" onTablet="span6" onDesktop="span3">
                    <div class="boxchart">5,6,7,2,0,-4,-2,4,8,2,3,3,2</div>
                    <div class="number"><?php echo getYesterdayClick();?><i class="icon-arrow-up"></i></div>
                    <div class="title">Clicks Yesterday</div>
                    <div class="footer">
                        <a href="#"> read full report</a>
                    </div>
                </div>
                <div class="span3 statbox yellow usersyesterday" onTablet="span6" onDesktop="span3">
                    <div class="boxchart">7,2,2,2,1,-4,-2,4,8,,0,3,3,5</div>
                    <div class="number"><?php echo getYesterdayUsers();?><i class="icon-arrow-down"></i></div>
                    <div class="title">Users Yesterday</div>
                    <div class="footer">
                        <a href="#"> read full report</a>
                    </div>
                </div>    
                
            </div>
            <div class="row-fluid">    

                <a class="quick-button metro yellow span2 totalusers">
                    <i class="icon-group"></i>
                    <p>Users</p>
                    <span class="badge"><?php echo getTotalUsers();?></span>
                </a>
                <a class="quick-button metro red span2 totalretailers">
                    <i class="icon-comments-alt"></i>
                    <p>Retailers</p>
                    <span class="badge"><?php echo getTotalRetailers();?></span>
                </a>
                <a class="quick-button metro blue span2 totalcoupons">
                    <i class="icon-tags"></i>
                    <p>Coupons</p>
                    <span class="badge"><?php echo getTotalCoupons();?></span>
                </a>
                  
                <a class="quick-button metro pink span2 totalpaid">
                    <i class="icon-money"></i>
                    <p>Paid Cashback</p>
                    <span class="badge"><?php echo (float)getTotalPaidCashback();?> <?php echo $currency;?></span>
                </a>
                
                 <a class="quick-button metro pink span2 totalearned">
                    <i class="icon-money"></i>
                    <p>Earned Commision</p>
                    <span class="badge"><?php echo (float)getTotalEarnedCashback();?> <?php echo $currency;?></span>
                </a>
                 <a class="quick-button metro pink span2 totalpending">
                    <i class="icon-money"></i>
                    <p>Pending Cashback</p>
                    <span class="badge"><?php echo (float)getTotalPendingCashback();?> <?php echo $currency;?></span>
                </a>      
                 
                  
                
                <div class="clearfix"></div>
                 
              
                                
            </div><!--/row-->
            
            <div class="row-fluid">
            
               <a class="quick-button metro pink span2 totalrequested">
                    <i class="icon-money"></i>
                    <p>Requested Cashback</p>
                    <span class="badge"><?php echo (float)getTotalRequestedCashback();?> <?php echo $currency;?></span>
                </a>        
                 
            </div>
            
            
             <?php $usersrec=getLastUsers();
                   
             ?>
            <div class="row-fluid">
            <div class="box black span4 lastfiveusers" onTablet="span6" onDesktop="span4">
                    <div class="box-header">
                        <h2><i class="halflings-icon white user"></i><span class="break"></span>Last 5 Users</h2>
                        <div class="box-icon">
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                            <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                        <ul class="dashboard-list metro">
                            <?php 
                            $userstatus=array(0=>'Pending',1=>'Approved',2=>'Rejected');
                            $color=array('green','yellow','purple','red','blue');
                            $c=0;
                            foreach($usersrec as $rec):?>
                            <li class="<?php echo $color[$c];?>">
                                
                                <?php
                                     if($rec['user_image']!="")
                                     {
                                         if(file_exists(USER_IMAGE_PATH_UPLOAD.$rec['user_image']))
                                         {
                                             $rec['user_image']= USER_IMAGE_PATH_SHOW.$rec['user_image'];
                                         }      
                                         
                                         ?> 
                                  <a href="<?php echo WEB_ROOT?>admin/module/users/index.php?view=modify&Code=<?php echo $rec['user_id']?>">    <img  class="avatar" src="<?php echo $rec['user_image']?>" width="50"   />   </a>    
                                        
                                        <?php
                                     }
                                     else
                                     {
                                         ?>
                                          <a href="<?php echo WEB_ROOT?>admin/module/users/index.php?view=modify&Code=<?php echo $rec['user_id']?>">    <img  class="avatar" src="<?php echo WEB_ROOT?>images/user-image.jpg" width="50"   />   </a>    
                                         <?php
                                     } 
                                     
                                     ?>           
                               
                                <strong>Name:</strong> <?php echo $rec['name']?><br>
                                <strong>Since:</strong> <?php echo date("M d,Y",strtotime($rec['created_date']));?><br>
                                <strong>Status:</strong> <?php echo $userstatus[$rec['verified']];?>             
                            </li>
                            <?php $c++; endforeach;?>                         
                                 
                        </ul>
                    </div>
                </div>
            
            </div>
                        
    </div>
     <!-- this set of pageguide steps will be displayed by default. -->
            <ul id="tlyPageGuide" data-tourtitle="check out these elements">
              <!-- use these classes to indicate where each number bubble should be
              positioned. data-tourtarget should contain the selector for the element to
              which you want to attach this pageguide step. -->
              <li class="tlypageguide_left" data-tourtarget=".clickstoday">
               Here You can know how many members have clicked and redirected to retailers site today.
              </li>
              <li class="tlypageguide_right" data-tourtarget=".userstoday">
                Here you can know how many users are registered on site today.
              </li>
              <li class="tlypageguide_right" data-tourtarget=".clickyesterday">
                Here You can know how many members have clicked and redirected to retailers site yesterday.
              </li>   
               <li class="tlypageguide_right" data-tourtarget=".usersyesterday">
                Here you can know how many users are registered on site yesterday.
              </li>   
               <li class="tlypageguide_right" data-tourtarget=".totalusers">
                Here you can know how many users are registered on site.
              </li>   
               <li class="tlypageguide_right" data-tourtarget=".totalretailers">
                Here you can know how many retailers are added.
              </li> 
               <li class="tlypageguide_right" data-tourtarget=".totalcoupons">
                Here you can know how many coupons are added.
              </li>
               <li class="tlypageguide_right" data-tourtarget=".totalpaid">
                Here you can know how much cashback are paid till now.
              </li>
               <li class="tlypageguide_right" data-tourtarget=".totalearned">
                Here you can know how much commission you have earned till now.
              </li>
               <li class="tlypageguide_right" data-tourtarget=".totalpending">
                Here you can know how much cashback are pending till now.
              </li>
               <li class="tlypageguide_right" data-tourtarget=".totalrequested">
                Here you can know how much cashback withdraw has been requested.
              </li>
               <li class="tlypageguide_right" data-tourtarget=".lastfiveusers">
                Here you can know Latest 5 users on the site.
              </li>  
            </ul>   
 <script src="js/jquery-ui-1.10.0.custom.min.js"></script>      