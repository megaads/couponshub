      <?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>

<script>    

    $().ready(function() {
        // validate the comment form when it is submitted

        
        $("#frm").validate({
             rules: {
               name :"required",
               aff_key:"required", 
            },
            
        });                                     
          
    });
    
    </script>
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                       
                    </div>
                    <div class="box-content">
                         <form name="frm"  id="userfrm" class="form-horizontal"  method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            <input type="hidden" name="affiliate_id" value="<?php echo $Code;?>" />
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level"> Affiliate Name</label>  
                              <div class="controls">
                       <input type="text" class="re-input" id="name"  size="100" value="<?php echo (isset($rec['name']))?$rec['name']:""?>" name="name">
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Affiliate Id</label>
                              <div class="controls">
                                  <input type="text" class="re-input" id="aff_key" size="100" value="<?php echo (isset($rec['aff_key']))?$rec['aff_key']:""?>" name="aff_key">
                              </div>
                            </div>
                              <div class="control-group">
                              <label class="control-label" for="date01">Network Id</label>
                              <div class="controls">
                                  <input type="text" class="re-input" id="access_key" size="100" value="<?php echo (isset($rec['access_key']))?$rec['access_key']:""?>" name="access_key">
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Token</label>
                              <div class="controls">
                            <input type="text" class="re-input" id="token" size="100" value="<?php echo (isset($rec['token']))?$rec['token']:""?>" name="token">
                                 
                              </div>
                            </div>
                             
                            <div class="control-group">
                                <label class="control-label">Status</label>
                                <div class="controls">
                                  <label class="radio">
                                       <input type="radio" name="status" value="1" <?php echo (isset($rec['status']) && $rec['status']==1)?"checked":""?> checked="checked" />Active
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" name="status" value="0" <?php echo (isset($rec['status']) && $rec['status']==0)?"checked":""?> />InActive
                                  </label>
                                </div>
                              </div>
                            
                                                                 
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save changes</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>
    
     