    <?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>
<script>tinymce.init({
  selector: '.description',
  height: 300,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools'
  ],
  toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons',
  image_advtab: true,
       
        
 });</script>
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#frm").validate({
                 rules: {
                name: "required",
                 affiliate_id: "required",  
                 url: "required",  
                 cashback: "required",                       
                
            },
                         
            
        });                                     
          
    });
 
    
    
    </script>
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                        <div class="box-icon">
                            <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                            <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                          <form name="frm" id="frm" class="form-horizontal"    method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            <input type="hidden" name="retailer_id" value="<?php echo $Code;?>" />
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level"> Retailer Name</label>  
                              <div class="controls">
                                           <input type="text" class="re-input" id="name"  size="100" value="<?php echo (isset($rec['name']))?$rec['name']:""?>" name="name">
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Path</label>
                              <div class="controls">
                               <input type="text" class="re-input" id="identifier"  size="100" value="<?php echo (isset($rec['identifier']))?$rec['identifier']:""?>" name="identifier">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Categories</label>
                              <div class="controls">
                                  <div style="height: 200px; overflow: scroll;">
                                 <?php echo getCategoriesTree1();?> 
                                 </div>
                                 
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Upload Logo</label>
                              <div class="controls">
                                <input type="file" class="re-input" id="ctl00_MainContent_txtMUSRXvarPas0"  name="logo">
                                   <?php
                                     if($rec['logo']!="")
                                     {
                                         ?> 
                                    <img src="<?php echo RETAILER_IMAGE_PATH_SHOW.$rec['logo']?>" width="150"   />    
                                        
                                        <?php
                                     } 
                                     
                                     ?>
                              </div>
                            </div>
                            
                             <div class="control-group">
                              <label class="control-label" for="date01">Menu Logo</label>
                              <div class="controls">
                                <input type="file" class="re-input" id="ctl00_MainContent_txtMUSRXvarPas0"  name="menu_logo">
                                   <?php
                                     if($rec['menu_logo']!="")
                                     {
                                         ?> 
                                    <img src="<?php echo RETAILER_IMAGE_PATH_SHOW.$rec['menu_logo']?>" width="150"   />    
                                        
                                        <?php
                                     } 
                                     
                                     ?>
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01"> Affiliate Network</label>
                              <div class="controls">
                               <?php 
                                    $affiliateslist=getAffilliates();
                                                                 
                                      echo funcCombo($affiliateslist,"name","affiliate_id","affiliate_id",$rec['affiliate_id'],$Extra);
                                   ?> 
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Merchant Id</label>
                              <div class="controls">
                               <input type="text" class="re-input" id="merchant_id" size="100" value="<?php echo (isset($rec['merchant_id']))?$rec['merchant_id']:""?>" name="merchant_id">
                                     
                                    <em>Enter merchant id or offer id                                        
                                    
                                    </em>
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Url</label>
                              <div class="controls">
                               <input type="text" class="re-input" id="url" size="100" value="<?php echo (isset($rec['url']))?$rec['url']:""?>" name="url">
                                     
                                    <em>Example : http://tracking.icubeswire.com/aff_c?offer_id=x&aff_id=xyz&<b>aff_sub={USERID}</b>
                                    <br>where   aff_sub={USERID} is a Sub Parameter to track your your user
                                    
                                    </em>
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Cashback</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="cashback" maxlength="50" value="<?php echo (isset($rec['cashback']))?$rec['cashback']:""?>" name="cashback">
                                     <select style="width: 130px;" class="re-input" id="ctl00_MainContent_ddlMUSRXintPri0" name="fixed_percent">
                                       
                                        <option <?php echo (isset($rec['fixed_percent']) && $rec['fixed_percent']=="fixed")?"selected":""?> value="fixed" selected="selected"><?php echo trim(getConfiguration('currency')); ?></option>
                                        <option <?php echo (isset($rec['fixed_percent']) && $rec['fixed_percent']=="percent")?"selected":""?> value="percent"  >%</option> 
                                        </select>
                                 
                              </div>
                            </div>
                            
                             <div class="control-group">
                                <label class="control-label">Disable Cashback</label>
                                <div class="controls">
                                  <label class="radio">
                                    <input type="radio" name="cashback_disable" value="0" <?php echo (isset($rec['cashback_disable']) && $rec['cashback_disable']==0)?"checked":""?> checked="checked" />No
                                       
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                        <input type="radio" name="cashback_disable" value="1" <?php echo (isset($rec['cashback_disable']) && $rec['cashback_disable']==1)?"checked":""?>  />Yes             
                                  </label>
                                </div>
                              </div>
                            
                              <div class="control-group">
                              <label class="control-label" for="date01">Description</label>
                              <div class="controls">
                                 <textarea name="description" class="description" id="description"><?php echo (isset($rec['description']))?$rec['description']:""?></textarea>
                                 
                              </div>
                            </div>
                            
                             <div class="control-group">
                                <label class="control-label">Enable Warranty</label>
                                <div class="controls">
                                  <label class="radio">
                                    <input type="radio" name="enable_warranty" value="0" <?php echo (isset($rec['enable_warranty']) && $rec['enable_warranty']==0)?"checked":""?> checked="checked" />No
                                       
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                        <input type="radio" name="enable_warranty" value="1" <?php echo (isset($rec['enable_warranty']) && $rec['enable_warranty']==1)?"checked":""?>  />Yes             
                                  </label>
                                </div>
                              </div>
                            
                            <div class="control-group">
                              <label class="control-label" for="date01">Warranty Title</label>
                              <div class="controls">
                                  <input type="text" class="re-input" id="warranty_title"  size="100" value="<?php echo (isset($rec['warranty_title']))?$rec['warranty_title']:""?>" name="warranty_title">
                                 
                              </div>
                            </div>
                            
                               <div class="control-group">
                              <label class="control-label" for="date01">Warranty Description</label>
                              <div class="controls">
                                 <textarea name="warranty_description" class="description" id="warranty_description"><?php echo (isset($rec['warranty_description']))?$rec['warranty_description']:""?></textarea>
                                 
                              </div>
                            </div>
                            
                            
                              <div class="control-group">
                              <label class="control-label" for="date01">Meta Title</label>
                              <div class="controls">
                                  <input type="text" class="re-input" id="meta_title"  size="100" value="<?php echo (isset($rec['meta_title']))?$rec['meta_title']:""?>" name="meta_title">
                                 
                              </div>
                            </div>
                              <div class="control-group">
                              <label class="control-label" for="date01">Meta Keyword</label>
                              <div class="controls">
                                  <textarea name="meta_keyword" cols="60" id="meta_keyword"><?php echo (isset($rec['meta_keyword']))?$rec['meta_keyword']:""?></textarea>
                                 
                              </div>
                            </div>
                              <div class="control-group">
                              <label class="control-label" for="date01">Meta Description</label>
                              <div class="controls">
                                 <textarea name="meta_description"  cols="60" id="meta_description"><?php echo (isset($rec['meta_description']))?$rec['meta_description']:""?></textarea>
                                 
                              </div>
                            </div>
                            
                            
                            
                                     <div class="control-group">
                                <label class="control-label">Featured</label>
                                <div class="controls">
                                  <label class="radio">
                                       <input type="radio" name="is_featured" value="1" <?php echo (isset($rec['is_featured']) && $rec['is_featured']==1)?"checked":""?> checked="checked" />Yes
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" name="is_featured" value="0" <?php echo (isset($rec['is_featured']) && $rec['is_featured']==0)?"checked":""?> />No
                                  </label>
                                </div>
                              </div>
                                <div class="control-group">
                                <label class="control-label">Show in Navigation</label>
                                <div class="controls">
                                  <label class="radio">
                                       <input type="radio" name="is_menu" value="1" <?php echo (isset($rec['is_menu']) && $rec['is_menu']==1)?"checked":""?> checked="checked" />Yes
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" name="is_menu" value="0" <?php echo (isset($rec['is_menu']) && $rec['is_menu']==0)?"checked":""?> />No
                                  </label>
                                </div>
                              </div>
                            
                            
                            <div class="control-group">
                                <label class="control-label">Status</label>
                                <div class="controls">
                                  <label class="radio">
                                       <input type="radio" name="status" value="1" <?php echo (isset($rec['status']) && $rec['status']==1)?"checked":""?> checked="checked" />Active
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" name="status" value="0" <?php echo (isset($rec['status']) && $rec['status']==0)?"checked":""?> />InActive
                                  </label>
                                </div>
                              </div>
                            
                                                                 
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save changes</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>