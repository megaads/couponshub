<?php
	if (!defined('WEB_ROOT')) {
			echo "You Cannot directly access this page";
			exit;
		}  
	  
	/*********Show Record Per Page*******************/ 
	  
	// Set Variable for Paging	 
	  if(!$_POST && isset($_SESSION['Retailer_RecPerPage']))
		$RecPerPage=$_SESSION['Retailer_RecPerPage'];
	  else  
	  	$RecPerPage=(isset($_POST['recPerPage']))?$_POST['recPerPage']:SHOW_PER_PAGE; 
	  
	  $_SESSION['Retailer_RecPerPage']=$RecPerPage;
	  
	  $PageNum=(isset($_POST['PageNum']))?$_POST['PageNum']:((isset($_GET['PageNum']))?$_GET['PageNum']:"1");	
	// ************************************		
	/*********Show Record Per Page*******************/ 
	 $msg="";
	/*User Level*/
	
		$db=new databaseLayer(HOST,USER,PASS,DB); 
		$record="";	
		//checkUser();
	
	  $action = isset($_POST['txtMode']) ? $_POST['txtMode'] : '';
	 
	switch ($action) {
		
		case 'add' :
			add();
			break;
		 case 'import' :
            import();
            break;	
		case 'update' :
			modify();
			break;
		case 'Active' :
				changeStatus('retailer_id',1,'S');
				break;
		case 'InActive' :
				changeStatus('retailer_id',0,'S');
				break; 
		case 'Delete' :
				changeStatus('retailer_id',2,'S');
				break;
		case 'Restore' :
				changeStatus('retailer_id',1,'S');
				break;  		  
		case 'MActive' :
			changeStatus('retailer_id',1,'M');
			break;
		case 'MInActive' :
			changeStatus('retailer_id',0,'M');
			break;
		case 'MDelete' :
			changeStatus('retailer_id',2,'M');
			break;
		case 'MRestore' :
			changeStatus('retailer_id',1,'M');
			break;				
		 
	} 
	
	switch($view)
	{
		case 'list':
			$record=showRecord();
			break;
		case 'add': 
			$latestrecord=TOP10RECORD();	
		     	break;
		case 'modify': 
			  $Code= $_GET['Code'];
			$rec=getRecord($Code);  
			$latestrecord=TOP10RECORD();	
		     	break;
		case 'detail': 
			  $Code= $_GET['Code'];
			$rec=getRecord($Code);  
			$latestrecord=TOP10RECORD();	
		     	break;						
		default:
			$record=showRecord();
			break;
	
	}
	
	
	function showRecord()
	{ 
	    global $db;
		global $RecPerPage;
		global $PageNum; 
		$condField=array();
		   //*********Search Criteria**********/
		 
		   $fields=array();
		 $fields=array(
		 	'name'=>'like',               
			'status'=>'=',  
		 ); 
		  
		   //*********Search Criteria**********/
		   
		   $searchCond="";
		    //if(count($condField)>0)
		   // $searchCond=implode(" and ",$condField); 
		  $searchCond= getSearchConditions($fields);  
		   $SortBy=(isset($_POST['txtSortBy']))?$_POST['txtSortBy']:"name";		
		   $SortOrder=(isset($_POST['txtSortOrder']))?$_POST['txtSortOrder']:"asc";					 
		   
		   $sql_paging="select count(*) as totRec from ".TABLE_NAME." where $searchCond"; 
		   $PagLink=getPaging($sql_paging,$RecPerPage,$PageNum);
		 
		   $offset = ($PageNum - 1) * $RecPerPage; 
		 
		   $sql_query="select retailer_id,name,concat(cashback,'',fixed_percent) as cashbk,created_date,status from ".TABLE_NAME." where $searchCond order by $SortBy $SortOrder limit $offset,$RecPerPage"; 
		   $record =$db->objSelect($sql_query, "ASSOCIATIVE") ;  
			
		// Set Parameter Start**************/
		
		/***Set Field for Tables*/
		$UserColHeader=array("Id","Retailer Name","Cashback","Created Date","Cashback","Status","Edit","Delete"); // Header  set this as your requirement
		  
		$UserColValue=array("retailer_id","name","cashbk","created_date","catcashback","status","edit","delete"); // Column name defined in database number of column should be same as Header
		  
		$extrafeatures=array('cashbk',"catcashback",'status','edit','delete'); 
		 
		 $parameters=array( 
				 'header'=>$UserColHeader,
				 'colnval'=>$UserColValue,
				 'EXTFEA'=>$extrafeatures,
				 'sorting'=>array("Id"=>"retailer_id",'Retailer Name'=>'name'), // set Sorting Field
				 'sortby'=>$SortBy,
				 'sortorder'=>$SortOrder,
				 'PagLink'=>$PagLink, 
				 'page'=>'module/'.MODULE_PAGE, // set module name
				 'colsize'=>array("2%","22%","20%","24%","4%","4%","4%","4%"), // set width of columns
				 'checkbox'=>array(  
		 				'allow'=>1,  // set allow =0 for no check box required and 1 for check box required
						'name'=>'chk', // set name for check box default name is chk No need to change
		 				'value'=>'retailer_id', // set column for value
					 ),
				  
		        ); 
			 
		/*************************/
		 
		// Set Parameter End**************/
		$Recordtable=getTable($record,$parameters);
		 
		return $Recordtable;
	}
	
	function TOP10RECORD()
	{
		//Show Record Per Page
	 	global $db; 
		 $sql_query="select retailer_id,name from ".TABLE_NAME." where status <> 2 order by retailer_id desc limit 10"; 
		 $latestRecord="";
		 $latestRecord =$db->objSelect($sql_query, "ASSOCIATIVE") ;  
		 return $latestRecord;
	}
	
	function getRecord($Code)
	{
		//Show Record Per Page
	 	global $db; 
		 $sql_query="select * from ".TABLE_NAME." where retailer_id='".$Code."'";  
		 $rec =$db->objSelect($sql_query, "ROW") ;  
		 return $rec;
	}
	                         
	
    
    
    
	function add()
    {
        global $db;
        global $msg;
        global $rec ;
         
        // Insert into Horse table
        $fields=array();
        $fields=array('name','affiliate_id','merchant_id','url','cashback','fixed_percent','cashback_disable','description','enable_warranty','warranty_title','warranty_description','meta_title','meta_keyword','meta_description','is_featured','is_menu','status');
         
         //exit;
        foreach($_POST as $key=>$val)
        { 
            if(in_array($key,$fields))
            {
                if(substr_count($key,"date")>0)
                {  
                  if($val!="" && $val!="mm-dd-yyyy")
                      $rec[$key] = dateFormat($val,"Y-m-d");
                  else
                          $rec[$key]="";    
                }
                else    
                {   
                  $rec[$key] = addslashes(trim($val));
                }
            }    
        } 
        
      
       $identifier=(($_POST['identifier']!=""))?$_POST['identifier']:$_POST['name'];
         $identifier=createSlug($identifier);                                           
         
        $sqlextra=" and status <> 2";
        $cols1="identifier";
        $vals1="'".$identifier."'";  
        
        $resChk = isDuplicate("retailer_id",$cols1,$vals1,TABLE_NAME, $sqlextra);
         
        if(!empty($resChk) && $resChk > 0) {
              $identifier.="-".rand();         
        }
                  
           
           
          if($_FILES['logo']['name']!="")
         {   
            $allowed=array('jpg','JPG','gif','GIF','png','PNG');  
             $Pre="";
              $logo= upload_file("logo",RETAILER_IMAGE_PATH_UPLOAD, MAX_FILE_SIZE, $allowed,$Pre);
             
            if($_FILES['logo']['name']!="" && !$logo)
            {
             $msg="imgerr";
                return;
            }    
        }      
        
          if($_FILES['menu_logo']['name']!="")
         {   
            $allowed=array('jpg','JPG','gif','GIF','png','PNG');  
             $Pre="";
              $menu_logo= upload_file("menu_logo",RETAILER_IMAGE_PATH_UPLOAD, MAX_FILE_SIZE, $allowed,$Pre);
             
            if($_FILES['menu_logo']['name']!="" && !$menu_logo)
            {
             $msg="imgerr";
                return;
            }    
        }      
           
          
         $cols =implode(",",$fields).',created_date,logo,identifier,menu_logo'; 
         $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec))."',NOW(),'".$logo."','".$identifier."','".$menu_logo."'";
     
        $retailer_id =$db->objInsert(TABLE_NAME,$cols,$vals,"LASTID"); 
        
       foreach($_POST['cat'] as $catId)
       {
         $cols ='retailer_id,category_id'; 
         $vals ="'".$retailer_id."','".$catId."'";                                                 
         $catres =$db->objInsert("tbl_category_retailer",$cols,$vals); 
         
       }  
          
        
           
        if($retailer_id>0)
        {      
             
            if($_GET['PageNum']!="")
                $url="&PageNum=".$_GET['PageNum'];
             header("location: index.php?view=list".$url."&msg=addsuc");
             exit;
        }
          
          
    }
    
               
    
	
	   function modify()
    {
        global $db;
        global $msg; 
        global $view; 
          $fields=array('name','affiliate_id','merchant_id','url','cashback','fixed_percent','cashback_disable','description','enable_warranty','warranty_title','warranty_description','meta_title','meta_keyword','meta_description','is_featured','is_menu','status');    
        $vals1="";
        foreach($_POST as $key=>$val)
        {
            if(in_array($key,$fields))
            {  
                if(substr_count($key,"date")>0)
                {  
                     $vals1.=  "'".dateFormat($val,"Y-m-d")."'*@@*"; 
                }
                else    
                {    
                     $vals1.="'".addslashes(trim($val))."'*@@*";
                }
                  
            }    
        } 
        
        
           
        $retailer_id=$_POST['retailer_id'];
     
        if($retailer_id=="")
        {
            $msg="invalidcode";
            return;
        
        }  
        $identifier=($_POST['identifier']!="")?$_POST['identifier']:$_POST['name'];
         $identifier=createSlug($identifier);                                           
          
        $sqlextra=" and retailer_id <> ".$retailer_id." and  status <> 2";
        $cols2="identifier";
        $vals2="'".$identifier."'";  
        
        $resChk = isDuplicate("retailer_id",$cols2,$vals2,TABLE_NAME, $sqlextra);
        
        if(!empty($resChk) && $resChk > 0) {
             $identifier.="-".rand();    
        }     
               
         if($_FILES['logo']['name']!="")
         {   
             
              $sql_query="select logo from ".TABLE_NAME." where retailer_id=".$retailer_id;
              $rec =$db->objSelect($sql_query, "ROW") ;   
            
               $filePath=RETAILER_IMAGE_PATH_UPLOAD;
              $fileName=$rec['logo'];
              
             funcDeleteFile($filePath,$fileName); 
             
             $allowed=array('jpg','JPG','gif','GIF','png','PNG');  
             $Pre="";
                $logo= upload_file("logo",RETAILER_IMAGE_PATH_UPLOAD, MAX_FILE_SIZE, $allowed,$Pre);
             
            if($_FILES['logo']['name']!="" && !$logo)
            {
             $msg="imgerr";
                return;
            }    
        } 
         
         if($_FILES['menu_logo']['name']!="")
         {   
             
              $sql_query="select menu_logo from ".TABLE_NAME." where retailer_id=".$retailer_id;
              $rec =$db->objSelect($sql_query, "ROW") ;   
            
               $filePath=RETAILER_IMAGE_PATH_UPLOAD;
              $fileName=$rec['menu_logo'];
              
             funcDeleteFile($filePath,$fileName); 
             
             $allowed=array('jpg','JPG','gif','GIF','png','PNG');  
             $Pre="";
                $menu_logo= upload_file("menu_logo",RETAILER_IMAGE_PATH_UPLOAD, MAX_FILE_SIZE, $allowed,$Pre);
             
            if($_FILES['menu_logo']['name']!="" && !$menu_logo)
            {
             $msg="imgerr";
                return;
            }    
        } 
         
         
         
          $cols =implode(",",$fields)."";  
          $vals = substr($vals1,0,strlen($vals1)-4)."";
         if($_FILES['logo']['name']!="")
         {   
            $cols.=",logo";
            $vals.="*@@*'".$logo."'";
            
        }     
        
         if($_FILES['menu_logo']['name']!="")
         {   
            $cols.=",menu_logo";
            $vals.="*@@*'".$menu_logo."'";
            
        }     
        $cols.=",identifier";
        $vals.="*@@*'".$identifier."'";  
         $Cond=" where retailer_id= ".$retailer_id;
         $res =$db->objUpdate(TABLE_NAME,$cols,$vals,$Cond); 
         
      // Update Category
      
      $cond="where retailer_id=".$retailer_id;
      $db->objDelete("tbl_category_retailer",$cond);  
         
       foreach($_POST['cat'] as $catId)
       {
         $cols ='retailer_id,category_id'; 
         $vals ="'".$retailer_id."','".$catId."'";                                                 
         $catres =$db->objInsert("tbl_category_retailer",$cols,$vals); 
         
       }  
          
         
        if($res)
        { 
               if($_GET['PageNum']!="")
                $url="&PageNum=".$_GET['PageNum'];
             header("location: index.php?view=list".$url."&msg=updsuc");
             exit;
        }  
    } 
    
    
     function import()
    {
        global $db;
        global $msg;
        global $rec ;
        $stores=array();
        require(DOC_ROOT.'admin/excelreader/php-excel-reader/excel_reader2.php');
          require(DOC_ROOT.'admin/excelreader/SpreadsheetReader.php');
         if($_FILES['import_file']['name']!="")
         {
             $Reader = new SpreadsheetReader($_FILES['import_file']['tmp_name']);
               $i=0; 
                foreach ($Reader as $Row)
                {      
                  
                
                    
                    $i++;  
                    if($i==1) continue;                          
                    
                    $sql="select * from tbl_retailer where name='".$Row[0]."' and status=1";
                       
                     $store =$db->objSelect($sql, "ROW") ; 
                     if(isset($store['retailer_id'])) continue;                                                         
                     //$Row[0]=$store['retailer_id'];                                             
                     //$stores[$data[0]]=$store['retailer_id'];
                     importRecord($Row); 
                       
                    
                } 
                         
                
         }        
        
         header("location: index.php?view=list&msg=importsuc");
            
          
          
    }
    
   function importRecord($data)
   {
         global $db;
      // Insert into Horse table
       // print_r($data);exit;
         $affiliate_id=$_POST['affiliate_id'];
        $fields=array();
        $fields=array('name','affiliate_id','merchant_id','logo','url','cashback','fixed_percent','cashback_disable','description','meta_keyword','meta_title','meta_description','is_featured','is_menu','status');
        
        $rec['name']= $data[0];
        $rec['affiliate_id']= $affiliate_id;  
        $rec['merchant_id']= $data[3];
        $rec['logo']= $data[2];   
        $rec['url']= $data[4];   
        $rec['cashback']= $data[5];   
        $rec['fixed_percent']= $data[6];
        $rec['cashback_disable']= $data[7];
        $rec['description']= $data[8];
         $rec['meta_keyword']= $data[10]; 
          $rec['meta_title']= $data[9]; 
            
        $rec['meta_description']= $data[11];                                       
        $rec['is_featured']= $data[12];
         $rec['is_menu']= $data[13];                                                                    
                                                                               
        $rec['status']= $data[14];   
             
                                             
                 
         $identifier=$rec['name'];
         $identifier=createSlug($identifier);                                           
         
        $sqlextra="";
        $cols1="identifier";
        $vals1="'".$identifier."'";  
        
        $resChk = isDuplicate("retailer_id",$cols1,$vals1,TABLE_NAME, $sqlextra);
         
       if(!empty($resChk) && $resChk > 0) {
            
            continue;
            
        }      
           
          
          
           $cols =implode(",",$fields).',created_date,identifier'; 
         
           $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec))."',NOW(),'".$identifier."'";
            
          $retailer_id =$db->objInsert(TABLE_NAME,$cols,$vals,"LASTID"); 
       $categories=explode(",",$data[1]);    
          
       foreach($categories as $_category)
       {
         
           $categoryId=getCategoryId($_category);
           
         $cols ='retailer_id,category_id'; 
         $vals ="'".$retailer_id."','".$categoryId."'";                                                 
         $catres =$db->objInsert("tbl_category_retailer",$cols,$vals); 
         
       }  
          
          
          
          
                       
   }   
    
    
    
    
    
    
    
    
    
    
           function getCategoriesTree1()
{  
    global $db;
    global $global_menu; 
    $sql="select * from tbl_category where status=1 order by parent_id";
    $res=$db->objSelect($sql,"ASSOC");
    
     $Code= $_GET['Code'];    
    $query="select category_id from tbl_category_retailer where retailer_id=".$Code;
     $rescat=$db->objSelect($query,"ASSOC");  
     $selectedCat=array();
     foreach($rescat as $ct)
     {
         $selectedCat[]=$ct['category_id'];
     } 
    
    
$menu = array(0 => array('children' => array()));
     foreach($res as $data){
  $menu[$data['category_id']] = $data;
  $menu[(is_null($data['parent_id']) ? '0' : $data['parent_id'] )]['children'][] = $data['category_id'];
}
        
      
$global_menu = $menu;
$nav = '<ul class="categorylist">';
foreach($menu[0]['children'] as $child_id) {
  $nav .= makeNav3($menu[$child_id],$selectedCat);
}
$nav .= '</ul>';
 return $nav;       
                
                
}

function makeNav3($menu,$selectedCat) {
    
    
  global $global_menu;
  $checked="";
  if(in_array($menu['category_id'],$selectedCat))
  {
     $checked="checked='checked'"; 
  }
  
  $nav_one = '<li>'."\n\t".'<input type="checkbox" name="cat[]" value="'.$menu['category_id'].'" '.$checked.'>' . $menu['category_name'].'';
  if(isset($menu['children']) && !empty($menu['children'])) {
    $nav_one .= "<ul>\n";
    foreach($menu['children'] as $child_id) {
       // print_r($global_menu[$child_id]);exit; 
        $nav_one .= makeNav3($global_menu[$child_id],$selectedCat);
    }
    $nav_one .= "</ul>\n";
  }
  $nav_one .= "</li>\n";
  return $nav_one;
}            

        

    
    
    
              
                  
	
?>