<?php
   

if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#frm").validate({
                 rules: {
                category_name: "required",
                 identifier: "required",                         
                
            },
                         
            
        });                                     
          
    });
    </script>
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                          
                    </div>
                    <div class="box-content">
                         <form name="frm"  id="frm" class="form-horizontal"  method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            <input type="hidden" name="category_id" value="<?php echo $Code;?>" />
                           <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">Category Name</label>  
                              <div class="controls">
           <input type="text" class="re-input" id="category_name" maxlength="50" value="<?php echo (isset($rec['category_name']))?$rec['category_name']:""?>" name="category_name">
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Path</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="identifier" size="100" value="<?php echo (isset($rec['identifier']))?$rec['identifier']:""?>" name="identifier">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Parent Category</label>
                              <div class="controls">
                                 <?php 
                                      echo getCategoriesTree($rec['parent_id']);
                                           
                                     // echo funcCombo($catlist,"parent_id","parent_id","parent_id",$rec['make'],$Extra);
                                
                             
                                   ?> 
                                 
                              </div>
                            </div>     
                            
                             <div class="control-group">
                              <label class="control-label" for="date01"> Upload Banner</label>
                              <div class="controls">
                              <input type="file" class="re-input" id="ctl00_MainContent_txtMUSRXvarPas0"   name="category_banner"> 
                                 <?php
                                
                                     if($rec['category_banner']!="")
                                     {
                                         ?> 
                                    <img src="<?php echo CAT_BANNER_IMAGE_PATH_SHOW.$rec['category_banner']?>" width="150"   />    
                                        
                                        <?php
                                     } 
                                     
                                     ?> 
                              </div>
                            </div>                 
                            
                             <div class="control-group">
                              <label class="control-label" for="date01"> Category Image</label>
                              <div class="controls">
                              <input type="file" class="re-input" id="ctl00_MainContent_txtMUSRXvarPas0"   name="category_image"> 
                                 <?php
                                     if($rec['category_image']!="")
                                     {
                                         ?> 
                                    <img src="<?php echo CAT_IMAGE_PATH_SHOW.$rec['category_image']?>" width="150"   />    
                                        
                                        <?php
                                     } 
                                     
                                     ?> 
                              </div>
                            </div>
                           <div class="control-group">
                              <label class="control-label" for="date01"> Category Icon (Android)</label>
                              <div class="controls">
                              <input type="file" class="re-input" name="category_icon"> 
                                 <?php
                                     if($rec['category_icon']!="")
                                     {
                                         ?> 
                                    <img src="<?php echo CAT_ICON_PATH_SHOW.$rec['category_icon']?>" width="150"   />    
                                        
                                        <?php
                                     } 
                                     
                                     ?> 
                              </div>
                            </div>  
                             <div class="control-group">
                             <label class="control-label" for="user_level">Banner Code</label>  
                              <div class="controls">
           <textarea class="re-input" id="banner_code"   name="banner_code" ><?php echo (isset($rec['banner_code']))?$rec['banner_code']:""?></textarea>
                                  
                              </div>
                            </div>
                            
                            
                             <div class="control-group">
                                <label class="control-label">Show on Home</label>
                                <div class="controls">
                                  <label class="radio">
                                       <input type="radio" name="show_home" value="1" <?php echo (isset($rec['show_home']) && $rec['show_home']==1)?"checked":""?> checked="checked" />Yes
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" name="show_home" value="0" <?php echo (isset($rec['show_home']) && $rec['show_home']==0)?"checked":""?> />No
                                  </label>
                                </div>
                              </div>  
                            
                                    
                            <div class="control-group">
                                <label class="control-label">Status</label>
                                <div class="controls">
                                  <label class="radio">
                                       <input type="radio" name="status" value="1" <?php echo (isset($rec['status']) && $rec['status']==1)?"checked":""?> checked="checked" />Active
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" name="status" value="0" <?php echo (isset($rec['status']) && $rec['status']==0)?"checked":""?> />InActive
                                  </label>
                                </div>
                              </div>
                            
                                                                 
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save changes</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>