<?php
	if (!defined('WEB_ROOT')) {
			echo "You Cannot directly access this page";
			exit;
		}  
	  
	/*********Show Record Per Page*******************/ 
	    
	// Set Variable for Paging	 
	  if(!$_POST && isset($_SESSION['Brand_RecPerPage']))
		$RecPerPage=$_SESSION['Brand_RecPerPage'];
	  else  
	  	$RecPerPage=(isset($_POST['recPerPage']))?$_POST['recPerPage']:SHOW_PER_PAGE; 
	  
	  $_SESSION['Brand_RecPerPage']=$RecPerPage;
	  
	  $PageNum=(isset($_POST['PageNum']))?$_POST['PageNum']:((isset($_GET['PageNum']))?$_GET['PageNum']:"1");	
	// ************************************		
	/*********Show Record Per Page*******************/ 
	 $msg="";
	/*User Level*/
	
		$db=new databaseLayer(HOST,USER,PASS,DB); 
		$record="";	
		//checkUser();
	
	  $action = isset($_POST['txtMode']) ? $_POST['txtMode'] : '';
	 
	switch ($action) {
		
		case 'add' :
			add();
			break;
			
		case 'update' :
			modify();
			break;
		case 'Active' :
				changeStatus('brand_id',1,'S');
				break;
		case 'InActive' :
				changeStatus('brand_id',0,'S');
				break; 
		case 'Delete' :
				changeStatus('brand_id',2,'S');
				break;
		case 'Restore' :
				changeStatus('brand_id',1,'S');
				break;  		  
		case 'MActive' :
			changeStatus('brand_id',1,'M');
			break;
		case 'MInActive' :
			changeStatus('brand_id',0,'M');
			break;
		case 'MDelete' :
			changeStatus('brand_id',2,'M');
			break;
		case 'MRestore' :
			changeStatus('brand_id',1,'M');
			break;				
		 
	} 
	
	switch($view)
	{
		case 'list':
			$record=showRecord();
			break;
		case 'add': 
			$latestrecord=TOP10RECORD();	
		     	break;
		case 'modify': 
			  $Code= $_GET['Code'];
			$rec=getRecord($Code);  
			$latestrecord=TOP10RECORD();	
		     	break;
		case 'detail': 
			  $Code= $_GET['Code'];
			$rec=getRecord($Code);  
			$latestrecord=TOP10RECORD();	
		     	break;						
		default:
			$record=showRecord();
			break;
	
	}
	
	
	function showRecord()
	{ 
	    global $db;
		global $RecPerPage;
		global $PageNum; 
		$condField=array();
		   //*********Search Criteria**********/
		 
		   $fields=array();
		 $fields=array(
		 	'name'=>'like',               
			'status'=>'=',  
		 ); 
		  
		   //*********Search Criteria**********/
		   
		   $searchCond="";
		    //if(count($condField)>0)
		   // $searchCond=implode(" and ",$condField); 
		  $searchCond= getSearchConditions($fields);  
		   $SortBy=(isset($_POST['txtSortBy']))?$_POST['txtSortBy']:"name";		
		   $SortOrder=(isset($_POST['txtSortOrder']))?$_POST['txtSortOrder']:"asc";					 
		   
		   $sql_paging="select count(*) as totRec from ".TABLE_NAME." where $searchCond"; 
		   $PagLink=getPaging($sql_paging,$RecPerPage,$PageNum);
		 
		   $offset = ($PageNum - 1) * $RecPerPage; 
		 
		   $sql_query="select brand_id,name,created_date,status from ".TABLE_NAME." where $searchCond order by $SortBy $SortOrder limit $offset,$RecPerPage"; 
		   $record =$db->objSelect($sql_query, "ASSOCIATIVE") ;  
			
		// Set Parameter Start**************/
		
		/***Set Field for Tables*/
		$UserColHeader=array("Brand Name","Created Date","Status","Detail","Edit","Delete"); // Header  set this as your requirement
		  
		$UserColValue=array("name","created_date","status","detail","edit","delete"); // Column name defined in database number of column should be same as Header
		  
		$extrafeatures=array('status','detail','edit','delete'); 
		 
		 $parameters=array( 
				 'header'=>$UserColHeader,
				 'colnval'=>$UserColValue,
				 'EXTFEA'=>$extrafeatures,
				 'sorting'=>array('Brand Name'=>'name'), // set Sorting Field
				 'sortby'=>$SortBy,
				 'sortorder'=>$SortOrder,
				 'PagLink'=>$PagLink, 
				 'page'=>'module/'.MODULE_PAGE, // set module name
				 'colsize'=>array("2%","42%","20%","4%","4%","4%","4%"), // set width of columns
				 'checkbox'=>array(  
		 				'allow'=>1,  // set allow =0 for no check box required and 1 for check box required
						'name'=>'chk', // set name for check box default name is chk No need to change
		 				'value'=>'brand_id', // set column for value
					 ),
				  
		        ); 
			 
		/*************************/
		 
		// Set Parameter End**************/
		$Recordtable=getTable($record,$parameters);
		 
		return $Recordtable;
	}
	
	function TOP10RECORD()
	{
		//Show Record Per Page
	 	global $db; 
		 $sql_query="select brand_id,name from ".TABLE_NAME." where status <> 2 order by brand_id desc limit 10"; 
		 $latestRecord="";
		 $latestRecord =$db->objSelect($sql_query, "ASSOCIATIVE") ;  
		 return $latestRecord;
	}
	
	function getRecord($Code)
	{
		//Show Record Per Page
	 	global $db; 
		 $sql_query="select * from ".TABLE_NAME." where brand_id='".$Code."'";  
		 $rec =$db->objSelect($sql_query, "ROW") ;  
		 return $rec;
	}
	
	
	function add()
    {
        global $db;
        global $msg;
        global $rec ;
         
        // Insert into Horse table
        $fields=array();
        $fields=array('name','description','is_featured','status');
         
         //exit;
        foreach($_POST as $key=>$val)
        { 
            if(in_array($key,$fields))
            {
                if(substr_count($key,"date")>0)
                {  
                  if($val!="" && $val!="mm-dd-yyyy")
                      $rec[$key] = dateFormat($val,"Y-m-d");
                  else
                          $rec[$key]="";    
                }
                else    
                {   
                  $rec[$key] = addslashes(trim($val));
                }
            }    
        } 
           
           
          if($_FILES['logo']['name']!="")
         {   
             $allowed=array('jpg','JPG','png','PNG'); 
             $Pre="";
              $logo= upload_file("logo",BRAND_IMAGE_PATH_UPLOAD, MAX_FILE_SIZE, $allowed,$Pre);
             
            if($_FILES['logo']['name']!="" && !$logo)
            {
             $msg="imgerr";
                return;
            }    
        }      
           
          
         $cols =implode(",",$fields).',created_date,logo'; 
         $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec))."',NOW(),'".$logo."'";
     
        $brand_id =$db->objInsert(TABLE_NAME,$cols,$vals,"LASTID"); 
           
        if($brand_id>0)
        {      
              
             header("location: index.php?view=list");
             exit;
        }
          
          
    }
	
	   function modify()
    {
        global $db;
        global $msg; 
        global $view; 
          $fields=array('name','description','is_featured','status');    
        $vals1="";
        foreach($_POST as $key=>$val)
        {
            if(in_array($key,$fields))
            {  
                if(substr_count($key,"date")>0)
                {  
                     $vals1.=  "'".dateFormat($val,"Y-m-d")."'*@@*"; 
                }
                else    
                {    
                     $vals1.="'".addslashes(trim($val))."'*@@*";
                }
                  
            }    
        } 
         
         $brand_id=$_POST['brand_id'];
     
        if($brand_id=="")
        {
            $msg="invalidcode";
            return;
        
        }    
        
         if($_FILES['logo']['name']!="")
         {   
             
              $sql_query="select logo from tbl_brand where brand_id=".$brand_id;
              $rec =$db->objSelect($sql_query, "ROW") ;   
            
               $filePath=BRAND_IMAGE_PATH_UPLOAD;
              $fileName=$rec['logo'];
              
             funcDeleteFile($filePath,$fileName); 
             
             $allowed=array('jpg','JPG','gif','GIF','png','PNG');  
             $Pre="";
                $logo= upload_file("logo",BRAND_IMAGE_PATH_UPLOAD, MAX_FILE_SIZE, $allowed,$Pre);
             
            if($_FILES['logo']['name']!="" && !$logo)
            {
             $msg="imgerr";
                return;
            }    
        } 
         
         
          $cols =implode(",",$fields)."";  
          $vals = substr($vals1,0,strlen($vals1)-4)."";
         if($_FILES['logo']['name']!="")
         {   
            $cols.=",logo";
            $vals.="*@@*'".$logo."'";
            
        }     
         
         $Cond=" where brand_id= ".$brand_id;
         $res =$db->objUpdate(TABLE_NAME,$cols,$vals,$Cond); 
         
         
        if($res)
        { 
            if($_GET['PageNum']!="")
                $url="&PageNum=".$_GET['PageNum'];
             
            header("location: index.php?view=list".$url);
             exit;
        }  
    }           
                  
	
?>