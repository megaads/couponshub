<?php
	 	  
		define('TABLE_NAME','tbl_user'); // define Table Name
		define('MODULE','Change Password'); // define Table Name
		define('MODULE_PAGE','changepassword'); // define Table Name
		$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';	
	 
		/*** Required Files ****************/
		require_once("../../system/constant.php"); 
		require_once("../../system/databaseLayer.php"); 
		require_once("../../functions/common.php"); 
		require_once("../../functions/listfunction.php"); 
	  
	    require_once("changepassword_field.php"); 			
		/*** Required Files ****************/ 
		
		/*****Check User Authentication*************/
		 checkUser();
		/*****Check User Authentication*************/ 
	
	  
	/**** Page *****************************/
	switch ($view) {
		  
		case 'modify' :
			$content 	= MODULE_PAGE.'.php';
			$Title 	= MODULE;
			$mode="update";		
			$pageTitle 	= SITE_NAME.' Admin Control Panel - '.MODULE;
			break; 
		default :
			$content 	= MODULE_PAGE.'.php';
			$Title 	= MODULE;
			$mode="update";		
			$pageTitle 	= SITE_NAME.' Admin Control Panel - '.MODULE;
			break; 
	}
	/**** Page *****************************/
	  
	/**** Javascript File InClude  *****************************/
	$script    = array('check.js','common.js','jquery-1.10.2.js','jquery.validate.js');
	/**** Javascript File InClude  *****************************/ 
	
	 /**** Common Template  *****************************/  
	 require_once '../../inc/template.php';
	/**** Common Template   *****************************/  
?>
