       <?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#pwdfrm").validate({
             rules: {
               oldpassword: {
                    required: true,
                    minlength: 5,                    
                },       
              newpassword: {
                    required: true,
                    minlength: 5,                    
                },    
              confnewpassword: {
                    required: true,
                    minlength: 5,
                    equalTo: "#newpassword"
                },  
            },
            
        });                                     
          
    });
    </script>
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                            
                    </div>
                    <div class="box-content">
                         <form name="frm"  id="userfrm" class="form-horizontal"  method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            <input type="hidden" name="user_id" value="<?php echo $Code;?>" />
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">User Name</label>  
                              <div class="controls">
                                       <input type="text" class="re-input"   id="username" maxlength="50" value="<?php echo (isset($rec['username']))?$rec['username']:""?>" name="username" readonly>
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Old Password</label>
                              <div class="controls">
                              <input type="password" class="re-input"   id="oldpassword" maxlength="50" value=""   name="oldpassword">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">New Password</label>
                              <div class="controls">
                               <input type="password" class="re-input"   id="newpassword" maxlength="50" value=""  name="newpassword">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Confirm New Password</label>
                              <div class="controls">
                             <input type="password" class="re-input"   id="confnewpassword" maxlength="50" value=""   name="confnewpassword">
                                 
                              </div>
                            </div>
                                       
                                                                 
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save changes</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>