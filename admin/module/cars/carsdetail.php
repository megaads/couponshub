<?php
if (!defined('WEB_ROOT')) {
			echo "You Cannot directly access this page";
			exit;
		}  
			$msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg'];    
?>
<style>
.upload{
	
	font-weight:bold; font-size:1.3em;
	font-family:Arial, Helvetica, sans-serif;
	text-align:center;
	background:#f2f2f2;
	color:#3366cc;
	border:1px solid #ccc;
	width:150px;
	cursor:pointer !important;
	-moz-border-radius:5px; -webkit-border-radius:5px;
}
.darkbg{
	background:#ddd !important;
}
#status{
	font-family:Arial; padding:5px;
}
ul#files{ list-style:none; padding:0; margin:0; }
ul#files li{ padding:10px; margin-bottom:2px; width:200px; float:left; margin-right:10px;}
ul#files li img{ max-width:180px; max-height:150px; }
.success{ background:#99f099; border:1px solid #339933; }
.error{ background:#f0c6c3; border:1px solid #cc6622; }
</style>
<script type="text/javascript" src="<?php echo WEB_ROOT?>admin/js/jquery-1.3.2.js" ></script>
<script type="text/javascript" src="<?php echo WEB_ROOT?>admin/js/ajaxupload.3.5.js" ></script>
<script>
function deleteImage(frm,imageName)
{
	 
	frm.txtMode.value="removeImage";
	frm.removeImage.value=imageName;
	frm.submit();		


}
</script>
<table cellspacing="0" cellpadding="0" width="100%" border="0" height="100%">
                                 <tr>
				   <td align="center"><?php echo getMessage($msgcode);?> </td>
			  
				   </tr>
							    <tr>

                                    <td class="contantpart" height="100%" valign="top">
                                        <!--Middle Start-->

    <div id="divLoading" class="loading">
        <div style="display: none;" id="ctl00_MainContent_Up1">
	
                
            
</div>
    </div>
    <!--middle-->
    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="height: 100%;">
        <tbody><tr>
            <td valign="top" colspan="3">
                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tbody><tr>
                        <td class="bottomborder">
                            <h1>
                                <div id="ctl00_MainContent_UpdatePanel6">
	
                                        <span id="ctl00_MainContent_lblMainTitle"><?php echo $Title;?></span>
                                    
</div>
                            </h1>
                        </td>
                        <td align="right" valign="bottom" class="bottomborder">
                            <div id="ctl00_MainContent_UpdatePanel1">
	
                                      <a href="<?php echo WEB_ROOT;?>admin/module/<?php echo MODULE_PAGE;?>/index.php?view=modify&Code=<?php echo $Code?><?php echo isset($_GET['PageNum'])?"&PageNum=".$_GET['PageNum']:""?>" title="Edit <?php echo MODULE;?>" id="ctl00_MainContent_lbtnAdd"><img alt="Search" src="<?php echo WEB_ROOT;?>admin/images/ico-add.gif"> Edit <?php echo MODULE;?></a>
                |    <a href="<?php echo WEB_ROOT;?>admin/module/<?php echo MODULE_PAGE?>/index.php?view=list<?php echo isset($_GET['PageNum'])?"&PageNum=".$_GET['PageNum']:""?>" title="Back to Information List" id="ctl00_MainContent_lbtnAdd"><img alt="Search" src="<?php echo WEB_ROOT;?>admin/images/back-arw.gif"> Back to Cars List</a>                    
</div>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="2">
                            <div id="ctl00_MainContent_UpdatePanel2">
	
                                    <table cellspacing="0" cellpadding="1" border="0">
                                        <tbody><tr>
                                            <td style="height: 32px;">&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                                
</div>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
        <tr>
            <td class="leftpart" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="dashbordboxhead"> <?php echo MODULE;?> List</td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellpadding="5" cellspacing="5" id="ctl00_MainContent_EditLeft1_rptEditList" style="border-collapse: collapse;">
                  <tbody>
                    
					<?php
					 
					if(count($latestrecord)>0) { $i=1;
					  foreach($latestrecord as $Lrec) { ?>
					<tr>
                      <td><span id="ctl00_MainContent_EditLeft1_rptEditList_ctl00_lblno"><?php echo $i;?>. </span> 
                        <a style="font-weight: normal;" href="<?php echo WEB_ROOT;?>admin/module/<?php echo MODULE_PAGE;?>/index.php?view=detail&Code=<?php echo $Lrec['car_id']?><?php echo isset($_GET['PageNum'])?"&PageNum=".$_GET['PageNum']:""?>" class="leftlink" id="ctl00_MainContent_EditLeft1_rptEditList_ctl00_lnkEdit"><?php 
						 
						if($Lrec['car_id']==$Code)
							echo "<strong>".$Lrec['reg_no']."</strong>";
						else
							echo  $Lrec['reg_no'] ;
						?></a></td>
                      </tr>
					  <?php $i++; } } ?>
                     
                    </tbody>
                  </table></td>
              </tr>
            </table>
                <!--Left Part Start-->
                <!--Left Part End-->
            </td>
            <td width="24">
               </td>
            <td valign="top">
                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                  <tr>
                    <td class="dashbordboxhead"><?php echo $Title;?></td>
                  </tr>
                    <tbody>
                    <tr>
                      <td valign="top" class="tableborder">
                        <!--Middle Part start-->
                        <div id="ctl00_MainContent_UpdatePanel4">
                          
                          <div onKeyPress="onenterdata(event);" id="ctl00_MainContent_pnlData">
                             <form name="frm" method="post">
						  <input type="hidden" name="txtMode" value=""  />
						  <input type="hidden" name="txtCode" value=""  /> 
						    <input type="hidden" name="carid" value="<?php echo $rec['car_id']?>"  /> 
						   <input type="hidden" name="removeImage" value="" />
							 
							 	
							<table cellspacing="1" cellpadding="2" border="0" align="center" width="98%">
                              <tbody><tr>
                                <td width="17%">&nbsp;                                  </td>
                                <td width="65%" >
                                  <div style="float:left" align="center">
								  
								  <!--Message Display End--> 
								  </div>								  </td>
                                <td width="16%" >&nbsp;</td>
                              </tr> 
                                <tr>
                                  <td class="labelclass" valign="top">
                                   Make:                                    </td>
                                  <td align="left" valign="middle">
                             <?php echo $rec['make'];?>                         </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass" valign="top">
                                   Model:                                    </td>
                                  <td align="left" valign="middle">
                               <?php echo $rec['model'];?>                                           </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass" valign="top">
                                   Version:                                    </td>
                                  <td align="left" valign="middle">
                                <?php echo $rec['version'];?>                           </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass" valign="top">
                                   State:                                    </td>
                                  <td align="left" valign="middle">
                               <?php echo $rec['state'];?>                                      </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass" valign="top">
                                   Year:                                    </td>
                                  <td align="left" valign="middle">
                               <?php echo $rec['year'];?>                       </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								
								<tr>
                                  <td class="labelclass">
                                    Price:                                    </td>
                                  <td align="left" valign="middle">
                                    <?php echo $rec['price'];?>           </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr> 
								<tr>
                                  <td class="labelclass">
                                    Registration#:                                    </td>
                                  <td align="left" valign="middle">
                                    <?php echo (isset($rec['reg_no']))?$rec['reg_no']:""?>                             </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Kilo Meters:                                    </td>
                                  <td align="left" valign="middle">
                                  <?php echo (isset($rec['kilometer']))?$rec['kilometer']:""?>                               </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Colors:                                    </td>
                                  <td align="left" valign="middle">
                                    <?php echo (isset($rec['color']))?$rec['color']:""?>                                </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Transmission:                                    </td>
                                  <td align="left" valign="middle">
                                  <?php echo (isset($rec['transmission']))?$rec['transmission']:""?>                          </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Gears:                                    </td>
                                  <td align="left" valign="middle">
                                   <?php echo (isset($rec['gear']))?$rec['gear']:""?>                           </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Fuel Type:                                    </td>
                                  <td align="left" valign="middle">
                                   <?php echo (isset($rec['fuel_type']))?$rec['fuel_type']:""?>                     </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Seating Capacity:                                    </td>
                                  <td align="left" valign="middle">
                                   <?php echo (isset($rec['seating_capacity']))?$rec['seating_capacity']:""?>                     </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Owner:                                    </td>
                                  <td align="left" valign="middle">
                                   <?php echo (isset($rec['owner']))?$rec['owner']:""?>                              </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Insurance:                                    </td>
                                  <td align="left" valign="middle">
                                  <?php echo (isset($rec['insurance']))?$rec['insurance']:""?>                               </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Tax:                                    </td>
                                  <td align="left" valign="middle">
                                   <?php echo (isset($rec['tax']))?$rec['tax']:""?>                              </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Show On Home:                                    </td>
                                  <td align="left" valign="middle">
                                 <?php echo ($rec['show_home']==1)?"Yes":"No"?>                       </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr> 
								  <tr>
                                  <td class="labelclass">
                                  Main Image:                                    </td>
                                  <td align="left" valign="middle">
                                  <?php
									 if($rec['car_image']!="")
									 {
									 	?> 
									<img src="<?php echo CAR_IMAGE_PATH_SHOW.$rec['car_image']?>" width="150"   />	
										
										<?php
									 } 
									 
									 ?>  </td>
                                  <td rowspan="3" align="left" valign="middle">&nbsp;
								 
								  
								  </td>
                                  <td width="2%" align="left" valign="middle">&nbsp;</td>
								  </tr>
                                  <tr>
								<td height="10"></td>
								</tr>
							 
								<tr>
                                  <td align="left" height="10" colspan="2" style="font-size:18px"><strong>Upload Additional Images </strong></td>
                                </tr>
								
                                <tr>
                                  <td align="left" colspan="2">
								  <?php 
								$allowed = array('jpg','JPG'); 
								$car_id=$rec['car_id'];
								$path=DOC_ROOT."admin/images/additional/".$car_id."/";
								$images=getFiles($path,$allowed);
								  ?>
								   <ul id="files" >
								   <?php 
								   if(!empty($images)){
								   foreach($images as $img) {
								   $showpath=WEB_ROOT."admin/images/additional/".$car_id."/".$img;
								    ?>
								   <li style="text-align:center">
								   <img   src="<?php echo WEB_ROOT?>SampleImage.php?src=<?php echo $showpath;?>&w=143&h=143&q=40&zc=0"  > <br /><input type="button" class="button" onclick="deleteImage(this.form,'<?php echo $img; ?>')" name="btnremove" value="Remove" />
								   </li>
								   <?php
								   }
								   
								   }?>
								   
								   </ul>  
								  </td>
                                </tr>
								<tr>
								<td>
								  <div class="upload" id="upload"  ><span>Upload Image<span></div>
								   <span id="status" >
								   </span>
								   
								   <script>
								$(function(){
								 
									var btnUpload=$('#upload');
									carid="<?php echo  $rec['car_id'];?>"; 
									
									serverpath="<?php echo WEB_ROOT?>admin/functions/"; 
									var status=$('#status');
									
									new AjaxUpload(btnUpload, {
									 
										action: serverpath+'upload-file.php?carid='+carid,
										 
										//Name of the file input box
										name: 'uploadfile',
										onSubmit: function(file, ext){
											if (! (ext && /^(jpg|jpeg)$/.test(ext))){
												  // check for valid file extension
												alert("Only JPG files are allowed")
												//status.text('Only JPG files are allowed');
												return false;
											}
											status.text('Uploading...');
										},
										onComplete: function(file, response){
											
											//On completion clear the status
											imgpath="<?php echo WEB_ROOT?>"+"admin/images/additional/"+carid+"/";
											status.text('Image Uploaded Successfully');
											  
											//Add uploaded file to list
											
											if(response!=""){ 
											 
													$('<li style="text-align:center"></li>').appendTo('#files').html('<img src="'+imgpath+response+'" alt="" width="143" height="143" /><br /><input type="button" class="button" onclick="deleteImage(this.form,\''+response+'\')" name="btnremove" value="Remove" />').addClass('');
												} else{
												 status.text('Error in Uploading');
											}
										}
									});
								});
								
								</script>
								   
								</td>
								</tr>
								 
								    
                                 
                                <tr>
                                  <td align="left" colspan="3">&nbsp;                                    </td>
                                  </tr>
                                </tbody></table>  
							 </form>  
                          </div>
                          
  </div>
                        <!--Middle Part End-->
                        </td>
                    </tr>
                    <tr>
                        <td height="25" class="fieldheader">
                            <img hspace="5" align="absmiddle" vspace="8" title="Back" alt="Back" src="<?php echo WEB_ROOT;?>admin/images/back-arw.gif">
                            <a href="<?php echo WEB_ROOT;?>admin/module/<?php echo MODULE_PAGE?>/index.php?view=list<?php echo isset($_GET['PageNum'])?"&PageNum=".$_GET['PageNum']:""?>">Back</a>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>
    <!--end middle-->

                                        <!--Middle End-->
                                    </td>
                                </tr>
                            </table>