<?php
if (!defined('WEB_ROOT')) {
			echo "You Cannot directly access this page";
			exit;
		}  
			$msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>
<table cellspacing="0" cellpadding="0" width="100%" border="0" height="100%">
                             <tr>
				   <td align="center"><?php echo getMessage($msgcode);?> </td>
			  
				   </tr>
						        <tr>

                                    <td class="contantpart" height="100%" valign="top">
                                        <!--Middle Start-->
 
    <div id="divLoading" class="loading">
        <div style="display: none;" id="ctl00_MainContent_Up1">
	
                
            
</div>
    </div>
    <!--middle-->
    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="height: 100%;">
        <tbody><tr>
            <td valign="top" colspan="3">
                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tbody><tr>
                        <td class="bottomborder">
                            <h1>
                                <div id="ctl00_MainContent_UpdatePanel6">
	
                                        <span id="ctl00_MainContent_lblMainTitle"><?php echo $Title;?></span>
                                    
</div>
                            </h1>
                        </td>
                        <td align="right" valign="bottom" class="bottomborder">
                            <div id="ctl00_MainContent_UpdatePanel1">
	
                                <a href="<?php echo WEB_ROOT;?>admin/module/<?php echo MODULE_PAGE?>/index.php?view=list<?php echo isset($_GET['PageNum'])?"&PageNum=".$_GET['PageNum']:""?>" title="Back to Event List" id="ctl00_MainContent_lbtnAdd"><img alt="Search" src="<?php echo WEB_ROOT;?>admin/images/back-arw.gif"> Back to Cars List</a>        
                                
</div>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="2">
                            <div id="ctl00_MainContent_UpdatePanel2">
	
                                    <table cellspacing="0" cellpadding="1" border="0">
                                        <tbody><tr>
                                            <td style="height: 32px;">&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                                
</div>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
        <tr>
            <td class="leftpart" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="dashbordboxhead"> <?php echo MODULE;?> List</td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellpadding="5" cellspacing="5" id="ctl00_MainContent_EditLeft1_rptEditList" style="border-collapse: collapse;">
                  <tbody>
                    
					<?php
					 
					if(count($latestrecord)>0) { $i=1;
					  foreach($latestrecord as $Lrec) { ?>
					<tr>
                      <td><span id="ctl00_MainContent_EditLeft1_rptEditList_ctl00_lblno"><?php echo $i;?>. </span> 
                        <a style="font-weight: normal;" href="<?php echo WEB_ROOT;?>admin/module/<?php echo MODULE_PAGE;?>/index.php?view=modify&Code=<?php echo $Lrec['car_id']?><?php echo isset($_GET['PageNum'])?"&PageNum=".$_GET['PageNum']:""?>" class="leftlink" id="ctl00_MainContent_EditLeft1_rptEditList_ctl00_lnkEdit"><?php 
						 
						if($Lrec['car_id']==$Code)
							echo "<strong>".$Lrec['reg_no']."</strong>";
						else
							echo  $Lrec['reg_no'] ;
						?></a></td>
                      </tr>
					  <?php $i++; } } ?>
                     
                    </tbody>
                  </table></td>
              </tr>
            </table>
                <!--Left Part Start-->
                <!--Left Part End-->
            </td>
            <td width="24">
               </td>
            <td valign="top">
                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                  <tr>
                    <td class="dashbordboxhead"><?php echo $Title;?></td>
                  </tr>
                    <tbody>
                    <tr>
                      <td valign="top" class="tableborder">
                        <!--Middle Part start-->
                        <div id="ctl00_MainContent_UpdatePanel4">
                          
                          <div   id="ctl00_MainContent_pnlData">
                            <form name="frm" method="post" enctype="multipart/form-data">
							<input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
							<input type="hidden" name="car_id" value="<?php echo $Code;?>" />
							<table cellspacing="1" cellpadding="2" border="0" align="center" width="98%">
                              <tbody><tr>
                                <td width="17%">&nbsp;                                  </td>
                                <td width="65%" >
                                  <div style="float:left" align="center">
								  
								  <!--Message Display End--> 
								  </div>								  </td>
                                <td width="16%" >&nbsp;</td>
                              </tr> 
                                <tr>
                                  <td class="labelclass" valign="top">
                                   Make:                                    </td>
                                  <td align="left" valign="middle">
                               <?php 
							    	  $makelist=getMake();
									
									  echo funcCombo($makelist,"make","make","make",$rec['make'],$Extra);
								   ?> 
								   <input type="text" name="make_other" value="Other"  
								   onfocus="javascript:this.value=(this.value=='Other')?'':this.value"
								   onblur="javascript:this.value=(this.value=='')?'Other':this.value"
								   
								    />
                                    <font class="red">*</font>
                                    <span class="red" style="visibility: hidden;" id="ctl00_MainContent_rfvfname">Required</span>                                    </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass" valign="top">
                                   Model:                                    </td>
                                  <td align="left" valign="middle">
                               <?php 
							    	$modellist=getModel();
									
									  echo funcCombo($modellist,"model","model","model",$rec['model'],$Extra);
								   ?> 
								   <input type="text" name="model_other" value="Other"  
								   onfocus="javascript:this.value=(this.value=='Other')?'':this.value"
								   onblur="javascript:this.value=(this.value=='')?'Other':this.value"
								   
								    />
                                    <font class="red">*</font>
                                    <span class="red" style="visibility: hidden;" id="ctl00_MainContent_rfvfname">Required</span>                                    </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass" valign="top">
                                   Version:                                    </td>
                                  <td align="left" valign="middle">
                               <?php 
							    	$versionlist=getVersion();
									
									  echo funcCombo($versionlist,"version","version","version",$rec['version'],$Extra);
								   ?> 
								   <input type="text" name="version_other" value="Other"  
								   onfocus="javascript:this.value=(this.value=='Other')?'':this.value"
								   onblur="javascript:this.value=(this.value=='')?'Other':this.value"
								   
								    />
                                    <font class="red">*</font>
                                    <span class="red" style="visibility: hidden;" id="ctl00_MainContent_rfvfname">Required</span>                                    </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass" valign="top">
                                   State:                                    </td>
                                  <td align="left" valign="middle">
                               <?php 
							    	$statelist=getState();
									
									  echo funcCombo($statelist,"state","state","state",$rec['state'],$Extra);
								   ?> 
								   <input type="text" name="state_other" value="Other"  
								   onfocus="javascript:this.value=(this.value=='Other')?'':this.value"
								   onblur="javascript:this.value=(this.value=='')?'Other':this.value"
								   
								    />
                                    <font class="red">*</font>
                                    <span class="red" style="visibility: hidden;" id="ctl00_MainContent_rfvfname">Required</span>                                    </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass" valign="top">
                                   Year:                                    </td>
                                  <td align="left" valign="middle">
                               <?php 
							    	$yearlist=getYear();
									
									  echo funcCombo($yearlist,"year","year","year",$rec['year'],$Extra);
								   ?> 
								   <input type="text" name="year_other" value="Other"  
								   onfocus="javascript:this.value=(this.value=='Other')?'':this.value"
								   onblur="javascript:this.value=(this.value=='')?'Other':this.value"
								   
								    />
                                    <font class="red">*</font>
                                    <span class="red" style="visibility: hidden;" id="ctl00_MainContent_rfvfname">Required</span>                                    </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								
								<tr>
                                  <td class="labelclass">
                                    Price:                                    </td>
                                  <td align="left" valign="middle">
                                    <input type="text"   class="re-input" id="ctl00_MainContent_txtMUSRXvarFnm0" maxlength="50" value="<?php echo (isset($rec['price']))?$rec['price']:""?>" name="price"  	>
                                                                      </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr> 
								<tr>
                                  <td class="labelclass">
                                    Registration#:                                    </td>
                                  <td align="left" valign="middle">
                                    <input type="text"   class="re-input" id="ctl00_MainContent_txtMUSRXvarFnm0" maxlength="50" value="<?php echo (isset($rec['reg_no']))?$rec['reg_no']:""?>" name="reg_no"  	>
                                                                      </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Kilo Meters:                                    </td>
                                  <td align="left" valign="middle">
                                    <input type="text"   class="re-input" id="ctl00_MainContent_txtMUSRXvarFnm0" maxlength="50" value="<?php echo (isset($rec['kilometer']))?$rec['kilometer']:""?>" name="kilometer"  	>
                                                                   </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Colors:                                    </td>
                                  <td align="left" valign="middle">
                                    <input type="text"   class="re-input" id="ctl00_MainContent_txtMUSRXvarFnm0" maxlength="50" value="<?php echo (isset($rec['color']))?$rec['color']:""?>" name="color"  	>
                                                             </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Transmission:                                    </td>
                                  <td align="left" valign="middle">
                                    <input type="text"  class="re-input" id="ctl00_MainContent_txtMUSRXvarFnm0" maxlength="50" value="<?php echo (isset($rec['transmission']))?$rec['transmission']:""?>" name="transmission"  	>
                                                </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Gears:                                    </td>
                                  <td align="left" valign="middle">
                                    <input type="text"   class="re-input" id="ctl00_MainContent_txtMUSRXvarFnm0" maxlength="50" value="<?php echo (isset($rec['gear']))?$rec['gear']:""?>" name="gear"  	>
                                                                      </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Fuel Type:                                    </td>
                                  <td align="left" valign="middle">
                                    <input type="text"  class="re-input" id="ctl00_MainContent_txtMUSRXvarFnm0" maxlength="50" value="<?php echo (isset($rec['fuel_type']))?$rec['fuel_type']:""?>" name="fuel_type"  	>
                                                                     </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Seating Capacity:                                    </td>
                                  <td align="left" valign="middle">
                                    <input type="text"  class="re-input" id="ctl00_MainContent_txtMUSRXvarFnm0" maxlength="50" value="<?php echo (isset($rec['seating_capacity']))?$rec['seating_capacity']:""?>" name="seating_capacity"  	>
                                                                     </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Owner:                                    </td>
                                  <td align="left" valign="middle">
                                    <input type="text"   class="re-input" id="ctl00_MainContent_txtMUSRXvarFnm0" maxlength="50" value="<?php echo (isset($rec['owner']))?$rec['owner']:""?>" name="owner"  	>
                                                                     </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Insurance:                                    </td>
                                  <td align="left" valign="middle">
                                    <input type="text"  class="re-input" id="ctl00_MainContent_txtMUSRXvarFnm0" maxlength="50" value="<?php echo (isset($rec['insurance']))?$rec['insurance']:""?>" name="insurance"  	>
                                                                   </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Tax:                                    </td>
                                  <td align="left" valign="middle">
                                    <input type="text"  class="re-input" id="ctl00_MainContent_txtMUSRXvarFnm0" maxlength="50" value="<?php echo (isset($rec['tax']))?$rec['tax']:""?>" name="tax"  	>
                                                                       </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr>
								<tr>
                                  <td class="labelclass">
                                    Show On Home:                                    </td>
                                  <td align="left" valign="middle">
                                  <input type="radio" name="show_home" value="0"  <?php echo ($rec['show_home']==0)?"checked":""?>/>No
								  <input type="radio" name="show_home" value="1"  <?php echo ($rec['show_home']==1)?"checked":""?> />Yes
                                                                  </td>
                                  <td align="left" valign="middle">&nbsp;</td>
                                </tr> 
								  <tr>
                                  <td class="labelclass">
                                   Upload Photo:                                    </td>
                                  <td align="left" valign="middle">
                                   <input type="file" class="re-input" id="ctl00_MainContent_txtMUSRXvarPas0" maxlength="20" name="car_image">                                    </td>
                                  <td rowspan="3" align="left" valign="middle">
								  <?php
									 if($rec['car_image']!="")
									 {
									 	?> 
									<img src="<?php echo CAR_IMAGE_PATH_SHOW.$rec['car_image']?>" width="150"   />	
										
										<?php
									 } 
									 
									 ?>
								  
								  &nbsp;</td>
                                  <td width="2%" align="left" valign="middle">&nbsp;</td>
								  </tr>
                                  
                                 
								    
                                <tr>
                                  <td>&nbsp;                                    </td>
                                  <td>
                                    <input type="submit" class="button" id="ctl00_MainContent_btnSave" onClick="return checkForm(this.form)" value="Save" name="ctl00$MainContent$btnSave">
                                    &nbsp;
                                    <input type="reset" class="button" value="Clear" id="ctl00_MainContent_Reset1" name="ctl00$MainContent$Reset1">                                    </td>
                                </tr>
                                <tr>
                                  <td align="left" colspan="3">&nbsp;                                    </td>
                                  </tr>
                                </tbody></table> 


							</form> 
                            </div>
                          
  </div>
                        <!--Middle Part End-->
                        </td>
                    </tr>
                    <tr>
                        <td height="25" class="fieldheader">
                            <img hspace="5" align="absmiddle" vspace="8" title="Back" alt="Back" src="<?php echo WEB_ROOT;?>admin/images/back-arw.gif">
                            <a href="<?php echo WEB_ROOT;?>admin/module/<?php echo MODULE_PAGE?>/index.php?view=list
							<?php echo isset($_GET['PageNum'])?"&PageNum=".$_GET['PageNum']:""?>">Back</a>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>
    <!--end middle-->

                                        <!--Middle End-->
                                    </td>
                                </tr>
                            </table>