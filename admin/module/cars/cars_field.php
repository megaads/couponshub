<?php
	if (!defined('WEB_ROOT')) {
			echo "You Cannot directly access this page";
			exit;
		}  
	  
	/*********Show Record Per Page*******************/ 
	 if(!$_POST && isset($_SESSION['Cars_RecPerPage']))
		$RecPerPage=$_SESSION['Cars_RecPerPage'];
	  else  
	  	$RecPerPage=(isset($_POST['recPerPage']))?$_POST['recPerPage']:SHOW_PER_PAGE; 
	  
	  $_SESSION['Cars_RecPerPage']=$RecPerPage;
	  
	  $PageNum=(isset($_POST['PageNum']))?$_POST['PageNum']:((isset($_GET['PageNum']))?$_GET['PageNum']:"1");
	/*********Show Record Per Page*******************/ 
	 $msg="";
	/*User Level*/
	
		$db=new databaseLayer(HOST,USER,PASS,DB); 
		$record="";	
		//checkUser();
	
	  $action = isset($_POST['txtMode']) ? $_POST['txtMode'] : '';
	 
	switch ($action) {
		
		case 'add' :
			add();
			break; 
		case 'update' :
			modify();
			break;
		case 'Active' :
				changeStatus('car_id',1,'S');
				break;
		case 'InActive' :
				changeStatus('car_id',0,'S');
				break; 
		case 'Delete' :
				changeStatus('car_id',2,'S');
				break; 	  
		case 'MActive' :
			changeStatus('car_id',1,'M');
			break;
		case 'MInActive' :
			changeStatus('car_id',0,'M');
			break;
		case 'MDelete' :
			changeStatus('car_id',2,'M');
			break;
		 case 'removeImage' :
					removeImage();
					break;
		 
	} 
	
	switch($view)
	{
		case 'list':
			$record=showRecord();
			break;
		case 'add': 
			$latestrecord=TOP10RECORD();	
		     	break;
		case 'modify': 
			  $Code= $_GET['Code'];
			$rec=getRecord($Code);  
			$latestrecord=TOP10RECORD();	
		     	break;
		case 'detail': 
			$Code= $_GET['Code'];
			$rec=getRecord($Code);  
			$latestrecord=TOP10RECORD();	
		     	break;				
		default:
			  $record=showRecord();
			break;
	
	}
	
	//function show record
	function showRecord()
	{ 
	    global $db;
		global $RecPerPage; 
		global $PageNum; 
		$condField=array();
		   //*********Search Criteria**********/
		  
		     
		   $fields=array();
		  if(isset($_POST['status']) && $_POST['status']!="-1" )
		  {
		  		
		  	$searchCond=" status=".$_POST['status'];
		  }
		  else
		  {
		  	$searchCond=" status<>2";
		  }
		   //*********Search Criteria**********/
		    
		   
		   $SortBy=(isset($_POST['txtSortBy']))?$_POST['txtSortBy']:"make";		
		   $SortOrder=(isset($_POST['txtSortOrder']))?$_POST['txtSortOrder']:"asc";					 
		   
		   $sql_paging="select count(*) as totRec from ".TABLE_NAME."    where  
		    $searchCond"; 
		   $PagLink=getPaging($sql_paging,$RecPerPage,$PageNum);
		 
		   $offset = ($PageNum - 1) * $RecPerPage; 
		 
		    $sql_query="select  car_id,make,model,version,year,state,price,status,created_date from ".TABLE_NAME." where $searchCond order by $SortBy $SortOrder limit $offset,$RecPerPage"; 
		   $record =$db->objSelect($sql_query, "ASSOCIATIVE") ;  
			 
		// Set Parameter Start**************/
		
		/***Set Field for Tables*/
		$UserColHeader=array("Make", "Model","Version","Year","State","Price","Add Date","Status","Details","Edit","Delete"); // Header  set this as your requirement
		  
		$UserColValue=array("make", "model","version","year","state","price","created_date","status","detail","edit","delete"); // Column name defined in database number of column should be same as Header
		  
		$extrafeatures=array('status',"detail",'edit','delete'); 
		 
		  $parameters=array( 
				 'header'=>$UserColHeader, 
				 'colnval'=>$UserColValue,
				 'EXTFEA'=>$extrafeatures, 
				 'sorting'=>array('Make'=>'make','Model'=>'model','Version'=>'version','Year'=>'year'), // set Sorting Field
				 'sortby'=>$SortBy, 
				 'sortorder'=>$SortOrder,
				 'PagLink'=>$PagLink,
				 'page'=>'module/'.MODULE_PAGE, // set module name
				 'colsize'=>array("2%","17%","15","12%","10%","15%",'"15%"',"4%","4%","4%","4%"), // set width of columns
				 'checkbox'=>array(  
		 				'allow'=>1,  // set allow =0 for no check box required and 1 for check box required
						'name'=>'chk', // set name for check box default name is chk No need to change
		 				'value'=>'car_id', // set column for value
					 ),
				  
		        );  
		 
		// Set Parameter End**************/
	 	$Recordtable=getTable($record,$parameters);
		 
		return $Recordtable;
	}
	
	// function top record
 	function TOP10RECORD()
	{
		//Show Record Per Page
	 	global $db; 
		 $sql_query="select car_id,reg_no from ".TABLE_NAME." where status <> 2 order by car_id desc limit ".TOP_RECORD_LIMIT; 
		 $latestRecord="";
		 $latestRecord =$db->objSelect($sql_query, "ASSOCIATIVE") ;  
		 return $latestRecord;
	}
	
	// function get record
	function getRecord($Code)
	{
		//Show Record Per Page
	    global $db; 
	 	    $sql_query="select * from ".TABLE_NAME."  where 
	  car_id='".$Code."'";  
		 $rec =$db->objSelect($sql_query, "ROW") ;  
		 return $rec;
	}
	
	function add()
	{
		global $db;
		global $msg;
		global $rec ;
		 
		// Insert into Horse table
		$fields=array();
		$fields=array('make','model','version','state','year','price','reg_no','kilometer',
		'color','transmission','gear','fuel_type','seating_capacity','owner','insurance',
		'tax','show_home');
		 
		 //exit;
		foreach($_POST as $key=>$val)
		{ 
			if(in_array($key,$fields))
			{
				if(substr_count($key,"date")>0)
				{  
				  if($val!="" && $val!="mm-dd-yyyy")
					  $rec[$key] = dateFormat($val,"Y-m-d");
				  else
				  		$rec[$key]="";	
				}
				else	
				{
					switch($key)
					{
						case "make":
							$val=($val!="")?$val:$_POST['make_other'];
							break;
						case "model":
							$val=($val!="")?$val:$_POST['model_other'];
							break;
						case "version":
							$val=($val!="")?$val:$_POST['version_other'];
							break;
						case "state":
							$val=($val!="")?$val:$_POST['state_other'];
							break;
						case "year":
							$val=($val!="")?$val:$_POST['year_other'];
							break;  
					}
						
						
								
				  $rec[$key] = addslashes(trim($val));
				}
			}	
		} 
		   
		   
		  if($_FILES['car_image']['name']!="")
		 {   
			 $allowed=array('jpg','JPG'); 
			 $Pre="";
			  $car_image= upload_file("car_image",CAR_IMAGE_PATH_UPLOAD, MAX_FILE_SIZE, $allowed,$Pre);
			 
			if($_FILES['car_image']['name']!="" && !$car_image)
			{
			 $msg="imgerr";
				return;
			}	
		}	  
		   
		  
		 $cols =implode(",",$fields).',created_date,car_image'; 
		 $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec))."',NOW(),'".$car_image."'";
	 
		$car_id =$db->objInsert(TABLE_NAME,$cols,$vals,"LASTID"); 
		   
		if($car_id>0)
		{ 	 
			  
			 header("location: index.php?view=list");
			 exit;
		}
		  
		  
	}
	// modify
	function modify()
	{
		global $db;
		global $msg; 
		global $view; 
		$fields=array('make','model','version','state','year','price','reg_no','kilometer',
		'color','transmission','gear','fuel_type','seating_capacity','owner','insurance',
		'tax','show_home');
		$vals1="";
		foreach($_POST as $key=>$val)
		{
			if(in_array($key,$fields))
			{  
				if(substr_count($key,"date")>0)
				{  
					 $vals1.=  "'".dateFormat($val,"Y-m-d")."'*@@*"; 
				}
				else	
				{	  
					 switch($key)
					{
						case "make":
							$val=($val!="")?$val:$_POST['make_other'];
							break;
						case "model":
							$val=($val!="")?$val:$_POST['model_other'];
							break;
						case "version":
							$val=($val!="")?$val:$_POST['version_other'];
							break;
						case "state":
							$val=($val!="")?$val:$_POST['state_other'];
							break;
						case "year":
							$val=($val!="")?$val:$_POST['year_other'];
							break;  
					} 
					 
					 $vals1.="'".addslashes(trim($val))."'*@@*";
				}
				  
			}	
		} 
		 
	 	$car_id=$_POST['car_id'];
	 
		if($car_id=="")
		{
			$msg="invalidcode";
			return;
		
		}	
		
		 if($_FILES['car_image']['name']!="")
		 {   
			 
			  $sql_query="select car_image from tbl_car where car_id=".$car_id;
			  $rec =$db->objSelect($sql_query, "ROW") ;   
			
			   $filePath=CAR_IMAGE_PATH_UPLOAD;
			  $fileName=$rec['car_image'];
			  
			 funcDeleteFile($filePath,$fileName); 
			 
			 $allowed=array('jpg','JPG','gif','GIF');  
			 $Pre="";
			    $car_image= upload_file("car_image",CAR_IMAGE_PATH_UPLOAD, MAX_FILE_SIZE, $allowed,$Pre);
			 
			if($_FILES['car_image']['name']!="" && !$car_image)
			{
			 $msg="imgerr";
				return;
			}	
		} 
		 
		 
		  $cols =implode(",",$fields)."";  
		  $vals = substr($vals1,0,strlen($vals1)-4)."";
		 if($_FILES['car_image']['name']!="")
		 {   
			$cols.=",car_image";
			$vals.="*@@*'".$car_image."'";
			
		}	 
		 
		 $Cond=" where car_id= ".$car_id;
		 $res =$db->objUpdate(TABLE_NAME,$cols,$vals,$Cond); 
		 
		 
		if($res)
		{ 
			if($_GET['PageNum']!="")
				$url="&PageNum=".$_GET['PageNum'];
			 
			header("location: index.php?view=list".$url);
			 exit;
		}  
	} 
	
		function removeImage()
		{
			 global $db;
			 global $msg;
			 $carid=$_POST['carid'];
			 
			 $imageName=$_POST['removeImage'];
			 
			$path=DOC_ROOT."admin/images/additional/".$carid."/";
			
			//echo $path,$imageName; 
			$msg=funcDeleteFile($path,$imageName);
			if($msg=="")
				$msg="imagedelete";
			else
				$msg="filenofound";	
			return;
		}
	 
	 
?>