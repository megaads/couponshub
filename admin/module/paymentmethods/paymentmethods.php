 <?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#frm").validate({
             rules: {
                name: "required",
                 description: "required",              
            },
            
        });                                     
          
    });
    </script>
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                          
                    </div>
                    <div class="box-content">
                         <form name="frm"  id="frm" class="form-horizontal"  method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            <input type="hidden" name="pm_id" value="<?php echo $Code;?>" />
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">Title</label>  
                              <div class="controls">
                                          <input type="text" class="re-input" id="name" maxlength="50" value="<?php echo (isset($rec['name']))?$rec['name']:""?>" name="name">
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Description</label>
                              <div class="controls">
                                 <textarea name="description" id="description"><?php echo (isset($rec['description']))?$rec['description']:""?></textarea>
                                 
                              </div>
                            </div>
                                   
                            
                            <div class="control-group">
                                <label class="control-label">Status</label>
                                <div class="controls">
                                  <label class="radio">
                                       <input type="radio" name="status" value="1" <?php echo (isset($rec['status']) && $rec['status']==1)?"checked":""?> checked="checked" />Active
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" name="status" value="0" <?php echo (isset($rec['status']) && $rec['status']==0)?"checked":""?> />InActive
                                  </label>
                                </div>
                              </div>
                            
                                                                 
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save changes</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>
<ul id="tlyPageGuide" data-tourtitle="Do you need Help?">
              <!-- use these classes to indicate where each number bubble should be
              positioned. data-tourtarget should contain the selector for the element to
              which you want to attach this pageguide step. -->
              <li class="tlypageguide_left" data-tourtarget="#name">
               Set Payment method Title Here Like Paytm,Mobikwik, WireTransfer etc.
               
              </li>
              <li class="tlypageguide_left" data-tourtarget="#description">
               Set which information do you need from members for transfer cashback money <br>
               Example : If you set paytm as paymemt method then you can add below description here
               "Please enter your registered paytm mobile number."
               
              </li>
              </ul>