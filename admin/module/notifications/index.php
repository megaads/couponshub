<?php
	 	 
		define('TABLE_NAME','tbl_notification'); // define Table Name
		define('MODULE','Notifications'); // define Table Name
		define('MODULE_PAGE','notifications'); // define Table Name
		$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';	
	 
		/*** Required Files ****************/
		require_once("../../system/constant.php"); 
		require_once("../../system/databaseLayer.php"); 
		require_once("../../functions/common.php"); 
		require_once("../../functions/listfunction.php"); 
         require_once("../../functions/commonfun.php");    
	    /*****Check User Authentication*************/
		if(isset($_POST['txtMode']) && $_POST['txtMode']=="getnotification" && !isset($_SESSION['CARE_ADMIN_ID']))
        {
             echo "Session Out";exit;     
        } 
         
         
         checkUser();
		/*****Check User Authentication*************/ 
	
		require_once("notifications_field.php"); 			
		/*** Required Files ****************/ 
		 
		
	  
	/**** Page *****************************/
	switch ($view) {
		case 'list' :
			$content 	= MODULE_PAGE.'list.php';
			$Title 	= MODULE.' List';		
			$pageTitle 	= SITE_NAME.' Admin Control Panel - View '.MODULE;
			break;
	
		case 'add' :
			$content 	= MODULE_PAGE.'.php';
			$Title 	= 'Add '.MODULE;	
			$mode="add";		
			$pageTitle 	= SITE_NAME.' Admin Control Panel - Add '.MODULE;
			break;
	
		case 'modify' :
			$content 	= MODULE_PAGE.'.php';
			$Title 	= 'Edit '.MODULE;
			$mode="update";		
			$pageTitle 	= SITE_NAME.' Admin Control Panel - Modify '.MODULE;
			break;
	
		case 'detail' :
			$content    = MODULE_PAGE.'detail.php';
			$Title 	= MODULE.' Detail';
			$pageTitle  = SITE_NAME.' Admin Control Panel - View '.MODULE.' Detail';
			break;
			
		default :
			$content 	= MODULE_PAGE.'list.php';
			$Title 	= MODULE.' List';			
			$pageTitle 	= SITE_NAME.' Admin Control Panel - View '.MODULE;
	}
	/**** Page *****************************/
	  
	/**** Javascript File InClude  *****************************/
	$script    = array('check.js','common.js','jquery-1.10.2.js','jquery.validate.js','tinymce/tinymce.min.js','retailers.js');         
	/**** Javascript File InClude  *****************************/ 
	
	 /**** Common Template  *****************************/  
	 require_once '../../inc/template.php';
	/**** Common Template   *****************************/  
?>
