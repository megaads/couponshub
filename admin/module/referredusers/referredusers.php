<?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>

<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#userfrm").validate({
                 rules: {
                name: "required",
                 city: "required",  
                 country: "required",  
                  phone: "required",     
               <?php if($mode!='update'): ?>
                password: {
                    required: true,
                    minlength: 5    
                },
                <?php endif;?>
                     
                email: {
                    required: true,
                    email: true
                },
                     
                
            },
                         
            
        });                                     
          
    });
    </script>
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                        <div class="box-icon">
                            <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                            <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                         <form name="frm"  id="userfrm" class="form-horizontal"  method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            <input type="hidden" name="user_id" value="<?php echo $Code;?>" />
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">User Level</label>  
                              <div class="controls">
                                          <select id="user_level" name="user_level" data-rel="chosen">
                                             <option value="">Select Privilege</option>
                                        <option <?php echo (isset($rec['user_level']) && $rec['user_level']==0)?"selected":""?> value="0" selected="selected">User</option>
                                        <option <?php echo (isset($rec['user_level']) && $rec['user_level']==1)?"selected":""?> value="1"  >Admin</option> 
                                        </select>
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Name</label>
                              <div class="controls">
                                <input type="text" class="input"   id="name" maxlength="50" value="<?php echo (isset($rec['name']))?$rec['name']:""?>" name="name">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">City</label>
                              <div class="controls">
                                <input type="text" class="input"   id="city" maxlength="50" value="<?php echo (isset($rec['city']))?$rec['city']:""?>" name="city">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Country</label>
                              <div class="controls">
                                <input type="text" class="input"   id="country" maxlength="50" value="<?php echo (isset($rec['country']))?$rec['country']:""?>" name="country">
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Phone#</label>
                              <div class="controls">
                                <input type="text" class="re-input"   id="phone" maxlength="50" value="<?php echo (isset($rec['phone']))?$rec['phone']:""?>" name="phone">
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Email Id</label>
                              <div class="controls">
                               <input type="email"   class="re-input" id="email" maxlength="50" value="<?php echo (isset($rec['email']))?$rec['email']:""?>" name="email">
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Password:</label>
                              <div class="controls">
                                <input type="password"   class="re-input" id="password" maxlength="50" value="<?php echo (isset($rec['password']))?$rec['password']:""?>" <?php echo ($mode=='update')?"readonly":""?> name="password">
                                 
                              </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label">Status</label>
                                <div class="controls">
                                  <label class="radio">
                                       <input type="radio" name="status" value="1" <?php echo (isset($rec['status']) && $rec['status']==1)?"checked":""?> checked="checked" />Active
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" name="status" value="0" <?php echo (isset($rec['status']) && $rec['status']==0)?"checked":""?> />InActive
                                  </label>
                                </div>
                              </div>
                            
                                                                 
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save changes</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>