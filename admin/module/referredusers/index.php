<?php
	 	 
		define('TABLE_NAME','tbl_user'); // define Table Name
		define('MODULE','Referred Users'); // define Table Name
		define('MODULE_PAGE','referredusers'); // define Table Name
		$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';	
	 
		/*** Required Files ****************/
		require_once("../../system/constant.php"); 
		require_once("../../system/databaseLayer.php"); 
		require_once("../../functions/common.php"); 
		require_once("../../functions/listfunction.php"); 
	    /*****Check User Authentication*************/
		 checkUser();
		/*****Check User Authentication*************/ 
	
		require_once("referredusers_field.php"); 			
		/*** Required Files ****************/ 
		 
		
	  
	/**** Page *****************************/
	switch ($view) {
		case 'list' :
			$content 	= MODULE_PAGE.'list.php';
			$Title 	= MODULE.' List';		
			$pageTitle 	= SITE_NAME.' Admin Control Panel - View '.MODULE;
			break;
	
		                     
			
		default :
			$content 	= MODULE_PAGE.'list.php';
			$Title 	= MODULE.' List';			
			$pageTitle 	= SITE_NAME.' Admin Control Panel - View '.MODULE;
	}
	/**** Page *****************************/
	  
	/**** Javascript File InClude  *****************************/
	$script    = array('check.js','common.js','jquery-1.10.2.js','jquery.validate.js','users.js');
	/**** Javascript File InClude  *****************************/ 
	
	 /**** Common Template  *****************************/  
	 require_once '../../inc/template.php';
	/**** Common Template   *****************************/  
?>
