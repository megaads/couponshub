<?php
        if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>
<div id="content" class="span10"> 
<?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
 <div class="row-fluid sortable"> 
   
  <div class="box span12">
  <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white user"></i><span class="break"></span><?php echo $Title; ?></h2>
                              
                    </div>
                      <div class="box-content"> 
                        <form name="frm" method="post" action="module/<?php echo MODULE_PAGE;?>/index.php?view=list">
      <!--Hidden Fields Start-->
      <input type="hidden" name="txtMode" value=""  />
      <input type="hidden" name="txtCode" value=""  />  
      <!--Record Show Per Page-->
           <?php include_once "../../inc/recperpage.inc";?>
                   <a style="float: right" href="module/<?php echo MODULE_PAGE;?>/index.php?view=add" title="Add <?php echo MODULE;?>" id="ctl00_MainContent_lbtnAdd"><img alt="Search" src="<?php echo WEB_ROOT;?>admin/images/ico-add.gif"> Add <?php echo MODULE;?></a> 
        <!--Record Show Per Page-->
        
        <div   class="vbmenu_popup" id="ctl00_MainContent_rolloverdivsearch"> <br>
              <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tbody>
                <tr class="fieldheader-bg">
                  <td valign="bottom">
                    <table cellspacing="0" cellpadding="0" border="0" align="right" width="100%">
                      <tbody><tr>
                        <td valign="bottom"> <img height="11" width="1" alt=" " src="<?php echo WEB_ROOT;?>admin/images/blue-sk.gif"></td>
                        <td> <font class="field-header"> <img align="top" title="Search Coupon" alt="Search Coupon" src="<?php echo WEB_ROOT;?>admin/images/ico-serach.gif"> Search Coupon </font> </td>
                        <td align="right" valign="bottom"> </td>
                        <td align="right" width="7" valign="bottom"> <img height="11" width="1" alt=" " src="<?php echo WEB_ROOT;?>admin/images/blue-sk.gif"></td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
                <tr>
                  <td class="fieldset-b">
                  
                    <!--Search Criteria Start-->
                    <table cellspacing="1" cellpadding="1" border="0" align="center" width="98%">
                      <tbody>
                      <tr>
                        <td align="center" colspan="6">
                          <span class="success" id="ctl00_MainContent_lblGeneral"></span>
                        </td>
                      </tr>
                      <tr>
                        <td align="left" colspan="3"> <font class="error">NOTE:</font> Enter few letters for any or all criteria.</td>
                        <td align="right" colspan="3"> </td>
                      </tr>
                      <tr>
                        
                         <td width="10%"> <b>Retailers:</b> </td>
                        <td>
                             <?php 
                                    $retailerlist=getRetailers();
                                                                 
                                      echo funcCombo($retailerlist,"name","retailer_id","retailer_id",$_POST['store_id'],$Extra);
                                   ?> 
                        </td>   
                         
                      </tr>
                      <tr>
                       <td width="10%"> <b>Transaction Date:</b> </td>
                        <td>
                          <input type="text"   id="transaction_date" name="transaction_date" value="<?php echo (isset($_POST['transaction_date']))?$_POST['transaction_date']:""?>">
                        </td>   
                         <td nowrap="nowrap" align="left" width="10%"> <b>Current Status:</b></td>
                        <td>
                           <select style="width: 130px;"  id="ctl00_MainContent_ddlMUSRXintPri0" name="ticket_status">
                                        <option <?php echo ($_POST['ticket_status']=="-1" || $_POST['ticket_status']=="")?"selected":""?> value="-1"  selected="selected">All</option>
                                        <option <?php echo (isset($_POST['ticket_status']) && $_POST['ticket_status']=='New')?"selected":""?> value="New"  >New</option>
                                        <option <?php echo (isset($_POST['ticket_status']) && $_POST['ticket_status']=='Sent To Retailer')?"selected":""?> value="Sent To Retailer"  >Sent To Retailer</option>
                                        <option <?php echo (isset($_POST['ticket_status']) && $_POST['ticket_status']=='Declined')?"selected":""?> value="Declined">Declined</option> 
                                        <option <?php echo (isset($_POST['ticket_status']) && $_POST['ticket_status']=='Resolved')?"selected":""?> value="Resolved">Resolved</option> 
                            </select>
                        </td>                   
                        <td nowrap="nowrap" align="left" width="10%"> <b>Status:</b></td>
                        <td>
                           <select style="width: 130px;"  id="ctl00_MainContent_ddlMUSRXintPri0" name="status">
                                        <option <?php echo ($_POST['status']=="-1" || $_POST['status']=="")?"selected":""?> value="-1"  selected="selected">All (Exclude Deleted)</option>
                                        <option <?php echo (isset($_POST['status']) && $_POST['status']==1)?"selected":""?> value="1"  >Active</option>
                                        <option <?php echo (isset($_POST['status']) && $_POST['status']==0)?"selected":""?> value="0"  >InActive</option>
                                        <option <?php echo (isset($_POST['status']) && $_POST['status']==2)?"selected":""?> value="2">Deleted</option> 
                            </select>
                        </td>
                        </tr>
                         
                        <tr>
                        <td nowrap="nowrap" align="left" width="9%"> </td>
                        <td> 
                        </td>
                        <td>&nbsp; </td>
                        <td>&nbsp; </td>
                      </tr>
                      <tr>
                        <td align="center" style="height: 28px;" colspan="6">
                         <button type="button" onclick="return doSearch(this.form)" class="btn btn-primary">Search</button>    
                          &nbsp;
                           <button type="button" onclick="showAllRecord(this.form)" class="btn btn-primary">Show All</button>    
                        </td>
                      </tr>
                    </tbody></table>
                    <!--Search Criteria End-->
                    
                  </td>
                </tr>
              </tbody></table>
            </div>
        
        
        
        
                       <?php echo  $record;?>   
              <?php if((isset($_POST['status']) && $_POST['status']==2)) {  ?>
                        <button type="button" onclick="doMultiAction(document.frm,'Restore','chk[]')" class="btn btn-primary">Restore</button>   
                        
                         <?php }else{  ?>
                         <button type="button" onclick="doMultiAction(document.frm,'Delete','chk[]')" class="btn btn-primary">Delete</button>   
                         
                          <?php } ?>
                          <button type="button" onclick="doMultiAction(document.frm,'Active','chk[]')" class="btn btn-primary">Active</button>   
                          <button type="button" onclick="doMultiAction(document.frm,'InActive','chk[]')" class="btn btn-primary">InActive</button>   
                          
                          
                          <input type="hidden" name="hdnID" id="hdnID">          
                       
                       </form>             
                             </div>
                            </div>
</div>
</div>
