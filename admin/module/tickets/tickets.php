 <?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>    
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#frm").validate({
                 rules: {
                user_id: "required",
                 retailer_id: "required",  
                 transaction_date: "required",  
                  ticket_status: "required",
                
            },
                         
            
        });                                     
          
    });
 
      $(function() {
    $( "#transaction_date" ).datepicker();
         
  });
    
    </script>
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                        <div class="box-icon">
                            <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                            <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                    <form name="frm" id="frm"   method="post"  class="form-horizontal" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            <input type="hidden" name="ticket_id" value="<?php echo $Code;?>" />
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">  User Id                                 </label>  
                              <div class="controls">
                                              <input type="text" class="re-input" id="ctl00_MainContent_txtMUSRXvarFnm0" maxlength="50" value="<?php echo (isset($rec['user_id']))?$rec['user_id']:$_REQUEST['user_id']?>" name="user_id">  
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01"> Retailer</label>
                              <div class="controls">
                                   <?php  $retailerlist=getRetailers();
                                                                 
                                      echo funcCombo($retailerlist,"name","retailer_id","retailer_id",$rec['retailer_id'],$Extra);
                                   ?> 
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Transaction Amount</label>
                              <div class="controls">
                               <input type="text" class="re-input" id="transaction_amount" maxlength="50" value="<?php echo (isset($rec['transaction_amount']))?$rec['transaction_amount']:""?>" name="transaction_amount">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01"> Transaction Date</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="transaction_date" maxlength="50" value="<?php echo (isset($rec['transaction_date']))?$rec['transaction_date']:""?>" name="transaction_date">
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Description</label>
                              <div class="controls">
                                <textarea name="description" class="description"><?php echo (isset($rec['description']))?$rec['description']:""?></textarea>
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Reference  Image</label>
                              <div class="controls">
                              <input type="file" class="re-input" id="ticket_image"  name="ticket_image">                                    </td>
                                  <td rowspan="3" align="left" valign="middle">
                                  <?php
                                     if($rec['ticket_image']!="")
                                     {
                                         ?> 
                                    <img src="<?php echo TICKET_IMAGE_PATH_SHOW.$rec['ticket_image']?>" width="150"   />    
                                        
                                        <?php
                                     } 
                                     
                                     ?>
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01"> Current Status</label>
                              <div class="controls">
                                <select style="width: 130px;" class="re-input" id="ticket_status" name="ticket_status">
                                        <option value="">Select Status</option>
                                        <option <?php echo (isset($rec['ticket_status']) && $rec['ticket_status']=='New')?"selected":""?> value="New" >New</option>
                                         <option <?php echo (isset($rec['ticket_status']) && $rec['ticket_status']=='Sent To Retailer')?"selected":""?> value="Sent To Retailer" >Sent To Retailer</option>
                                          <option <?php echo (isset($rec['ticket_status']) && $rec['ticket_status']=='Declined')?"selected":""?> value="Declined" >Declined</option>
                                        <option <?php echo (isset($rec['ticket_status']) && $rec['ticket_status']=='Resolved')?"selected":""?> value="1"  >Resolved</option> 
                                        </select>
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Reason</label>
                              <div class="controls">
                                 <textarea name="reason" class="reason"><?php echo (isset($rec['reason']))?$rec['reason']:""?></textarea>        
                                 
                              </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Status</label>
                                <div class="controls">
                                  <label class="radio">
                                       <input type="radio" name="status" value="1" <?php echo (isset($rec['status']) && $rec['status']==1)?"checked":""?> checked="checked" />Active
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" name="status" value="0" <?php echo (isset($rec['status']) && $rec['status']==0)?"checked":""?> />InActive
                                  </label>
                                </div>
                              </div>
                            
                                                                 
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save changes</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>