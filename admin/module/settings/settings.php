 <?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>
 
 <script>tinymce.init({
  selector: '.description',
  height: 300,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools'
  ],
  toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons',
  image_advtab: true,
       
        
 });</script>
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#frm").validate({
                 rules: {
                name: "required",
                 store_id: "required",  
                 url: "required",  
                 
                
            },
                         
            
        });                                     
          
    });
 
      $(function() {
    $( "#start_date" ).datepicker();
     $( "#end_date" ).datepicker();    
  });
    
    </script>
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>General</h2>
                          
                    </div>
                    <div class="box-content">
                         <form name="frm"  id="frm" class="form-horizontal"  method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />                                                                    
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">Title</label>  
                              <div class="controls">
                             <input type="text" class="re-input" id="name" size="100" value="<?php echo (isset($rec['name']))?$rec['name']:""?>" name="name">
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Path</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="identifier" size="100" value="<?php echo (isset($rec['identifier']))?$rec['identifier']:""?>" name="identifier">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01"> Coupon Code</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="coupon_code" maxlength="50" value="<?php echo (isset($rec['coupon_code']))?$rec['coupon_code']:""?>" name="coupon_code">
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Image</label>
                              <div class="controls">
                              <input type="file" class="re-input" id="coupon_image"  name="coupon_image">
                                <?php
                                     if($rec['coupon_image']!="")
                                     {
                                         ?> 
                                    <img src="<?php echo COUPON_IMAGE_PATH_SHOW.$rec['coupon_image']?>" width="150"   />    
                                        
                                        <?php
                                     } 
                                     
                                     ?>   
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Url</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="url" size="100" value="<?php echo (isset($rec['url']))?$rec['url']:""?>" name="url">
                                <em>Example : http://tracking.icubeswire.com/aff_c?offer_id=x&aff_id=xyz&<b>aff_sub={USERID}</b>
                                    <br>where   aff_sub={USERID} is a Sub Parameter to track your your user
                                    
                                    </em>     
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Start Date</label>
                              <div class="controls">
                                <input type="text" class="re-input"   size="100" id="start_date" value="<?php echo (isset($rec['start_date']))?$rec['start_date']:""?>" name="start_date">
                                 
                              </div>
                            </div>      
                             <div class="control-group">
                              <label class="control-label" for="date01">Expiry Date</label>
                              <div class="controls">
                                 <input type="text" class="re-input" id="end_date" size="100" value="<?php echo (isset($rec['end_date']))?$rec['end_date']:""?>" name="end_date">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Description</label>
                              <div class="controls">
                                <textarea name="description" class="description"><?php echo (isset($rec['description']))?$rec['description']:""?></textarea>
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Meta Title</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="meta_title"  size="100" value="<?php echo (isset($rec['meta_title']))?$rec['meta_title']:""?>" name="meta_title">
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Meta Keyword</label>
                              <div class="controls">
                                <textarea name="meta_keyword" cols="60" id="meta_keyword"><?php echo (isset($rec['meta_keyword']))?$rec['meta_keyword']:""?></textarea>
                                 
                              </div>
                            </div>
                            
                             <div class="control-group">
                              <label class="control-label" for="date01">Meta Description</label>
                              <div class="controls">
                                <textarea name="meta_description"  cols="60" id="meta_description"><?php echo (isset($rec['meta_description']))?$rec['meta_description']:""?></textarea>
                                 
                              </div>
                            </div>
                            
                             <div class="control-group">
                              <label class="control-label" for="date01">Sort Order</label>
                              <div class="controls">
                              <input type="text" class="re-input" id="ctl00_MainContent_txtMUSRXvarFnm0" size="100" value="<?php echo (isset($rec['sort_order']))?$rec['sort_order']:""?>" name="sort_order">
                                 
                              </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Featured</label>
                                <div class="controls">
                                  <label class="radio">
                                       <input type="radio" name="is_featured" value="1" <?php echo (isset($rec['is_featured']) && $rec['is_featured']==1)?"checked":""?> checked="checked" />Yes
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" name="is_featured" value="0" <?php echo (isset($rec['is_featured']) && $rec['is_featured']==0)?"checked":""?> />No
                                  </label>
                                </div>
                              </div>  
                            
                            
                            <div class="control-group">
                                <label class="control-label">Status</label>
                                <div class="controls">
                                  <label class="radio">
                                       <input type="radio" name="status" value="1" <?php echo (isset($rec['status']) && $rec['status']==1)?"checked":""?> checked="checked" />Active
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" name="status" value="0" <?php echo (isset($rec['status']) && $rec['status']==0)?"checked":""?> />InActive
                                  </label>
                                </div>
                              </div>
                            
                                                                 
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save changes</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>