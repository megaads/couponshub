 <?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
    $currency=getConfiguration('currency');   
        ?>
 
 <script>tinymce.init({
  selector: '.maintenance_content',
  height: 300,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools'
  ],
  toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons',
  image_advtab: true,
       
        
 });</script>      
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#frm").validate({
                 rules: {
                website_title: "required",
                 website_url: "required",  
                 min_payout: "required",  
                 admin_email:"required",
                
            },
                         
            
        });                                     
          
    });   
    
    </script>
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
<form name="frm"  id="frm" class="form-horizontal"  method="post" enctype="multipart/form-data" > 
                                 <div style="float: right; margin-bottom: 10px;">
                              <button type="submit" class="btn btn-primary">Save changes</button>
                             
                            </div>
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>General</h2>
                       <div class="box-icon">
                            
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                          
                        </div>   
                    </div>
                    <div class="box-content">
                         
                            <input type="hidden" name="txtMode" value="update" />                                                                    
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">Site Name</label>  
                              <div class="controls">
                             <input type="text" class="re-input" id="website_title"   value="<?php echo (isset($rec['website_title']))?$rec['website_title']:""?>" name="data[website_title]">
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Home Page Title</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="home_page_title"   value="<?php echo (isset($rec['home_page_title']))?$rec['home_page_title']:""?>" name="data[home_page_title]">
                                 
                              </div>
                            </div>
                            
                             <div class="control-group">
                              <label class="control-label" for="date01">Upload Logo</label>
                              <div class="controls">
                                <input type="file" class="re-input" id="logo"  name="logo">
                                   <?php
                                     if($rec['website_logo']!="")
                                     {
                                         ?> 
                                    <img src="<?php echo LOGO_IMAGE_PATH_SHOW.$rec['website_logo']?>" width="150"   />    
                                        
                                        <?php
                                     } 
                                     
                                     ?>
                              </div>
                            </div>
                            
                          <div class="control-group">
                              <label class="control-label" for="date01">Upload Favicon</label>
                              <div class="controls">
                                <input type="file" class="re-input" id="favicon"  name="favicon">
                                   <?php
                                     if($rec['website_favicon']!="")
                                     {
                                         ?> 
                                    <img src="<?php echo LOGO_IMAGE_PATH_SHOW.$rec['website_favicon']?>"    />    
                                        
                                        <?php
                                     } 
                                     
                                     ?>
                              </div>
                            </div>  
                            
                            
                             <div class="control-group">
                              <label class="control-label" for="date01">Home Page Meta keyword </label>
                              <div class="controls">
                                   <textarea name="data[home_page_meta_keyword]" id="home_page_meta_keyword"><?php echo (isset($rec['home_page_meta_keyword']))?$rec['home_page_meta_keyword']:""?></textarea>
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Home Page Meta Description</label>
                              <div class="controls">
                                  <textarea name="data[home_page_meta_description]" id="home_page_meta_description"><?php echo (isset($rec['home_page_meta_description']))?$rec['home_page_meta_description']:""?></textarea>
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Site Url</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="website_url"   value="<?php echo (isset($rec['website_url']))?$rec['website_url']:""?>" name="data[website_url]">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Secure Site Url</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="secure_site_url"   value="<?php echo (isset($rec['secure_site_url']))?$rec['website_url']:""?>" name="data[secure_site_url]">
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Enable Secure Mode</label>
                              <div class="controls">
                                     <select id="site_mode" name="data[site_mode]" data-rel="chosen">
                                     <option <?php echo (isset($rec['secure_site_enable']) && $rec['secure_site_enable']=="0")?"selected":""?> value="0" selected="selected"  >No</option> 
                                     <option <?php echo (isset($rec['secure_site_enable']) && $rec['secure_site_enable']=="1")?"selected":""?> value="1" >Yes</option>
                                        
                                        </select>
                              </div>
                            </div>                
                            <div class="control-group">
                              <label class="control-label" for="date01">Site Mode</label>
                              <div class="controls">
                                     <select id="site_mode" name="data[site_mode]" data-rel="chosen">
                                     <option <?php echo (isset($rec['site_mode']) && $rec['site_mode']=="Live")?"selected":""?> value="Live" selected="selected">Live</option>
                                        <option <?php echo (isset($rec['site_mode']) && $rec['site_mode']=="Maintenance")?"selected":""?> value="Maintenance"  >Maintenance</option> 
                                        </select>
                              </div>
                            </div>                
                            
                             <div class="control-group">
                              <label class="control-label" for="date01">Site Maintenance Message</label>
                              <div class="controls">
                                      <textarea name="data[maintenance_content]" id="maintenance_content" class="maintenance_content"><?php echo (isset($rec['maintenance_content']))?$rec['maintenance_content']:""?></textarea>
                              </div>
                            </div>   
                            
                            <div class="control-group">
                              <label class="control-label" for="date01">Copyright</label>
                              <div class="controls">
                                     <input type="text" class="re-input" id="copyright"   value="<?php echo (isset($rec['copyright']))?$rec['copyright']:""?>" name="data[copyright]">
                              
                              </div>
                            </div>                                                                   
                            
                            <div class="control-group">
                              <label class="control-label" for="date01">Cashback Share</label>
                              <div class="controls">
                                     <input type="text" class="re-input" id="cashback_share"   value="<?php echo (isset($rec['cashback_share']))?$rec['cashback_share']:""?>" name="data[cashback_share]"> %
                              
                              </div>
                            </div>                                                      
                             <div class="control-group">
                              <label class="control-label" for="date01">Minimum Payout</label>
                              <div class="controls">
                                     <input type="text" class="re-input" id="min_payout"   value="<?php echo (isset($rec['min_payout']))?$rec['min_payout']:""?>" name="data[min_payout]">
                              <?php echo $currency;?>
                              </div>
                            </div>                                                     
                            <div class="control-group">
                             <label class="control-label" for="user_level">Enable Cashback Email</label>  
                              <div class="controls">
                               <select id="cashback_email" name="data[cashback_email]" data-rel="chosen">
                                     <option <?php echo (isset($rec['cashback_email']) && $rec['cashback_email']=="0")?"selected":""?> value="0" selected="selected">No</option>
                                        <option <?php echo (isset($rec['cashback_email']) && $rec['cashback_email']=="1")?"selected":""?> value="1"  >Yes</option> 
                                        </select>
                                  
                              </div>
                            </div>
                          </fieldset>
                       

                    </div>
                </div><!--/span-->

            </div><!--/row-->                         
            
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>Emails</h2>
                        <div class="box-icon">
                            
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                          
                        </div>       
                    </div>
                    <div class="box-content">
                         
                            <input type="hidden" name="txtMode" value="update" />                                                                    
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">Admin Email</label>  
                              <div class="controls">
                             <input type="text" class="re-input" id="admin_email"   value="<?php echo (isset($rec['admin_email']))?$rec['admin_email']:""?>" name="data[admin_email]">
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Support Email</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="support_email"   value="<?php echo (isset($rec['support_email']))?$rec['support_email']:""?>" name="data[support_email]">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">No Reply Email</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="noreply_email"   value="<?php echo (isset($rec['noreply_email']))?$rec['noreply_email']:""?>" name="data[noreply_email]">
                                 
                              </div>
                            </div>   
                             <div class="control-group">
                              <label class="control-label" for="date01">Email Logs</label>
                              <div class="controls">
                              <select id="email_log" name="data[email_log]" data-rel="chosen">
                                     <option <?php echo (isset($rec['email_log']) && $rec['email_log']=="1")?"selected":""?> value="1" selected="selected">Enable</option>
                                     <option <?php echo (isset($rec['email_log']) && $rec['email_log']=='0')?"selected":""?> value="0">Disable</option>
                                   
                                     </select>                 
                                 
                              </div>
                            </div>   
                          </fieldset>
                       

                    </div>
                </div><!--/span-->

            </div>   
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>Customer</h2>
                        <div class="box-icon">
                            
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                          
                        </div>       
                    </div>
                    <div class="box-content">
                         
                            <input type="hidden" name="txtMode" value="update" />                                                                    
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">Sign Up Email Activation</label>  
                              <div class="controls">
                               <select id="signup_email_activation" name="data[signup_email_activation]" data-rel="chosen">
                                     <option <?php echo (isset($rec['signup_email_activation']) && $rec['signup_email_activation']=="On")?"selected":""?> value="On" selected="selected">On</option>
                                        <option <?php echo (isset($rec['signup_email_activation']) && $rec['signup_email_activation']=="Off")?"selected":""?> value="Off"  >Off</option> 
                                        </select>
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Sign Up Bonus</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="signup_bonus" size="100" value="<?php echo (isset($rec['signup_bonus']))?$rec['signup_bonus']:""?>" name="data[signup_bonus]">
                                <?php echo $currency;?>           
                              </div>
                            </div>
                                
                          
                          </fieldset>
                       

                    </div>
                </div><!--/span-->

            </div>
            
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>SMTP Configuration</h2>
                        <div class="box-icon">
                            
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                          
                        </div>       
                    </div>
                    <div class="box-content">
                         
                            <input type="hidden" name="txtMode" value="update" />                                                                    
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">Enable SMTP Authentication</label>  
                              <div class="controls">
                               <select id="enable_smtp" name="data[enable_smtp]" data-rel="chosen">
                                     <option <?php echo (isset($rec['enable_smtp']) && $rec['enable_smtp']=="0")?"selected":""?> value="0" selected="selected">No</option>
                                        <option <?php echo (isset($rec['enable_smtp']) && $rec['enable_smtp']=="1")?"selected":""?> value="1"  >Yes</option> 
                                        </select>
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">SMTP Host</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="smtp_host" size="100" value="<?php echo (isset($rec['smtp_host']))?$rec['smtp_host']:""?>" name="data[smtp_host]">
                                           
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">SMTP Username</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="smtp_username" size="100" value="<?php echo (isset($rec['smtp_username']))?$rec['smtp_username']:""?>" name="data[smtp_username]">
                                
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">SMTP Password</label>
                              <div class="controls">
                                <input type="password" class="re-input" id="smtp_password" size="100" value="<?php echo (isset($rec['smtp_password']))?$rec['smtp_password']:""?>" name="data[smtp_password]">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                             <label class="control-label" for="user_level">Enable TLS encryption</label>  
                              <div class="controls">
                               <select id="smtp_secure" name="data[smtp_secure]" data-rel="chosen">
                                     <option <?php echo (isset($rec['smtp_secure']) && $rec['smtp_secure']=="ssl")?"selected":""?> value="ssl" selected="selected">SSL</option>
                                        <option <?php echo (isset($rec['smtp_secure']) && $rec['smtp_secure']=="tls")?"selected":""?> value="tls"  >TLS</option> 
                                        </select>
                                  
                              </div>
                            </div>    
                            <div class="control-group">
                              <label class="control-label" for="date01">SMTP Port</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="smtp_port" size="100" value="<?php echo (isset($rec['smtp_port']))?$rec['smtp_port']:""?>" name="data[smtp_port]">
                                 
                              </div>
                            </div>
                          </fieldset>
                       

                    </div>
                </div><!--/span-->

            </div>
            
            
            
           <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>Locale</h2>
                        <div class="box-icon">
                            
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                          
                        </div>       
                    </div>
                    <div class="box-content">
                         
                            <input type="hidden" name="txtMode" value="update" />                                                                    
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">Language</label>  
                              <div class="controls" id="site_language">
                               <?php 
                                    $langlist=getLanguages();
                                                                   
                                      echo funcCombo($langlist,"name","lang_id","data[site_language]",$rec['site_language'],$Extra);
                                   ?>     
                                  
                              </div>
                            </div>
                             <div class="control-group">
                             <label class="control-label" for="user_level">Currency</label>  
                              <div class="controls">
                               <select id="currency" name="data[currency]" data-rel="chosen">
                                     <option <?php echo (isset($rec['currency']) && $rec['currency']=="Rs.")?"selected":""?> value="Rs." selected="selected">INR (Rs.)</option>
                                     <option <?php echo (isset($rec['currency']) && $rec['currency']=='$')?"selected":""?> value="$">Dollar ($)</option>
                                     <option <?php echo (isset($rec['currency']) && $rec['currency']=='THB')?"selected":""?> value="THB">THB</option>
                                      <option <?php echo (isset($rec['currency']) && $rec['currency']=='GBP')?"selected":""?> value="GBP">GBP</option>
                                      <option <?php echo (isset($rec['currency']) && $rec['currency']=='Eur')?"selected":""?> value="Eur">Euro</option>  
                                     </select>
                                  
                              </div>
                            </div>                                 
                          
                          </fieldset>
                       

                    </div>
                </div><!--/span-->

            </div> 
            
            
           <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>Retailer Configuration</h2>
                       <div class="box-icon">
                            
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                          
                        </div>   
                    </div>
                    <div class="box-content">
                         
                            <input type="hidden" name="txtMode" value="update" />                                                                    
                          <fieldset>
                                 
                            <div class="control-group">
                              <label class="control-label" for="date01">Meta Title Formula</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="retailer_page_meta_title"   value="<?php echo (isset($rec['retailer_page_meta_title']))?$rec['retailer_page_meta_title']:""?>" name="data[retailer_page_meta_title]">
                                 
                              </div>
                            </div>         
                                                           
                             <div class="control-group">
                              <label class="control-label" for="date01">Meta keyword Formula </label>
                              <div class="controls">
                                   <textarea name="data[retailer_page_meta_keyword]" id="retailer_page_meta_keyword"><?php echo (isset($rec['retailer_page_meta_keyword']))?$rec['retailer_page_meta_keyword']:""?></textarea>
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Meta Description Formula</label>
                              <div class="controls">
                                  <textarea name="data[retailer_page_meta_description]" id="retailer_page_meta_description"><?php echo (isset($rec['retailer_page_meta_description']))?$rec['retailer_page_meta_description']:""?></textarea>
                                 
                              </div>
                            </div>
                                                                           
                            
                                            
                            
                            
                                                                                 
                          
                          </fieldset>
                       

                    </div>
                </div><!--/span-->

            </div> 
            
            
         <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>Coupons Configuration</h2>
                       <div class="box-icon">
                            
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                          
                        </div>   
                    </div>
                    <div class="box-content">
                         
                            <input type="hidden" name="txtMode" value="update" />                                                                    
                          <fieldset>
                                 
                            <div class="control-group">
                              <label class="control-label" for="date01">Show Expired Coupons</label>
                              <div class="controls">
                                      <select id="show_expire_coupons" name="data[show_expire_coupons]" data-rel="chosen">
                                     <option <?php echo (isset($rec['show_expire_coupons']) && $rec['show_expire_coupons']=="1")?"selected":""?> value="1" selected="selected">Yes</option>
                                        <option <?php echo (isset($rec['show_expire_coupons']) && $rec['show_expire_coupons']=="0")?"selected":""?> value="0"  >No</option> 
                                        </select>
                              </div>
                            </div>         
                                    
                                                                                 
                          
                          </fieldset>
                       

                    </div>
                </div><!--/span-->

            </div>   
            
            
            
            
            
            
           <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>Facebook</h2>
                        <div class="box-icon">
                            
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                          
                        </div>       
                    </div>
                    <div class="box-content">
                         
                            <input type="hidden" name="txtMode" value="update" />                                                                    
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">Enable Facebook Login</label>  
                              <div class="controls">
                               <select id="facebook_login" name="data[facebook_login]" data-rel="chosen">
                                     <option <?php echo (isset($rec['facebook_login']) && $rec['facebook_login']=="On")?"selected":""?> value="On" selected="selected">On</option>
                                        <option <?php echo (isset($rec['facebook_login']) && $rec['facebook_login']=="Off")?"selected":""?> value="Off"  >Off</option> 
                                        </select>
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">APP ID</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="facebook_app_id"   value="<?php echo (isset($rec['facebook_app_id']))?$rec['facebook_app_id']:""?>" name="data[facebook_app_id]">
                              
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01" >Secret Key</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="facebook_app_secret"  value="<?php echo (isset($rec['facebook_app_secret']))?$rec['facebook_app_secret']:""?>" name="data[facebook_app_secret]">
                                 
                              </div>
                            </div>   
                             <div class="control-group">
                              <label class="control-label" for="date01" >Facebook Page Url</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="facebook_url"  value="<?php echo (isset($rec['facebook_url']))?$rec['facebook_url']:""?>" name="data[facebook_url]">
                                 
                              </div>
                            </div>   
                          
                          </fieldset>
                       

                    </div>
                </div><!--/span-->

            </div>    
            
           <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>Google</h2>
                        <div class="box-icon">
                            
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                          
                        </div>       
                    </div>
                    <div class="box-content">
                         
                            <input type="hidden" name="txtMode" value="update" />                                                                    
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">Enable Google Login</label>  
                              <div class="controls">
                               <select id="google_login" name="data[google_login]" data-rel="chosen">
                                     <option <?php echo (isset($rec['google_login']) && $rec['google_login']=="On")?"selected":""?> value="On" selected="selected">On</option>
                                        <option <?php echo (isset($rec['google_login']) && $rec['google_login']=="Off")?"selected":""?> value="Off"  >Off</option> 
                                        </select>
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Client ID</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="google_client_id"   value="<?php echo (isset($rec['google_client_id']))?$rec['google_client_id']:""?>" name="data[google_client_id]">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01" >Secret Key</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="google_secret_key"  value="<?php echo (isset($rec['google_secret_key']))?$rec['google_secret_key']:""?>" name="data[google_secret_key]">
                                 
                              </div>
                            </div>   
                            <div class="control-group">
                              <label class="control-label" for="date01" >Redirect Url</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="google_redirect_url"  value="<?php echo (isset($rec['google_redirect_url']))?$rec['google_redirect_url']:""?>" name="data[google_redirect_url]">
                                 
                              </div>
                            </div>  
                            <div class="control-group">
                              <label class="control-label" for="date01" >Push Notification Secret Key</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="android_google_key"  value="<?php echo (isset($rec['android_google_key']))?$rec['android_google_key']:""?>" name="data[android_google_key]">
                                 
                              </div>
                            </div>    
                             <div class="control-group">
                              <label class="control-label" for="date01" >Google Analytics</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="google_analytics"  value="<?php echo (isset($rec['google_analytics']))?$rec['google_analytics']:""?>" name="data[google_analytics]">
                                 
                              </div>
                            </div>   
                              <div class="control-group">
                              <label class="control-label" for="date01">Sidebar ads</label>
                              <div class="controls">
                                  <textarea name="data[sidebar_ads]" id="sidebar_ads"><?php echo (isset($rec['sidebar_ads']))?$rec['sidebar_ads']:""?></textarea>
                                 
                              </div>
                            </div>
                          </fieldset>
                       

                    </div>
                </div><!--/span-->

            </div>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>Twitter</h2>
                        <div class="box-icon">
                            
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                          
                        </div>       
                    </div>
                    <div class="box-content">
                         
                            <input type="hidden" name="txtMode" value="update" />                                                                    
                          <fieldset>
                                                          <div class="control-group">
                              <label class="control-label" for="date01">Twitter Page Url</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="twitter_url"   value="<?php echo (isset($rec['twitter_url']))?$rec['twitter_url']:""?>" name="data[twitter_url]">
                                 
                              </div>
                            </div>
                                           
                               
                          
                          </fieldset>
                       

                    </div>
                </div><!--/span-->

            </div> 
            
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>Whatsapp</h2>
                        <div class="box-icon">
                            
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                          
                        </div>       
                    </div>
                    <div class="box-content">
                         
                            <input type="hidden" name="txtMode" value="update" />                                                                    
                          <fieldset>
                          <div class="control-group">
                              <label class="control-label" for="date01">Whatsapp Share Content</label>
                              <div class="controls">
                                <textarea type="text" class="re-input" id="whatsapp_content"  name="data[whatsapp_content]"><?php echo (isset($rec['whatsapp_content']))?$rec['whatsapp_content']:""?></textarea>
                                 
                              </div>
                            </div>
                                           
                               
                          
                          </fieldset>
                       

                    </div>
                </div><!--/span-->

            </div>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>Referal</h2>
                        <div class="box-icon">
                            
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                          
                        </div>       
                    </div>
                    <div class="box-content">
                         
                            <input type="hidden" name="txtMode" value="update" />                                                                    
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">Enable Referral</label>  
                              <div class="controls">
                               <select id="enable_referral" name="data[enable_referral]" data-rel="chosen">
                                     <option <?php echo (isset($rec['enable_referral']) && $rec['enable_referral']=="1")?"selected":""?> value="1" selected="selected">Yes</option>
                                        <option <?php echo (isset($rec['enable_referral']) && $rec['enable_referral']=="0")?"selected":""?> value="0"  >No</option> 
                                        </select>
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Refferal Bonus</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="refer_bonus"   value="<?php echo (isset($rec['refer_bonus']))?$rec['refer_bonus']:""?>" name="data[refer_bonus]">
                               <?php echo $currency;?>  
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01" >Referral Commission</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="referral_commision"  value="<?php echo (isset($rec['referral_commision']))?$rec['referral_commision']:""?>" name="data[referral_commision]">
                                 %
                              </div>
                            </div>    
                            <div class="control-group">
                             <label class="control-label" for="user_level">Referral Level</label>  
                              <div class="controls">
                               <select id="referral_level" name="data[referral_level]" data-rel="chosen">
                                    <?php for($i=1;$i<=10;$i++): ?> 
                                     <option <?php echo (isset($rec['referral_level']) && $rec['referral_level']==$i)?"selected":""?> value="<?php echo $i?>" ><?php echo $i?></option>
                                   <?php endfor;?>     
                                        </select>
                                  
                              </div>
                            </div>
                            
                              <div class="control-group">
                             <label class="control-label" for="user_level">Enable Referral Invite Contacts</label>  
                              <div class="controls">
                               <select id="enable_referral_invite" name="data[enable_referral_invite]" data-rel="chosen">
                                      <option <?php echo (isset($rec['enable_referral_invite']) && $rec['enable_referral_invite']=="0")?"selected":""?> value="0" selected="selected"  >No</option> 
                                     <option <?php echo (isset($rec['enable_referral_invite']) && $rec['enable_referral_invite']=="1")?"selected":""?> value="1" >Yes</option>
                                       
                                        </select>
                                  
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01" >Referral Invite Cookie time</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="referral_invite_cookie_time"  value="<?php echo (isset($rec['referral_invite_cookie_time']))?$rec['referral_invite_cookie_time']:""?>" name="data[referral_invite_cookie_time]">
                                 Days
                              </div>
                            </div>    
                             <div class="control-group">
                              <label class="control-label" for="date01">Referral invite message</label>
                              <div class="controls">
                                <textarea type="text" class="re-input" id="referral_invite_message"  name="data[referral_invite_message]"><?php echo (isset($rec['referral_invite_message']))?$rec['referral_invite_message']:""?></textarea>
                                 
                              </div>
                            </div>
                                    
                             <div class="control-group">
                              <label class="control-label" for="date01">Referral add message</label>
                              <div class="controls">
                                <textarea type="text" class="re-input" id="notification_referral_add"  name="data[notification_referral_add]"><?php echo (isset($rec['notification_referral_add']))?$rec['notification_referral_add']:""?></textarea>
                                 
                              </div>
                            </div>
                                            
                                    
                          </fieldset>
                       

                    </div>
                </div><!--/span-->

            </div>
            
           <!---Enable OtP---> 
          <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>SMS</h2>
                        <div class="box-icon">
                            
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                          
                        </div>       
                    </div>
                    <div class="box-content">
                         
                            <input type="hidden" name="txtMode" value="update" />                                                                    
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">Enable OTP</label>  
                              <div class="controls">
                               <select id="enable_otp" name="data[enable_otp]" data-rel="chosen">
                                     <option <?php echo (isset($rec['enable_otp']) && $rec['enable_otp']=="1")?"selected":""?> value="1" selected="selected">Yes</option>
                                        <option <?php echo (isset($rec['enable_otp']) && $rec['enable_otp']=="0")?"selected":""?> value="0"  >No</option> 
                                        </select>
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Gateway Url</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="msg_gateway_url"   value="<?php echo (isset($rec['msg_gateway_url']))?$rec['msg_gateway_url']:""?>" name="data[msg_gateway_url]">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01" >SMS Auth key</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="msg_auth_key"  value="<?php echo (isset($rec['msg_auth_key']))?$rec['msg_auth_key']:""?>" name="data[msg_auth_key]">
                                 
                              </div>
                            </div>    
                              <div class="control-group">
                              <label class="control-label" for="date01" >SMS Sender Id</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="msg_sender_id"  value="<?php echo (isset($rec['msg_sender_id']))?$rec['msg_sender_id']:""?>" name="data[msg_sender_id]">
                               
                              </div>
                            </div>  
                             <div class="control-group">
                              <label class="control-label" for="date01" >OTP Message</label>
                              <div class="controls">
                                <textarea  class="re-input" id="sms_message_text"   name="data[sms_message_text]"><?php echo (isset($rec['sms_message_text']))?$rec['sms_message_text']:""?></textarea>
                                
                              </div>
                            </div>  
                             <div class="control-group">
                             <label class="control-label" for="user_level">Enable Cashback Message</label>  
                              <div class="controls">
                               <select id="enable_cashback_message" name="data[enable_cashback_message]" data-rel="chosen">
                                     <option <?php echo (isset($rec['enable_cashback_message']) && $rec['enable_cashback_message']=="1")?"selected":""?> value="1" selected="selected">Yes</option>
                                        <option <?php echo (isset($rec['enable_cashback_message']) && $rec['enable_cashback_message']=="0")?"selected":""?> value="0"  >No</option> 
                                        </select>
                                  
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01" >Cashback Message</label>
                              <div class="controls">
                                <textarea  class="re-input" id="cashback_message"   name="data[cashback_message]"><?php echo (isset($rec['cashback_message']))?$rec['cashback_message']:""?></textarea>
                                
                              </div>
                            </div>  
                             <div class="control-group">
                              <label class="control-label" for="date01" >Cashback Confirmed Message</label>
                              <div class="controls">
                                <textarea  class="re-input" id="cashback_confirm_message"   name="data[cashback_confirm_message]"><?php echo (isset($rec['cashback_confirm_message']))?$rec['cashback_confirm_message']:""?></textarea>
                                
                              </div>
                            </div>  
                             <div class="control-group">
                              <label class="control-label" for="date01" >Scheduled Cashback Message</label>
                              <div class="controls">
                                <textarea  class="re-input" id="scheduled_cashback_sms"   name="data[scheduled_cashback_sms]"><?php echo (isset($rec['scheduled_cashback_sms']))?$rec['scheduled_cashback_sms']:""?></textarea>
                                
                              </div>
                            </div>  
                          </fieldset>
                       

                    </div>
                </div><!--/span-->

                
          <!---Enable OtP---> 
          <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span>Mobile Recharge</h2>
                        <div class="box-icon">
                            
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                          
                        </div>       
                    </div>
                    <div class="box-content">
                         
                            <input type="hidden" name="txtMode" value="update" />                                                                    
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">Enable Mobile Recharge</label>  
                              <div class="controls">
                               <select id="enable_mobilerecharge" name="data[enable_mobilerecharge]" data-rel="chosen">
                                     <option <?php echo (isset($rec['enable_mobilerecharge']) && $rec['enable_mobilerecharge']=="1")?"selected":""?> value="1" selected="selected">Yes</option>
                                        <option <?php echo (isset($rec['enable_mobilerecharge']) && $rec['enable_mobilerecharge']=="0")?"selected":""?> value="0"  >No</option> 
                                        </select>
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">User Name</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="recharge_username"   value="<?php echo (isset($rec['recharge_username']))?$rec['recharge_username']:""?>" name="data[recharge_username]">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01" >API Key</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="recharge_apikey"  value="<?php echo (isset($rec['recharge_apikey']))?$rec['recharge_apikey']:""?>" name="data[recharge_apikey]">
                                 
                              </div>
                            </div>    
                                    
                          </fieldset>
                       

                    </div>
                </div><!--/span-->      
                
                
                
            </div>  
            
            
            
                           
     </form>    
    </div>
 <!-- this set of pageguide steps will be displayed by default. -->
            <ul id="tlyPageGuide" data-tourtitle="check out these elements">
              <!-- use these classes to indicate where each number bubble should be
              positioned. data-tourtarget should contain the selector for the element to
              which you want to attach this pageguide step. -->
              <li class="tlypageguide_left" data-tourtarget="#website_title">
               Here You can set Website Title.
              </li>
              <li class="tlypageguide_right" data-tourtarget="#home_page_title">
               Here You can set Home Page Title. It will display on Title bar of home page ,It helps in Search ranking
               
              </li>
              <li class="tlypageguide_right" data-tourtarget="#logo">
               Here you can upload Logo of your website. It must be in jpg,png or gif format
              </li>
              <li class="tlypageguide_right" data-tourtarget="#home_page_meta_keyword">
                It is Home Page Meta Keyword, It is used in Search engine optimization
              </li>
              <li class="tlypageguide_right" data-tourtarget="#home_page_meta_description">
                 It is Home Page Meta Description, It is used in Search engine optimization
              </li>
              <li class="tlypageguide_right" data-tourtarget="#website_url">
                Here you can set your Website url.
              </li>
              <li class="tlypageguide_right" data-tourtarget="#site_mode">
                If you are planning to update your website then you can keep your website on maintaince mode from here.
                If you will set it as maintaince then site will be in maintaince mode and below site maintaince message will 
                be display to users , if you set it as Live then site will be live for users.
              </li>
              <li class="tlypageguide_right" data-tourtarget="#maintenance_content">
                If you set site mode as maintaince then u can set your own custom message for maintaince which will display to
                users who will visit your website during maintaince mode.
              </li>
              <li class="tlypageguide_right" data-tourtarget="#min_payout">
                You can set minimum threshold to create withdraw request. If users will not reached minimum payout then user will
                not able to go to withdraw form. User must reach minimum payout to create withdraw request. 
              </li>
              <li class="tlypageguide_right" data-tourtarget="#admin_email">
                You can set Admin email id , All Contact us email will be sent to this email id.
              </li>
              <li class="tlypageguide_right" data-tourtarget="#support_email">
              You can set Support email id
              </li>
              <li class="tlypageguide_right" data-tourtarget="#email_log">
                This is used for Email Members Section, If you are sending emails to your members and want to know 
                that who have opened your email then enable it otherwise disable it
                For check email logs you can go to Report > Email Logs
              </li>
              <li class="tlypageguide_right" data-tourtarget="#signup_email_activation">
                Here you can set if you want users to confirm their email after registration to avoid fake registration on 
                your site then it helps to do it. Enable it to avoid fake registration
              </li>
                <li class="tlypageguide_right" data-tourtarget="#signup_bonus">
                 If you want to give signup bonus to your users upon register then set price for sign up bonus,
                 If you dont want to give signup bonus then keep it 0.
              </li>
                <li class="tlypageguide_right" data-tourtarget="#signup_email_activation">
                Here you can set if you want users to confirm their email after registration to avoid fake registration on 
                your site then it helps to do it. Enable it to avoid fake registration
              </li>
                <li class="tlypageguide_right" data-tourtarget="#site_language">
              We are offering multilanguage website, now you can run your website in any country. Here you can set in which
              lanugage you want to run this website. 
              Steps
              <ol>
                 <li>Go to System > Language</li>
                 <li>Click to Add Language</li>
                 <li>Download Format of Lanugage</li>
                 <li>Add your  sentence in front of every english sentence</li>
                 <li>Upload file and save</li>
                 <li>Come to System > Configuration and set your language from here</li>
              </ol>
              
              
              </li>
                <li class="tlypageguide_right" data-tourtarget="#currency">
             We are offering multi currency, now you can run your website with any currency. Here you can set your currency.
              </li>
                <li class="tlypageguide_right" data-tourtarget="#retailer_page_meta_title">
                   Here you can set retailer page meta title forumla.<br>
                   Use {retailer_name} variable for Retailer Name<br>
                   Use {cashback} variable for Cashback <br>
                   Use {fixed_percent} variable for Fixed or Percentage.<br>
                   Use {SITE_NAME} variable for Website Name. <br>
                   Example : Buy from {retailer_name} and Get upto {cashback} {fixed_percent} Cashback 
                   
              </li>
                <li class="tlypageguide_right" data-tourtarget="#retailer_page_meta_keyword">
                   Here you can set retailer page meta keyword forumla. <br>
                   Use {retailer_name} variable for Retailer Name   <br>
                   Use {cashback} variable for Cashback  <br>
                   Use {fixed_percent} variable for Fixed or Percentage.  <br>
                   Use {SITE_NAME} variable for Website Name.    <br>
                   Example : Shop from {retailer_name},Get {cashback} {fixed_percent} Cashback                                                                                            
              </li>
                <li class="tlypageguide_right" data-tourtarget="#retailer_page_meta_description">
                   Here you can set retailer page meta description forumla. <br>
                   Use {retailer_name} variable for Retailer Name   <br>
                   Use {cashback} variable for Cashback       <br>
                   Use {fixed_percent} variable for Fixed or Percentage.   <br>
                   Use {SITE_NAME} variable for Website Name.    <br>
                   Example : Join {SITE_NAME} and Buy from {retailer_name} via {SITE_NAME} and Get upto {cashback} {fixed_percent} Cashback and also win 100% Cashback every month
              </li>
              <li class="tlypageguide_right" data-tourtarget="#facebook_login">
               If You want to enable facebook login for your users then enable it otherwise disable it.
              </li>
              <li class="tlypageguide_right" data-tourtarget="#facebook_app_id">
                 For Facebook Login you have to set app id and secret key<br> 
                 How to Get Facebook APP Id and secret key
                 <ol>
                  <li>Login/Register to facebook.com </li>       
                 <li>Go to https://developers.facebook.com/apps/ and Click Add a New App </li>
                 <li>Click on Add New App</li>
                 <li>Choose Website</li>
                 <li>Choose Name for you App and Click Create New Facebook App ID </li>
                 <li>Choose a category for you App and click Create App ID</li>
                 <li> Now Click Skip Quick Test</li>
                 <li>Under settings, Provide values for App domain ( Eg:www.yourdomain.com ) and Contact Email and click Add Platform</li>
                 <li> Provide Values for Site URL and Mobile site URl ( Optional )</li>
                 <li> Now under Status & Review, Click the button to make you App live</li>
                 <li>Copy and paste your APP ID and Secret key in  APP ID and  Secret Key textbox</li>
                 </ol>  
                   
              </li>
              
              <li class="tlypageguide_right" data-tourtarget="#facebook_url">
                 Here you can set your facebook fan page<br> 
                 How to create Facebook fan page
                 <ol>
                  <li>Login/Register to facebook.com </li>      
                 <li>Click on Create Page ( you can find it on top right corner menu) </li>
                 <li>Choose your page type</li>
                 <li>Choose Website</li>
                 <li>Fill Required information and click on Get started </li>
                 <li>Upload your profile photo</li>
                 <li> Complete the About section</li>
                 <li> Provide more information on your fan pag</li>
                 <li> Enter your facebook page url</li>                                      
                 </ol>  
                   
              </li>
              
               <li class="tlypageguide_right" data-tourtarget="#google_login">
                    Here you can enable Google Login for your users      
                   
              </li>
               <li class="tlypageguide_right" data-tourtarget="#google_client_id">
                     For Google Login you have to create Client Id and Secret Key
                     Please follow these steps to create that.
                 <ol>
                  <li>Go to the Google Developers Console � https://console.developers.google.com/ </li>      
                 <li> Select an existing project, or create a new project by clicking Create Project:
                    <ul>
                    <li>In the Project name field, type in a name for your new project.</li>
                    <li>In the Project ID field, the console has created project ID. Optionally you can type in a project ID for your project. But project ID must be unique world-wide.</li>
                    <li>Click on the Create button and the project to be created within some seconds. Once the project is created successfully, the project name would be appearing at the top of the left sidebar.</li>
                    </ul>
                 </li>
                 <li> In the left sidebar, select APIs under the APIs & auth section. A list of Google APIs appears.</li>
                 <li> Find the Google+ API service and set its status to Enable.</li>
                 <li> In the sidebar, select Credentials under the APIs & auth section.</li>
                 <li> In the OAuth section of the page, select Create New Client ID.
                 <ul>
                 <li>Create Client ID dialog box would be appearing for choosing application type.</li>
                 <li>In the Application type section of the dialog, select Web application and click on the Configure consent screen button.</li>
                 <li>Choose Email address, enter Product name and save the form.</li>
                 <li>In the Authorized JavaScript origins field, enter your app origin. If you want to allow your app to run on different protocols, domains, or subdomains, then you can enter multiple origins.</li>
                 <li>In the Authorized redirect URI field, enter the redirect URL. which will be http://www.yourdomainname/login.php</li>
                 <li>Click on Create Client ID.</li>
                 <li> Now you can see the Client ID for web application section. Note the Client ID and Client secret that will need to use to access the APIs.</li>
                 <li>Copy and paste here in client id and secret key section</li>                 
                 </ul>
                 
                 </li>
                 <li></li>
                 </ol>  
                   
              </li>
               <li class="tlypageguide_right" data-tourtarget="#google_redirect_url">
                   Enter Google Redirect login for google login  <br>
                   Example: http://yourdomainname/login.php  
                   
              </li>
               <li class="tlypageguide_right" data-tourtarget="#google_analytics">
                     It is used for Google Analytics tracking. <br>
                    <a href="https://www.youtube.com/watch?v=uyrKu-yb05c" target="_blank">Click Here</a> View Video to see how to setup google analytics account
                    
                                                    
                   
              </li>
               <li class="tlypageguide_right" data-tourtarget="#sidebar_ads">
                  Here you can set google adsense or any ads you want to display
                 <a href="https://www.youtube.com/watch?v=C5xIum4q8ME" target="_blank">Click Here</a> View Video to see how to setup google adsense account                     
                   
              </li>
          <li class="tlypageguide_right" data-tourtarget="#twitter_url">
                  Here you can set Twitter page Url
                 <a href="https://www.youtube.com/watch?v=J0xbjIE8cPM" target="_blank">Click Here</a> View Video to see how to setup google adsense account                     
                   
              </li>
               <li class="tlypageguide_right" data-tourtarget="#enable_referral">
                  From Here you can enable or Disable Referral Feature in your site
                 
                   
              </li>
               <li class="tlypageguide_right" data-tourtarget="#refer_bonus">
                  Here you can set Referral bonus for signup If you dont want to give referral bonus for signup just set here 0
               
                   
              </li>
               <li class="tlypageguide_right" data-tourtarget="#referral_commision">
                  Here You can set referral commission     <br>
                 It will work like below example               <br>
                 
                 You have referred 1 user and he did online shopping and get 100 rs as cashback then you will get his 10% means 10 rs.
                 as referred commission. and its everytime whever your referred user will get cashback you will get his 10% referred commission.                       
                   
              </li>
               <li class="tlypageguide_right" data-tourtarget="#referral_level">
                Here you can set referral level how much level you want to share referring commission  <br>
                
                Example           <br>
              Suppose you set level 3. <br>
              
             So if A has reffered B and B has reffered C and C has referred D    <br>
             
             and If D user will get 1000 Rs. as cashback for his shopping.<br>
             then C will get D's 10% means 100 rs.           <br>
             B will get C's 10% means 10 Rs.             <br>
             and A will get B's 10% means 1 rs. as referred commission. <br>
             Here 10% is just example it will be whatever you will set. <br>                                                            
                
              </li>     
              
              
                       
            </ul>   