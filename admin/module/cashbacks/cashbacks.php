  <?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>
      
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#frm").validate({
                 rules: {
                name: "required",
                 affiliate_id: "required",  
                 url: "required",  
                 cashback: "required",                       
                
            },
                         
            
        });                                     
          
    });
 
    
    
    </script>
<div id="content" class="span10">
            
<?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                          
                    </div>
                    <div class="box-content">
                         <form name="frm"  id="userfrm" class="form-horizontal"  method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            <input type="hidden" name="cashback_id" value="<?php echo $Code;?>" />
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">Retailer</label>  
                              <div class="controls">
                                                                               <?php 
                                    $retailerlist=getRetailers();
                                    $rec['retailer_id']=($rec['retailer_id']!="")?$rec['retailer_id']:$_GET['retailer_id'];                             
                                      echo funcCombo($retailerlist,"name","retailer_id","retailer_id",$rec['retailer_id'],$Extra);
                                   ?> 
                                  
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Title</label>
                              <div class="controls">
                                 <input type="text" class="re-input" id="title"  size="100" value="<?php echo (isset($rec['title']))?$rec['title']:""?>" name="title">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Commission</label>
                              <div class="controls">
                                   <input type="text" class="re-input" id="commission" maxlength="50" value="<?php echo (isset($rec['commission']))?$rec['commission']:""?>" name="commission">
                                     <select style="width: 130px;" class="re-input" id="fixed_percent_com" name="fixed_percent_com">
                                       
                                        <option <?php echo (isset($rec['fixed_percent_com']) && $rec['fixed_percent_com']=="fixed")?"selected":""?> value="fixed" selected="selected"><?php echo trim(getConfiguration('currency')); ?></option>
                                        <option <?php echo (isset($rec['fixed_percent_com']) && $rec['fixed_percent_com']=="percent")?"selected":""?> value="percent"  >%</option> 
                                        </select>
                              </div>
                            </div>  
                             <div class="control-group">
                              <label class="control-label" for="date01">Cashback</label>
                              <div class="controls">
                                   <input type="text" class="re-input" id="cashback" maxlength="50" value="<?php echo (isset($rec['cashback']))?$rec['cashback']:""?>" name="cashback">
                                     <select style="width: 130px;" class="re-input" id="ctl00_MainContent_ddlMUSRXintPri0" name="fixed_percent">
                                       
                                        <option <?php echo (isset($rec['fixed_percent']) && $rec['fixed_percent']=="fixed")?"selected":""?> value="fixed" selected="selected"><?php echo trim(getConfiguration('currency')); ?></option>
                                        <option <?php echo (isset($rec['fixed_percent']) && $rec['fixed_percent']=="percent")?"selected":""?> value="percent"  >%</option> 
                                        </select>
                              </div>
                            </div>  
                             <div class="control-group">
                              <label class="control-label" for="date01">Goal Id</label>
                              <div class="controls">
                                 <input type="text" class="re-input" id="goal_id"  size="100" value="<?php echo (isset($rec['goal_id']))?$rec['goal_id']:""?>" name="goal_id">
                                 
                              </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Status</label>
                                <div class="controls">
                                  <label class="radio">
                                       <input type="radio" name="status" value="1" <?php echo (isset($rec['status']) && $rec['status']==1)?"checked":""?> checked="checked" />Active
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" name="status" value="0" <?php echo (isset($rec['status']) && $rec['status']==0)?"checked":""?> />InActive
                                  </label>
                                </div>
                              </div>
                            
                                                                 
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save changes</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>