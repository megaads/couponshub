 <?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>
 
 <script>tinymce.init({
  selector: '.description',
  height: 300,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools'
  ],
  toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons',
  image_advtab: true,
       
        
 });</script>
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#frm").validate({
                 rules: {
                name: "required",
                 store_id: "required",  
                 url: "required",  
                 
                
            },
                         
            
        });                                     
          
    });
 
      $(function() {
    $( "#start_date" ).datepicker();
     $( "#end_date" ).datepicker();    
  });
    
    </script>
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                          
                    </div>
                    <div class="box-content">
                    <form name="frm"  class="form-horizontal"  method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            <input type="hidden" name="coupon_id" value="<?php echo $Code;?>" />
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">File</label>  
                              <div class="controls">
                                  <input type="file" class="re-input" id="import_file"  name="import_file">                                    
                                  
                              </div>
                            </div>  
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Import</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>