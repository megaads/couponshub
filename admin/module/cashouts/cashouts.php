   <?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                          
                    </div>
                    <div class="box-content">
                         <form name="frm"  id="userfrm" class="form-horizontal"  method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            <input type="hidden" name="reference_id" value="<?php echo $Code;?>" />
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">User Id</label>  
                              <div class="controls">
                           <?php echo $rec['email']?>
                          <input type="hidden" name="user_id" value="<?php echo $rec['user_id'];?>" />            
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Ref. Id</label>
                              <div class="controls">
                                    <?php echo $rec['reference_id']?>                                                                                                                                                                             
                                        
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Payment Method</label>
                              <div class="controls">
                                <?php echo $rec['payment_method']?>
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Payment Details</label>
                              <div class="controls">
                                                                  <?php echo $rec['payment_details']?>
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Amount</label>
                              <div class="controls">
                                <?php echo $rec['totalcashback']?> <?php echo $currency;?>
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Mark as</label>
                              <div class="controls">
                                <select name="payment_status">
                                    <option value="Paid">Paid</option>
                                    <option value="Declined">Declined</option>                                               
                                    </select>    
                              </div>
                            </div>  
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save changes</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>