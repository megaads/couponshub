 <?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?> 
<script>
  $(function() {
    $( "#start_date" ).datepicker();
     $( "#end_date" ).datepicker();    
  });

</script>    
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                          
                    </div>
                    <div class="box-content">
                    <form name="frm"  class="form-horizontal"  method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            
                          <fieldset>
                               <div class="control-group">
                                <label class="control-label">Status</label>
                                <div class="controls">
                                     <select style="width: 130px;"  id="ctl00_MainContent_ddlMUSRXintPri0" name="status">
                                        <option <?php echo ($_POST['status']=="-1" || $_POST['status']=="")?"selected":""?> value="-1"  selected="selected">All (Exclude Deleted)</option>
                                        <option <?php echo (isset($_POST['status']) && $_POST['status']==1)?"selected":""?> value="1"  >Active</option>
                                        <option <?php echo (isset($_POST['status']) && $_POST['status']==0)?"selected":""?> value="0"  >InActive</option>
                                        <option <?php echo (isset($_POST['status']) && $_POST['status']==2)?"selected":""?> value="2">Deleted</option> 
                            </select>
                                </div>
                              </div>                              <div class="control-group">
                              <label class="control-label" for="date01">From Date</label>
                              <div class="controls">
                                <input type="text" class="re-input"   size="100" id="start_date" value="<?php echo (isset($rec['start_date']))?$rec['start_date']:""?>" name="start_date">
                                 
                              </div>
                            </div>      
                             <div class="control-group">
                              <label class="control-label" for="date01">To Date</label>
                              <div class="controls">
                                 <input type="text" class="re-input" id="end_date" size="100" value="<?php echo (isset($rec['end_date']))?$rec['end_date']:""?>" name="end_date">
                                 
                              </div>
                            </div>
                            
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Export</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>