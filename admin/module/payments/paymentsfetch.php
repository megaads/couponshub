 <?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?> 
<script>
  $(function() {
    $( "#start_date" ).datepicker();
     $( "#end_date" ).datepicker();    
  });

</script>    
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                          
                    </div>
                    <div class="box-content">
                    <form name="frm"  class="form-horizontal"  method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            <input type="hidden" name="coupon_id" value="<?php echo $Code;?>" />
                          <fieldset>
                               <div class="control-group">
                              <label class="control-label" for="date01"> Affiliate Network</label>
                              <div class="controls">
                               <?php 
                                    $affiliateslist=getAffilliates();
                                                                 
                                      echo funcCombo($affiliateslist,"name","affiliate_id","affiliate_id",$rec['affiliate_id'],$Extra);
                                   ?> 
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">From Date</label>
                              <div class="controls">
                                <input type="text" class="re-input"   size="100" id="start_date" value="<?php echo (isset($rec['start_date']))?$rec['start_date']:""?>" name="start_date">
                                 
                              </div>
                            </div>      
                             <div class="control-group">
                              <label class="control-label" for="date01">To Date</label>
                              <div class="controls">
                                 <input type="text" class="re-input" id="end_date" size="100" value="<?php echo (isset($rec['end_date']))?$rec['end_date']:""?>" name="end_date">
                                 
                              </div>
                            </div>
                            
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Fetch</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>