<?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#frm").validate({
                 rules: {
                user_id: "required",                     
                 payment_type: "required",  
                  amount: "required",
                
            },
                         
            
        });                                     
          
    });
         
    </script>
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                        <div class="box-icon">
                            <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                            <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
                        </div>
                    </div>
                    <div class="box-content"> 
                                                                                                             
                         <form name="frm"  id="frm" class="form-horizontal"  method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            <input type="hidden" name="transaction_id" value="<?php echo $Code;?>" />
                          <fieldset>
                            <div class="control-group">
                             <label class="control-label" for="user_level">User Id</label>  
                              <div class="controls">
                                            <input type="text" class="re-input" id="user_id" maxlength="50" value="<?php echo (isset($rec['user_id']))?$rec['user_id']:$_REQUEST['user_id']?>" name="user_id">
                                 <em>Enter User Id or Email Id</em>                                    
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Retailer</label>
                              <div class="controls">
                               <?php 
                                    $retailerlist=getRetailers();
                                                                 
                                      echo funcCombo($retailerlist,"name","retailer_id","retailer_id",$rec['retailer_id'],$Extra);
                                   ?> 
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Payment Type</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="payment_type" maxlength="50" value="<?php echo (isset($rec['payment_type']))?$rec['payment_type']:""?>" name="payment_type">
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Sale Amount</label>
                              <div class="controls">
                                <input type="text" <?php echo ($Code!="")?"readonly='readonly'":""?>  class="re-input" id="sale_amount" maxlength="50" value="<?php echo (isset($rec['sale_amount']))?$rec['sale_amount']:""?>" name="sale_amount">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Earned Amount</label>
                              <div class="controls">
                                <input type="text" <?php echo ($Code!="")?"readonly='readonly'":""?>  class="re-input" id="earned_amount" maxlength="50" value="<?php echo (isset($rec['earned_amount']))?$rec['earned_amount']:""?>" name="earned_amount">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Cashback Amount</label>
                              <div class="controls">
                                <input type="text" <?php echo ($Code!="")?"readonly='readonly'":""?>  class="re-input" id="amount" maxlength="50" value="<?php echo (isset($rec['amount']))?$rec['amount']:""?>" name="amount">
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Status</label>
                              <div class="controls">
                                <select name="payment_status">
                                    <option value="Confirmed">Confirmed</option>
                                    <option value="Pending">Pending</option> 
                                     <option value="Declined">Declined</option>                                               
                                    </select>    
                                 
                              </div>
                            </div>                  
                                                                 
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save changes</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>