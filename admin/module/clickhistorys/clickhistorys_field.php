<?php
	if (!defined('WEB_ROOT')) {
			echo "You Cannot directly access this page";
			exit;
		}  
	  
	/*********Show Record Per Page*******************/ 
	    
	// Set Variable for Paging	 
	  if(!$_POST && isset($_SESSION['Retailer_RecPerPage']))
		$RecPerPage=$_SESSION['Retailer_RecPerPage'];
	  else  
	  	$RecPerPage=(isset($_POST['recPerPage']))?$_POST['recPerPage']:SHOW_PER_PAGE; 
	  
	  $_SESSION['Retailer_RecPerPage']=$RecPerPage;
	  
	  $PageNum=(isset($_POST['PageNum']))?$_POST['PageNum']:((isset($_GET['PageNum']))?$_GET['PageNum']:"1");	
	// ************************************		
	/*********Show Record Per Page*******************/ 
	 $msg="";
	/*User Level*/
	
		$db=new databaseLayer(HOST,USER,PASS,DB); 
		$record="";	
		//checkUser();
	
	  $action = isset($_POST['txtMode']) ? $_POST['txtMode'] : '';
	 
	switch ($action) {
		
		case 'add' :
			add();
			break;
			
		case 'update' :
			modify();
			break;
		case 'Active' :
				changeStatus('retailer_id',1,'S');
				break;
		case 'InActive' :
				changeStatus('retailer_id',0,'S');
				break; 
		case 'Delete' :
				changeStatus('retailer_id',2,'S');
				break;
		case 'Restore' :
				changeStatus('retailer_id',1,'S');
				break;  		  
		case 'MActive' :
			changeStatus('retailer_id',1,'M');
			break;
		case 'MInActive' :
			changeStatus('retailer_id',0,'M');
			break;
		case 'MDelete' :
			changeStatus('retailer_id',2,'M');
			break;
		case 'MRestore' :
			changeStatus('retailer_id',1,'M');
			break;				
		 
	} 
	
	switch($view)
	{
		case 'list':
			$record=showRecord();
			break;
		case 'add': 
			$latestrecord=TOP10RECORD();	
		     	break;
		case 'modify': 
			  $Code= $_GET['Code'];
			$rec=getRecord($Code);  
			$latestrecord=TOP10RECORD();	
		     	break;
		case 'detail': 
			  $Code= $_GET['Code'];
			$rec=getRecord($Code);  
			$latestrecord=TOP10RECORD();	
		     	break;						
		default:
			$record=showRecord();
			break;
	
	}
	
	
	function showRecord()
	{ 
	    global $db;
		global $RecPerPage;
		global $PageNum; 
		$condField=array();
		   //*********Search Criteria**********/
		 
		   $fields=array();
		 $fields=array(
		 	'user_id'=>'like',               
			'status'=>'=',  
		 ); 
		  
		   //*********Search Criteria**********/
		   
		   $searchCond="";
		    //if(count($condField)>0)
		   // $searchCond=implode(" and ",$condField); 
		  $searchCond= getSearchConditions($fields,'u.');  
          
          if($_REQUEST['user_id']!="")
          {
             $searchCond.=" and u.user_id=".$_REQUEST['user_id']; 
          }
          
		   $SortBy=(isset($_POST['txtSortBy']))?$_POST['txtSortBy']:"c.created_date";		
		   $SortOrder=(isset($_POST['txtSortOrder']))?$_POST['txtSortOrder']:"desc";					 
		   
		   $sql_paging="select count(*) as totRec from ".TABLE_NAME." c left join tbl_user u on u.user_id=c.user_id left join tbl_retailer r on r.retailer_id=c.retailer_id where $searchCond"; 
		   $PagLink=getPaging($sql_paging,$RecPerPage,$PageNum);
		 
		   $offset = ($PageNum - 1) * $RecPerPage; 
		 
		$sql_query="select c.click_id,u.email,c.created_date,r.name as retailer_name from ".TABLE_NAME." c left join tbl_user u on u.user_id=c.user_id left join tbl_retailer r on r.retailer_id=c.retailer_id where $searchCond order by $SortBy $SortOrder limit $offset,$RecPerPage"; 
		   $record =$db->objSelect($sql_query, "ASSOCIATIVE") ;  
			
		// Set Parameter Start**************/
		
		/***Set Field for Tables*/
		$UserColHeader=array("User Email","Store","Visited Date","Delete"); // Header  set this as your requirement
		  
		$UserColValue=array("email","retailer_name","created_date","delete"); // Column name defined in database number of column should be same as Header
		  
		$extrafeatures=array('delete'); 
		 
		 $parameters=array( 
				 'header'=>$UserColHeader,
				 'colnval'=>$UserColValue,
				 'EXTFEA'=>$extrafeatures,
				 'sorting'=>array('Visited Date'=>'created_date'), // set Sorting Field
				 'sortby'=>$SortBy,
				 'sortorder'=>$SortOrder,
				 'PagLink'=>$PagLink, 
				 'page'=>'module/'.MODULE_PAGE, // set module name
				 'colsize'=>array("2%","45%","20%","20%","4%"), // set width of columns
				 'checkbox'=>array(  
		 				'allow'=>1,  // set allow =0 for no check box required and 1 for check box required
						'name'=>'chk', // set name for check box default name is chk No need to change
		 				'value'=>'click_id', // set column for value
					 ),
				  
		        ); 
			 
		/*************************/
		 
		// Set Parameter End**************/
		$Recordtable=getTable($record,$parameters);
		 
		return $Recordtable;
	}
	                  
	
?>