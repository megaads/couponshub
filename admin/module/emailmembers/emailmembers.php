    <?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>
<script>tinymce.init({
  selector: 'textarea',
  height: 300,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools'
  ],
  toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons',
  image_advtab: true,
       
        
 });</script>
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#frm").validate({
                 rules: {
                template_name: "required",
                 email_subject: "required",                             
                
            },
                         
            
        });                                     
          
    });
 
    
    
    </script>
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                        <div class="box-icon">
                            <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                            <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                         <form name="frm"  id="frm" class="form-horizontal"  method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="emailsent" />                                                           
                          <fieldset>
                                    
                            <div class="control-group">
                              <label class="control-label" for="date01">Subject</label>
                              <div class="controls">
                                <input type="text" class="re-input" id="email_subject" size="80"     value="<?php echo (isset($rec['email_subject']))?$rec['email_subject']:""?>" name="email_subject">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Message</label>
                              <div class="controls">
                               <textarea name="content" id="content"><?php echo (isset($rec['content']))?$rec['content']:""?>                               
                              <p>&nbsp;</p>
<p>Dear {{name}}</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><br /> <br /> <br /> &nbsp;</p>
<p>--------------------------------------------------------------------------------------------
<br /> You are receiving this email as you have directly signed up to Cashbackclub.<br />
 If you do not wish to receive these messages in the future, please
  <a href="{{unsubscribe_link}}" target="_blank">unsubscribe</a>.</p>
<p>&nbsp;</p></textarea>
                                 
                              </div>
                            </div>
                                             
                            
                            <div class="control-group">
                                <label class="control-label">Members</label>
                                <div class="controls">
                                  <label class="radio">
                                   <input type="radio" name="sent_to" value="1" <?php echo (isset($rec['sent_to']) && $rec['sent_to']==1)?"checked":""?> checked="checked" />All Members
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" name="sent_to" value="0" <?php echo (isset($rec['sent_to']) && $rec['sent_to']==0)?"checked":""?> />Only Subscribers
                                  </label>
                                </div>
                              </div>
                            
                                                                 
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Sent</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>