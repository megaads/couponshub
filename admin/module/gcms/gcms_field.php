<?php
 
    if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
      
    /*********Show Record Per Page*******************/ 
        
    // Set Variable for Paging     
      if(!$_POST && isset($_SESSION['Content_RecPerPage']))
        $RecPerPage=$_SESSION['Content_RecPerPage'];
      else  
          $RecPerPage=(isset($_POST['recPerPage']))?$_POST['recPerPage']:SHOW_PER_PAGE; 
      
      $_SESSION['Content_RecPerPage']=$RecPerPage;
      
      $PageNum=(isset($_POST['PageNum']))?$_POST['PageNum']:((isset($_GET['PageNum']))?$_GET['PageNum']:"1");    
    // ************************************        
    /*********Show Record Per Page*******************/ 
     $msg="";
    /*User Level*/
    
        $db=new databaseLayer(HOST,USER,PASS,DB); 
        $record="";    
        //checkUser();
    
      $action = isset($_POST['txtMode']) ? $_POST['txtMode'] : '';
     
    switch ($action) {
        
        case 'add' :
            msgsent();
            break;
            
        case 'update' :
            modify();
            break;
        case 'Active' :
                changeStatus('content_id',1,'S');
                break;
        case 'InActive' :
                changeStatus('content_id',0,'S');
                break; 
        case 'Delete' :
                changeStatus('content_id',2,'S');
                break;
        case 'Restore' :
                changeStatus('content_id',1,'S');
                break;            
        case 'MActive' :
            changeStatus('content_id',1,'M');
            break;
        case 'MInActive' :
            changeStatus('content_id',0,'M');
            break;
        case 'MDelete' :
            changeStatus('content_id',2,'M');
            break;
        case 'MRestore' :
            changeStatus('content_id',1,'M');
            break;                
         
    } 
    
    switch($view)
    {
        case 'list':
            $record=showRecord();
            break;
        case 'add': 
            $latestrecord=TOP10RECORD();    
                 break;
        case 'modify': 
              $Code= $_GET['Code'];
            $rec=getRecord($Code);  
            $latestrecord=TOP10RECORD();    
                 break;
        case 'detail': 
              $Code= $_GET['Code'];
            $rec=getRecord($Code);  
            $latestrecord=TOP10RECORD();    
                 break;                        
        default:
            $record=showRecord();
            break;
    
    }
    
    
    function showRecord()
    { 
        global $db;
        global $RecPerPage;
        global $PageNum; 
        $condField=array();
           //*********Search Criteria**********/
         
           $fields=array();
         $fields=array(
             'name'=>'like',               
            'status'=>'=',  
         ); 
          
           //*********Search Criteria**********/
           
           $searchCond="";
            //if(count($condField)>0)
           // $searchCond=implode(" and ",$condField); 
          $searchCond= getSearchConditions($fields);  
           $SortBy=(isset($_POST['txtSortBy']))?$_POST['txtSortBy']:"name";        
           $SortOrder=(isset($_POST['txtSortOrder']))?$_POST['txtSortOrder']:"asc";                     
           
           $sql_paging="select count(*) as totRec from ".TABLE_NAME." where $searchCond"; 
           $PagLink=getPaging($sql_paging,$RecPerPage,$PageNum);
         
           $offset = ($PageNum - 1) * $RecPerPage; 
         
           $sql_query="select content_id,name,created_date,status from ".TABLE_NAME." where $searchCond order by $SortBy $SortOrder limit $offset,$RecPerPage"; 
           $record =$db->objSelect($sql_query, "ASSOCIATIVE") ;  
            
        // Set Parameter Start**************/
        
        /***Set Field for Tables*/
        $UserColHeader=array("Title","Created Date","Status","Edit","Delete"); // Header  set this as your requirement
          
        $UserColValue=array("name","created_date","status","edit","delete"); // Column name defined in database number of column should be same as Header
          
        $extrafeatures=array('status','edit','delete'); 
         
         $parameters=array( 
                 'header'=>$UserColHeader,
                 'colnval'=>$UserColValue,
                 'EXTFEA'=>$extrafeatures,
                 'sorting'=>array('Title'=>'name'), // set Sorting Field
                 'sortby'=>$SortBy,
                 'sortorder'=>$SortOrder,
                 'PagLink'=>$PagLink, 
                 'page'=>'module/'.MODULE_PAGE, // set module name
                 'colsize'=>array("2%","42%","20%","4%","4%","4%","4%"), // set width of columns
                 'checkbox'=>array(  
                         'allow'=>1,  // set allow =0 for no check box required and 1 for check box required
                        'name'=>'chk', // set name for check box default name is chk No need to change
                         'value'=>'content_id', // set column for value
                     ),
                  
                ); 
             
        /*************************/
         
        // Set Parameter End**************/
        $Recordtable=getTable($record,$parameters);
         
        return $Recordtable;
    }
    
    function TOP10RECORD()
    {
        //Show Record Per Page
         global $db; 
         $sql_query="select content_id,name from ".TABLE_NAME." where status <> 2 order by content_id desc limit 10"; 
         $latestRecord="";
         $latestRecord =$db->objSelect($sql_query, "ASSOCIATIVE") ;  
         return $latestRecord;
    }
    
              
    
   function msgsent()
    {
        global $db;
        
     
        require_once  DOC_ROOT.'admin/functions/firebase.php';
        require_once DOC_ROOT.'admin/functions/push.php';
        
        define('FIREBASE_API_KEY','AIzaSyDan70vTrnMAfcgYpHzzVa8yrtr9H1L_XQ');  
          
        $message=trim($_POST['message']);
         $userslist=str_replace(",","','",trim($_POST['users'])); 
         $userslists=trim($_POST['users']);
           $sent_to=trim($_POST['sent_to']);
        switch ($sent_to)
        {
            case "1":     $query = "SELECT * FROM tbl_user WHERE user_level=0 and email != ''"; break;
            case "0":        $query = "SELECT * FROM tbl_user WHERE email in ('$userslist') and user_level=0   AND status=1 and verified=1"; break;;
        }
            
        $users =$db->objSelect($query, "ASSOC") ;     
           
        foreach($users as $rec)
        {
           $registrationIds[]=$rec['android_reg_id'] ;     
             
        }         
       
         
                 
       $count=0;
        
        $android_google_key=getConfiguration('android_google_key');                                               
        
        
        $firebase = new Firebase();
        $push = new Push();
 
        // optional payload
        $payload = array();
        $payload['team'] = 'India';
        $payload['score'] = '5.6';
 
        // notification title
        $title = isset($_POST['title']) ? $_POST['title'] : 'First Notification';
         
        // notification message
        $message = isset($_POST['message']) ? trim($_POST['message'])   : '';
         
        // push type - single user / topic
        $push_type = isset($_GET['push_type']) ? $_GET['push_type'] : 'individual';
         
        // whether to include to image or not
        $include_image =   FALSE;
        
 
        $push->setTitle($title);
        $push->setMessage($message);
     /*   if ($include_image) {
            $push->setImage('http://api.androidhive.info/images/minion.jpg');
        } else {
            $push->setImage('');
        }   */
        $push->setIsBackground(FALSE);
       // $push->setPayload($payload);
 
 
        $json = '';
        $response = '';
 
        if ($push_type == 'topic') {
            $json = $push->getPush();
            $response = $firebase->sendToTopic('global', $json);
        } else if ($push_type == 'individual') {
             $json = $push->getPush();
            $regId = $registrationIds[0];
            $response = $firebase->send($regId, $json);
        }
                          
              
           
                                                                       
          if($res)
        {
             
             header("location: index.php?msg=msg_sent_succ");
             exit;
        }
                                                                                                                       
        
    } 
    
    
     
    
?>