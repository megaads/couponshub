   <?php
if (!defined('WEB_ROOT')) {
            echo "You Cannot directly access this page";
            exit;
        }  
        $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg']; 
?>
    
<script>    

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#frm").validate({
                 rules: {
                
                 message: "required",                             
                
            },
                         
            
        });                                     
          
    });
 
    
    
    </script>
<div id="content" class="span10">
            
            
                              <?php include 'breadcrumb.inc';?> 
<?php echo getMessage($msgcode);?>
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo $Title;?></h2>
                          
                    </div>
                    <div class="box-content">
                         <form name="frm"  id="frm" class="form-horizontal"  method="post" enctype="multipart/form-data" >
                            <input type="hidden" name="txtMode" value="<?php echo $mode;?>" />
                            <input type="hidden" name="content_id" value="<?php echo $Code;?>" />
                          <fieldset>
                                 <div   class="control-group">
                                <label class="control-label">Members</label>
                                <div class="controls">
                                  <label class="radio">
                                   <input type="radio" name="sent_to" value="1" <?php echo (isset($rec['sent_to']) && $rec['sent_to']==1)?"checked":""?> checked="checked" />All Members
                                  </label>
                                  <div style="clear:both"></div>
                                  <label class="radio">
                                  <input type="radio" name="sent_to" value="0" <?php echo (isset($rec['sent_to']) && $rec['sent_to']==0)?"checked":""?> />Individual
                                  </label>
                                </div>
                              </div>
                            <div   class="control-group">
                              <label class="control-label" for="date01">Individual Membets List</label>
                              <div class="controls">
                                 <textarea name="users" id="users"><?php echo (isset($rec['users']))?$rec['users']:""?></textarea>
                                 
                              </div>
                            </div>
                            <div class="control-group">
                              <label class="control-label" for="date01">Title</label>
                              <div class="controls">
                                <input type="text" class="input"   id="title"  value="<?php echo (isset($rec['title']))?$rec['title']:""?>" name="title">
                                 
                              </div>
                            </div>
                             <div class="control-group">
                              <label class="control-label" for="date01">Message</label>
                              <div class="controls">
                                     <textarea name="message" id="message"></textarea>
                                 
                              </div>
                            </div>    
                                            
                                                                 
                            <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Send</button>
                              <button type="reset" class="btn">Cancel</button>
                            </div>
                          </fieldset>
                        </form>   

                    </div>
                </div><!--/span-->

            </div><!--/row-->

                                 

    </div>