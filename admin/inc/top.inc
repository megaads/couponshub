<div id="sidebar-left" class="span2">
                <div class="nav-collapse sidebar-nav">
                    <ul class="nav nav-tabs nav-stacked main-menu">
                    
                        <li><a href="<?php echo WEB_ROOT?>admin"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>    
                       <li>
                            <a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">System</span></a>
                            <ul>
                                <li><a class="submenu" href="<?php echo WEB_ROOT?>admin/module/settings"><i class="icon-file-alt"></i><span class="hidden-tablet"> Configuration</span></a></li>
                                <li><a class="submenu" href="<?php echo WEB_ROOT?>admin/module/paymentmethods"><i class="icon-file-alt"></i><span class="hidden-tablet">Payment Methods</span></a></li>
                                <li><a class="submenu" href="<?php echo WEB_ROOT?>admin/module/affiliates"><i class="icon-file-alt"></i><span class="hidden-tablet"> Affiliate Network</span></a></li>
                                <li><a class="submenu" href="<?php echo WEB_ROOT?>admin/module/banners"><i class="icon-file-alt"></i><span class="hidden-tablet">Banners</span></a></li>
                                <li><a class="submenu" href="<?php echo WEB_ROOT?>admin/module/languages"><i class="icon-file-alt"></i><span class="hidden-tablet">Language</span></a></li>
                                <li><a class="submenu" href="<?php echo WEB_ROOT?>admin/module/emailtemplates"><i class="icon-file-alt"></i><span class="hidden-tablet">Email Template</span></a></li>
                            </ul>    
                        </li>
                        <li><a href="<?php echo WEB_ROOT?>admin/module/users"><i class="icon-bar-chart"></i><span class="hidden-tablet">Members</span></a></li>   
                        <li><a href="<?php echo WEB_ROOT?>admin/module/categories"><i class="icon-bar-chart"></i><span class="hidden-tablet">Categories</span></a></li>   
                        <li><a href="<?php echo WEB_ROOT?>admin/module/retailers"><i class="icon-bar-chart"></i><span class="hidden-tablet">Retailers</span></a></li>   
                        <li><a href="<?php echo WEB_ROOT?>admin/module/coupons"><i class="icon-bar-chart"></i><span class="hidden-tablet">Coupons</span></a></li>   
                        <li><a href="<?php echo WEB_ROOT?>admin/module/payments"><i class="icon-bar-chart"></i><span class="hidden-tablet">Payments</span></a></li>   
                        <li><a href="<?php echo WEB_ROOT?>admin/module/cashouts"><i class="icon-bar-chart"></i><span class="hidden-tablet">Withdraw Request</span></a></li>     
                        <li><a href="<?php echo WEB_ROOT?>admin/module/clickhistorys"><i class="icon-bar-chart"></i><span class="hidden-tablet">Click History</span></a></li>     
                        <li><a href="<?php echo WEB_ROOT?>admin/module/contents"><i class="icon-bar-chart"></i><span class="hidden-tablet">Contents</span></a></li>     
                        <li><a href="<?php echo WEB_ROOT?>admin/module/tickets"><i class="icon-bar-chart"></i><span class="hidden-tablet">Missing Cashback</span></a></li>     
                         <li><a href="<?php echo WEB_ROOT?>admin/module/emailmembers"><i class="icon-bar-chart"></i><span class="hidden-tablet">Email Members</span></a></li>     
                          <li><a href="<?php echo WEB_ROOT?>admin/module/gcms"><i class="icon-bar-chart"></i><span class="hidden-tablet">Push Notification</span></a></li>
                          <li><a href="<?php echo WEB_ROOT?>admin/module/testimonials"><i class="icon-bar-chart"></i><span class="hidden-tablet">Testimonials</span></a></li>     
                          <li><a href="<?php echo WEB_ROOT?>admin/module/notifications"><i class="icon-bar-chart"></i><span class="hidden-tablet">Notifications</span></a></li>     
                         <li>
                            <a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Report</span></a>
                            <ul>
                            <li><a class="submenu" href="<?php echo WEB_ROOT?>admin/module/rechargereports"><i class="icon-file-alt"></i><span class="hidden-tablet">Recharge Report</span></a></li>
                                <li><a class="submenu" href="<?php echo WEB_ROOT?>admin/module/emaillogs"><i class="icon-file-alt"></i><span class="hidden-tablet">Email Logs</span></a></li>
                              
                            </ul>    
                        </li>    
                        
                           
                    </ul>
                </div>
            </div>