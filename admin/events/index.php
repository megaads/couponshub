<?php
	 	require_once("../inc/constant.php"); 
	//$_SESSION['login_return_url'] = $_SERVER['REQUEST_URI'];
	//checkUser(); 
	
	$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';
	
	switch ($view) {
		case 'list' :
			$content 	= 'eventlist.php';		
			$pageTitle 	= 'Liequine Admin Control Panel - View Event';
			break;
	
		case 'add' :
			$content 	= 'event.php';
			$action="add";		
			$pageTitle 	= 'Liequine Admin Control Panel - Add Event';
			break;
	
		case 'modify' :
			$content 	= 'event.php';
			$content="update";		
			$pageTitle 	= 'Liequine Admin Control Panel - Modify Event';
			break;
	
		case 'detail' :
			$content    = 'eventdetail.php';
			$pageTitle  = 'Liequine Admin Control Panel - View Event Detail';
			break;
			
		default :
			$content 	= 'eventlist.php';		
			$pageTitle 	= 'Liequine Admin Control Panel - View Event';
	}
	
	  
	$script    = array('check.js','validate.js','common.js','events.js');
	
	require_once '../Inc/template.php';
?>
