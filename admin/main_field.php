<?php
	if (!defined('WEB_ROOT')) {
			echo "You Cannot directly access this page";
			exit;
		}  
	/*User Level*/
	
		$db=new databaseLayer(HOST,USER,PASS,DB); 
		$record="";	
	 $nextweek= date("Y-m-d", strtotime("+1 week"));
	
	//  Expired Properties Listing
	// $expbusiness= getExpBusinessListing();
	//   Expire with this week Properties Listing
	//$exp_inweek_business= getExpInWeekBusinessListing();
	 
	 
 
	 
	function getTodaysClick()
	{ 
	    global $db;
		 
		 
		   $sql_query="select count(*) as todayclick from tbl_clickhistory where created_date='".date("Y-m-d")."'"; 
		 $record =$db->objSelect($sql_query, "ROW") ;  
		 
		 return $record['todayclick'];
	 
	}
    
    function getYesterdayClick()
    { 
        global $db;
      $yesterday= date("Y-m-d", strtotime("-1 day"));   
         
           $sql_query="select count(*) as yesterdayclick from tbl_clickhistory where created_date='".$yesterday."'"; 
         $record =$db->objSelect($sql_query, "ROW") ;  
         
         return $record['yesterdayclick'];
     
    }
	
     function getSevenDaysClick()
    { 
        global $db;
        $lastweek= date("Y-m-d", strtotime("-1 week"));
       
           $sql_query="select count(*) as weekclick from tbl_clickhistory where created_date between '".$lastweek."' and '".TODAY."'"; 
         $record =$db->objSelect($sql_query, "ROW") ;  
         
         return $record['weekclick'];
     
    }
    
     function getMonthsClick()
    { 
        global $db;
      
     $lastmonth= date("Y-m-d", strtotime("-1 month"));      
        $sql_query="select count(*) as monthclick from tbl_clickhistory where created_date between '".$lastmonth."' and  '".TODAY."'"; 
         $record =$db->objSelect($sql_query, "ROW") ;  
         
         return $record['monthclick'];
     
    }
    
    // Users
    function getTodaysUsers()
    { 
        global $db;
         
         
           $sql_query="select count(*) as todayuser from tbl_user where created_date='".date("Y-m-d")."' and status=1"; 
         $record =$db->objSelect($sql_query, "ROW") ;  
         
         return $record['todayuser'];
     
    }
    
    function getLastUsers()
    { 
        global $db;
         
         
           $sql_query="select * from tbl_user order by user_id desc limit 5"; 
         $record =$db->objSelect($sql_query, "ASSOCIATIVE") ;  
         
         return $record;
     
    }
    
    
    function getYesterdayUsers()
    { 
        global $db;
      $yesterday= date("Y-m-d", strtotime("-1 day"));   
         
           $sql_query="select count(*) as yesterdayuser from tbl_user where created_date='".$yesterday."'  and status=1"; 
         $record =$db->objSelect($sql_query, "ROW") ;  
         
         return $record['yesterdayuser'];
     
    }
    
     function getSevenDaysUsers()
    { 
        global $db;
        $lastweek= date("Y-m-d", strtotime("-1 week"));
       
           $sql_query="select count(*) as weekuser from tbl_user where created_date between '".$lastweek."' and '".TODAY."'  and status=1"; 
         $record =$db->objSelect($sql_query, "ROW") ;  
         
         return $record['weekuser'];
     
    }
    
     function getMonthsUsers()
    { 
        global $db;
      
        $lastmonth= date("Y-m-d", strtotime("-1 month"));      
        $sql_query="select count(*) as monthuser from tbl_user where created_date between '".$lastmonth."' and  '".TODAY."'  and status=1"; 
         $record =$db->objSelect($sql_query, "ROW") ;  
         
         return $record['monthuser'];
     
    }
    
      function getTotalUsers()
    { 
        global $db;
         
         
         $sql_query="select count(*) as totRec from tbl_user where verified=1 and status=1"; 
         $record =$db->objSelect($sql_query, "ROW") ;  
         
         return $record['totRec'];
     
    }
    
      function getTotalRetailers()
    { 
        global $db;
         
         
         $sql_query="select count(*) as totRec from tbl_retailer where status=1"; 
         $record =$db->objSelect($sql_query, "ROW") ;  
         
         return $record['totRec'];
     
    }
    
       function getTotalCoupons()
    { 
        global $db;
         
         
         $sql_query="select count(*) as totRec from tbl_coupon where status=1"; 
         $record =$db->objSelect($sql_query, "ROW") ;  
         
         return $record['totRec'];
     
    }
    
      function getTotalPaidCashback()
    { 
        global $db;
         
         
         $sql_query="select sum(amount) as totRec from tbl_transaction where payment_status='Paid'"; 
         $record =$db->objSelect($sql_query, "ROW") ;  
         
         return $record['totRec'];
     
    }
    
     function getTotalEarnedCashback()
    { 
        global $db;                      
         $sql_query="select sum(earned_amount) as totRec from tbl_transaction"; 
         $record =$db->objSelect($sql_query, "ROW") ;  
         
         return $record['totRec'];
     
    }
    
    function getTotalPendingCashback()
    { 
         global $db;
         
         
         $sql_query="select sum(amount) as totRec from tbl_transaction where payment_status='Pending'"; 
         $record =$db->objSelect($sql_query, "ROW") ;  
         
         return $record['totRec'];
     
    }
    
    function getTotalRequestedCashback()
    { 
         global $db;
         
         
         $sql_query="select sum(amount) as totRec from tbl_transaction where payment_status='Requested'"; 
         $record =$db->objSelect($sql_query, "ROW") ;  
         
         return $record['totRec'];
     
    }
    
 
	function getExpInWeekBusinessListing()
	{ 
	    global $db;
		global $nextweek; 
		 
		 	 $sql_query="select member_id,member_company,member_plan_expdate from  tbl_company   
					  where  (member_plan_expdate between '".TODAY."' and '".$nextweek."') and status=1
					 	order by member_id desc limit 10";  
		   
		   $record =$db->objSelect($sql_query, "ASSOC") ;  
		 
		 return $record;
	 
	}
	
 
	
	
	
	 
	 
	 
?>