$(document).ready(function() {
$('#form input').keydown(function(e) {
		if (e.keyCode == 13) {
			$('#form').submit();
		}
	});
});

$(document).ready(function() {
	$('#txtUserName').focus();
	$('.error').hide().fadeIn('slow');
});

function chklogin() {
	
	with(window.document.frmLogin) {
	
		if (isEmpty(txtUserName, 'Please enter Username !')) {
			txtUserName.focus();
			return false;
		}
		if (isEmpty(txtPassword, 'Please enter Password !')) {
			txtPassword.focus();
			return false;
		}
		return true;
	}
}