// JavaScript Document

function checkChangeForm(frm)
{
    
	with (frm) { 
		 
		 if (isEmpty(oldpassword, 'Enter Old Password')) { 
			return false;
		}  
		else if (isEmpty(newpassword, 'Enter New Password')) { 
			return false;
		} else if (!isPassword(newpassword,6)) {  
			return false;
		} 
		else if (isEmpty(confnewpassword, 'Enter Confirm Password')) { 
			return false;
		} 
		if(newpassword.value!=confnewpassword.value)
		{
			alert("New Password and Cofirm Password does not match");
			confnewpassword.focus();
			return false;
		} 
		else {
			return true;
		} 
	}
} 
 