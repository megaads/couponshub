
function GetObject(strObjName)
	{
		if(document.all)
        {
            return document.all[strObjName];
        }
		else if(document.layers)
		{
			return document.layers[strObjName];
        }
        else if(!document.all && document.getElementById)
        {
            return document.getElementById(strObjName);
        }
        //alert(document.all.length)
        /*var i=0;
        for(i=0;i<=document.all.length-1;i++)
        {
			var elm = document.all[i];
			
			if(elm.name==strObjName)
			{
				//alert(elm.name)	
				return elm;
				break
			}
        }*/
	}
	
	//-------------------------------------------------------------------
// getElementIndex(input_object)
//   Pass an input object, returns index in form.elements[] for the object
//   Returns -1 if error
//-------------------------------------------------------------------
function getElementIndex(obj) 
	{
	var theform = obj.form;
	for (var i=0; i<theform.elements.length; i++) 
		{
		if (obj.name == theform.elements[i].name) 
			{return i;}
		}
		
	return -1;
	}

// -------------------------------------------------------------------
// tabNext(input_object)
//   Pass an form input object. Will focus() the next field in the form
//   after the passed element.
//   a) Will not focus to hidden or disabled fields
//   b) If end of form is reached, it will loop to beginning
//   c) If it loops through and reaches the original field again without
//      finding a valid field to focus, it stops
// -------------------------------------------------------------------
function tabNext(obj) 
	{
	if (navigator.platform.toUpperCase().indexOf("SUNOS") != -1) 
		{obj.blur(); return; }

	var theform = obj.form;
	var i = getElementIndex(obj);
	var j=i+1;

	if (j >= theform.elements.length) 
		{ j=0; }
	if (i == -1) 
		{ return; }
		
	while (j != i) 
		{
		if ((theform.elements[j].type!="hidden") && 
		    (theform.elements[j].name != theform.elements[i].name) && 
			(!theform.elements[j].disabled)) 
			{
			theform.elements[j].focus();
			break;
			}
		
		j++;
		
		if (j >= theform.elements.length) 
			{ j=0; }
		}
	}

function jump(obj)
	{
	if(obj.value.length == obj.maxLength)
		{ tabNext(obj); }
	}
//======= Auto Tab Ends =========================================================================
//-------------------------------------------------------------------------------------------------
//---> Alltrim an Expression
//-------------------------------------------------------------------------------------------------

// Function trim : deletes the leading and ending blank spaces
function trim(strComp)
	{
	ltrim = /^\s+/
	rtrim = /\s+$/
	strComp = strComp.replace(ltrim,'');
	strComp = strComp.replace(rtrim,'');
	return strComp;
	}

//-------------------------------------------------------------------------------------------------
//---> Required Fields
//-------------------------------------------------------------------------------------------------

// Function to require fields
function isRequired(formObject, fieldDescription) 
	{
	var tempFormValue ;
	var strError ="";	
	var iFocus =-1;

	for (var i =0; i< isRequired.arguments.length;i=i+2)
	{
	if(typeof(isRequired.arguments[i]) == 'undefined')
		{
		//alert('IsRequired-Error: Parameter no.'+(i+1)+' is not an object.')
		return false;
		}
		
	tempFormValue =trim(isRequired.arguments[i].value);
	
	if (tempFormValue.length < 15)
		{deleteLoop = tempFormValue.length}
	else
		{deleteLoop = 15}
		for (var j = 0; j < deleteLoop; j++) 
			{tempFormValue = tempFormValue.replace(/ / , "");
		}

	if (tempFormValue.length == 0)
		{	
		strError = strError+ isRequired.arguments[i+1] + "\n"
		if (iFocus ==-1)
			iFocus = i;
		}
	}

	if (strError.length != 0){	
		alert( 'Following fields are required.\n\n' + strError)
		
		isRequired.arguments[iFocus].focus()
		return(false)
	}
	else
		return(true)
	}

function isRequired1(formObject, fieldDescription) 
	{
	var tempFormValue ;
	var strError ="";	
	
	for (var i =0; i< isRequired1.arguments.length;i=i+2)
	{
	if(typeof(isRequired1.arguments[i]) == 'undefined')
		{
			return false;
		}
		
	tempFormValue =trim(isRequired1.arguments[i].value);
	
	if (tempFormValue.length < 15)
		{deleteLoop = tempFormValue.length}
	else
		{deleteLoop = 15}
		for (var j = 0; j < deleteLoop; j++) 
			{tempFormValue = tempFormValue.replace(/ / , "");
		}

		if (tempFormValue.length == 0)
		{	
			strError = strError+ isRequired1.arguments[i+1] + "\n"
		}
	}

	if (strError.length != 0){	
		alert( 'Following fields are required.\n\n' + strError)
			return(false)
	}
	else
		return(true)
	}

//-------------------------------------------------------------------------------------------------
//---> Validate SElection boxes
//-------------------------------------------------------------------------------------------------

// validate the select 
function chkSel(obj,strMsg)
{
	var iFocus =-1;
	var strError="";

	for(var i=0;i<chkSel.arguments.length;i=i+2)
	{
			if(chkSel.arguments[i].value=="" || chkSel.arguments[i].value=="-1")
			{
				
				strError = strError+chkSel.arguments[i+1] + "\n"
					if (iFocus ==-1)
							iFocus = i;					

			}
	}
	if (strError.length != 0)
	{	
		alert( 'Following fields are required.\n\n' + strError);
		chkSel.arguments[iFocus].focus()
		return false;

	}
	else
		return true;

}

// Function isEmail: Validates if the value of 'IsItReal' is a valid email address
function isEmail(IsItReal)
	{
		if (IsItReal.value != "")
		{
			var valEmail = IsItReal.value;
			var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if(reg.test(valEmail))
				return true;
			else
			{
				alert('- Invalid Email Address')
				IsItReal.focus();
				return false;
			}
		
		}
		else
		{
			alert('- Email Address Is Blank')
			IsItReal.focus();
			return false;
		}
			
	}

function isDate(IsItReal)
	{
		if (IsItReal.value != "")
		{
			var valDate = IsItReal.value;
			var reg = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.]((19|20)[0-9][0-9]+)$/;
			if(reg.test(valDate))
				return true;
			else
			{
				alert('Invalid Date Format\n\nPlease enter date in mm/dd/yyyy format, e.g."07/28/2006"')
				IsItReal.focus();
				IsItReal.select();
				return false;
			}
		}
		else
		{	
			return true;
		}
	}

function datecompare(date1,date2)
	{
		//datecompare function compare date1 >= date2 then true or false
		var datefrom;
		var dateto;

		datefrom = date1.split("/");
		dateto = date2.split("/");
		if (datefrom[2] < dateto[2])
		{	
			return false;
		}
		else 
		{
			if (datefrom[2] > dateto[2])
			{
				return true;
			}
			else
			{
				// year same then compare month
				if (datefrom[0] < dateto[0])
				{	
					return false;
				}
				else
				{
					if (datefrom[0] > dateto[0])
					{
						return true;
					}
					else
					{
						// month same then compare day
						if (datefrom[1] < dateto[1])
						{	
							return false;
						}
						else
						{
							/*if (datefrom[1] > dateto[1])
							{	
								return true;
							}
							else
							{
								return false;
							}*/
							return true;
						}
					}
				}
			}
		}
	}
	
function validatekey()
	{
		if((window.event.keyCode < 46) || (window.event.keyCode >57) || (window.event.keyCode == 47))
			window.event.keyCode=null;
	}
function onenter(e)//for search
{
//  var node = (e.target) ? e.target : ((e.srcElement) ? e.srcElement : null);
//  if (e.keyCode==13)
//  {
//   if(typeof(node.name) != 'undefined')// && node.name=='jsleft:txtskill')
//   {
//    document.getElementById("jsleft:ctl00_MainContent_btnSearch").focus();
//   }
//   else
//   {
//    document.getElementById("jsleft:ctl00_MainContent_btnSearch").blur();
//   }
//  }
  
    e?evt=e:evt=event;   
   /// javascript:return WebForm_FireDefaultButton(e,'ctl00_MainContent_btnSearch'); 
    var KeyID =  e.keyCode; 
    if(KeyID==13)
    {
        javascript:__doPostBack('ctl00$MainContent$btnSearch','');
        return false;
    }
}
function onenterdata(e)//for edit
{        
    var node = (e.target) ? e.target : ((e.srcElement) ? e.srcElement : null);  
    if (e.keyCode==13)
    {
       if(typeof(node).value != 'undefined')
       {           
           javascript:__doPostBack('ctl00$MainContent$btnSave','');
            return false;
       }
    }
}
function noenter()
	{
		if(window.event.keyCode == 13)
				window.event.keyCode=null;
	}
	
function isNumber(obj)
	{
	 
		if (obj.value!='')
		{
			if (isNaN(obj.value))
			{
				//alert("Please Enter Numeric Value!");
				//obj.focus();
				//obj.select();
				return false;
			}
			else
				return true;
		}
		else
			return true;
	}
	
function isWholenum()
	{
	 
		if((window.event.keyCode < 48) || (window.event.keyCode >57))
		{
			//alert('Invalid Number!')
			window.event.keyCode=null;
		}
	}	
	
function checklen(obj,intlen)
	{
		if (obj.value.length>intlen)
		{
			//alert("Total character should be less then " + intlen + "!");
			//obj.focus();
			return false;
		}
		else
			return true;
	}	
	
function isImage(obj)
	{
		if (obj.value.length>0)
		{
			if (obj.value.length>4)
			{
				var ext = obj.value.substring(obj.value.length-3,obj.value.length);
				if (ext == 'jpg' || ext == 'JPG' || ext == 'jpeg' || ext == 'JPEG' || ext == 'gif' || ext == 'GIF' || ext == 'png' || ext == 'PNG')
				{
					return true;
				}
				else
				{
					//alert('Upload only .jpg,.gif or .png file!')
					//obj.value="";
					//obj.focus();
					//obj.select();
					return false;
				}
			}
			else
			{
				//alert('Upload only .jpg,.gif or .png file!')
				//obj.value="";
				//obj.focus();
				//obj.select();
				return false;
			}
		}
	}
function isPhone(IsItReal)
	{
		if (IsItReal.value != "")
		{
			var valPhone = IsItReal.value;
			//var reg = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,3})|(\(?\d{2,3}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/
			var reg = /^\d{1,20}$/
			if(reg.test(valPhone))
				return true;
			else
			{
				//alert(strErr)
				//IsItReal.focus();
				//IsItReal.select();
				return false;
			}
		}
		else
		{	
			return true;
		}
	}
function isPhoneNumber(IsItReal)
{
	if(IsItReal.value=="")
		return true;
		
	valPhone=IsItReal.value
	var reg=/^[0-9\.\-\)\(]+$/;
	if(reg.test(valPhone))
			return true;
	else
	{
		alert('Please Enter a valid Phone Number')
		return false;
	}
}
	
function isZip(IsItReal)
	{
		if (IsItReal.value != "")
		{
			var valZip = IsItReal.value;
			var reg = /^[A-Za-z0-9\ \-]+$/;
			if(reg.test(valZip))
				return true;
			else
			{
				//alert('Invalid Zip Code!')
				//IsItReal.focus();
				//IsItReal.select();
				return false;
			}
		}
		else
		{	
			return true;
		}
	}

function isUrl(IsItReal)
	{
		if (IsItReal.value != "")
		{
			var valUrl = IsItReal.value;
			var reg = /^(?:(?:ftp|https?):\/\/){1}(?:[a-z0-9](?:[-a-z0-9]*[a-z0-9])?\.)+(?:com|edu|biz|org|gov|int|info|mil|net|name|museum|coop|aero|mobi|tv|us|[a-z][a-z])\b(?:\d+)?(?:\/[^;"'<>()\[\]{}\s\x7f-\xff]*(?:[.,?]+[^;"'<>()\[\]{}\s\x7f-\xff]+)*)?/;
			if(reg.test(valUrl))
				return true;
			else
			{
				//alert('Invalid Zip Code!')
				//IsItReal.focus();
				//IsItReal.select();
				return false;
			}
		}
		else
		{	
			return true;
		}
	}

function validateString(stringToTest,stringType,canBeEmpty,imgIdToChange)
	{
	      
		re = /^$/;
		if(canBeEmpty==true && stringToTest=='')
		{
			if(imgIdToChange!='')
			{
				document.getElementById(imgIdToChange).src = 'images/form_valid.gif';
			}
			return true;
		} //RESERVE stringType=='def' for DEFAULT
		if(stringType=='pas')
		{ //password
			re = /^\w{6,16}$/;
		}
		else if(stringType=='usr')
		{ //username
			re = /^[\ a-zA-z]{6,64}$/;
		}
		else if(stringType=='eml')
		{ //email
	 		re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		}
		else if(stringType=='mem')
		{ //multiple email address like the TO: Field on send email forms
	 		re = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*$/;
		}
		else if(stringType=='phn')
		{ //phone
			re = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,3})|(\(?\d{2,3}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/
		}
		else if(stringType=='zip')
		{ // zip code (includes "-" and space, for postal codes like Canada ("XJ4 5E5")
			re = /^[A-Za-z0-9\ \-]+$/;
		}
		else if(stringType=='url')
		{ //url
			re = /^(?:(?:ftp|https?):\/\/){1}(?:[a-z0-9](?:[-a-z0-9]*[a-z0-9])?\.)+(?:com|edu|biz|org|gov|int|info|mil|net|name|museum|coop|aero|mobi|tv|us|[a-z][a-z])\b(?:\d+)?(?:\/[^;"'<>()\[\]{}\s\x7f-\xff]*(?:[.,?]+[^;"'<>()\[\]{}\s\x7f-\xff]+)*)?/;
		}
		else if(stringType=='hmt')
		{ //homeType
			re = /^[A-Za-z0-9\ \.\-_\(\)\,&]+$/;
		}
		else if(stringType=='num')
		{ //regular integer
			re = /^[0-9]+$/;
		}
		else if(stringType=='flt')
		{ //floats (like "2.5")
			re = /^[0-9]+(\.[0-9]+)?$/;
		}
		else if(stringType=='ltx')
		{ //Link text (text inside <a href>link text</a>)
			re = /^[A-Za-z0-9\ \.\-_\(\)\,&!\?]+$/;
		}
		else if(stringType=='gen')
		{ //general check
			re = /^[A-Za-z0-9\ \.\-_\(\)\,&!\?;\']+$/;
		}
		else if(stringType=='alp')
		{ //alphabetical (with spaces)
			re = /^[A-Za-z\ ]+$/;
		}
		else if(stringType=='aln')
		{ //alphanumeric (no spaces)
			re = /^[A-Za-z0-9]+$/;
		}
		else if(stringType=='als')
		{ //alphanumeric (with spaces)
			re = /^[A-Za-z0-9\ ]+$/;
		}
		else {
      		re = /^[A-Za-z0-9\ \.\-_\(\)\,]+$/;
		}
		
		goodString = false;

		goodString = re.test(stringToTest);
		if(goodString){
			if(imgIdToChange!=''){
				document.getElementById(imgIdToChange).src = 'images/form_valid.gif';
			}
			return true;
		}
		else{
			if(imgIdToChange!=''){
				document.getElementById(imgIdToChange).src = 'images/form_not_valid.gif';
			}
			return false;
		}
	}

// Validating Extension
function checkExt(obj,extn)
{
	if (obj.value.length>0)
	{
		if (obj.value.length>4)
		{
			var ext = obj.value.substring(obj.value.length-3,obj.value.length);
			if (ext != extn)
			{
				alert('Upload only .' + extn + ' file!');
				obj.value = '';
				obj.focus();
				obj.select();
				return false;
			}
			else
				return true;
		}
		else
		{
			alert('Upload only .' + extn + ' file!');
			obj.value = '';
			obj.focus();
			obj.select();
			return false;
		}
	}
}	
function onenter(e)//for search
{
//  var node = (e.target) ? e.target : ((e.srcElement) ? e.srcElement : null);
//  if (e.keyCode==13)
//  {
//   if(typeof(node.name) != 'undefined')// && node.name=='jsleft:txtskill')
//   {
//    document.getElementById("jsleft:ctl00_MainContent_btnSearch").focus();
//   }
//   else
//   {
//    document.getElementById("jsleft:ctl00_MainContent_btnSearch").blur();
//   }
//  }
  
    e?evt=e:evt=event;   
   /// javascript:return WebForm_FireDefaultButton(e,'ctl00_MainContent_btnSearch'); 
    var KeyID =  e.keyCode; 
    if(KeyID==13)
    {
        javascript:__doPostBack('ctl00$MainContent$btnSearch','');
        return false;
    }
}
function onenterdata(e)//for edit
{        
    var node = (e.target) ? e.target : ((e.srcElement) ? e.srcElement : null);  
    if (e.keyCode==13)
    {
       if(typeof(node).value != 'undefined')
       {           
           javascript:__doPostBack('ctl00$MainContent$btnSave','');
            return false;
       }
    }
}


function onenterdataproduct(e,obj)//for edit
{   
    var o;
    o=obj;          
   var node = (e.target) ? e.target : ((e.srcElement) ? e.srcElement : null);  
    if (e.keyCode==13)
    {
       if(typeof(node).value != 'undefined')
       {           
            javascript:__doPostBack(o,'');
            return false;
       }
    }
}