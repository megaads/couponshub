// JavaScript Document
 
function checkForm(frm)
{
    
	with (frm) { 
		    
		if (make.value=="" && (make_other.value=="Other"|| make_other.value=="") ) {  
			alert("Please Select or Enter Make");
			make.focus();
			return false;
		}   
		else if (model.value=="" && (model_other.value=="Other"|| model_other.value=="") ) {  
			alert("Please Select or Enter Model");
			model.focus();
			return false;
		}   
		else if (version.value=="" && (version_other.value=="Other"|| version_other.value=="") ) {  
			alert("Please Select or Enter Version");
			version.focus();
			return false;
		}   
		else if (state.value=="" && (state_other.value=="Other"|| state_other.value=="") ) {  
			alert("Please Select or Enter State");
			state.focus();
			return false;
		}   	
		else if (year.value=="" && (year_other.value=="Other"|| year_other.value=="") ) {  
			alert("Please Select or Enter Year");
			year.focus();
			return false;
		}   	
		  
		 
		else { 
		 	return true;
		} 
	}
}
 
 
function showAllRecord(frmObj)
{  
 if(typeof frmObj.PageNum !="undefined")
		frmObj.PageNum.value=1;
	frmObj.faq_title.value=''; 
	  
	 frmObj.archive.value='-1'; 
	frmObj.status.value='-1'; 
  	
     frmObj.submit(); 
		
	
}