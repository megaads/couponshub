﻿// JavaScript Document
//Standard Rules
var objUsername 		= ".0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz";
var objNumber   		= ".0123456789";
var objMoney   		= ".,0123456789";
var objWholeNumber   = "0123456789";
var objTelephone     = "-()0123456789";
var objName     		= " .0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz'-";
var objAlpha 			="ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz";
var objCountry 			="ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz. ";
var objemail		=".@-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz";
var objUsername1 		= ".0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz ";

function isRule(oComp, sRule, nLength, fdecimal)
{
	if(fdecimal == "" || typeof(fdecimal) == "undefined")
	{
		fdecimal = false;
	}

	//If the object is not specified return false
	if (typeof(oComp) == 'undefined' || oComp == null || oComp == '')
	{
		alert('Error: Input object not specified.');
		return false;
	}
	//If neither rule nor max length is specified, return false
	else if (typeof(sRule) == 'undefined' && typeof(nLength) == 'undefined')
	{
		alert('Error: No rule/maximum length for input object specified.');
		return false;
	}

	var noErrorFlg = true;

	//If object is specified and either of rule is specified,
	if(typeof(sRule) != 'undefined' && sRule != null)
	{
		var temp;
		sRule = sRule + "";
		var discardChars = false;
		if(sRule.length > 0 && sRule.charAt(0) == "~")
		{
			sRule = sRule.substring(1);
			discardChars = true;
		}

		if(typeof(oComp) == "undefined" || typeof(sRule) == "undefined")
			return false;

		for (var i = 0;i < oComp.value.length;i++)
		{
			temp = oComp.value.charAt(i);

			if((!discardChars && sRule.indexOf(temp) == -1) || (discardChars && sRule.indexOf(temp) >= 0))
			{
				//alert("Field disobeys entry rule.  Following are the valid characters:\n" + sRule);
				alert("Invalid Character!");
				oComp.value = oComp.value.substring(0,i);// + (oComp.value.length > i ? oComp.value.substring(i+1):"");
				noErrorFlg = false;
				break;
			}
		}
	}
	if(nLength)
	{
		if(fdecimal)
		{
			nLength -= fdecimal;
			var dp = oComp.value.indexOf(".");
			var p1;
			var p2 = "";;
			if(dp >= 0)
			{
				p1 = oComp.value.substring(0,dp);
				p2 = oComp.value.substring(dp+1);
			}
			else
			{
				p1 = oComp.value;
			}
			if(p1.length > nLength)
			{
				oComp.value = oComp.value.substring(0,nLength);
				return noErrorFlg;
			}
			for(var i = 0;i < p2.length;i++)
			{
				var ch = p2.charAt(i);
				if(ch < '0' || ch > '9')
				{
					oComp.value = p1 + "." + p2.substring(0,i);
					return noErrorFlg;
				}
			}
			if(p2.length > fdecimal)
			{
				oComp.value = p1 + "." + p2.substring(0,fdecimal);
			}
		}
		else if(oComp.value.length > nLength)
		{
			oComp.value = oComp.value.substring(0,nLength);
		}
	}
	return noErrorFlg;
}
function trimText(strComp)
	{
		var ltrim = /^\s+/
		var rtrim = /\s+$/
		strComp = strComp.replace(ltrim,'');
		strComp = strComp.replace(rtrim,'');
		return strComp;
	}
function isValidEMail(str)
	{
		var emailexp = /^[a-z][a-z_0-9\-\.]+@[a-z_0-9\-\.]+\.[a-z]{2,4}$/i

		//Check that the email entry is valid
		if (!emailexp.test(str) || str.indexOf("..") >= 0)
		{
			return false;
		}
		return true;
	}
	function trim(s)
	{
		while (s.substring(0,1) == ' ') 
		{
			s = s.substring(1,s.length);
		}
		while (s.substring(s.length-1,s.length) == ' ') 
		{
			s = s.substring(0,s.length-1);
		}
		return s;
	}
	
function isRuleBlank(oComp, sRule, nLength, fdecimal)
{
	if(fdecimal == "" || typeof(fdecimal) == "undefined")
	{
		fdecimal = false;
	}

	//If the object is not specified return false
	if (typeof(oComp) == 'undefined' || oComp == null || oComp == '')
	{
		alert('Error: Input object not specified.');
		return false;
	}
	//If neither rule nor max length is specified, return false
	else if (typeof(sRule) == 'undefined' && typeof(nLength) == 'undefined')
	{
		alert('Error: No rule/maximum length for input object specified.');
		return false;
	}

	var noErrorFlg = true;

	//If object is specified and either of rule is specified,
	if(typeof(sRule) != 'undefined' && sRule != null)
	{
		var temp;
		sRule = sRule + "";
		var discardChars = false;
		if(sRule.length > 0 && sRule.charAt(0) == "~")
		{
			sRule = sRule.substring(1);
			discardChars = true;
		}

		if(typeof(oComp) == "undefined" || typeof(sRule) == "undefined")
			return false;

		for (var i = 0;i < oComp.value.length;i++)
		{
			temp = oComp.value.charAt(i);

			if((!discardChars && sRule.indexOf(temp) == -1) || (discardChars && sRule.indexOf(temp) >= 0))
			{
				//alert("Field disobeys entry rule.  Following are the valid characters:\n" + sRule);
				//alert("Invalid Character!");
				oComp.value = oComp.value.substring(0,i);// + (oComp.value.length > i ? oComp.value.substring(i+1):"");
				noErrorFlg = false;
				break;
			}
		}
	}
	if(nLength)
	{
		if(fdecimal)
		{
			nLength -= fdecimal;
			var dp = oComp.value.indexOf(".");
			var p1;
			var p2 = "";;
			if(dp >= 0)
			{
				p1 = oComp.value.substring(0,dp);
				p2 = oComp.value.substring(dp+1);
			}
			else
			{
				p1 = oComp.value;
			}
			if(p1.length > nLength)
			{
				oComp.value = oComp.value.substring(0,nLength);
				return noErrorFlg;
			}
			for(var i = 0;i < p2.length;i++)
			{
				var ch = p2.charAt(i);
				if(ch < '0' || ch > '9')
				{
					oComp.value = p1 + "." + p2.substring(0,i);
					return noErrorFlg;
				}
			}
			if(p2.length > fdecimal)
			{
				oComp.value = p1 + "." + p2.substring(0,fdecimal);
			}
		}
		else if(oComp.value.length > nLength)
		{
			oComp.value = oComp.value.substring(0,nLength);
		}
	}
	return noErrorFlg;
}

function numbersonly(myfield, e, dec)
{
 var key;
 var keychar;

 if (window.event)
    key = window.event.keyCode;
 else if (e)
    key = e.which;
 else
    return true;
 keychar = String.fromCharCode(key);

 // control keys
 if ((key==null) || (key==0) || (key==8) ||
  (key==9) || (key==13) || (key==27) )
    return true;

 // numbers
 else if ((("0123456789").indexOf(keychar) > -1))
    return true;

// decimal point jump
 else if (dec && (keychar == "."))
    {
    myfield.form.elements[dec].focus();
    return false;
    }
 else
    return false;
 }

//-->