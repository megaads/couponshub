 
function trim(str)
{
	return str.replace(/^\s+|\s+$/g,'');
}

 
function isEmpty(formElement, message) {
	 
	formElement.value = trim(formElement.value);
	
	_isEmpty = false;
	if (formElement.value == '') {
		_isEmpty = true;
		alert(message);
		formElement.focus();
	}
	
	return _isEmpty;
}

function isPassword(password,len)
{
	if(password.value.length <len)
	{
		alert("Password should be greater or equal to "+len+" character,number or symbol");	
		password.focus();
		return false;
	}
	return true;
}
 
function doPage(frmObj,page)
{
	    
		frmObj.txtMode.value='';
		frmObj.txtCode.value='';
		 
		frmObj.PageNum.value=page;
		frmObj.submit();
	
}

// Do Action like Active ,Delete ,InActive
function doAction(frmObj,action,Code)
{ 
	 
	var blnConfirm = confirm("Do you want to "+action+" this Record ?");
	if (blnConfirm)
	{
		frmObj.txtMode.value=action;
		frmObj.txtCode.value=Code;
		frmObj.submit();
	}
}


 function doMultiAction(frmObj,action,chkbox)
 {
 	  
	var chk=frmObj[chkbox];
	 
	var t=0;
	if(typeof(chk.length)=="undefined")
	{	 
		if(!chk.checked)
		{
			alert("No Records are Selected");
			return false; 
		}
		if(window.confirm('Are you sure to '+action+' the Selected Records?'))
		{
			 frmObj.txtMode.value='M'+action;
		     frmObj.submit(); 
			return;
		}
		return ;
	}
	
	for(i=0;i<chk.length;i++)
	{
		chk[i].checked?t++:null;
	}
	if(t==0)
	{
		 alert('No Records are Selected');
		return;
	}
	 if(window.confirm('Are you sure to '+action+' the Selected Records?'))
	{
		 frmObj.txtMode.value='M'+action;
		frmObj.submit(); 
	}	
		
 }
 
// Sorting
function doSort(frmObj,sortorder,sortby)
{ 
	 frmObj.txtSortBy.value=sortby;
	 frmObj.txtSortOrder.value=sortorder; 
     frmObj.submit();
}
function doSearch(frmObj)
{ 
	 frmObj.txtMode.value='Search'; 
		if(typeof frmObj.PageNum !="undefined")
				frmObj.PageNum.value=1;
     frmObj.submit();
}
 
function showRecord(frmObj,module,extra)
{
	
		frmObj.action="module/"+module+"/index.php?view=list"+extra;
	    
		if(typeof frmObj.PageNum !="undefined")
				frmObj.PageNum.value=1;
		frmObj.submit();
}
