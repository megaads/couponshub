// JavaScript Document

function checkUserForm(frm)
{
    
	with (frm) { 
		
		 if (isEmpty(user_level, 'Select  User Privilege')) {  
			return false;
		}  
		 if (isEmpty(name, 'Enter Name')) {  
			return false;
		} else if (!IsAlphanumeric(name.value)) {
			alert('Enter proper name without any special symbols');
			name.focus();
			return false;
		}
		else if (isEmpty(user_type, 'Enter User Type')) { 
			return false;
		}  
		else if (isEmpty(email, 'Enter Email')) { 
			return false;
		} else if (!isEmail(email)) { 
			email.focus();
			return false;
		}
		 else if (liemail.value!="" && !isEmail(liemail)) { 
			liemail.focus();
			return false;
		}
		else if (isEmpty(password, 'Enter Password')) { 
			return false;
		} else if (!isPassword(password,6)) {  
			return false;
		}  
		 
		else {
			return true;
		} 
	}
} 
function showAllRecord(frmObj)
{
	if(typeof frmObj.PageNum !="undefined")
		frmObj.PageNum.value=1;
	frmObj.name.value=''; 
	frmObj.username.value=''; 
	frmObj.user_level.value='-1'; 
	frmObj.status.value='-1';  
	
    frmObj.submit(); 
		
	
}