<?php 

/************* Function for various use  ******************* 
Functions : funcCombo,funcDeleteFile,upload_file,arrayToString,stringToArray,isDuplicate,checkUser,doLogin
Author : Hitesh M. Agrawal
Designation : PHP Developer
Description : There are different different functions for use
 **********************************************************/

	 // get Message
	 function getMessage($msgcode)
	 {
	 	   
		 
		if($msgcode=="")
			return "";
		$msg='<div align="center"   id="statusmsgDIV" class="status-msg"  ><strong>';
		
		$msgarr=array(
		'addsuc'=>'Record Added Successfully',
		'updsuc'=>'Record Updated Successfully',
		'delsuc'=>'Record Deleted Successfully', 
		'addsuc'=>'Record Added Successfully',
		'statsuc'=>'Status Changed Successfully',
		'imagedelete'=>'Image Deleted Successfully',
				'filenofound'=>'File Not Found',
		'appsuc'=>'Record Approved Successfully',
		'rejsuc'=>'Record Rejected Successfully',
		'arhsuc'=>'Record Archived Successfully',
		'unarhsuc'=>'Record UnArchived Successfully',
		'delunsuc'=>'Record Not Deleted',
		'statunsuc'=>'Status Not Changed',
		'invalidpass'=>'Password id Invalid',
		'changepasssuc'=>'Password Changed Successfully',
		'changepassunsuc'=>'Password Not Changed',
		 'duperr'=>MODULE.' Already Exist with this Email Please try with different',
		 'invalidcode'=>'Record is Invalid',
		 'imgerr'=>'Image is Not Uploaded for some reason Please Upload a Convnient Image
		 
		 ',
		  'docerr'=>'Document is Not Uploaded for some reason Please Upload a Convnient Document
		 
		 ',
		 'imgaddsuc'=>'Image Added Successfully',
		 'imgdelsuc'=>'Image Deleted Successfully',
		 'imgdelunsuc'=>'There is a Problem in Deletion of Image',
         'versuc'=>'Your Account is Activated Successfully',
         'verunsuc'=>'Invalid Code',
         'importsuc'=>"Records Imported Successfully",
         'pwdsent'=>"Password has been sent on your email",
		  'testimonial_add_suc'=>"Testimonial has been submit for moderate", 
            'testimonial_add_unsuc'=>"Error in Testimonial submission", 
            'msg_sent_succ'  =>"Message Sent Successfully"  ,
           'minthreshold'=>'You are not reached to minimum threshhold',
           'rechargefail'=>"Recharge Failed please try after some time" ,
           "rechargesuc"=>"Recharge Success!",
           "rechargepending"=>"Your Recharge is in processing",
           "dupmobile"=>"Mobile Number is already exist",
           "useralreadyexist"=>"User is already registered with this mobile number"
		);
		
		$msg.=$msgarr[$msgcode];
		
		$msg.='</strong> 
		
		</div>
		
		
		';
		$msg.='<script type="text/javascript">
       setTimeout("document.getElementById(\'statusmsgDIV\').style.display=\'none\'",6000);
     </script>';
		return $msg;
	 
	 }
     
     function getOperatorName($operatorcode)
     {
        $operatorlist=array(
        'AT'=>'Airtel',
        'AL'=>'Aircel',
        'BS'=>'BSNL',
        'BSS'=>'BSNL Special',
        'IDX'=>'Idea',
        'VF'=>'Vodafone',
        'TD'=>'Docomo GSM',
        'TDS'=>'Docomo GSM Special',
        'TI'=>'Docomo CDMA (Indicom)',
        'RG'=>'Reliance GSM',
        'RL'=>'Reliance CDMA',
        'MS'=>'MTS',
        'UN'=>'Uninor',
        'UNS'=>'Uninor Special',
        'VD'=>'Videocon',
        'VDS'=>'Videocon Special',
        'MTM'=>'MTNL Mumbai',
        'MTMS'=>'MTNL Mumbai Special',
        'MTD'=>'MTNL Delhi',
        'MTDS'=>'MTNL Delhi Special',
        'VG'=>'Virgin GSM',
        'VGS'=>'Virgin GSM Special',
        'VC'=>'Virgin CDMA',
        'T24'=>'T24',
        'T24S'=>'T24 Special',
        'APOS'=>'Airtel',
        'BPOS'=>'BSNL',
        'IPOS'=>'Idea',
        'VPOS'=>'Vodafone',
        'RGPOS'=>'Reliance GSM',
        'RCPOS'=>'Reliance CDMA',
        'DGPOS'=>'TATA DOCOMO GSM',
        'DCPOS'=>'TATA   INDICOM (CDMA)',
        'CPOS'=>'AIRCEL',                    
        
        )   ;
         
        return $operatorlist[$operatorcode]; 
         
     }
     
     
     
     
	 
     function createSlug($str)
    {
        if($str !== mb_convert_encoding( mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32') )
            $str = mb_convert_encoding($str, 'UTF-8', mb_detect_encoding($str));
        $str = htmlentities($str, ENT_NOQUOTES, 'UTF-8');
        $str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\\1', $str);
        $str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
        $str = preg_replace(array('`[^a-z0-9]`i','`[-]+`'), '-', $str);
        $str = strtolower( trim($str, '-') );
        return $str;
    }
     
     
     
	 function getNextMonth($date,$format,$howmany)
	 {
		   $year=date("Y",strtotime($date));
		   $month=date("m",strtotime($date));
		   $dt=date("d",strtotime($date)); 
		   $begin=mktime(0,0,0,$month,1,$year);
		   $end=strtotime($howmany,$begin); 
	 	   $end=date('Y-m',$end).'-'.$dt; 
			return date($format,strtotime($end));  
	 }
	 // function for Create Drop Down 
	 
	 function funcCombo($rows="",$text="",$value="",$comboName="",$seletedID="",$Extra="",$default="--Please Select--")
	 {
	  $data="<select  name='{$comboName}' {$Extra} >
	  <option value=''>$default</option>
	  
	  ";
	 	
		foreach($rows as $val)
	 	{
			 if($seletedID==$val[$value])
			 	$data.="<option selected value='{$val[$value]}'>{$val[$text]}</option>";
			else
				$data.="<option  value='{$val[$value]}'>{$val[$text]}</option>";
		}
	 	$data.="</select>";
	 	return $data;
	 }
	 
	 // set Date Format
	 function dateFormat($date,$format)
	 {
	  	 
	 	$dt=date($format,strtotime($date));
			return $dt;
 
	 }
	 
	 
	function getSearchConditions($fields,$alias='')
	{
		$condition="";
		if(count($fields)>0)
		{
               
			foreach($fields as $field=>$sign)
			{      
               
				switch($sign)
				{
                   
				case "like":
					 if(isset($_POST[$field]) &&  trim($_POST[$field])!="")
					 {
					     
						if(substr_count($field,"date")>0)
						{	 
							if(trim($_POST[$field])!='mm-dd-yyyy' && trim($_POST[$field])!='')
								$condField[]=$alias.$field." like '%".date("Y-m-d",strtotime($_POST[$field]))."%'"; 
						}
						else
							  $condField[]=$alias.$field." like '%".addslashes(trim($_POST[$field]))."%'"; 
					 }	
					break;
				default :
									
					
                     if(isset($_POST[$field])&& trim($_POST[$field])!="-1" && trim($_POST[$field])!="")
					{
                        if(substr_count($field,"date")>0)
                        {     
                            if(trim($_POST[$field])!='mm-dd-yyyy' && trim($_POST[$field])!='')
                                $condField[]=$alias.$field." $sign '".date("Y-m-d",strtotime($_POST[$field]))."'"; 
                        }
                        else
                        {
                               $condField[]=$alias.$field." ".$sign."  '".addslashes(trim($_POST[$field]))."'";
                        }
                        
                    }        
                    elseif($field=="ticket_status")
                    {
                        
                    }  	
					else if(substr_count($field,"status")>0) 
					{	 
						$condField[]=$alias.$field .'<> 2'; 
					}
					break;				
				}
			}
		}
					
		 if(count($condField)>0)
		      $condition=implode(" and ",$condField); 
		return $condition;
	
	} 
	 
	 // function  for Delete File
	function funcDeleteFile($filePath,$fileName)
	{
		
		if($fileName!="" )
		{ 
			 unlink($filePath.$fileName); 
			return true;
		}
		else
		{ 
			return false;
		}	 
	}
	
	function getFileContent($file)
	  {
	  	$theData="";
		$fh = fopen($file, 'r'); 
		 
		if(filesize($file)>0)

		{
			$theData = fread($fh, filesize($file));
		}
		fclose($fh); 				 
		
		return $theData;
	  
	  }
	 
	function upload_file($field = '', $dirPath = '', $maxSize = 100000, $allowed = array(),$Pre="")
	{
		  
		foreach ($_FILES[$field] as $key => $val)
			$$key = $val;
		  
		if ((!is_uploaded_file($tmp_name)) || ($error != 0) || ($size == 0) || ($size > $maxSize))
			return false;    // file failed basic validation checks
		  
		 //Extension
		  $tofind = ".";
		  
		  $filename_len = strlen($name);
		  $position_num = strpos($name,$tofind);
		   $extension = substr("$name",$position_num+1,$filename_len-$position_num);
		 ////////////////////////////////////
		
		if ((is_array($allowed)) && (!empty($allowed)))
			if (!in_array($extension, $allowed)) 
				return false;    // file is not an allowed type 
	  
	     $TImage =  $Pre.rand(1, 9999).strtolower(basename($name));
	  	                     
		 if(move_uploaded_file($tmp_name,"$dirPath".$TImage))
		 	return $TImage;
	 
		return false;
		 
	}
	
	function createDirectory($dirPath)
	{	 
 
		if(!is_dir($dirPath)) {
   			if(mkdir($dirPath, 0777));
			{	
				 
				return true; 
			}
			 
			}
		 
 		return false;
		 
	}
	
	
	function removeDirectory($dirPath)
	{
		$dir=$dirpPath.$dirName;
		if (is_dir($dir)) {
   			if(rmdir($dir));
				return true; 
			}
 		return false; 
	}
	
	function getFiles($dirPath,$allowed)
	{
		 if(!is_dir($dirPath)) 
		 	return false;
		if ($handle = opendir($dirPath)) {
				  
			/* This is the correct way to loop over the directory. */
			while (false !== ($file = readdir($handle))) {
				 preg_match("/\.([^\.]+)$/", $file, $matches);  
					if(in_array($matches[1],$allowed))
						$files[]=$file; 
			} 
		
			closedir($handle);
		} 
		if(count($files)>0)
			return $files;
		 
			return false;
	}	
	
	// Convert array into String
	function arrayToString($arr="",$separator="")
	{
		if($arr=="" || !is_array($arr))	
			return false;
		$stringObj=implode($separator,$arr);
			return $stringObj; 
	}
	
	// Convert string to Array
	function stringToArray($str="",$separator="")
	{
		if($str=="" || is_array($str))	
			return false;
		$arrayObj=explode($separator,$str);
			return $arrayObj; 
	}
	
	// Check Duplication 
	 function isDuplicate($Id = "", $cols="",$vals="", $Table = "",  $MoreCond = "") 
	  {
	    	global $db;
			$colArr=explode(",",$cols);
			$valArr=explode("*@@*",$vals);
			 $arr=array();
			for($i=0;$i<count($colArr);$i++)
			{ 
				 $arr[]=$colArr[$i]."=".$valArr[$i];
			}
			 $arrstr=implode(" and ",$arr); // Array to String
		  
		  if($Id != "" && $arrstr != "" && $Table != "") {
			    $sql_query = "SELECT ".$Id." FROM ".$Table." WHERE ".$arrstr." ".$MoreCond." limit 1 ";
			   $cntRecord =$db->objSelect($sql_query, "ROWCNT") ;  
			  
			 
			return  $cntRecord; 
		   } else {
			return 0;
		   }
	  }	
	  
	  // Check User is Login or Not
	  function checkUser()
	  {  
			if(!isset($_SESSION['CARE_ADMIN_ID']))
			{   
				 header('location: '.WEB_ROOT.'/admin/module/login'); 
	  			 exit;
			}
	  
	  }	
	  
	  // Check User is Login or Not
	  function checkSiteUser()
	  {  
			if(!isset($_SESSION['USER_ID']))
			{   
				 header('location: '.WEB_ROOT.'login.php'); 
	  			 exit;
			}
            
	  
	  }	
      
      function isLoggedIn()
      {  
            if(isset($_SESSION['USER_ID']))
            {   
                 return true;  
            }
              return false;   
      
      }    
      
	  //Admin Login
	  function doAdminLogin($lvl,$api=false)
	  {
	   global $msg;
	   global $db;
	    $username=$_POST['username'];
		$password=md5($_POST['password']);
	  	$Record=array();
	     $sql_query="select user_id,name from ".TABLE_NAME." where email='".$username."' and password='".$password."' and  user_level =$lvl and status=1 limit 1 ";
	  
		 $Record =$db->objSelect($sql_query, "ROW"); 	
	      
		  
		  if(!empty($Record) && count($Record)>0)
			{ 
                   
                
			 
				$_SESSION['CARE_ADMIN_ID']=$Record['user_id'];
				$_SESSION['CARE_ADMIN_USERNAME']=$username; 
                $_SESSION['CARE_ADMIN_NAME']=$Record['name']; 
				header('location: '.WEB_ROOT.'admin/index.php'); 
				exit;
			}
			else
			{
                     
                
				 $msg="Invalid UserName or Password";  	
				 return $msg="Invalid UserName or Password";
				 
			}    
	  
	  }
	  
	   //Admin Login
	  function doLogin()
	  {
	   global $msg;
	   global $db;
	    $username=$_POST['username'];
		$password=md5($_POST['password']);
        $remember=$_POST['remember'];    
        
        $ajax=(isset($_POST['ajax']))?$_POST['ajax']:0;  
	  	$Record=array();
	        $sql_query="select user_id,name,email from ".TABLE_NAME." where email='".$username."' and password='".$password."' and  user_level =0 and verified=1 and status=1 limit 1 ";
	        
		$Record =$db->objSelect($sql_query, "ROW"); 	
	 
		  
		  if(!empty($Record) && count($Record)>0)
			{ 
			 
			    $_SESSION['USER_ID']=$Record['user_id'];
				 
                $_SESSION['USER_EMAIL']=$username; 
				$_SESSION['USER_NAME']=$Record['name'];
                
                if($remember)
                {
                   setcookie("username", $username);
                   setcookie("password", $password); 
                   setcookie("remember", 1);   
                }
                else
                {
                    
                   setcookie("username", '');
                   setcookie("password", ''); 
                   setcookie("remember", '');   
                      
                }
                
                
				if($ajax==1)
                {
                    return "Login Success";
                }
                header('location: '.WEB_ROOT); 
				exit;
			}
			else
			{
				 $msg="Invalid UserName or Password";  	
				 return $msg="Invalid UserName or Password";
				 
			}    
	  
	  }
	  
	  
	  
	  // status change
	  
	function changeStatus($field,$status,$type)
	{
		global $db;
		global $msg;
		if($type=='S' && isset($_POST['txtCode']))
		    $Code=$_POST['txtCode']; 
		if($type=='M' && is_array($_POST['chk']))
		    $Code=implode(",",$_POST['chk']);  
		 
		$cols="status";
		$vals=$status;
		$Cond="WHERE $field in (".$Code.")";
		$res =$db->objUpdate(TABLE_NAME,$cols,$vals,$Cond);	
		if($res)
		{
			$msg=($status==2)?"delsuc":"statsuc";
		}
		else
		{
			$msg=($status==2)?"delunsuc":"statunsuc";
		} 
		 
		 return; 
	}
    
    function changeMarkRead($field,$status,$type)
    {
        global $db;
        global $msg;
        if($type=='S' && isset($_POST['txtCode']))
            $Code=$_POST['txtCode']; 
        if($type=='M' && is_array($_POST['chk']))
            $Code=implode(",",$_POST['chk']);  
         
        $cols="mark_read";
        $vals=$status;
        $Cond="WHERE $field in (".$Code.")";
        $res =$db->objUpdate(TABLE_NAME,$cols,$vals,$Cond);    
        if($res)
        {
            $msg=($status==2)?"delsuc":"statsuc";
        }
        else
        {
            $msg=($status==2)?"delunsuc":"statunsuc";
        } 
         
         return; 
    }
	 
	function updateNotification($Code)
    {
        global $db;
       
        $cols="mark_read";
        $vals=1;
        $Cond="WHERE id = ".$Code;
        $res =$db->objUpdate("tbl_notification",$cols,$vals,$Cond);    
            
         return; 
    }  
	  
	function truncate($string, $max = 20, $replacement = '')
	{
		if (strlen($string) <= $max)
		{
			return $string;
		}
		$leave = $max - strlen ($replacement);
		return substr_replace($string, $replacement, $leave);
	}
	 // Send Mail
	 
	// Send Mail
    
    
    function sendMail($mail_to,$mail_from,$msg,$mail_subject)    
    {
        require_once 'PHPMailer-master/PHPMailerAutoload.php';
        
                //Create a new PHPMailer instance
        
             if(is_array($mail_to))
             $to=$mail_to[0];
                else
                   $to=$mail_to;
                         
        
        $mail = new PHPMailer;
        
                $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = getConfiguration('smtp_host');//'ssl://lnxind3.cloudhostdns.net';  // Specify main and backup SMTP servers
        $mail->SMTPAuth =  getConfiguration('enable_smtp'); // true                              // Enable SMTP authentication
        $mail->Username = getConfiguration('smtp_username');                 // SMTP username
        $mail->Password = getConfiguration('smtp_password');// SMTP password
        $mail->SMTPSecure =getConfiguration('smtp_secure');// 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port =getConfiguration('smtp_port');// 465;                
        
        $noreplymail=getConfiguration('noreply_email');
        //Set who the message is to be sent from
        $mail->setFrom($mail_from, SITE_NAME);
        //Set an alternative reply-to address
        $mail->addReplyTo($noreplymail);
        //Set who the message is to be sent to
        $mail->addAddress($to);
        
         if(count($mail_to)>1)
             { 
                 array_shift($mail_to);
                foreach($mail_to as $mto)
                {
                    $mail->addBCC($mto);  
                } 
               
             
           }
        
        
       
        
        //Set the subject line
        $mail->Subject =$mail_subject;
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $mail->msgHTML($msg);
        //Replace the plain text body with one created manually
      //  $mail->AltBody = 'This is a plain-text message body';
        //Attach an image file
        //$mail->addAttachment('images/phpmailer_mini.png');

        //send the message, check for errors
        if (!$mail->send()) {
         // echo    "Mailer Error: " . $mail->ErrorInfo;
        } else {
            //echo "Message sent!";
        }
             // exit;
        
        
    }           
    
    
	function sendMailback($mail_to,$mail_from,$msg,$mail_subject)
{
	
 	 if(is_array($mail_to))
         $to=$mail_to[0];
    else
       $to=$mail_to;
 
	 if(count($mail_to)>1)
	 { 
		 array_shift($mail_to);
		 $bcc=implode(",",$mail_to);
	 		
	 
	 }
		 // sending mail
         
         
         
				
						$message = $msg;
						 
						$headers  = 'MIME-Version: 1.0' . "\r\n";
		
						$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						if ($cc != "")
						{
							$headers .= "Cc: " . $cc . "\n";
						}
					
						if ($bcc != "")
						{
							$headers .= "Bcc: " . $bcc . "\n";
						} 
		
			//the headers will contain the mime version and the type of message
						 
						 $headers .= "From: ". $mail_from . " \n";  
						 
						 
						 
						 if(mail($to,$mail_subject,$message,$headers))
						{ 
						 
							 
							return true;
						}
						else
						{
							return false;
						}	 

}	  
	  
	  
	  
	 
	 function getNameById($tblname,$field,$Cond)
	 {
	 	global $db;
		
		$sql="select $field from  $tblname $Cond";
	 	$checkres=$db->objSelect($sql,"ROW");
		$fieldname=$checkres[$field];
		return $fieldname;
	 }
		
	 
         function getCategoriesTree($selectedCat="",$Extra="")
{  
    global $db;
    global $global_menu; 
    $sql="select * from tbl_category where status=1 order by parent_id";
    $res=$db->objSelect($sql,"ASSOC");
         
    
$menu = array(0 => array('children' => array()));
     foreach($res as $data){
  $menu[$data['category_id']] = $data;
  $menu[(is_null($data['parent_id']) ? '0' : $data['parent_id'] )]['children'][] = $data['category_id'];
}
        
      
$global_menu = $menu;
$nav = '<select name="parent_id">';
$nav.='<option value=0>Root Category</option>';
foreach($menu[0]['children'] as $child_id) {
  $nav .= makeNav1($menu[$child_id],$selectedCat);
}
$nav .= '</select>';
 return $nav;       
                
                
}

function makeNav1($menu,$selectedCat,$sep="") {
    
    
  global $global_menu;
  $checked="";
  if($menu['category_id']==$selectedCat)
  {
     $selected="selected='selected'"; 
  }
  
  $nav_one = ''.'<option value="'.$menu['category_id'].'" '.$selected.'>'.$sep . $menu['category_name'].'</option>';
  if(isset($menu['children']) && !empty($menu['children'])) {
 
    foreach($menu['children'] as $child_id) {
         $sep="--";
       // print_r($global_menu[$child_id]);exit; 
        $nav_one .= makeNav1($global_menu[$child_id],$selectedCat,$sep);
    }
    $nav_one .= "";
  }
  //$nav_one .= "<\n";
  return $nav_one;
}   

function loginwithFacebook($userProfile)
    {
        global $db;
        
        $userProfile['outh_provider']='facebook';  
        $userProfile['id']="";
        $query="select * from tbl_user where email='".$userProfile['email']."' and  status <> 2 " ;
        
        $res = $db->objSelect($query,"ROW");
         
        if(!empty($res) && count($res) > 0) {                                  
           
                $_SESSION['USER_ID']=$res['user_id'];                                       
                $_SESSION['USER_EMAIL']=$res['email'];
                $_SESSION['USER_NAME']=$res['name'];
            
        } 
        else
        {
             addUser($userProfile); 
        }
        if($userProfile['ajax']==1)
        {
            echo "Login Success";return;
        }
        
         header('location: '.WEB_ROOT); 
         exit;
        
        
    } 

         
   
   
function loginwithGoogle($userProfile)
    {
        global $db;
        $outh_id= $userProfile['id'];
        $userProfile['outh_provider']='google';  
        
        $query="select * from tbl_user where email='".$userProfile['email']."'  and  status <> 2 " ;
        
        $res = $db->objSelect($query,"ROW");
         
        if(!empty($res) && count($res) > 0) {                                  
           
                $_SESSION['USER_ID']=$res['user_id'];                                       
                $_SESSION['USER_EMAIL']=$res['email'];
                $_SESSION['USER_NAME']=$res['name'];
            
        } 
        else
        {
             addUser($userProfile); 
        }
         header('location: '.WEB_ROOT); 
         exit;
        
        
    } 
    
    
     function addUser($userProfile)
    {
        global $db;
        global $msg;
        global $rec;
        
        $rec['name']=$name =$userProfile['given_name'];
        $rec['newsletter']=$newsletter = 1;                                     
        $rec['email']=$email = $userProfile['email']; 
         $rec['outh_provider']=$outh_provider = $userProfile['outh_provider'];
        $rec['outh_uid']=$outh_uid = $userProfile['id'];   
         $rec['user_image']=$user_image=  $userProfile['picture'];                                                  
         $rec['status']=$status=1;
          $rec['verified']=$verified=1;
         $user_level=0;      
         $signup_bonus=getConfiguration('signup_bonus');      
          if(isset($_COOKIE['ref_id'])) {
           $ref_id=$_COOKIE['ref_id'];
        }
        else
        {
            $ref_id=0;
        }                
         
          $cols="name,email,user_level,created_date,status,verified,ref_id,newsletter,user_image,outh_provider,outh_uid";
         $vals="'".$name."','".$email."','".$user_level."','".CURRENT_DATETIME."',$status,'$verified',$ref_id,$newsletter,'$user_image','$outh_provider','$outh_uid'";
         
            $user_id =$db->objInsert('tbl_user',$cols,$vals,"LASTID"); 
            
        
        if($signup_bonus!="" && $signup_bonus!="0")
         { 
             $fields=array('user_id','retailer_id','payment_type','amount','payment_status','exp_confirm_date','reference_id'); 
             
             $rec1['user_id']= $user_id ;
             $rec1['retailer_id']= 0;
             $rec1['payment_type']= "Sign Up Bonus";
               $rec1['amount']= $signup_bonus;     
               $rec1['payment_status']= "Confirmed";             
                 $rec1['exp_confirm_date']= date("Y-m-d");
               $rec1['reference_id']=GenerateReferenceID();               
               
              $cols =implode(",",$fields).',created_date'; 
             $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec1))."','".CURRENT_DATETIME."'";
         
            $pm_id =$db->objInsert("tbl_transaction",$cols,$vals,"LASTID");     
         }                                                                 
          
       // Email for Registration Email
       
       $sql_query="select * from tbl_emailtemplate where template_id=1 and status=1";
       $templaterec =$db->objSelect($sql_query, "ROW") ; 
         
       $site_login_url=WEB_ROOT.'login.php';
       
        $emailverifylink=WEB_ROOT."verify.php?v=".$email_verify_code;  
             
        $bad=array('{{SITE_NAME}}','{{name}}','{{SITE_LOGIN_URL}}','{{username}}','{{password}}','{{activation_link}}');
        $good=array(SITE_NAME,$name,$site_login_url,$email,$password,$emailverifylink);  
          
         $Message=str_replace($bad,$good,$templaterec['content']);                                                                 
         $email_subject=str_replace($bad,$good,$templaterec['email_subject']);
         
       $adminEmail=getConfiguration('admin_email');   
       sendMail($email,$adminEmail,$Message,$email_subject);   
             
       if($ref_id)
         {   
               // Referal Bonus
            $fields=array('user_id','retailer_id','payment_type','amount','payment_status','exp_confirm_date','reference_id');   
               
             $ref_bonus = getConfiguration('refer_bonus');
             $rec1['user_id']=$ref_id ;
             $rec1['retailer_id']= 0;
             $rec1['payment_type']= "Referral Bonus";
               $rec1['amount']= $ref_bonus;     
               $rec1['payment_status']= "Pending";             
                 $rec1['exp_confirm_date']= date("Y-m-d");
               $rec1['reference_id']=GenerateReferenceID();               
               
              $cols =implode(",",$fields).',created_date'; 
             $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec1))."',NOW()";
         
            $pm_id =$db->objInsert("tbl_transaction",$cols,$vals,"LASTID");                                                                        
             
         }
         
        $_SESSION['USER_ID']=$user_id;                                       
        $_SESSION['USER_EMAIL']=$rec['email'];
        $_SESSION['USER_NAME']=$rec['name'];   
          
       addNotification("New User Registration","users",$user_id);   
    }   
   
   
    
 function checkRemoteFile($url)
{
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    // don't download content
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if(curl_exec($ch)!==FALSE)
    {
        return true;
    }
    else
    {
        return false;
    }
}   

function GenerateRandString1($len, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
    {
        $string = '';
        for ($i = 0; $i < $len; $i++)
        {
            $pos = rand(0, strlen($chars)-1);
            $string .= $chars{$pos};
        }
        return $string;
    }


function GenerateReferenceID1()
    {
       global $db;    
        unset($num);

        $num = GenerateRandString1(11,"0123456789");
    
        $sql = "SELECT * FROM tbl_transaction WHERE reference_id='$num'";
        $res=$db->objSelect($sql,"ROWCNT");
        
        if ($res == 0)
        {
            return $num;
        }
        else
        {
           GenerateReferenceID();
        }
    }
     
   function forgotAdminpassword()
    {
        global $db;
        global $msg; 
                                         
        $username = addslashes(trim($_POST['username']));                                                        
               
         
        $sql_query="select * from tbl_user where  email='".$username."' and status=1 and user_level=1 limit 1";  
           
        $rec =$db->objSelect($sql_query, "ROW"); ;  
        if($rec)
        {   
              $sql_query="select * from tbl_emailtemplate where template_id=3 and status=1";
               $templaterec =$db->objSelect($sql_query, "ROW") ; 
             
           $email=$rec['email']; 
           
             $newpassword=  GenerateReferenceID1();
             $cols="password";
          $vals="'".md5($newpassword)."'";
         
          $Cond=" where user_id= ".$rec['user_id'];         
         $res =$db->objUpdate("tbl_user",$cols,$vals,$Cond,"AFFROWS");    
           
           
           $name=$rec['name'];
            $bad=array('{{SITE_NAME}}','{{name}}','{{username}}','{{password}}');
            $good=array(SITE_NAME,$name,$email,$newpassword);  
              
              $Message=str_replace($bad,$good,$templaterec['content']);                                                                 
             $email_subject=str_replace($bad,$good,$templaterec['email_subject']);
            
           $adminEmail=getConfiguration('admin_email');   
           sendMail($email,$adminEmail,$Message,$email_subject);                                                         
                  
        } 
       
       header("location: ".WEB_ROOT."admin/module/login/?msg=pwdsent");
       exit; 
                    
                              
    }
        
		
        
 function getConfiguration($key="")
{
    global $db;
    $cond="";
         
    $sql="select * from tbl_setting where status=1".$cond." and setting_key='".$key."' order by name asc";
    $res=$db->objSelect($sql,"ROW");
    return $res['value']; 
}      

$currency=getConfiguration('currency');         
$website_logo=getConfiguration('website_logo');         
        
define('SITE_LOGO',LOGO_IMAGE_PATH_SHOW.getConfiguration('website_logo'));     
define('SITE_NAME',getConfiguration('website_title'));	 
define('ENABLE_CASHBACK_EMAIL',getConfiguration('cashback_email'));      
define('CURRENCY',$currency);
define('ENABLE_WARRANTY',getConfiguration('enable_warranty'));    




function __($value)
{   
    
    global $db;
    
    $site_language=getConfiguration('site_language');                                                         
    
    
    
    
    if(isset($_SESSION['locale']) && $site_language==$_SESSION['language'])
    {
        $data=$_SESSION['locale']; 
        return (isset($data[$value]))?$data[$value]:$value;                                                     
    }
   $_SESSION['language']=$site_language;               
    
    
    require(DOC_ROOT.'admin/excelreader/php-excel-reader/excel_reader2.php');
          require(DOC_ROOT.'admin/excelreader/SpreadsheetReader.php');
         
      $sql="select * from tbl_language where lang_id='".$site_language."' and status=1";
      $lang =$db->objSelect($sql, "ROW") ;    
           
         if($lang['lang_file']!="")
         {
             $Reader = new SpreadsheetReader(LANG_FILE_PATH_UPLOAD.$lang['lang_file']);
               $i=0;   
                foreach ($Reader as $Row)
                { 
                    $data[$Row[0]]=$Row[1];
                      
                } 
                
               
                
                
              $_SESSION['locale']=$data; 
              
           return (isset($data[$value]))?$data[$value]:$value;
         }         
   
      
    
    
    
     }




                                        function timeAgo($time_ago){
                            $cur_time     = time();
                            $time_elapsed     = $cur_time - $time_ago;
                            $seconds     = $time_elapsed ;
                            $minutes     = round($time_elapsed / 60 );
                            $hours         = round($time_elapsed / 3600);
                            $days         = round($time_elapsed / 86400 );
                            $weeks         = round($time_elapsed / 604800);
                            $months     = round($time_elapsed / 2600640 );
                            $years         = round($time_elapsed / 31207680 );
                            // Seconds
                            if($seconds <= 60){
                                return "$seconds seconds ago";
                            }
                            //Minutes
                            else if($minutes <=60){
                                if($minutes==1){
                                    return "1 min ago";
                                }
                                else{
                                    return "$minutes minutes ago";
                                }
                            }
                            //Hours
                            else if($hours <=24){
                                if($hours==1){
                                    return "1 hour ago";
                                }else{
                                    return "$hours hours ago";
                                }
                            }
                            //Days
                            else if($days <= 7){
                                if($days==1){
                                    return "yesterday";
                                }else{
                                    return "$days days ago";
                                }
                            }
                            //Weeks
                            else if($weeks <= 4.3){
                                if($weeks==1){
                                    return "a week ago";
                                }else{
                                    return "$weeks weeks ago";
                                }
                            }
                            //Months
                            else if($months <=12){
                                if($months==1){
                                    return "1 month ago";
                                }else{
                                    return "$months months ago";
                                }
                            }
                            //Years
                            else{
                                if($years==1){
                                    return "1 year ago";
                                }else{
                                    return "$years years ago";
                                }
                            }
                }

                     
        function sendMessage($mobileNumber,$msg="",$api=false)
        {
            
            
                                      //Your authentication key
                        $authKey = getConfiguration('msg_auth_key');//"101982AUkSknlHM569129f5";
                                                 
                        //Sender ID,While using route4 sender id should be 6 characters long.
                        $senderId = getConfiguration('msg_sender_id');

                       if($msg!="")
                       {
                           $message= $msg;    
                       }else
                       { 
                        $verify_code=GenerateRandString1(4);
                        $_SESSION['verify_code']=$verify_code;
                        $_SESSION['phone']=$mobileNumber;   
                        //Your message to send, Add URL encoding here.
                        $message = str_replace("{verify_code}",$verify_code,getConfiguration('sms_message_text'));
                       }
                       
                       
                        
                          
                        //Define route 
                        $route = "Transactional";
                        //Prepare you post parameters
                        $postData = array(
                            'authkey' => $authKey,
                            'mobiles' => $mobileNumber,
                            'message' => $message,
                            'sender' => $senderId,
                            'route' => $route
                        );

                        //API URL
                        $url=getConfiguration('msg_gateway_url');

                        // init the resource
                        $ch = curl_init();
                        curl_setopt_array($ch, array(
                            CURLOPT_URL => $url,
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_POST => true,
                            CURLOPT_POSTFIELDS => $postData
                            //,CURLOPT_FOLLOWLOCATION => true
                        ));


                        //Ignore SSL certificate verification
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


                        //get response
                        $output = curl_exec($ch);
                           
                        //Print error if any
                        if(curl_errno($ch))
                        {
                            return 'error:' . curl_error($ch);
                        }

                        curl_close($ch);
                         
                        if($api)
                        {
                            return $verify_code;
                        } 
                         
                        return $output;                                
            
            
            
        }


        function rechargeMobile($params)
        {
            if(isset($params))
            {
                $mobile=$params['mobile']; 
                $operator=$params['operator']; 
                $amount=$params['amount'];
               $username= getConfiguration('recharge_username');   
               $yourapikey= getConfiguration('recharge_apikey');   
               $uniqueorderid = substr(number_format(time() * rand(),0,'',''),0,10);  
               
               $ch = curl_init();
               $timeout = 100; // set to zero for no timeout
               
               
               
            $myHITurl = "http://joloapi.com/api/recharge.php?mode=1&userid=$username&key=$yourapikey&operator=$operator&service=$mobile&amount=$amount&orderid=$uniqueorderid";
               
               
               curl_setopt ($ch, CURLOPT_URL, $myHITurl);
                curl_setopt ($ch, CURLOPT_HEADER, 0);
                curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                $file_contents = curl_exec($ch);
                $curl_error = curl_errno($ch);
                curl_close($ch);
                                                          
               $maindata = explode(",", $file_contents);
                $countdatas = count($maindata);
               
               if($countdatas > 2)
               {
                    //recharge is success
                    $joloapiorderid = $maindata[0]; //it is  joloapi.com    generated order id
                    $txnstatus = $maindata[1]; //it is status of recharge SUCCESS,FAILED
                    $operator= $maindata[2]; //operator code
                    $service= $maindata[3]; //mobile num  ber
                    $amount= $maindata[4]; //amount
                    $mywebsiteorderid= $maindata[5]; //your website order id
                    $errorcode= $maindata[6]; // api error code 
                    $operatorid= $maindata[7]; //original operator transaction id
                    $myapibalance= $maindata[8];  //my   joloapi.com  remaining balance
                    $myapiprofit= $maindata[9]; //my earning on this recharge
                    $txntime= $maindata[10]; // recharge time
                    }
               else{
                    //recharge is failed
                    $txnstatus = $maindata[0]; //it is status of recharge FAILED
                    $errorcode= $maindata[1]; // api error code 
               }
               
               
               if($curl_error=='28'){
                    //Request timeout, consider recharge status as pending/success
                    $txnstatus = "PENDING";
                                   
               }
              
              
              return $txnstatus; 
               
                   
                
                
            }
            
            
            
            
            
            
        }
        
        
 
 
    
?>