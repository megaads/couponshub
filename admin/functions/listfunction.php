<?php
/************* Function for get Table******************* 
Function : getTable
Author : Hitesh M. Agrawal
Designation : PHP Developer
Description : this function is for get List Your have to pass the result set AND necessary parameter 
you get the whole table with sorting and paging with dynamic links like edit ,delete
 **********************************************************/
  
  function getUserBalance1($uid="")
{
      global $db;
      $cond="";
      if($uid!="")
      $cond=" user_id=".$uid;
      
         
     $sql="select sum(amount) as total_amount from tbl_transaction where ".$cond." and payment_status='Confirmed' group by payment_status";
    $totalcashback=$db->objSelect($sql,"ROW");
              
     $sql="select sum(amount) as total_amount from tbl_transaction where ".$cond." and payment_status in ('Requested','Paid','Declined') group by payment_status";
    $totalpaid=$db->objSelect($sql,"ROW");          
                                            
    $balance= $totalcashback['total_amount'] -$totalpaid['total_amount'];
              
              
    return $balance; 
}
  
  
  
	function getLink($type="",$colval="",$Code=0,$page="",$links="",$editextralink="",$detailextralink="")
	{
	 	global $PageNum;  
		$link="";
		switch($type)
		{
		case "status":
			switch($colval)
			{
			case 0:
				$link='<a href="javascript:doAction(document.frm,\'Active\','.$Code.')" title="Make it Active"><img src="images/ico-inactive.gif" border="0"></a>';
				break;
			case 1:
				$link='<a href="javascript:doAction(document.frm,\'InActive\','.$Code .')" title="Make it InActive"><img src="images/ico-active.gif" border="0"></a>';
				break;	
			case 2:
				$link='<a href="javascript:doAction(document.frm,\'Restore\','.$Code.')" title="Make it Restore"><img src="images/icon-trash.png" border="0"></a>';
				break;
			default:
				$link="";
				break;  
			} 
			break;
        case "mark_read":
            switch($colval)
            {
            case 0:
                $link='<a href="javascript:doAction(document.frm,\'MarkRead\','.$Code.')" title="Mark as Read">Mark as Read</a>';
                break;
            case 1:
                $link='<a href="javascript:doAction(document.frm,\'MarkUnRead\','.$Code .')" title="Mark as UnRead">Mark as UnRead</a>';
                break;           
            default:
                $link="";
                break;  
            } 
            break;    
		case "verified":
			switch($colval)
			{
			case 0:
				$link='<a href="javascript:doAction(document.frm,\'Approve\','.$Code.')" title="Make it Approve"><img src="images/ico-pending.gif" border="0"></a>';
				break;
			case 1:
				$link='<a href="javascript:doAction(document.frm,\'Reject\','.$Code .')" title="Make it Reject"><img src="images/ico-approve.gif" border="0"></a>';
				break;	
			case 2:
				$link='<a href="javascript:doAction(document.frm,\'Approve\','.$Code.')" title="Make it Approve"><img src="images/icon-unapprove.gif" border="0"></a>';
				break;
			default:
				$link="";
				break;  
			} 
			break;	
		case "archive":
			switch($colval)
			{
			case 0:
				$link='<a href="javascript:doAction(document.frm,\'Archive\','.$Code.')" title="Make it Archive"><img src="images/icon-unarchi.gif" border="0"></a>';
				break;
			case 1:
				$link='<a href="javascript:doAction(document.frm,\'UnArchive\','.$Code .')" title="Make it UnArchive"><img src="images/icon-archi.gif" border="0"></a>';
				break; 
			default:
				$link="";
				break;  
			} 
			break;	
		case "user_level":
				$level= array('User','Admin','SuperAdmin'); 
				$link=$level[$colval];  
				break;
        case "cashbk":         
                $good=array('fixed','percent');
                $currency=trim(getConfiguration('currency'));
                $bad=array($currency,'%');
                $link=str_replace($good,$bad,$colval);  
                break;        
          case "transaction_amount":                            
                $currency=trim(getConfiguration('currency'));                                                
                $link=(($colval)?$colval:0)." ".$currency;  
                break;                
         case "user_balance":                            
                $balance=trim(getUserBalance1($Code));                                                
                $link=$balance." ".$currency;  
                break;             
                
		case "member_type":
				$member_type= array('Free','Paid'); 
				$link=$member_type[$colval];  
				break;
		case "listing_type":
				$listingtype= array('res'=>'Residential','com'=>'Commercial','ren'=>'Rental',
				'lan'=>'Land','con'=>'Condo'); 
				$link=$listingtype[$colval];  
				break;
		case "member_plan_expdate":
			 		$val=($colval<date("Y-m-d"))?"<font color='red'>".dateFormat($colval,"d/m/Y")."</font>":dateFormat($colval,"d/m/Y");
				$link=$val;  
				break;		
		case "county":
				$county= array('s'=>'Suffolk','n'=>'Nassau','q'=>'Queens',
				'u'=>'Upstate' ); 
				$link=$county[$colval];  
				break;	
        case "signup_url":
                $link='<a href="'.$colval.'" target="_blank">Click Here for Signup</a>';
                break;        			 			 				 			 
		case "edit":
				$link='<a href="'.$page.'/index.php?'.$editextralink.'view=modify&Code='.$Code.'&PageNum='.$PageNum.'" alt="Edit" title="Edit"><img src="images/icon-edit.gif" border="0"></a>';
				break;
                 
        case "referred_users":
                $link='<a href="'.WEB_ROOT.'/admin/module/referredusers/?ref_id='.$Code.'" alt="Referred Users" title="Referred Users"><img src="images/referral-icon.png" border="0"></a>';
                break;         
                
        case "total_click":
                $link='<a href="'.WEB_ROOT.'/admin/module/clickhistorys/?user_id='.$Code.'" alt="Total Click" title="Click History">'.$colval.'</a>';
                break; 
         case "total_open":
                $link='<a href="'.WEB_ROOT.'/admin/module/emaillogs/index.php?view=openlist&email_id='.$Code.'" alt="Click Here to view list of user" title="Open History">'.$colval.'</a>';
                break;         
        case "catcashback":
                $link='<a href="'.WEB_ROOT.'/admin/module/cashbacks/?retailer_id='.$Code.'" alt="Click Here to add category wise cashback" title="Click Here to add category wise cashback">Cashback</a>';
                break;          
        case "credit":
                $link='<a href="'.WEB_ROOT.'/admin/module/payments/index.php?view=add&user_id='.$Code.'" alt="Manual Credit" title="Manual Credit"><img src="images/transfer.png"  border="0"></a>';
                break;               
        case "pay":
                $link='<a href="'.$page.'/index.php?'.$editextralink.'view=modify&Code='.$Code.'&PageNum='.$PageNum.'" alt="Pay" title="Pay">Pay</a>';
                break;        
		case "delete":
				$link='<a href="javascript:doAction(document.frm,\'Delete\','.$Code.')" title="Delete" alt="Delete"><img src="images/icon-delete.gif" border="0"></a>';
				break;
		case "detail":
				$link='<a href="'.$page.'/index.php?'.$detailextralink.'view=detail&Code='.$Code.'&PageNum='.$PageNum.'" title="Details" alt="Details"><img src="images/icon-preview.gif" border="0"></a>';
				break; 
		default: 
				  $link='<a href="'.$links.'" target="_blank" alt="'.$links.'" title="'.$links.'" >'.$colval.'</a>';
				break;								
			  	
		}
		
		
	return $link; 
	}
	function getPaging($query,$recPerPage,$PageNum)
	{
	 	 global $db;
		  
		 $tmp=$db->objSelect($query, "ROW");
		 $RecordCnt=$tmp['totRec'];
		 $numLinks=3;
		 $maxpage=(int)($RecordCnt/$recPerPage); 
		  $lastNum= $_POST['lastNum'];
		  $startNum=$_POST['startNum'];	
		  
		  $rem=$RecordCnt%$recPerPage;
			if($rem>0)
				$maxpage++;
		 
		 $pageLink=""; 
		  
		  if($maxpage>1)
		  {
               
		  		  $end=0;
				  $pageLink.='<input type="hidden" name="PageNum" value="'.$PageNum.'"  />';
				  
				 
				  if($PageNum!=1)
				  {
				  	 $pageLink.='<a href="javascript:doPage(document.frm,1)">First</a> | ';
					 $pageLink.='<a href="javascript:doPage(document.frm,'.($PageNum-1).')">Prev</a> | ';
				  }
				   
				  if($lastNum==""  && $maxpage==$PageNum && $PageNum%$numLinks==0)
				  { 
							  $end=$PageNum;
							  $start=$end-$numLinks+1;
						  
		 		  } 			   
				  else if($lastNum==$PageNum && $PageNum == $maxpage)
				  {
				  	 $start = $startNum;
					 $end   = $start + $numLinks - 1; 
				   	 $end   = min($maxpage, $end);
				  }
				 else if($lastNum==$PageNum && $PageNum <= $maxpage)
				  {
				  	 $start = $PageNum;
					 $end   = $start + $numLinks - 1; 
				   	 $end   = min($maxpage, $end);
				  }
				  else if($PageNum >= $numLinks && $PageNum <= $maxpage)
				   {
					 	//echo $PageNum;
					  
						if(($PageNum== $maxpage) || $PageNum%$numLinks!=0)
						  $start = $PageNum-1 ;//- ($PageNum % $numLinks) ;
						else
						 $start = $PageNum ;//- ($PageNum % $numLinks) ;
			    	 	
						 $end   = $start + $numLinks - 1; 
				   		 $end   = min($maxpage, $end);
					}
					else if($PageNum < $numLinks)
					{
					 	$start=1;
						$end=$numLinks;
						$end  = min($maxpage, $end);
					} 
				  
				  for($i=$start;$i<=$end;$i++) {  
					if($PageNum==$i)
					$pageLink.='<strong><font color="#ff0000" >'.$i.'</font></strong> | ';
					else 
					$pageLink.='<a href="javascript:doPage(document.frm,'.$i.')">'.$i.'</a> | ';
					  }
				 if($PageNum!=$maxpage)
				  {
				  	 $pageLink.='<a href="javascript:doPage(document.frm,'.($PageNum+1).')">Next</a> | ';
					  $pageLink.='<a href="javascript:doPage(document.frm,'.$maxpage.')">Last</a> ';
				  }	   
				$pageLink.='<input type="hidden" name="startNum" value="'.$start.'"  />';	    
				$pageLink.='<input type="hidden" name="lastNum" value="'.$end.'"  />';	    
		  
		  } 
          
          $pageLink.="<div style='float:right'>Total Records $RecordCnt</div>";
		  return $pageLink;
	
	}
	
	
	function getTable($rows="",$allow="",$class="")
	{  
		/*********Variable Initialization Start*******************/
		$tblattr=(isset($class['tableattr']))?$class['tableattr']:"";
		$checkbox=(isset($allow['checkbox']))?$allow['checkbox']:"";
		$colsize=(isset($allow['colsize']))?$allow['colsize']:"";
		$links=(isset($allow['links']))?$allow['links']:"";
		$editextralink=(isset($allow['editextralink']))?$allow['editextralink']:"";
		$detailextralink=(isset($allow['detailextralink']))?$allow['detailextralink']:"";
		$sorting=(isset($allow['sorting']))?$allow['sorting']:"";
		$sortby=(isset($allow['sortby']))?$allow['sortby']:"";
		$sortorder=(isset($allow['sortorder']))?$allow['sortorder']:"";
		$page=(isset($allow['page']))?$allow['page']:"";  
		$header=(isset($allow['header']))?$allow['header']:"";  
		$columns=(isset($allow['colnval']))?$allow['colnval']:"";  
		$PagLink=(isset($allow['PagLink']))?$allow['PagLink']:"";  
		 
		$ExtraFea=(isset($allow['EXTFEA']))?$allow['EXTFEA']:"";  
		$ExtraFeaField=(isset($allow['EXTFEAFIELD']))?$allow['EXTFEAFIELD']:"";  
		/*********Variable Initialization End*******************/
		
		/*********Hidden Field for Sorting End*******************/
		$tbl='<input type="hidden" name="txtSortBy" value="'.$sortby.'">'; 
		$tbl.='<input type="hidden" name="txtSortOrder" value="'.$sortorder.'">';
		/*********Hidden Field for Sorting End*******************/ 
		
		/*********Create List Table Start*******************/ 
		$tbl.='<table cellspacing="1" cellpadding="2" border="0" style="width: 100%;" id="ctl00_MainContent_grdUserList" class="table table-striped table-bordered bootstrap-datatable datatable" ><tbody>';
		
		/*********Display Header Part Start*******************/ 
		$tbl.='<thead><tr style="white-space: nowrap;" class="fieldheader">'; 
		// if check box is true 
		
		$t=0;
		if(isset($checkbox) && is_array($checkbox) && $checkbox['allow']==1)  // if check box
			$tbl.="<th align='left' style='width: ".$colsize[$t]."' scope='col'><input type='checkbox' name='checkAll' onClick=\"$('input[name*=\'".$checkbox['name']."\']').attr('checked', this.checked);\"></th>";
		$t++;
	 
		foreach($header as $rec)
		{ 
			$tbl.= "<th>{$rec}";
			
			if(is_array($sorting) && array_key_exists($rec,$sorting))
			{
				 
				if($sortby==$sorting[$rec] && $sortorder=='asc')
					$tbl.=' <a href="javascript:doSort(document.frm,\'desc\',\''.$sorting[$rec].'\')" title="Descending"><img src="images/down.gif" border="0"></a>';
				else
					$tbl.=' <a href="javascript:doSort(document.frm,\'asc\',\''.$sorting[$rec].'\')" title="Asscending" ><img src="images/up.gif" border="0"></a>';
			
			}
			
			$tbl.="</th>";
			$t++;
		} 
		
		$tbl.="</tr></thead>";
		/*********Display Header Part End*******************/   
		$i=1;
		 /*********Display Rows Part Start*******************/   
		if(isset($rows) && count($rows)>0) 
		{ 
			foreach($rows as $col)
			{
				if($i%2==0)
				{
					$trbody="darkbg";
					$tdbody="cls_td2";
				}
				else
				{
					$trbody="lightbg";
					$tdbody="cls_td1";
				}
				
				 $Code=$col[$checkbox['value']];
				
				$tbl.="<tr class='".$trbody."'>";
				if($checkbox)  // if check box
				$tbl.="<td align='left' ><input type='checkbox' name='".$checkbox['name']."[]' value='".$Code."' ></td>";
				 
			 
				foreach($columns as $curcol)
				{ 
					if(in_array($curcol,$ExtraFea))
					{  
					   	$key=array_search($curcol,$ExtraFea);
						if($ExtraFeaField[$key]!="")
						{ 
							 $links=$col[$ExtraFeaField[$key]];
						}  
						 $link= getLink($curcol,$col[$curcol],$Code,$page,$links,$editextralink,$detailextralink); 
						 $tbl.= "<td align='left'>".$link."</td>";
					}
					else if(substr_count($curcol,"date")>0)
					{
						if(substr_count($curcol,"add")>0 || substr_count($curcol,"update")>0)
						$tbl.= "<td align='left'>".dateFormat($col[$curcol],"m/d/Y h:i:s A")."</td>";
						else
						$tbl.= "<td align='left'>".dateFormat($col[$curcol],"m/d/Y")."</td>";	
					} 
					else
					{  
						$tbl.= "<td align='left'>{$col[$curcol]}</td>";
					} 
				}
			 
				$tbl.="</tr>";
				
				$i++;
				
			}
		}
		else
		{
			$tbl.="<tr><td align='center' colspan='".($t)."' class='lightbg' ><b>Sorry! No Records Found<b></td></tr>";
		}
		 /*********Display Rows Part End*******************/    
		$tbl.="<tr><td align='center' colspan='".($t)."'>".$PagLink."</td></tr>";
		
		  $tbl.="</table>"; 
		 /*********Create List Table End*******************/ 
		return $tbl;
	}
	 

?>