<?php
        
  function getCategories($slug="")
{
    global $db;
      $cond="";
      if($slug!="")
      $cond=" and identifier='".$slug."'";
             
    $sql="select * from tbl_category where status=1".$cond." order by category_name asc";
    if($slug!="")  
    {
        $res=$db->objSelect($sql,"ROW");
                       
    }
    else
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}

 function getCategoryId($name="")
{
    global $db;
      $cond="";
      if($name!="")
      $cond=" and category_name='".$name."'";
             
    $sql="select * from tbl_category where status=1".$cond." limit 1";
    if($name!="")  
    {
        $res=$db->objSelect($sql,"ROW");
                        
    }
   
    return $res['category_id']; 
}

function getHomeCategories()
{
    global $db;
      $cond=" and show_home=1";
       
             
    $sql="select * from tbl_category where status=1".$cond." order by category_name asc";
       
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}
  function getLanguages($id="")
{
    global $db;
      $cond="";
      if($id!="")
      $cond=" and lang_id='".$id."'";
             
      $sql="select * from tbl_language where status=1".$cond." order by name asc";
    if($id!="")  
    {
        $res=$db->objSelect($sql,"ROW");
                       
    }
    else
       $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}
           

       
function getRetailers($store_id="",$slug="",$catId="",$offset="",$recPerPage="",$searchCond="")
{
    global $db;
      $cond="";
      if($slug!="")
      $cond=" and r.identifier='".$slug."'";
     if($store_id!="")
       $cond=" and r.retailer_id=".$store_id; 
       
        if($recPerPage!="")
        {
           $limit="limit $offset,$recPerPage"; 
        }       
       
       
      if($catId!="")
         $sql="select r.*,count(cp.coupon_id) as total_coupon from tbl_retailer r,tbl_category_retailer c left join tbl_coupon cp on c.retailer_id=cp.store_id and cp.status=1 where r.retailer_id=c.retailer_id and c.category_id=".$catId." and r.status=1 ".$cond."  group by r.retailer_id order by r.name asc $limit";   
      else   
           $sql="select r.*,count(c.coupon_id) as total_coupon from tbl_retailer r left join tbl_coupon c on r.retailer_id=c.store_id and c.status=1  where r.status=1 ".$cond." $searchCond group by r.retailer_id order by r.name asc $limit";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}

function getRetailersMenu()
{
    global $db;
      $cond="";
                               
    $sql="select r.*,count(c.coupon_id) as total_coupon from tbl_retailer r left join tbl_coupon c on r.retailer_id=c.store_id and c.status=1  where r.status=1 and r.is_menu=1  ".$cond." $searchCond group by r.retailer_id order by r.name asc limit 10";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}



function getBanners()
{
    global $db;
      $cond="";
     
        
    $sql="select * from tbl_banner where status=1 order by sort asc";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}

function getPageContent($id="",$slug="")
{
    global $db;
      $cond="";
      if($slug!="")
      $cond=" and identifier='".$slug."'";
     if($store_id!="")
       $cond=" and content_id=".$id; 
        
        $sql="select * from tbl_content where status=1".$cond."";
    $res=$db->objSelect($sql,"ROW");
    return $res; 
}

function getFeaturedRetailers()
{
    global $db;
               
    $sql="select r.retailer_id,r.name as retailer_name,r.identifier,r.logo,concat(r.cashback,'',fixed_percent) as cashback,count(c.coupon_id) as total_coupon  from tbl_retailer r,tbl_coupon c  where r.retailer_id=c.store_id and r.is_featured=1 and c.status=1 and r.status=1".$cond." group by c.store_id order by r.name asc limit 10";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}



function getPaymentMethods()
{
    global $db;
      $cond="";
         
    $sql="select * from tbl_paymentmethod where name <> 'Mobile Recharge' and status=1".$cond." order by name asc";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}


function getCashbackList($sid="")
{
    global $db;
      $cond="";
      if($sid!="")
      $cond=" and retailer_id=".$sid;
        
    $sql="select * from tbl_cashback where status=1".$cond." order by cashback desc";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}

function getRedirectDataViaStore($sid)
{
     global $db;
      $cond="";
      if($sid!="")
      $cond=" and retailer_id=".$sid;
        
    $sql="select r.retailer_id,r.url,a.aff_key,r.logo,concat(r.cashback,'',r.fixed_percent) as cashback,r.name as retailer_name from tbl_retailer r,tbl_affiliate a  where r.affiliate_id=a.affiliate_id and r.status=1".$cond;
    $res=$db->objSelect($sql,"ROW");
    return $res;                       
    
}


function getRedirectDataViaCoupon($cid)
{
     global $db;
      $cond="";
      if($cid!="")
      $cond=" and coupon_id=".$cid;
        
    $sql="select c.coupon_id,c.url,a.aff_key,r.logo,concat(r.cashback,'',r.fixed_percent) as cashback,r.name as retailer_name,r.retailer_id from tbl_coupon c,tbl_retailer r,tbl_affiliate a  where c.store_id=r.retailer_id and r.affiliate_id=a.affiliate_id and r.status=1".$cond;
    $res=$db->objSelect($sql,"ROW");
    return $res; 
    
    
    
    
}

function getUserId($email="")
{
    global $db;
      $cond="";
      if($email!="")
      $cond=" and email='".$email."'";
        
       $sql="select user_id from tbl_user where status=1".$cond;    
    $res=$db->objSelect($sql,"ROW");
    return $res; 
}


function getUserInfo($userid="")
{
    global $db;
      $cond="";
      if($userid!="")
      $cond=" and user_id='".$userid."'";
        
     $sql="select * from tbl_user where status=1".$cond;    
    $res=$db->objSelect($sql,"ROW");
    return $res; 
}


function getCoupons($sid="",$slug="",$coupon_id="",$featured="",$offset="",$recPerPage="",$searchCond="",$type="")
{
    global $db;              
    
                                                 
      $cond="";
                 
       if($searchCond!="")
           $cond.=$searchCond;
      if($slug!="")
           $cond.=" and c.identifier='".$slug."'";
      elseif($coupon_id!="")
           $cond.=" and coupon_id=".$coupon_id;  
      
 if($type=="coupons")
      {
         $cond.=" and coupon_code!=''"; 
          
      }
       if($type=="deals")
      {
         $cond.=" and (coupon_code='' || coupon_code is NULL)"; 
          
      }


      if($sid!="")
      $cond.=" and store_id=".$sid;
       
        if($featured!="")
        {
          $cond.=" and c.end_date>='".TODAY."' and c.is_featured=".$featured;
          $limit="limit 12";
        }
        elseif($recPerPage!="")
        {
           $limit="limit $offset,$recPerPage"; 
        }
     
      
        
           $sql="select c.*,concat(r.cashback,'',r.fixed_percent) as cashback,r.logo as retailer_logo from tbl_coupon c,tbl_retailer r where r.retailer_id=c.store_id and c.status=1".$cond." order by coupon_id desc ".$limit;
    if($coupon_id!="" || $slug!="")
        $res=$db->objSelect($sql,"ROW");
    else
       $res=$db->objSelect($sql,"ASSOC"); 
    return $res; 
}

function getCouponsbyCat($catId,$offset="",$recPerPage="",$searchCond="",$type="")
{
    global $db;              
    
                                                 
      $cond="";
                 
       if($searchCond!="")
           $cond.=$searchCond;
          
           if($type=="coupons")
      {
         $cond.=" and coupon_code!=''"; 
          
      }
       if($type=="deals")
      {
         $cond.=" and (coupon_code='' || coupon_code is NULL)"; 
          
      }  
           $limit="limit $offset,$recPerPage"; 
               
        
              $sql="select c.*,concat(r.cashback,'',r.fixed_percent) as cashback,r.logo as retailer_logo from tbl_coupon c left join tbl_category_coupon cc on cc.coupon_id=c.coupon_id inner join tbl_retailer r  where r.retailer_id=c.store_id and c.status=1 and cc.category_id=$catId ".$cond." order by coupon_id desc ".$limit;
            
       $res=$db->objSelect($sql,"ASSOC"); 
    return $res; 
}
function getAffilliates($affiliate_id="")
{
    global $db;
      $cond="";
      if($affiliate_id!="")
      $cond=" and affiliate_id=".$affiliate_id;
        
      $sql="select * from tbl_affiliate where status=1".$cond;
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}

function getVersion($make="",$model="")
{
    global $db;
    
    if(trim($make)!="")
    {
        $searchCond=" and make='$make'";
    }
    if(trim($model)!="")
    {
        $searchCond.=" and model='$model'";
    }
    
      $sql="select distinct version from tbl_cars where status=1 $searchCond";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}

function getState($make="",$model="",$version="")
{
    global $db;
    
    if(trim($make)!="")
    {
        $searchCond=" and make='$make'";
    }
    if(trim($model)!="")
    {
        $searchCond.=" and model='$model'";
    }
    if(trim($version)!="")
    {
        $searchCond.=" and version='$version'";
    }
    
    $sql="select distinct state from tbl_cars where status=1 $searchCond";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}

function getYear($make="",$model="",$version="",$state="")
{
    global $db;
    
    if(trim($make)!="")
    {
        $searchCond=" and make='$make'";
    }
    if(trim($model)!="")
    {
        $searchCond.=" and model='$model'";
    }
    if(trim($version)!="")
    {
        $searchCond.=" and version='$version'";
    }
    if(trim($state)!="")
    {
        $searchCond.=" and state='$state'";
    }
    
    $sql="select distinct year from tbl_cars where status=1 $searchCond";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}
function getPrice($make="",$model="",$version="",$state="",$year="")
{
    global $db;
    
    if(trim($make)!="")
    {
        $searchCond=" and make='$make'";
    }
    if(trim($model)!="")
    {
        $searchCond.=" and model='$model'";
    }
    if(trim($version)!="")
    {
        $searchCond.=" and version='$version'";
    }
    if(trim($state)!="")
    {
        $searchCond.=" and state='$state'";
    }
    if(trim($year)!="")
    {
        $searchCond.=" and year='$year'";
    }
    
    $sql="select distinct price from tbl_cars where status=1 $searchCond";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}

function getColor()
{
    global $db;
    $sql="select distinct color from tbl_cars where status=1";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}

 
    function GenerateRandString($len, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
    {
        $string = '';
        for ($i = 0; $i < $len; $i++)
        {
            $pos = rand(0, strlen($chars)-1);
            $string .= $chars{$pos};
        }
        return $string;
    }
 


/**
 * Returns random payment's reference ID
 * @return    string    Reference ID
*/


    function GenerateReferenceID()
    {
       global $db;    
        unset($num);

        $num = GenerateRandString(11,"0123456789");
    
        $sql = "SELECT * FROM tbl_transaction WHERE reference_id='$num'";
        $res=$db->objSelect($sql,"ROWCNT");
        
        if ($res == 0)
        {
            return $num;
        }
        else
        {
           GenerateReferenceID();
        }
    }
    
    function getMyCashback($uid="",$paymentstatus="")
{
      global $db;
      $cond="";
      if($uid!="")
      $cond=" user_id=".$uid;
      
      if($paymentstatus!="")
      {
         $cond.=" and payment_status='".$paymentstatus."'"; 
      }
        
    $sql="select payment_status,sum(amount) as total_amount from tbl_transaction where ".$cond." group by payment_status";
    $res=$db->objSelect($sql,"ASSOC");
              
    return $res; 
}

 function getUserBalance($uid="")
{
      global $db;
      $cond="";
      if($uid!="")
      $cond=" user_id=".$uid;
      
         
     $sql="select sum(amount) as total_amount from tbl_transaction where ".$cond." and payment_status='Confirmed' group by payment_status";
    $totalcashback=$db->objSelect($sql,"ROW");
              
   $sql="select sum(amount) as total_amount from tbl_transaction where ".$cond." and payment_status in ('Requested','Paid','Declined') and payment_type='Withdraw'";
    $totalpaid=$db->objSelect($sql,"ROW");          
   /*  echo $totalpaid['total_amount'];
     echo "<br>";
     echo $totalcashback['total_amount'];exit;  */                        
                    
    $balance= $totalcashback['total_amount'] -$totalpaid['total_amount'];
              
              
    return $balance; 
}

function getUserPendingAmount($uid="")
{
      global $db;
      $cond="";
      if($uid!="")
      $cond=" user_id=".$uid;
      
         
     $sql="select sum(amount) as total_amount from tbl_transaction where ".$cond." and payment_status='Pending' group by payment_status";
    $totalcashback=$db->objSelect($sql,"ROW");
              
    
                                            
    $balance= $totalcashback['total_amount'];
              
              
    return $balance; 
}
 function getMyWithdrawRequest($uid="",$offset,$recPerPage)
{
      global $db;
      $cond="";
      if($uid!="")
      $cond=" user_id=".$uid;
        
       
      $cond.=" and payment_status in ('Requested','Paid','Declined')";  
          
       $limit="";
      if($recPerPage!="")
      {
         $limit="limit $offset,$recPerPage"; 
      }
        
       $sql="select t.* from tbl_transaction t where ".$cond." order by transaction_id desc $limit";
       $res=$db->objSelect($sql,"ASSOC");
       return $res; 
}



 function getMyRechargeHistory($uid="",$offset,$recPerPage)
{
      global $db;
      $cond="";
      if($uid!="")
      $cond=" user_id=".$uid;
                       
       $limit="";
      if($recPerPage!="")
      {
         $limit="limit $offset,$recPerPage"; 
      }
        
        $sql="select t.* from tbl_recharge t where ".$cond." order by recharge_id desc $limit";
       $res=$db->objSelect($sql,"ASSOC");
       return $res; 
}




  function getMyEarnings($uid="",$payment_status="",$offset,$recPerPage)
{
      global $db;
      $cond="";
      if($uid!="")
      $cond=" user_id=".$uid;
        
     if($paymentstatus!="")
      {
         $cond.=" and payment_status='".$paymentstatus."'"; 
      }
      else
      {
        $cond.=" and payment_status!='Requested'";  
      }   
       $limit="";
      if($recPerPage!="")
      {
         $limit="limit $offset,$recPerPage"; 
      }
        
       $sql="select t.*,r.name as retailer_name from tbl_transaction t left join tbl_retailer r on t.retailer_id=r.retailer_id where ".$cond." order by transaction_id desc $limit";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}


 function getMyClicks($uid="",$offset,$recPerPage)
{
      global $db;
      $cond="";
      if($uid!="")
      $cond=" user_id=".$uid;
        
      
       $limit="";
      if($recPerPage!="")
      {
         $limit="limit $offset,$recPerPage"; 
      }
        
    $sql="select c.*,r.name as retailer_name,r.retailer_id,r.identifier from tbl_clickhistory c , tbl_retailer r where  ".$cond." and c.retailer_id=r.retailer_id and c.status=1 and r.status=1 group by c.retailer_id order by c.click_id  desc $limit";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}



 function getReferredFriends($uid="",$offset,$recPerPage)
{
      global $db;
      $cond="";
      if($uid!="")
      $cond=" ref_id=".$uid;
        
        
       $limit="";
      if($recPerPage!="")
      {
         $limit="limit $offset,$recPerPage"; 
      }
        
       $sql="select * from tbl_user where ".$cond." $limit";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}

 function getContactList($uid="",$offset,$recPerPage)
{
      global $db;
      $cond="";
      if($uid!="")
      $cond=" user_id=".$uid;
        
        
       $limit="";
      if($recPerPage!="")
      {
         $limit="limit $offset,$recPerPage"; 
      }
        
       $sql="select * from tbl_contactlist where ".$cond." $limit";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}


function getMyTickets($uid="",$offset,$recPerPage)
{
      global $db;
      $cond="";
      if($uid!="")
      $cond=" user_id=".$uid;
        
     
       $limit="";
      if($recPerPage!="")
      {
         $limit="limit $offset,$recPerPage"; 
      }
        
     $sql="select t.*,r.name as retailer_name from tbl_ticket t left join tbl_retailer r on t.retailer_id=r.retailer_id where ".$cond." order by t.ticket_id desc $limit";
    $res=$db->objSelect($sql,"ASSOC");
    return $res; 
}

function getTotalPages($query,$recPerPage)
{
       global $db;
     global $RecordCnt; 
      
     $tmp=$db->objSelect($query, "ROW");
     
     $RecordCnt=$tmp['totRec'];  
      return $maxpage=ceil($RecordCnt/$recPerPage); 
    
}





           function getPagination($query,$recPerPage,$PageNum,$path,$view='list',$extralink='',$maxRec='200')
    {
                   
         global $db;
         global $RecordCnt; 
          
         $tmp=$db->objSelect($query, "ROW");
         
         $RecordCnt=$tmp['totRec'];  
          
          
                        
         $numLinks=10;
          $maxpage=(int)($RecordCnt/$recPerPage); 
         if($PageNum==1 || $PageNum=="")
         {
              $lastNum= "";
              $startNum="";    
         }
          else
          {
              $lastNum= $_SESSION['lastNum'];
              $startNum=$_SESSION['startNum'];    
          }
          $rem=$RecordCnt%$recPerPage;
            if($rem>0)
                $maxpage++;
         
         $pageLink=""; 
           
          if($maxpage>1)
          {
                    $end=0;
                  $pageLink.='<input type="hidden" name="PageNum" value="'.$PageNum.'"  />';
                   
                  if($PageNum!=1)
                  {
                      $pageLink.=' <li><a href="'.$path.'/p/1'.$extralink.'"><i class="fa fa-angle-double-left"></i></a></li> ';
                     //  $pageLink.='<a href="'.$path.'?PageNum=1'.$extralink.'"><img src="images/arrow-left-page.jpg" alt="" title="" /><img src="images/arrow-left-page.jpg" alt="" title="" />&nbsp;</a>   ';
                   //  $pageLink.='<a href="'.$path.'?PageNum='.($PageNum-1).$extralink.'"><img src="images/arrow-left-page.jpg" alt="" title="" /></a>   ';
                  }
                   
                  if($lastNum==""  && $maxpage==$PageNum && $PageNum%$numLinks==0)
                  { 
                              $end=$PageNum;
                              $start=$end-$numLinks+1;
                          
                   }                
                  else if($lastNum==$PageNum && $PageNum == $maxpage)
                  {
                       $start = $startNum;
                     $end   = $start + $numLinks - 1; 
                        $end   = min($maxpage, $end);
                  }
                 else if($lastNum==$PageNum && $PageNum <= $maxpage)
                  {
                       $start = $PageNum;
                     $end   = $start + $numLinks - 1; 
                        $end   = min($maxpage, $end);
                  }
                  else if($PageNum >= $numLinks && $PageNum <= $maxpage)
                   {
                         //echo $PageNum;
                      
                        if(($PageNum== $maxpage) || $PageNum%$numLinks!=0)
                          $start = $PageNum-1 ;//- ($PageNum % $numLinks) ;
                        else
                         $start = $PageNum ;//- ($PageNum % $numLinks) ;
                         
                         $end   = $start + $numLinks - 1; 
                            $end   = min($maxpage, $end);
                    }
                    else if($PageNum < $numLinks)
                    {
                         $start=1;
                        $end=$numLinks;
                        $end  = min($maxpage, $end);
                    } 
                  
                  for($i=$start;$i<=$end;$i++) {  
                    if($PageNum==$i)
                    $pageLink.=' <li><a href="'.$path.'/p/'.($i).$extralink.'">'.$i.'</a></li>';
                    else 
                    $pageLink.='  <li><a href="'.$path.'/p/'.($i).$extralink.'">'.$i.'</a></li>';
                      }
                     
                  if($PageNum!=$maxpage)
                  {
                       $pageLink.='<li><a href="'.$path.'/p/'.($PageNum+1).$extralink.'"><i class="fa fa-angle-double-right"></i></a></li> ';
                    //  $pageLink.='<a href="'.$path.'?PageNum='.($maxpage).$extralink.'"><img src="images/arrow-right-page.jpg" alt="" title="" /><img src="images/arrow-right-page.jpg" alt="" title="" /></a> ';
                  }      
                
                 $_SESSION['lastNum']=$end;
                 $_SESSION['startNum']=$start;    
                //$pageLink.='<input type="hidden" name="startNum" value="'.$start.'"  />';        
                //$pageLink.='<input type="hidden" name="lastNum" value="'.$end.'"  />';        
          
          } 
          return $pageLink;
    
    }

       function getSearchPagination($query,$recPerPage,$PageNum,$path,$view='list',$extralink='',$maxRec='200')
    {
                   
         global $db;
         global $RecordCnt; 
          
         $tmp=$db->objSelect($query, "ROW");
       
           $RecordCnt=$tmp['totRec'];  
            
          
                        
         $numLinks=10;
          $maxpage=(int)($RecordCnt/$recPerPage); 
         if($PageNum==1 || $PageNum=="")
         {
              $lastNum= "";
              $startNum="";    
         }
          else
          {
              $lastNum= $_SESSION['lastNum'];
              $startNum=$_SESSION['startNum'];    
          }
          $rem=$RecordCnt%$recPerPage;
            if($rem>0)
                $maxpage++;
         
         $pageLink=""; 
           
          if($maxpage>1)
          {
                    $end=0;
                  $pageLink.='<input type="hidden" name="PageNum" value="'.$PageNum.'"  />';
                   
                  if($PageNum!=1)
                  {
                      $pageLink.=' <li><a href="'.$path.'&PageNum=1'.$extralink.'"><i class="fa fa-angle-double-left"></i></a></li> ';
                     //  $pageLink.='<a href="'.$path.'?PageNum=1'.$extralink.'"><img src="images/arrow-left-page.jpg" alt="" title="" /><img src="images/arrow-left-page.jpg" alt="" title="" />&nbsp;</a>   ';
                   //  $pageLink.='<a href="'.$path.'?PageNum='.($PageNum-1).$extralink.'"><img src="images/arrow-left-page.jpg" alt="" title="" /></a>   ';
                  }
                   
                  if($lastNum==""  && $maxpage==$PageNum && $PageNum%$numLinks==0)
                  { 
                              $end=$PageNum;
                              $start=$end-$numLinks+1;
                          
                   }                
                  else if($lastNum==$PageNum && $PageNum == $maxpage)
                  {
                       $start = $startNum;
                     $end   = $start + $numLinks - 1; 
                        $end   = min($maxpage, $end);
                  }
                 else if($lastNum==$PageNum && $PageNum <= $maxpage)
                  {
                       $start = $PageNum;
                     $end   = $start + $numLinks - 1; 
                        $end   = min($maxpage, $end);
                  }
                  else if($PageNum >= $numLinks && $PageNum <= $maxpage)
                   {
                         //echo $PageNum;
                      
                        if(($PageNum== $maxpage) || $PageNum%$numLinks!=0)
                          $start = $PageNum-1 ;//- ($PageNum % $numLinks) ;
                        else
                         $start = $PageNum ;//- ($PageNum % $numLinks) ;
                         
                         $end   = $start + $numLinks - 1; 
                            $end   = min($maxpage, $end);
                    }
                    else if($PageNum < $numLinks)
                    {
                         $start=1;
                        $end=$numLinks;
                        $end  = min($maxpage, $end);
                    } 
                  
                  for($i=$start;$i<=$end;$i++) {  
                    if($PageNum==$i)
                    $pageLink.=' <li><a href="'.$path.'&PageNum='.($i).$extralink.'">'.$i.'</a></li>';
                    else 
                    $pageLink.='  <li><a href="'.$path.'&PageNum='.($i).$extralink.'">'.$i.'</a></li>';
                      }
                     
                  if($PageNum!=$maxpage)
                  {
                       $pageLink.='<li><a href="'.$path.'&PageNum='.($PageNum+1).$extralink.'"><i class="fa fa-angle-double-right"></i></a></li> ';
                    //  $pageLink.='<a href="'.$path.'?PageNum='.($maxpage).$extralink.'"><img src="images/arrow-right-page.jpg" alt="" title="" /><img src="images/arrow-right-page.jpg" alt="" title="" /></a> ';
                  }      
                
                 $_SESSION['lastNum']=$end;
                 $_SESSION['startNum']=$start;    
                //$pageLink.='<input type="hidden" name="startNum" value="'.$start.'"  />';        
                //$pageLink.='<input type="hidden" name="lastNum" value="'.$end.'"  />';        
          
          } 
          return $pageLink;
    
    }

       function getAccountPagination($query,$recPerPage,$PageNum,$path,$view='list',$extralink='',$maxRec='200')
    {
                   
         global $db;
         global $RecordCnt; 
          
         $tmp=$db->objSelect($query, "ROW");
       
           $RecordCnt=$tmp['totRec'];  
            
          
                        
         $numLinks=10;
          $maxpage=(int)($RecordCnt/$recPerPage); 
         if($PageNum==1 || $PageNum=="")
         {
              $lastNum= "";
              $startNum="";    
         }
          else
          {
              $lastNum= $_SESSION['lastNum'];
              $startNum=$_SESSION['startNum'];    
          }
          $rem=$RecordCnt%$recPerPage;
            if($rem>0)
                $maxpage++;
         
         $pageLink=""; 
           
          if($maxpage>1)
          {
                    $end=0;
                  $pageLink.='<input type="hidden" name="PageNum" value="'.$PageNum.'"  />';
                   
                  if($PageNum!=1)
                  {
                      $pageLink.=' <li><a href="'.$path.'?PageNum=1'.$extralink.'"><i class="fa fa-angle-double-left"></i></a></li> ';
                     //  $pageLink.='<a href="'.$path.'?PageNum=1'.$extralink.'"><img src="images/arrow-left-page.jpg" alt="" title="" /><img src="images/arrow-left-page.jpg" alt="" title="" />&nbsp;</a>   ';
                   //  $pageLink.='<a href="'.$path.'?PageNum='.($PageNum-1).$extralink.'"><img src="images/arrow-left-page.jpg" alt="" title="" /></a>   ';
                  }
                   
                  if($lastNum==""  && $maxpage==$PageNum && $PageNum%$numLinks==0)
                  { 
                              $end=$PageNum;
                              $start=$end-$numLinks+1;
                          
                   }                
                  else if($lastNum==$PageNum && $PageNum == $maxpage)
                  {
                       $start = $startNum;
                     $end   = $start + $numLinks - 1; 
                        $end   = min($maxpage, $end);
                  }
                 else if($lastNum==$PageNum && $PageNum <= $maxpage)
                  {
                       $start = $PageNum;
                     $end   = $start + $numLinks - 1; 
                        $end   = min($maxpage, $end);
                  }
                  else if($PageNum >= $numLinks && $PageNum <= $maxpage)
                   {
                         //echo $PageNum;
                      
                        if(($PageNum== $maxpage) || $PageNum%$numLinks!=0)
                          $start = $PageNum-1 ;//- ($PageNum % $numLinks) ;
                        else
                         $start = $PageNum ;//- ($PageNum % $numLinks) ;
                         
                         $end   = $start + $numLinks - 1; 
                            $end   = min($maxpage, $end);
                    }
                    else if($PageNum < $numLinks)
                    {
                         $start=1;
                        $end=$numLinks;
                        $end  = min($maxpage, $end);
                    } 
                  
                  for($i=$start;$i<=$end;$i++) {  
                    if($PageNum==$i)
                    $pageLink.=' <li><a href="'.$path.'?PageNum='.($i).$extralink.'">'.$i.'</a></li>';
                    else 
                    $pageLink.='  <li><a href="'.$path.'?PageNum='.($i).$extralink.'">'.$i.'</a></li>';
                      }
                     
                  if($PageNum!=$maxpage)
                  {
                       $pageLink.='<li><a href="'.$path.'?PageNum='.($PageNum+1).$extralink.'"><i class="fa fa-angle-double-right"></i></a></li> ';
                    //  $pageLink.='<a href="'.$path.'?PageNum='.($maxpage).$extralink.'"><img src="images/arrow-right-page.jpg" alt="" title="" /><img src="images/arrow-right-page.jpg" alt="" title="" /></a> ';
                  }      
                
                 $_SESSION['lastNum']=$end;
                 $_SESSION['startNum']=$start;    
                //$pageLink.='<input type="hidden" name="startNum" value="'.$start.'"  />';        
                //$pageLink.='<input type="hidden" name="lastNum" value="'.$end.'"  />';        
          
          } 
          return $pageLink;
    
    }
    
    
    
     function getCategoriesMenu()
{  
    global $db;
    global $global_menu; 
    $sql="select * from tbl_category where status=1 and parent_id=0 order by parent_id";
    $res=$db->objSelect($sql,"ASSOC");
    
     $Code= $_GET['Code'];    
    $query="select category_id from tbl_category_retailer where retailer_id=".$Code;
     $rescat=$db->objSelect($query,"ASSOC");  
     $selectedCat=array();
     foreach($rescat as $ct)
     {
         $selectedCat[]=$ct['category_id'];
     } 
    
    
$menu = array(0 => array('children' => array()));
     foreach($res as $data){
  $menu[$data['category_id']] = $data;
  $menu[(is_null($data['parent_id']) ? '0' : $data['parent_id'] )]['children'][] = $data['category_id'];
}
        
      
$global_menu = $menu;
$nav = '<ul class="dropdown-menu cat-dropdown">';
foreach($menu[0]['children'] as $child_id) {
  $nav .= makeNav($menu[$child_id],$selectedCat);
}
$nav .= '</ul>';
 return $nav;       
                
                
}

function makeNav($menu,$selectedCat) {
    
    
  global $global_menu;
  $checked="";
  if(in_array($menu['category_id'],$selectedCat))
  {
     $checked="checked='checked'"; 
  }
  
  $nav_one = '<li>'."\n\t".'<a href="'.WEB_ROOT."category/".$menu['identifier'].'">' . $menu['category_name'].'</a>';
  if(isset($menu['children']) && !empty($menu['children'])) {
    $nav_one .= "<ul>\n";
    foreach($menu['children'] as $child_id) {
       // print_r($global_menu[$child_id]);exit; 
        $nav_one .= makeNav($global_menu[$child_id],$selectedCat);
    }
    $nav_one .= "</ul>\n";
  }
  $nav_one .= "</li>\n";
  return $nav_one;
}            


function getOffersGoal5($offerid)
{      
    define('HASOFFERS_API_URL', 'https://api.hasoffers.com/Apiv3/json');
    
    $args = array(
        'NetworkId' => 'vcm',
        'Target' => 'Affiliate_Goal',
        'Method' => 'findAll',
        'api_key' => 'e76f977a4523ed9da83106d721d5344691f7e307d49b0a8982bf067ae49d45d1',
        'filters' => array(
            'offer_id' => $offerid
        )
    );
    
     // Initialize cURL
    $curlHandle = curl_init();
 
    // Configure cURL request
    curl_setopt($curlHandle, CURLOPT_URL, HASOFFERS_API_URL . '?' . http_build_query($args));
 
    // Make sure we can access the response when we execute the call
    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
 
    // Execute the API call
    $jsonEncodedApiResponse = curl_exec($curlHandle);
 
    // Ensure HTTP call was successful
    if($jsonEncodedApiResponse === false) {
        throw new \RuntimeException(
            'API call failed with cURL error: ' . curl_error($curlHandle)
        );
    }
 
    // Clean up the resource now that we're done with cURL
    curl_close($curlHandle);
 
    // Decode the response from a JSON string to a PHP associative array
    $apiResponse = json_decode($jsonEncodedApiResponse, true);
 
    // Make sure we got back a well-formed JSON string and that there were no
    // errors when decoding it
    $jsonErrorCode = json_last_error();
    if($jsonErrorCode !== JSON_ERROR_NONE) {
        throw new \RuntimeException(
            'API response not well-formed (json error code: ' . $jsonErrorCode . ')'
        );
    }
 
    // Print out the response details
    if($apiResponse['response']['status'] === 1) {
        // No errors encountered
        echo 'API call successful';
        echo PHP_EOL;
        echo 'Response Data: ' . print_r($apiResponse['response']['data'], true);
        echo PHP_EOL;
    }
    else {
        // An error occurred
        echo 'API call failed (' . $apiResponse['response']['errorMessage'] . ')';
        echo PHP_EOL;
        echo 'Errors: ' . print_r($apiResponse['response']['errors'], true);
        echo PHP_EOL;
    }      
    
    
}

function addNotification($title,$module,$id,$icon)
{
    
    global $db;
             
          $cols="title,module,id,icon,created_date";
         $vals="'".$title."','".$module."','".$id."','".$icon."','".CURRENT_DATETIME."'";
         
         $res =$db->objInsert("tbl_notification",$cols,$vals); 
               
        return;
    
}

 
    
   


?>
