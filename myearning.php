<?php include_once 'myearning_field.php';?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('My Earning');?> - <?php echo SITE_NAME;?></title>
                               
<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/main.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
         

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
  <script type="text/javascript">
    $(document).ready(function(){
        $(".ui-tabs").tabs();
    });
</script>
</head>
<body>
<div class="se-pre-con"></div>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="myaccount">
     <div class="container">
      <?php include_once 'breadcrumb.php';?>
     <div class="ui-tabs">
      <div class="col-md-3"><?php include 'custmenu.php';?></div>    
       <div class="col-md-9 white-body">    
    <div id="tabs-1" class="ui-tabs-panel">
        <h4><?php echo('Cashback History');?>
         <button type="button" onclick="window.location='<?php echo WEB_ROOT?>payment.php'" class="btn"><?php echo('Withdraw Money');?></button> </h4>
        
       <div class="table-responsive">
                  <table class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th><?php echo('Date');?></th>
                        <th><?php echo('Retailer');?></th>
                        <th><?php echo('Amount');?></th>
                        <th><?php echo('Status');?></th>
                        <th><?php echo('Expected Confirmation');?></th>   
                        <th><?php echo('# Ref. ID');?></th>
                      </tr>
                    </thead>
                   <tbody>
                   <?php foreach($earnings as $earn):?>
                   <tr>
                   <td><?php echo date('M d,Y',strtotime($earn['created_date']))?></td>
                   <td><?php echo (isset($earn['retailer_name']) && $earn['retailer_name']!="")?$earn['retailer_name']:$earn['payment_type']?></td>
                   <td> <?php echo $currency; ?><?php echo $earn['amount'];?></td>
                    <td><?php echo $earn['payment_status'];?></td>      
                   <td><?php echo date('M d,Y',strtotime($earn['exp_confirm_date']))?></td>
                   <td><?php echo $earn['reference_id'];?></td>
                   </tr>
                   
                   <?php endforeach;?>
                   
                   </tbody>       
                  </table>
                    <ul class="pagination">
            <?php echo $paging;?>
             
        </ul>
                </div>
    </div>
    </div>
        
</div>
     
     
     </div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
     <?php include 'js.php';?>      
</body>
</html>