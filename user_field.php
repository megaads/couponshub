<?php 
        define('TABLE_NAME','tbl_user');
        require_once("admin/system/constant.php"); 
        require_once("admin/system/databaseLayer.php"); 
        require_once("admin/functions/common.php"); 
        require_once("admin/functions/listfunction.php"); 
        require_once("admin/functions/commonfun.php"); 
        require_once("config.php");                     
        include('facebook/hybridauth/Hybrid/Auth.php');
      
     if(isset($_GET['provider']))
        {
            $provider = $_GET['provider'];
            
            try{
            
            $hybridauth = new Hybrid_Auth( $config );
            
            $authProvider = $hybridauth->authenticate($provider);

            $user_profile = $authProvider->getUserProfile();
            
                if($user_profile && isset($user_profile->identifier))
                {   
                    $userProfile['given_name']=$user_profile->displayName;
                    $userProfile['email']=$user_profile->email;
                    $userProfile['picture']= $user_profile->photoURL;
                    
                   loginwithFacebook($userProfile);                                   
                }            

            }
            catch( Exception $e )
            { 
               

            }
        
        }  
                  
          
          
          
     $action = isset($_POST['txtMode']) ? $_POST['txtMode'] : '';
     
    switch ($action) {
        
        case 'add' :
            add();
             
            break;   
       case 'sendcode' :
            sendcode();               
            break;        
         case 'fblogin' :                      
           echo loginwithFacebook($_POST);
           exit;
            break;        
                
        case 'login' :
           echo doLogin();
           exit;
            break;
            case 'update' :
            modify();
            break;
         case 'changepwd' :
            changepassword();
            break;    
           case 'forgotpassword' :
            forgotpassword();
            break;       
         default:
              $rec=getRecord();  
            
              
         
    }    
    
    
    function getRecord($Code="")
    {
        //Show Record Per Page
         global $db; 
         
         if(!isset($_SESSION['USER_ID']))
         {
             return;
         }
         
         $Code=$_SESSION['USER_ID'];
         $sql_query="select * from tbl_user where user_id='".$Code."'";  
         $rec =$db->objSelect($sql_query, "ROW") ;  
         return $rec;
    }
        
        
    function add()
    {
        global $db;
        global $msg;
        global $rec;
                    
         
        $signup_email_activation=getConfiguration('signup_email_activation');     
        $signup_bonus=getConfiguration('signup_bonus');     
        $rec['name']=$name = addslashes(trim($_POST['name']));
         $rec['newsletter']=$newsletter = addslashes(trim($_POST['newsletter']));
        // $rec['city']=$city = addslashes(trim($_POST['city']));
        // $rec['country']=$country = addslashes(trim($_POST['country']));
        $rec['email']=$email = addslashes(trim($_POST['email']));
         $rec['phone']=$phone = addslashes(trim($_POST['phone']));
        $rec['password']=$password = addslashes(trim($_POST['password']));
         $rec['status']=$status=1;
         $user_level=0; 
         $ajax=(isset($_POST['ajax']))?$_POST['ajax']:0;          
         $ref_id=$_COOKIE['ref_id'];
        $ref_id=($ref_id!="")?$ref_id:0;
        
        if(ENABLE_REF_CODE):
            $referencecode=  addslashes(trim($_POST['ref_code']));   
            if($referencecode!="")
            {
                 $sql="select * from tbl_user where status=1 and verified=1 and ref_code='$referencecode'";
               $refrec =$db->objSelect($sql,"ROW");
               if(isset($refrec['user_id']))
                  $ref_id=$refrec['user_id'];
            }
        endif;
          
        if(ENABLE_MOBILE_REF_CODE):
              $referencecode=  addslashes(trim($_POST['mobile_ref_code']));   
            if($referencecode!="")
            {
                  $sql="select * from tbl_user where status=1 and verified=1 and phone='$referencecode'";
               $refrec =$db->objSelect($sql,"ROW");
               if(isset($refrec['user_id']))
                  $ref_id=$refrec['user_id'];
            }
        endif;  
          
            
         
        $ref_code=GenerateRandString1(4); 
        // $enable_otp=1;
        if($enable_otp && $ajax && ($_SESSION['verify_code']!=$_POST['verification_code'] || $_SESSION['phone']!=$_POST['phone']))
        {
            
              echo "invalidotp";  exit;   
        }
        
        if($enable_otp && $ajax && ($_SESSION['verify_code']!=$_POST['verification_code'] || $_SESSION['phone']!=$_POST['phone']))
        {
             $msg="invalidotp";
             return;
        }
        
         
        ///
        $sqlextra=" and status <> 2";
        $cols="email";
        $vals="'".$email."'";  
        
           $resChk = isDuplicate("user_id",$cols,$vals,'tbl_user', $sqlextra);
             
       if($ajax && !empty($resChk) && $resChk > 0)
        {
           echo "duperr";  exit;
        } 
         
        if(!empty($resChk) && $resChk > 0) {
              $msg="duperr";
             return;  
        }
        ///   
        
        if($signup_email_activation=="Off")
        {
           $verified=1;
           $email_verify_code=""; 
           $template_id=4;
        }
        else
        {
            $email_verify_code=GenerateReferenceID();
             $verified=0; 
             $template_id=1;     
        }     
         
         
           $cols="name,email,phone,password,user_level,created_date,status,email_verified_code,ref_id,newsletter,verified,ref_code";
           $vals="'".$name."','".$email."','".$phone."','".md5($password)."','".$user_level."','".CURRENT_DATETIME."',$status,'$email_verify_code',$ref_id,$newsletter,$verified,'$ref_code'";
             
            $user_id =$db->objInsert('tbl_user',$cols,$vals,"LASTID"); 
                 
         $fields=array('user_id','retailer_id','payment_type','amount','payment_status','exp_confirm_date','reference_id'); 
         
         if($signup_bonus!="" && $signup_bonus!="0")
         {
             $rec1['user_id']= $user_id ;
             $rec1['retailer_id']= 0;
             $rec1['payment_type']= "Sign Up Bonus";
               $rec1['amount']= $signup_bonus;     
               $rec1['payment_status']= "Confirmed";             
                 $rec1['exp_confirm_date']= date("Y-m-d");
               $rec1['reference_id']=GenerateReferenceID();               
               
              $cols =implode(",",$fields).',created_date'; 
             $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec1))."','".CURRENT_DATETIME."'";
         
            $pm_id =$db->objInsert("tbl_transaction",$cols,$vals,"LASTID");                                                                    
         } 
       // Email for Registration Email
           
       $sql_query="select * from tbl_emailtemplate where template_id=1 and status=1";
       $templaterec =$db->objSelect($sql_query, "ROW") ; 
         
       $site_login_url=WEB_ROOT.'login.php';
       
        $emailverifylink=WEB_ROOT."verify.php?v=".$email_verify_code;  
             
        $bad=array('{{SITE_NAME}}','{{name}}','{{SITE_LOGIN_URL}}','{{username}}','{{password}}','{{activation_link}}');
        $good=array(SITE_NAME,$name,$site_login_url,$email,$password,$emailverifylink);  
          
         $Message=str_replace($bad,$good,$templaterec['content']);                                                                 
         $email_subject=str_replace($bad,$good,$templaterec['email_subject']);
         
       $adminEmail=getConfiguration('admin_email');   
       sendMail($email,$adminEmail,$Message,$email_subject);   
             
         if($signup_email_activation=="Off" && $ref_id)
         {   
               // Referal Bonus
            $fieldsref=array('user_id','retailer_id','payment_type','amount','payment_status','exp_confirm_date','reference_id');   
               
             $ref_bonus = getConfiguration('refer_bonus');
             $rec1['user_id']=$ref_id ;
             $rec1['retailer_id']= 0;
             $rec1['payment_type']= "Referral Bonus";
               $rec1['amount']= $ref_bonus;     
               $rec1['payment_status']= "Pending";             
                 $rec1['exp_confirm_date']= date("Y-m-d");
               $rec1['reference_id']=GenerateReferenceID();               
               
              $cols =implode(",",$fieldsref).',created_date'; 
             $vals ="'".str_replace("*@@*","','",implode("*@@*",$rec1))."','".CURRENT_DATETIME."'";
         
            $pm_id =$db->objInsert("tbl_transaction",$cols,$vals,"LASTID");                                                                        
             
         }
        
        addNotification("New User Registration","users",$user_id,"user");
        if($ajax && $user_id)
        {
           echo "Registration Success";  exit;
        }
         
        if($user_id)
        {
            unset($_COOKIE['ref_id']);
             $action="";
             header("location: thanks.php?msg=addsuc");
             exit;
        }
          
          
    }
    
    function modify()
    {
        global $db;
        global $msg;
         
        $name = addslashes(trim($_POST['name']));
        $city = addslashes(trim($_POST['city']));
        $country = addslashes(trim($_POST['country']));
        $phone = addslashes(trim($_POST['phone'])); 
        $email = addslashes(trim($_POST['email'])); 
         
       
         $user_id=(isset($_SESSION['USER_ID']))?trim($_SESSION['USER_ID']):0; 
        if($user_id=="")
        {
            $msg="invalidcode";
            return;
        
        }  
        
        $sqlextra=" and user_id <> ".$user_id." and  status <> 2";
        $cols="email";
        $vals="'".$email."'";  
        
          $resChk = isDuplicate("user_id",$cols,$vals,TABLE_NAME, $sqlextra);
         
        if(!empty($resChk) && $resChk > 0) {
              $msg="duperr";
             return;  
        } 
        
          if($_FILES['user_image']['name']!="")
         {   
             $allowed=array('jpg','JPG','png','PNG'); 
             $Pre="";
              $user_image= upload_file("user_image",USER_IMAGE_PATH_UPLOAD, MAX_FILE_SIZE, $allowed,$Pre);
             
            if($_FILES['user_image']['name']!="" && !$user_image)
            {
             $msg="imgerr";
                return;
            }    
        }      
           
           $cols="name,email,city,country ,phone,user_image";
          
           $vals="'".$name."'*@@*'".$email."'*@@*'".$city."'*@@*'".$country."'*@@*'".$phone."'*@@*'".$user_image."'";
                    
              
         $Cond=" where user_id= ".$user_id;
         $res =$db->objUpdate(TABLE_NAME,$cols,$vals,$Cond); 
        if($res)
        { 
            if($_GET['PageNum']!="")
                $url="&PageNum=".$_GET['PageNum'];
             header("location: myprofile.php?msg=updsuc");
             exit;
        } 
    } 
    
    
     function changepassword()
    {
        global $db;
        global $msg; 
                                         
        $oldpassword = md5(addslashes(trim($_POST['oldpassword'])));
        $newpassword = md5(addslashes(trim($_POST['newpassword'])));
         
         $user_id=(isset($_SESSION['USER_ID']))?trim($_SESSION['USER_ID']):0; 
        if($user_id=="")
        {
            $msg="invalidcode";
            return;
        
        }    
            $sql_query="select * from tbl_user where  user_id='".$user_id."' and password='".$oldpassword."' limit 1";  
           
           $rec =$db->objSelect($sql_query, "ROWCNT"); ;  
        if($rec<=0)
        {   
            $msg="invalidpass";
            return;
        } 
                    
          $cols="password";
          $vals="'".$newpassword."'";
         
          $Cond=" where user_id= ".$user_id;         
         $res =$db->objUpdate(TABLE_NAME,$cols,$vals,$Cond,"AFFROWS"); 
        if($res>0)
        { 
             $msg="changepasssuc";
             return;
        }
        else
        {
             $msg="changepassunsuc";
             return;
        } 
    } 
    
    
     function forgotpassword()
    {
        global $db;
        global $msg; 
                                         
        $username = addslashes(trim($_POST['username']));                                                        
               
         
        $sql_query="select * from tbl_user where  email='".$username."' and status=1 limit 1";  
           
        $rec =$db->objSelect($sql_query, "ROW"); ;  
        if($rec)
        {   
              $sql_query="select * from tbl_emailtemplate where template_id=2 and status=1";
               $templaterec =$db->objSelect($sql_query, "ROW") ; 
                 
               $site_login_url=WEB_ROOT.'login.php';
               $newpassword=  GenerateReferenceID();
             $cols="password";
          $vals="'".md5($newpassword)."'";
         
          $Cond=" where user_id= ".$rec['user_id'];         
         $res =$db->objUpdate("tbl_user",$cols,$vals,$Cond,"AFFROWS");    
               
               
                    
           $email=$rec['email']; 
            $password=$newpassword; 
           $name=$rec['name'];
            $bad=array('{{SITE_NAME}}','{{name}}','{{SITE_LOGIN_URL}}','{{username}}','{{password}}');
            $good=array(SITE_NAME,$name,$site_login_url,$email,$password,$emailverifylink);  
              
              $Message=str_replace($bad,$good,$templaterec['content']);                                                                 
             $email_subject=str_replace($bad,$good,$templaterec['email_subject']);
            
           $adminEmail=getConfiguration('admin_email');   
           sendMail($email,$adminEmail,$Message,$email_subject);                                                         
                  
        } 
       
       header("location: ".WEB_ROOT."login.php?msg=pwdsent");
       exit; 
                    
                              
    }         
             
    
   $retailerlist= getRetailersMenu();  
   
   
   function sendcode()
   {
       
       $mobile=$_POST['mobile'];
       
         $sqlextra=" and status <> 2";
        $cols="phone";
        $vals="'".$mobile."'";  
        
           $resChk = isDuplicate("user_id",$cols,$vals,'tbl_user', $sqlextra);
             
       if(!empty($resChk) && $resChk > 0)
        {
           echo "dupmobile";  exit;
        }       
       
         
       echo sendMessage($mobile);  
       exit;                    
       
   }        
                            
    
   
                       
        
?>
