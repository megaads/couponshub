<!--Add the following script at the bottom of the web page (before </body></html>)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type='text/javascript'>
$(document).ready(function(){ 
    $(window).scroll(function(){ 
        if ($(this).scrollTop() > 100) { 
            $('#scroll').fadeIn(); 
        } else { 
            $('#scroll').fadeOut(); 
        } 
    }); 
    $('#scroll').click(function(){ 
        $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    }); 
});
</script>
<script type="text/javascript">
 jQuery(function(){
    jQuery( "#search" ).autocomplete({
		
        source: 'getsearch.php' ,
         minLength: 3,
           select: function(event, ui) {
         
        jQuery("#search").val(ui.item.label);
        jQuery("#frmsearch").submit();
           }
    });
});  

 $(function(){
    jQuery( "#searchmobile" ).autocomplete({
        source: 'getsearch.php' ,
         minLength: 3,
           select: function(event, ui) { 
        jQuery("#searchmobile").val(ui.item.label);
       
        jQuery("#frmsearchmobile").submit(); }
    });
});  
</script>


<script id="_webengage_script_tag" type="text/javascript">
var webengage; !function(w,e,b,n,g){function o(e,t){e[t[t.length-1]]=function(){r.__queue.push([t.join("."),arguments])}}var i,s,r=w[b],z=" ",l="init options track screen onReady".split(z),a="feedback survey notification".split(z),c="options render clear abort".split(z),p="Open Close Submit Complete View Click".split(z),u="identify login logout setAttribute".split(z);if(!r||!r.__v){for(w[b]=r={__queue:[],__v:"6.0",user:{}},i=0;i<l.length;i++)o(r,[l[i]]);for(i=0;i<a.length;i++){for(r[a[i]]={},s=0;s<c.length;s++)o(r[a[i]],[a[i],c[s]]);for(s=0;s<p.length;s++)o(r[a[i]],[a[i],"on"+p[s]])}for(i=0;i<u.length;i++)o(r.user,["user",u[i]]);setTimeout(function(){var f=e.createElement("script"),d=e.getElementById("_webengage_script_tag");f.type="text/javascript",f.async=!0,f.src=("https:"==e.location.protocol?"https://ssl.widgets.webengage.com":"http://cdn.widgets.webengage.com")+"/js/webengage-min-v-6.0.js",d.parentNode.insertBefore(f,d)})}}(window,document,"webengage");     

webengage.init("~10a5cb10d");
</script>
  
<a href="javascript:void(0);" id="scroll" title="Scroll to Top" style="display: none;">Top<span></span></a>
   <footer class="padtop_padbot bg2b2 color-white">
      <div class="container">
        <div class="row clearfix">
         <div class="col-md-2 col-sm-2 col-xs-6">
            <h4 class="text-uppercase">Stores</h4>
              <ul>
                <li>
                  <a href="<?php echo WEB_ROOT;?>store/flipkart"><?php echo('Flipkart'); ?></a>
                </li>
                <li>
                  <a href="<?php echo WEB_ROOT;?>store/amazon"><?php echo('Amazon'); ?></a>
                </li>
                <li>
                  <a href="<?php echo WEB_ROOT;?>store/jabong"><?php echo('Ajio'); ?></a>
                </li>
                 <li>
                  <a href="<?php echo WEB_ROOT;?>store/abof"><?php echo('Abof'); ?></a>
                </li>
              </ul>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-6">
            <h4 class="text-uppercase">help</h4>
              <ul>
                <li>
                  <a href="<?php echo WEB_ROOT;?>faqs.php"><?php echo('FAQs'); ?></a>
                </li>
                <li>
                  <a href="<?php echo WEB_ROOT;?>how-it-works.php"><?php echo('How it works'); ?></a>
                </li>
                <li>
                  <a href="<?php echo WEB_ROOT;?>createticket.php"><?php echo('Claim Missing Cashback'); ?></a>
                </li>
				<li><a href="<?php echo WEB_ROOT;?>testimonial"><?php echo('Testimonials'); ?></a> </li>
              </ul>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-6">
            <h4 class="text-uppercase">Couponshub</h4>
              <ul>
                <li>
                  <a href="<?php echo WEB_ROOT;?>page/aboutus"><?php echo('About US'); ?></a>
                </li>
                
                <li>
                 <a href="<?php echo WEB_ROOT;?>contact"><?php echo('Contact US'); ?></a>
                </li>
<li>
                 <a href="<?php echo WEB_ROOT;?>page/terms-conditions"><?php echo('Terms and Conditions'); ?></a>
                </li>
<li>
                 <a href="<?php echo WEB_ROOT;?>page/privacy-policy"><?php echo('Privacy Policy'); ?></a>
                </li>
              </ul>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-12 social-section">
            <h4 class="text-uppercase">social</h4>
              <div class="social">                              
                <a href="https://www.facebook.com/207.246.98.232:83/"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                <a href="https://twitter.com/Couponshub_"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                <a href="https://plus.google.com/+CouponshubInd"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
              </div>
<a href="https://msg91.com/startups/?utm_source=startup-banner"><img src="https://msg91.com/images/startups/msg91Badge.png" width="120" height="90" title="MSG91 - SMS for Startups" alt="Bulk SMS - MSG91"></a>
          </div>

        </div>
      </div>
    </footer>	
<div class="footer-bot-bg ">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					
					<ul class="top-space list-inline clearfix">
					<li class="clr-theme">Fashion Coupons : </li>
						<li><a href="<?php echo WEB_ROOT;?>category/fashion-accessories">Fashion accessories coupons</a></li>
						<li><a href="<?php echo WEB_ROOT;?>category/clothing">Clothing Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/jewellery">Jewellery Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/nightwear-and-lingerie">Lingarie Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/sunglasses-and-contact-lens">Sunglasses Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/watches">Watches Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/footwear">Footwear Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/luggage-and-bags">Luggage Coupons</a></li>
												
					</ul>
					<ul class="top-space list-inline clearfix">
					<li class="clr-theme">Electronic Coupons : </li>
						<li><a href="http://207.246.98.232:83/category/cameras-and-accessories">Cameras Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/audio-video-and-home-entertainment">Audio Video Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/computer-accessories">Computer Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/laptops-and-desktops">Laptops Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/televisions">Television Coupons</a></li>						
					</ul>
					<ul class="top-space list-inline clearfix">
					<li class="clr-theme">Recharge Coupons : </li>
						<li><a href="http://207.246.98.232:83/category/mobile-dth-recharge">Mobile& DTH recharge Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/bill-payment">Bill payment Coupons</a></li>
											
					</ul>
					<ul class="top-space list-inline clearfix">
					<li class="clr-theme">Travel Coupons : </li>
						<li><a href="http://207.246.98.232:83/category/bus">Bus Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/flights">Flight Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/holiday-packages">Holiday Package Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/hotels">Hotels Coupons</a></li>
						<li><a href="http://207.246.98.232:83/category/cab-and-auto">Cab and Auto Coupons</a></li>						
					</ul>
				</div>
			</div>
			
		</div>
	</div>	
	<div class="footer-bot-bg ">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
				<p>We are one of India&rsquo;s top Coupon store with a wide range of coupons, deals, discounts from top online brands and websites like Amazon, Flipkart,Snapdeal, Paytm, Freecharge, KFC, McDonalds, Jabong, eBay, MakeMyTrip, Myntra and many more. Couponshub is the bridge between deal driven online shoppers, and their favourite shopping destinations. We quench the wish of shoppers with comparisons, Cashback and other discounts in a lucid manner. We combine the most rambeled options for satisfying shopping and additional features, especially Cashback, under one stop. Couponshub aims to make your shopping rewarding with the introduction of  cashback and price comparison. We ensure that shoppers enjoy double savings with our hand picked hot deals, promo codes and with our extra cash back offers. We have partnered with hundereds of leading on-line retailers to cover all your shopping needs. Shoppers can enjoy these benefits pretty much on everything you buy from the e-commerce sites listed with us.
First stop for online shopping - Couponshub where we make sure that our consumers always start their shopping with Couponshub making earning while shopping a routine. Our goal is to make you shop smartly from a wide range of categories like electronics, recharge, fashion, mobiles and tablets, food and dining, travel, beauty and health, home decor and furnishing, sports and fitness, cameras and accessories, education and learning, flowers and gifts, web hosting and domains etc. You can find online food coupons, recharge coupons, electronic coupons, travel coupons, domestic flights coupons, fashion coupons and much more. A-Z, we have almost every product for you. We provide fast and reliable Cashback over and above the discounted price, at the most popular online stores. 
Visit 207.246.98.232:83 or you can also use our services on the go with our Android Mobile App and search for the best deals and offers. You can either select a merchant or select a particular deal. When you shop on portals like Flipkart, Amazon, Snapdeal, Jabong, Ebay, Paytm, Foodpanda, Myntra, Makemytrip, Goibibo, and many other sites, we help you get discounts and on top of that offer you a cash back on that deal. You use those offers and get more in form of cashback within a few weeks. You can then use this cashback to get recharges or transfer it to your bank account to use it as actual cash! With Couponshub, you will always get more because we believe "the bigger the better".

How do you do that? It&rsquo;s simple and easy. follow some simple steps - visit 207.246.98.232:83 every time you want to purchase online. Then, Login, Find your favorite coupon or Deal;even better, you can search for perticular online store. Visit the retailer website by clicking on &rsquo;get code&rsquo; button & buy like you normally do, Voila! all done. We'll track your order and pay cash back amount. You can also earn referal bonus by inviting friends to our portal. We wish to make your shopping rewarding.</p>
				</div>
				</div>
				</div>
				</div>
	<div class="footer-bot-bg ">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
				<p class="copyright text-center mar20">Copyright &copy;
				Couponshub 2016-2017. All rights reserved. </p>
				</div>
				</div>
				</div>
				</div>
<script type="text/javascript">stLight.options({publisher: "8a6e59a7-af40-42c0-b95e-c0b9fca7daf1", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<script>
var options={ "publisher": "8a6e59a7-af40-42c0-b95e-c0b9fca7daf1", "position": "left", "ad": { "visible": false, "openDelay": 5, "closeDelay": 0}, "chicklets": { "items": ["facebook", "twitter", "googleplus", "whatsapp", "linkedin", "pinterest", "email", "sharethis"]}};
var st_hover_widget = new sharethis.widgets.hoverbuttons(options);
</script>