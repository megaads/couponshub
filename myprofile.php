<?php include_once 'user_field.php';
  checkSiteUser();
$crumbs[]=__('My Profile');
?>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo('My Profile');?>   - <?php echo SITE_NAME;?></title>
                                               

<!-- FONTS ONLINE -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

<!--MAIN STYLE-->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/main.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
         

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
  <script type="text/javascript">
    $(document).ready(function(){
        $(".ui-tabs").tabs();
    });
</script>
</head>
<body>
<div class="se-pre-con"></div>
<!-- Page Wrap ===========================================-->
<div id="wrap"> 
  
  <!--======= TOP BAR =========-->
                        <?php  include 'top.php'?>
     <?php  include 'header.php'?> 
         
      
   
       
  <!--======= SIGN UP =========-->
  <section class="myaccount">
     <div class="container">
      <?php include_once 'breadcrumb.php';?>
     <div class="ui-tabs">
         <div class="col-md-3"><?php include 'custmenu.php';?></div>
         <div class="col-md-9 white-body ticketform">
    <div id="tabs-1" class="ui-tabs-panel">
        <h4><?php echo('My Profile');?></h4>
                   <?php   $msgcode=(isset($msg) && $msg!="")?$msg:$_GET['msg'];
             echo getMessage($msgcode);?>
          <form method="post" name="frm" enctype="multipart/form-data" >
          <input type="hidden" name="txtMode" value="update" >
           
            <ul class="pad0">
              <li class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label for=""><?php echo('Name');?> *
                    <input type="text" class="form-control" id="" value="<?php echo (isset($rec['name']))?$rec['name']:"";?>" placeholder="" name="name">
                  </label>
                </div>
              </li>
              <li class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label for=""><?php echo('City');?> *
                    <input type="text" class="form-control" id="" value="<?php echo (isset($rec['city']))?$rec['city']:"";?>" placeholder="" name="city">
                  </label>
                </div>
              </li>
               <li class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label for=""><?php echo('Country');?> *
                    <input type="text" class="form-control" id="" value="<?php echo (isset($rec['country']))?$rec['country']:"";?>" placeholder="" name="country">
                  </label>
                </div>
              </li>
               <li class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label for=""><?php echo('Phone#');?> *
                    <input type="text" class="form-control" id="phone" value="<?php echo (isset($rec['phone']))?$rec['phone']:"";?>" placeholder="" name="phone" readonly="">
                  </label>
                </div>
              </li>
              <li class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label for=""><?php echo('E-mail Address');?> *
                    <input type="email" class="form-control" id="" value="<?php echo (isset($rec['email']))?$rec['email']:"";?>" readonly="" placeholder="" name="email">
                  </label>
                </div>
              </li>
              <li class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label for=""><?php echo('Upload Photo');?> *
                    <input type="file"  id="" placeholder="" name="user_image">
                      <?php
                                     if($rec['user_image']!="")
                                     {
                                         if(file_exists(USER_IMAGE_PATH_UPLOAD.$rec['user_image']))
                                         {
                                             $rec['user_image']= USER_IMAGE_PATH_SHOW.$rec['user_image'];
                                         }      
                                         
                                         ?> 
                                    <img src="<?php echo $rec['user_image']?>" width="150"   />    
                                        
                                        <?php
                                     } 
                                     
                                     ?>
                    
                  </label>
                </div>
              </li>  
                    
              <li class="col-md-12 col-sm-12 text-left col-xs-12">
                <button type="submit" class="btn"><?php echo('Update');?></button>
              </li>
            </ul>
          </form>                   
        
        
    </div>
    </div>
        
</div>
     
     
     </div>
  </section>
  
  <!--======= FOOTER =========-->
  <?php include 'footer.php';?> </div>
    <?php include 'js.php';?>    
  
</body>
</html>