
<section>
  <div class="grid_frame page-content">
            <div class="container_grid">
                <div class="layout-2cols clearfix">
                    <div class="grid_8 content">
                        <div class="mod-coupons-code">
 
<?php include_once 'breadcrumb.php';?> 
             
                   
                  
        
                <div class="mod-grp-coupon block clearfix">
                    <div class="grid_12">
                        <h3 class="title-block has-link">
                            COUPONS 
                            
                        </h3>
                    </div>
                    
                                
          <ul class="wrap-list" id="couponslist">
          <!--======= COUPEN DEALS =========-->
          <?php foreach($coupons as $rec): 
 $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
            if($rec['coupon_imageurl']!="")
            {
                $rec['coupon_image']=$rec['coupon_imageurl'];
            }
           else
            {
                 $rec['coupon_image']=COUPON_IMAGE_PATH_SHOW.(($rec['coupon_image']!="")?$rec['coupon_image']:'no-image.jpg');
                   $rec['coupon_image']= WEB_ROOT."SampleImage.php?src=".$rec['coupon_image']."&h=135&q=40&zc=0";           
            }
                
               $rec['retailer_logo']=RETAILER_IMAGE_PATH_SHOW.(($rec['retailer_logo']!="")?$rec['retailer_logo']:'no-image.jpg');                                                                                                                     
                       
            
           
            
          ?>
          <li>
		  <div class="coupons-code-item right-action flex">
		  
                           <div class="brand-logo thumb-left">
                                        <div class="wrap-logo">
                                            <div class="center-img">
                                                <span class="ver_hold"></span>
                <a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>" class="ver_container">  <img class="img-responsive" src="<?php echo $rec['retailer_logo']?>"" alt="<?php echo $rec['identifier']; ?>" height="135px" width="143px" ></a>
       </div>
                                        </div>
										 <div class="wrap-logo">
                                            <div class="center-img">
                                                <span class="ver_hold"><?php echo $rec['price'] ?></span>
                
       </div>
                                        </div>
                                    </div>
									 
                                       
                                   
              
             <div class="right-content flex-body">
			 <p class="rs save-price"><a href="<?php echo WEB_ROOT ?>coupon/<?php echo $rec['identifier']; ?>"><?php echo substr($rec['name'],0,58)?></a></p>
               
               
<p class="rs coupon-desc"><?php echo substr($rec['description'],0,100)?></p>
 <div class="bottom-action">
                                            <div class="left-vote">
											<?php if (str_replace($good,$bad,$rec['cashback'])==0):?>
											 <span class="lbl-work">Cashback Not Available </span>
											 <?php else: ?>
                                                <span class="lbl-work">+upto <?php echo str_replace($good,$bad,$rec['cashback'])?> Extra Cashback</span>
                                             <?php endif; ?>
                                            </div>
               
                     
                
                 <?php if($rec['coupon_code']==NULL): ?> 
                <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn showcoupon" ><?php echo('Get Cashback');?></a>
                <?php else: ?>
                  <a id="showcouponbtn-<?php echo $rec['coupon_id']; ?>" href="#couponbox"   class="btn showcoupon" ><?php echo('Get Code');?></a>
                <?php endif; ?>
                </div>
              </div>
                
            </div>
            
          </li>
         <?php endforeach;?> 
              
           </ul>    
		   
				
            </div>
        </div>
        
        <!--======= PAGINATION =========-->
                 <button class="loadMore" style="
    display: inline-block;
    padding: 15px 25px;
    font-size: 24px;
    cursor: pointer;
    text-align: center;
    text-decoration: none;
    outline: none;
    color: #fff;
    background-color:#e74f1d;
    border: none;
    border-radius: 15px;
    box-shadow: 0 9px #999;
">Load More</button>
         <div id="pagination" style="text-align: center;display: none;">
             <!----  <img src="<?php echo WEB_ROOT?>images/loading.gif">  --->
   <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
<span class="sr-only">Loading...</span>
          </div>
      </div>
                          <div class="grid_4 sidebar" id="sidebar">
                        <div class="mod-search block">
                            <h3 class="title-block">Find your coupon code</h3>
                            <div class="block-content">
                                <label class="lbl-wrap" for="sys_search_coupon_code">
                                    <input class="keyword-search" id="sys_search_coupon_code" type="search" placeholder="Search"/>
                                    <input type="submit" class="btn-search" value="">
                                </label>
                            </div>
                        </div><!--end: .mod-search -->
                        <div class="mod-list-store block">
                            <h3 class="title-block">Popular store</h3>
                            <div class="block-content">
                                <div class="wrap-list-store clearfix">
								 <ul class="row">
								  <?php foreach($featuredstores as $rec):                                           
        $good=array("percent","fixed");
        $bad=array("%"," ".$currency);
        
        ?>
		<li>
                                    <a class="brand-logo" href="<?php echo WEB_ROOT?>store/<?php echo $rec['identifier']; ?>" >
                                        <span class="wrap-logo">
                                            <span class="center-img">
                                                <span class="ver_hold"><p>Up to <?php echo str_replace($good,$bad,$rec['cashback'])?> Cashback &
              <?php echo $rec['total_coupon']?> More Offers</p></span>
                                                <span class="ver_container"><img src="<?php echo RETAILER_IMAGE_PATH_SHOW.$rec['logo'];?>" alt="<?php echo $rec['identifier']; ?>"></span>
                                            </span>
											
                                        </span>
										
                                    </a>
									</li>
        <?php endforeach;?>

                                  
                                </div>
                            </div>
                        </div><!--end: .mod-list-store -->

						
            </div>
			
  </section>
  
  <!--======= FOOTER =========-->
  <script type="text/javascript">
var url="<?php echo WEB_ROOT."getcoupons.php"?>";
var docroot="<?php echo DOC_ROOT."images/"?>";
   function lastmsg_funtion(pagenumber){
    $('#pagination').show();                                              
    $.post(url, {PageNum: pagenumber}, function(result){
    $('#pagination').hide();                                              
      $('#couponslist').append(result);
      $(".btncashback").leanModal({top : 100, overlay : 0.6, closeButton: ".modal_close" });  
       $(".showcoupon").leanModal({top :100 , overlay : 0.6, closeButton: ".modal_close" }); 
    });
} 

$(window).scroll(function(){

if ($(window).scrollTop() == $(document).height() - $(window).height()){
  
   $('.loadMore').fadeIn(200);
  
   
}

});

$('.loadMore').click(function () {
    
   var pagenumber=eval($('#pagenumber').val());
    
   $('#pagenumber').val(pagenumber+1); 
   var total_page=$('#total_page').val();   
   
   if(pagenumber<=total_page) 
{
lastmsg_funtion(pagenumber);

}
 else
jQuery('button.loadMore').attr('disabled', 'disabled').text('No More Coupons ');
  

});

$(document).ready(function () {
    var length = $("#sidebar").height() - $("#sidebar").height();

    $(window).scroll(function () {
        var scroll = $(this).scrollTop();
        var height = $('#sidebar').height() + 'px';

        if(scroll <= 0) {
            $("#sidebar").css({
                'position': 'absolute',
                'top': '0'
            });
        } else if(scroll >= length) {
            $("#sidebar").css({
                'position': 'absolute',
                'bottom': '0',
                'top': 'auto'
            });
        } else {
            $("#sidebar").css({
                'position': 'fixed',
                'top': '100px'
            });
        }
    });
});
</script>                                          