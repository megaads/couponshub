<?php
$site_mode=getConfiguration('site_mode'); 

if($site_mode=="Maintenance")
{      
   include("maintenance.php");
   exit; 
}

define('ENABLE_MOBILE_REF_CODE',$clientId = trim(getConfiguration('enable_mobile_refferal_code'))) ;
define('ENABLE_REF_CODE',$clientId = trim(getConfiguration('enable_referral_code'))) ;  

session_start();
include_once("src/Google_Client.php");
include_once("src/contrib/Google_Oauth2Service.php");
######### edit details ##########
 
 
$clientId = trim(getConfiguration('google_client_id'))   ; //Google CLIENT ID
$clientSecret =  trim(getConfiguration('google_secret_key'))   ;//Google CLIENT SECRET
$redirectUrl =trim(getConfiguration('google_redirect_url'))   ; '';  //return url (url to script)
$homeUrl = WEB_ROOT;  //return to home

$facebookappId=  trim(getConfiguration('facebook_app_id'));
$facebookappSecret=  trim(getConfiguration('facebook_app_secret')); 
##################################

$gClient = new Google_Client();
$gClient->setApplicationName('Login to '.SITE_NAME);
$gClient->setClientId($clientId);
$gClient->setClientSecret($clientSecret);
$gClient->setRedirectUri($redirectUrl);

$google_oauthV2 = new Google_Oauth2Service($gClient);


$config =array(
        "base_url" => WEB_ROOT."hybridauth/hybridauth/hybridauth/index.php", 
        "providers" => array ( 
                                
            "Facebook" => array ( 
                "enabled" => true,
                "keys"    => array ( "id" => $facebookappId, "secret" => $facebookappSecret ), 
            ),
                   
        ),
        // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
        "debug_mode" => false,
        "debug_file" => "",
    );

$enable_otp=  trim(getConfiguration('enable_otp')); 

?>
