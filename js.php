<script src="<?php echo WEB_ROOT;?>js/jquery-1.11.0.min.js"></script>
<script src="<?php echo WEB_ROOT;?>js/matchheight.js"></script>
<script src="<?php echo WEB_ROOT;?>js/wow.min.js"></script> 
<script src="<?php echo WEB_ROOT;?>js/bootstrap.min.js"></script> 
<script src="<?php echo WEB_ROOT;?>js/jquery.stellar.js"></script> 
<script src="<?php echo WEB_ROOT;?>js/jquery.isotope.min.js"></script> 
<script src="<?php echo WEB_ROOT;?>js/jquery.flexslider-min.js"></script> 
<script src="<?php echo WEB_ROOT;?>js/owl.carousel.min.js"></script> 
<script src="<?php echo WEB_ROOT;?>js/jquery.sticky.js"></script> 
<script src="<?php echo WEB_ROOT;?>js/own-menu.js"></script> 
<script src="<?php echo WEB_ROOT;?>js/main.js"></script>
<script src="<?php echo WEB_ROOT;?>js/jquery.validate.js"></script> 
<script type="text/javascript" src="<?php echo WEB_ROOT;?>js/jquery.leanModal.min.js"></script>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />
<script async src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script type="text/javascript">
   base_url="<?php echo WEB_ROOT ?>";
   
   /* $("#modal_trigger").leanModal({top : 10, overlay : 0.6, closeButton: ".modal_close" });
    $("#modal_trigger1").leanModal({top : 10, overlay : 0.6, closeButton: ".modal_close" });
     $("#modal_testimonial").leanModal({top : 10, overlay : 0.6, closeButton: ".modal_close" });
    $(".btncashback").leanModal({top : 10, overlay : 0.6, closeButton: ".modal_close" });
    $(".btncashbackstore").leanModal({top : 10, overlay : 0.6, closeButton: ".modal_close" }); */
    $(".showcoupon").leanModal({top :100 , overlay : 0.6, closeButton: ".modal_close" });
    $(function(){
        // Calling Login Form
        $("#login_form").click(function(){
            $(".social_login").hide();
             $(".user_register").hide(); 
            $(".user_login").show();
            return false;
        });

        // Calling Register Form
        $("#register_form").click(function(){
            $(".user_register").show();
            $(".user_login").hide();     
            $(".social_login").hide();
            $(".user_register").show();
            $(".header_title").text('Register');
            return false;
        });     
        
        $(".register_form").click(function(){
            $(".social_login").hide();
            $(".user_login").hide();     
            $(".user_register").show();
            $(".header_title").text('Register');
            $('.cashback-link').hide();  
            return false;
        });
        
        $("#modal_trigger").click(function(){
            $(".social_login").show();
            $(".user_register").hide();
            $(".header_title").text('Login');                                     
            $('.cashback-link').hide();  
            return false;
        });

        // Going back to Social Forms
        $(".back_btn").click(function(){
            $(".user_login").hide();
            $(".user_register").hide();
            $(".social_login").show();
            $(".header_title").text('Login');
            return false;
        });
        
         $(".btncashback").click(function(){
              $(".social_login").show();
            $(".user_register").hide();
            $(".header_title").text('Login'); 
             cid=this.id;
              coupon_id=cid.replace('cashbackbtn-','');
              redirectUrl=base_url+"doRedirect.php?cid="+coupon_id;
              $('#cashback-link').attr('href',redirectUrl);
              $('.cashback-link').show();
                     
        });
        
        
         $( "body" ).on( "click", ".showcoupon", function(e) { 
                      
                       
             cid=this.id;
              coupon_id=cid.replace('showcouponbtn-','');
              redirectUrl=base_url+"doRedirect.php?cid="+coupon_id;
              showCoupon(coupon_id);
              window.open(redirectUrl);
                });
        
        
      /*  $(".showcoupon").click(function(){
              
             cid=this.id;
              coupon_id=cid.replace('showcouponbtn-','');
              redirectUrl=base_url+"doRedirect.php?cid="+coupon_id;
              showCoupon(coupon_id);
              window.open(redirectUrl);
                     
        });   */
        
        
        
       $( "body" ).on( "click", ".btncashback", function() {
               
               $(".social_login").show();
            $(".user_register").hide();
            $(".header_title").text('Login'); 
             cid=this.id;
              coupon_id=cid.replace('cashbackbtn-','');
              redirectUrl=base_url+"doRedirect.php?cid="+coupon_id;
              $('#cashback-link').attr('href',redirectUrl);
              $('.cashback-link').show();
            }); 
            
          $( "body" ).on( "click", ".btncashbackstore", function() {
               
               $(".social_login").show();
            $(".user_register").hide();
            $(".header_title").text('Login'); 
             sid=this.id;
              store_id=sid.replace('cashbackbtn-','');
              redirectUrl=base_url+"doRedirect.php?sid="+store_id;
              $('#cashback-link').attr('href',redirectUrl);
              $('.cashback-link').show();
            });    
            
            
        

    })       
                   
 
</script>